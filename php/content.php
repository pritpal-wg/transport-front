<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

$id = isset($_GET['id']) ? $_GET['id'] : 0;

$content = add_metatags("Content", 'Content', 'Content');

$query = new content;
$list = $query->getItem($id);
$smarty->assign_notnull("list", $list, true);

$smarty->renderLayout();
?>
