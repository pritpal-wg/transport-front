<?
function PageControl($page,$totalPages,$totalRecords,$url,$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Records',$LClass='cat')
	{
			# $Pp-previous page
			# $Np- next page
			($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
			($page<=1)?$Pp=1:$Pp=$page-1;
			if($totalPages>10):
				if(($page+10) <=$totalPages):
					$end=$page+10;
					$begin=$page;
				else:
					$begin=$totalPages-10;
					$end=$totalPages;
				endif;
			else:
				$begin=1;
				$end=$totalPages;
			endif;
			?>
					<div class="pagination">
                        <a href="<?=$url?>?page=<?=$Pp?>&<?=$querystring?>" title="Previous Page">&laquo;</a>
						<?php for($i=$begin;$i<=$totalPages && $i<=$end;$i++):?>
								<?php if($i==$page):?>
								<a href="<?=$url?>?page=<?=$i?>&<?=$querystring?>" class="active" title="Page No: <?=$i?>"><?=$i?></a>
								<?php else:?>
								<a href="<?=$url?>?page=<?=$i?>&<?=$querystring?>"  title="Page No: <?=$i?>"><?=$i?></a>
								<?php endif;?>

						<?php endfor;?>
                                                                <a href="<?=$url?>?page=<?=$Np?>&<?=$querystring?>" title="Next Page">&raquo;</a>
					</div>
			<?php


	}

	function PageControl_front($page,$totalPages,$totalRecords,$url,$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Products',$LClass='cat')
{
	if($type==1):
		?>
		<table width="100%" cellspacing="1" cellpadding="2" align="center" class="<?=$Class?>">
			<tr>
				<td class="<?=$tdclass?>" width="30%" align="left">Total&nbsp;<?=$Title?>:&nbsp;&nbsp;<?=$totalRecords?></td>
				<td align="right" >Pages:&nbsp;&nbsp;
				<?php for($i=1;$i<=$totalPages;$i++):?>
					<?php if($i==$page):?>
						<?=display_url($i, $url, 'p='.$i.'&'.$querystring,'blockselected');?>
					<?php else:?>
						<?=display_url($i, $url, 'p='.$i.'&'.$querystring,$LClass);?>
					<?php endif;?>
				<?php endfor;?>
				</td>
			</tr>
		</table>
		<?
	elseif($type==2):
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
		?>
		<table width="100%" cellspacing="0" cellpadding="0" align="center" >
                    <thead>
			<tr>
				<th width="30%"  align="left">&nbsp;<?=$Title?> :&nbsp;&nbsp;<?=$totalRecords?></th>
				<th width="25%" >Pages:&nbsp;&nbsp;<?=$totalPages?></th>
				<th width="45%" align="right" class="pageinggg" >
				<?php if($page!='1'):?>
					<a href="<?php echo make_url($url, 'p='.$Pp.'&'.$querystring);?>" class="pnp" title="Previous Page"><?echo"<<"?></a>&nbsp;
				<?php endif;?>
				<?
				for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
					if($i==$page):
						echo display_url($i, $url, 'p='.$i.'&'.$querystring,'blockselected'); echo'&nbsp;&nbsp;';
					else:
						echo display_url($i, $url, 'p='.$i.'&'.$querystring,$LClass);echo'&nbsp;&nbsp;';
					endif;					
				endfor;
				?>
				<?php if($page <($totalPages-3)):?>
					<a href="<?php echo make_url($url, 'p='.$Np.'&'.$querystring);?>" class="pnp" title="Next Page"><?echo">>"?></a>
				<?php endif;?>
				</th>
			</tr>
                    </thead>
		</table>
		<?
	endif;
}	

	function PageControl_front_pro($page,$totalPages,$totalRecords,$url,$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Products',$LClass='cat')
{
	if($type==1):
		?><div style="clear:both;"></div>
		<ul>
				<?php for($i=1;$i<=$totalPages;$i++):?>
					<?php if($i==$page):?>
					<li id="blockselected">	<?=display_url($i, $url, 'p='.$i.'&'.$querystring,'blockselected');?></li>
					<?php else:?>
					<li>	<?=display_url($i, $url, 'p='.$i.'&'.$querystring,$LClass);?></li>
					<?php endif;?>
				<?php endfor;?>
		</ul>		
		<?
	elseif($type==2):
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
		?>
		
			<ul>
				<!--
				<?php if($page!='1'):?>
					<a href="<?php echo make_url($url, 'p='.$Pp.'&'.$querystring);?>" class="pnp" title="Previous Page"><?echo"<<"?></a>&nbsp;
				<?php endif;?> -->
				<?
				for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
					if($i==$page): ?>
					<li id="blockselected">
					<?php	echo display_url($i, $url, 'p='.$i.'&'.$querystring,'blockselected'); echo'&nbsp;&nbsp;'; ?>
					</li> <?php 
					else: ?><li>
					<?php echo display_url($i, $url, 'p='.$i.'&'.$querystring,$LClass);echo'&nbsp;&nbsp;'; ?>
					</li>
					<?php endif;					
				endfor;
				?>
				<!--
				<?php if($page <($totalPages-3)):?>
					<a href="<?php echo make_url($url, 'p='.$Np.'&'.$querystring);?>" class="pnp" title="Next Page"><?echo">>"?></a>
				<?php endif;?> -->
			</ul>
		<?
	endif;
}	

	function PageControl_front12($page,$totalPages,$totalRecords,$url,$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Records',$LClass='cat')
{

	if($type==1):
		?>
		<ul>
				<?php for($i=1;$i<=$totalPages;$i++):?>
					<?php if($i==$page):?>
					<li id="blockselected"> <?=display_url($i, $url, 'p='.$i.'&'.$querystring,'blockselected');?></li>
					<?php else: ?>
					<li>	<?=display_url($i, $url, 'p='.$i.'&'.$querystring,$LClass);?></li>
					<?php endif;?>
				<?php endfor;?>
		</ul>

		<?
	elseif($type==2):
		
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
		?>
           <ul>
				<?
				for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
					if($i==$page): ?>
					<li id="blockselected">	<?php echo display_url($i, $url.'&'.'search_page='.$_GET['search_page'], 'p='.$i.'&'.$querystring,'blockselected'); echo'&nbsp;&nbsp;'; ?> </li><?php
					else: ?>
					<li><?php echo display_url($i, $url.'&'.'search_page='.$_GET['search_page'], 'p='.$i.'&'.$querystring,$LClass);echo'&nbsp;&nbsp;'; ?></li>
					<?php endif;					
				endfor;
				?>
			</ul>
             
		<?
	endif;
}	

	function PageControl_front_brand($page,$totalPages,$totalRecords,$url,$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Records',$LClass='cat')
{

	if($type==1):
		?>
		<ul>
				<?php for($i=1;$i<=$totalPages;$i++):?>
					<?php if($i==$page):?>
					<li id="blockselected"> <?=display_url($i, $url, 'p='.$i.'&'.$querystring,'blockselected');?></li>
					<?php else: ?>
					<li>	<?=display_url($i, $url, 'p='.$i.'&'.$querystring,$LClass);?></li>
					<?php endif;?>
				<?php endfor;?>
		</ul>

		<?
	elseif($type==2):
		
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
		?>
           <ul>
				<?
				for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
					if($i==$page): ?>
					<li id="blockselected">	<?php echo display_url($i, $url.'&'.'brand='.$_GET['brand'], 'p='.$i.'&'.$querystring,'blockselected'); echo'&nbsp;&nbsp;'; ?> </li><?php
					else: ?>
					<li><?php echo display_url($i, $url.'&'.'brand='.$_GET['brand'], 'p='.$i.'&'.$querystring,$LClass);echo'&nbsp;&nbsp;'; ?></li>
					<?php endif;					
				endfor;
				?>
			</ul>
             
		<?
	endif;
}
?>