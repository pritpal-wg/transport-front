{*<div style="margin-top: 500px"></div>*}
<form method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body" style="padding: 0">
                        <img class="col-md-12 img" src="//maps.googleapis.com/maps/api/staticmap?size=1200x220&maptype=roadmap&markers=color:green|label:A|{$quote_addresses['source']['lat']},{$quote_addresses['source']['lng']}&markers=color:red|label:B|{$quote_addresses['destination']['lat']},{$quote_addresses['destination']['lng']}&path=color:purple|{$quote_addresses['source']['lat']},{$quote_addresses['source']['lng']}|{$quote_addresses['destination']['lat']},{$quote_addresses['destination']['lng']}&key={$gapi_key}" style="padding: 5px 5px 0" />
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6" style="font-size: 20px;">
                    <div class="text-success">
                        <b>Total Distance:</b> {$quote_info->distance_miles} Mile{if $quote_info->distance_val > 1000}s{/if} <small>(~{$quote_info->distance_text})</small>
                    </div>
                    <div class="text-info">
                        <b>A:</b> <i class="fa fa-map-marker"></i> {$quote_info->source_port_name}
                    </div>
                    <div class="text-info">
                        <b>B:</b> <i class="fa fa-map-marker"></i> {$quote_info->destination_port_name}
                    </div>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-condensed" id="pricings_total_tbody">
                        <thead>
                            <tr class="warning">
                                <th style="width: 50%">Quote for</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {assign var=i value=1}
                            {assign var=pricing_total value=0}
                            {foreach from=$quote_pricings item=qp key=quote_option}
                                <tr>
                                    <td>
                                        {if $quote_option eq 'deposit'}Initial Deposit {else} Total Carrier Charges{/if}</td>
                                    <td><div class="{if $quote_option eq 'deposit'}total_deposit_charges_amount{/if}">${number_format($qp['total'], 2)}</div></td>
                                </tr>
                                {assign var=i value=$i+1}
                                {assign var=pricing_total value=$pricing_total+$qp['total']}
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th>${number_format($pricing_total, 2)}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">User Information (Shipper)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="user[first_name]" class="form-control" placeholder="First Name" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="user[last_name]" class="form-control" placeholder="Last Name" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="email" name="user[email_address]" class="form-control" placeholder="Email" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="user[phone_number]" class="form-control" placeholder="Phone" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="user[company]" class="form-control" placeholder="Company" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Source Location</h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="address[source][id]" value="{$quote_addresses['source']['id']}" />
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <input type="text" name="address[source][address1]" class="form-control" placeholder="Address1" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <input type="text" name="address[source][address2]" class="form-control" placeholder="Address2 [Optional]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="address[source][zip]" class="form-control" placeholder="ZIP Code" value="{$quote_addresses['source']['zip']}" />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="address[source][city]" class="form-control" placeholder="City" value="{$quote_addresses['source']['city']}"{if $quote_addresses['source']['city']} readonly{else} required{/if} />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="State" value="{$quote_addresses['source']['state']}" readonly />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Country" value="{$quote_addresses['source']['country']}" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Destination Location</h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="address[destination][id]" value="{$quote_addresses['destination']['id']}" />
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <input type="text" name="address[destination][address1]" class="form-control" placeholder="Address1" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <input type="text" name="address[destination][address2]" class="form-control" placeholder="Address2 [Optional]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="address[destination][zip]" class="form-control" placeholder="ZIP Code" value="{$quote_addresses['destination']['zip']}" />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="address[destination][city]" class="form-control" placeholder="City" value="{$quote_addresses['destination']['city']}"{if $quote_addresses['destination']['city']} readonly {/if} />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="State" value="{$quote_addresses['destination']['state']}" readonly />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Country" value="{$quote_addresses['destination']['country']}" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Vehicle Information</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div id="quote_vehicle_container">
                                {foreach from=$quote_vehicles item=quote_vehicle}
                                    <div class="col-md-3" data-for="quote_vehicle">
                                        <div class="quote_vehicle check_delete">
                                            <input type="hidden" class="quote_vehicle_type_id" value="{$quote_vehicle['type_id']}"/>
                                            <input type="hidden" class="quote_vehicle_quote_id" value="{$quote_info->id}"/>
                                            <input type="hidden" class="quote_vehicle_pk" value="{$quote_vehicle['id']}"/>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-xs btn-danger delete_quote_vehile" data-quote_request_vehicle="{$quote_vehicle['id']}" data-quote_id="{$quote_info->id}"><i class="fa fa-times"></i></button>
                                            </div>
                                            <div class="quote_vehicle_top transition_400">
                                                <h4 id="make_text">{$allMakes[$quote_vehicle['make_id']]['name']}</h4>
                                                <h4 id="model_text">{$allModels[$quote_vehicle['model_id']]['model_name']}</h4>
                                                <h4 id="type_text">{$allTypes[$quote_vehicle['type_id']]['name']}</h4>
                                                <h4 id="year_text">{$quote_vehicle['year']}</h4>
                                                <h4 id="running_status_id_text">{$runningStatuses[$quote_vehicle['running_status_id']]['name']}</h4>
                                                <button class="btn btn-info btn-block btn-sm edit_quote_vehicle"><i class="fa fa-edit"></i>Edit</button>
                                            </div>
                                            <div class="quote_vehicle_bottom transition_400">
                                                <div class="form-group">
                                                    <select class="form-control quote_vehicle_make">
                                                        <option value="" hidden>--Select Make--</option>
                                                        {foreach from=$allMakes item=allMake key=allMake_id}
                                                            {assign var=selected value=''}
                                                            {if $allMake_id eq $quote_vehicle['make_id']}
                                                                {assign var=selected value=' selected'}
                                                            {/if}
                                                            <option value="{$allMake_id}"{$selected}>{$allMake['name']}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control quote_vehicle_model">
                                                        <option value="" hidden>--Select Model--</option>
                                                        {foreach from=$allModels item=allModel key=allModel_id}
                                                            {if $quote_vehicle['make_id'] eq $allModel['manufacturer_id']}
                                                                {assign var=selected value=''}
                                                                {if $allModel_id eq $quote_vehicle['model_id']}
                                                                    {assign var=selected value=' selected'}
                                                                {/if}
                                                                <option value="{$allModel_id}"{$selected}>{$allModel['model_name']}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control quote_vehicle_type" value="{$allTypes[$quote_vehicle['type_id']]['name']}" readonly />
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control quote_vehicle_year" autocomplete="off">
                                                        {for $i=$start_year to $end_year step=-1}
                                                            {assign var=selected value=''}
                                                            {if $i eq $quote_vehicle['year']}
                                                                {assign var=selected value=' selected'}
                                                            {/if}
                                                            <option value="{$i}"{$selected}>{$i}</option>
                                                        {/for}
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control quote_vehicle_running_status" autocomplete="off">
                                                        {foreach from=$runningStatuses item=runningStatus}
                                                            {assign var=selected value=''}
                                                            {if $runningStatus['id'] eq $quote_vehicle['running_status_id']}
                                                                {assign var=selected value=' selected'}
                                                            {/if}
                                                            <option value="{$runningStatus['id']}"{$selected}>{$runningStatus['name']}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-sm btn-block btn-success" id="save_vehicle_data">Done</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                            <div class="col-md-3" id="quote_vehicle_add" data-quote_id="{$quote_info->id}">
                                <div class="quote_vehicle_add">
                                    <i class="fa fa-plus"></i> Add Vehicle
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Additional Information</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label">Service Level</label>
                            <select id="service_level_id" name="quote_request[service_level_id]" class="form-control" data-quote_id="{$quote_info->id}" required>
                                <option value="" hidden>--Select Service Level--</option>
                                {foreach from=$service_levels item=service_level}
                                    {assign var=selected value=''}
                                    {if $quote_info->service_level_id eq $service_level['id']}
                                        {assign var=selected value=' selected'}
                                    {/if}
                                    <option value="{$service_level['id']}"{$selected}>{$service_level['name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Lead Source</label>
                            <select id="lead_source_id" name="quote_request[lead_source_id]" class="form-control" data-quote_id="{$quote_info->id}" required>
                                <option value="" hidden>--Select Lead Source--</option>
                                {foreach from=$lead_sources item=lead_source}
                                    {assign var=selected value=''}
                                    {if $quote_info->lead_source_id eq $lead_source['id']}
                                        {assign var=selected value=' selected'}
                                    {/if}
                                    <option value="{$lead_source['id']}"{$selected}>{$lead_source['name']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-9">
                <div class="pull-right" style="margin-top: 7px">
                    Make Payment: <span style="color:orangered;font-size: 20px" class="total_deposit_charges"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div style="display: inline">
                    <button type="submit" class="btn btn-block btn-lg btn-success" name="save">Proceed To Payment</button></div>
            </div>
        </div>
    </div>
</form>