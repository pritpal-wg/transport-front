<?php

$obj = new query('quote_request_vehicle');
$obj->Data['quote_id'] = $quote_id;
$obj->Data['type_id'] = $type_id;
$obj->Data['year'] = $year;
$obj->Data['make_id'] = $make_id;
$obj->Data['model_id'] = $model_id;
$obj->Data['running_status_id'] = $running_status_id;
if ($quote_request_vehicle_id != 0) {
    $obj->Data['id'] = $quote_request_vehicle_id;
    $obj->Update();
} else {
    $obj->Insert();
}
$quote_pricings = getPricings($quote_id);
$pricing_total = 0;
echo '<thead><tr class="warning"><th style="width: 50%">Quote for</th><th>Price</th></tr></thead>';
echo "<tbody>";
foreach ($quote_pricings as $quote_option => $qp) {
    echo "<tr><td>" . ucfirst($quote_option) . "</td><td>$" . number_format($qp['total'], 2) . "</td></tr>";
    $pricing_total+=$qp['total'];
}
echo "</tbody>";
echo '<tfoot><tr><th>Total</th><th>$' . number_format($pricing_total, 2) . '</th></tr></tfoot>';