<?php

/*
 * tax Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class tax extends cwebc {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($table = 'supplier_tax', $order = 'desc', $orderby = 'name') {
        parent::__construct($table);
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'name', 'rate', 'is_active', 'is_deleted', 'date_add', 'date_upd');
    }

    /*
     * Create new tax or update existing tax
     */

    function saveTax($POST) {
        global $supplier_id;
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($POST['is_active']) ? "1" : "0";
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            $this->Data['supplier_id'] = $supplier_id;
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * Get tax by id
     */

    function getTax($id) {
        global $supplier_id;
        $this->Where = " where id='$id' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }

    /*
     * Get List of all taxes
     */

    function listTaxes($rtype = '') {
        global $supplier_id;
        $this->Where.=" where supplier_id='$supplier_id' and is_deleted='0' order by $this->orderby $this->order";
        if ($rtype == 'array'):
            return $this->ListOfAllRecords();
        else:
            return $this->DisplayAll();
        endif;
    }

    /*
     * get available taxes and apply the highest priority tax
     */

    public static function getCartTax($zone_id = '', $cart = '') {
        if (APPLY_TAX):
            if ($cart == ''):
                $cart = cart::getCart();
            endif;
            if ($zone_id == '0'):
                $zone_id = zone_detail::getZoneByAddress($cart);
            endif;

            $cart_content_obj = new cart_detail();
            $cart_items = $cart_content_obj->getCartItems();
            if (!empty($cart_items)):
                foreach ($cart_items as $item):
                    $product_tax = tax::getProductTaxForZone($item['tax_rule_group_id'], $zone_id, $item['unit_price']);

                    $Query_obj = new cart_detail();
                    $Query_obj->updateProductTax($item['id'], $product_tax);
                endforeach;
            endif;
            return cart_detail::getCartTax();
        else:
            return '0';
        endif;
    }

    /*
     * calculate tax for zone as per the tax rule group
     */

    public static function getProductTaxForZone($tax_rule_group, $zone_id, $product_price) {
        $QueryObj = new tax_rule_group;
        $group = $QueryObj->getTaxGroup($tax_rule_group);

        if ($group && $group->is_active = '1'):
            $taxes = array();
            $Query = new query('supplier_tax_rule as ctr,tax as ct');
            $Query->Field = "ctr.*,ct.rate as rate";
            $Query->Where = " where ctr.tax_rule_group_id='$tax_rule_group' and ctr.zone_id='$zone_id' and ctr.is_active='1'";
            $Query->Where .= " and ctr.tax_id=ct.id and ct.is_active='1' and ct.is_deleted='0'";
            $Query->Where .= " order by ctr.priority asc";
            $taxes = $Query->ListOfAllRecords();

            $total_tax = '0';
            if (!empty($taxes)):
                $combine = '0';
                $one_another = '0';
                $sr = 1;
                $total_tax_rate = '0';
                foreach ($taxes as $tax):
                    $calculated_tax = '0';
                    if ($sr == '1' && $tax['behaviour'] == 'combine'):
                        $combine = '1';
                    elseif ($sr == '1' && $tax['behaviour'] == 'one_another'):
                        $one_another = '1';
                    endif;

                    if ($combine != '1'):
                        $QueryObj = new tax();
                        $calculated_tax = $QueryObj->calculateTaxFromPrice($product_price, $tax['rate']);
                        $total_tax = $total_tax + $calculated_tax;
                    elseif ($combine == '1'):
                        $total_tax_rate = $total_tax_rate + $tax['rate'];
                    endif;

                    if ($sr == '1' && $tax['behaviour'] == 'tax_only'):
                        break;
                    endif;

                    if ($one_another == '1'):
                        $product_price = $product_price + $calculated_tax;
                    endif;
                    $sr++;
                endforeach;
                if ($combine == '1'):
                    $QueryObj = new tax();
                    $calculated_tax = $QueryObj->calculateTaxFromPrice($product_price, $total_tax_rate);
                    $total_tax = $total_tax + $calculated_tax;
                endif;
            endif;
            return number_format($total_tax, 2);
        endif;
        return false;
    }

    /*
     * calculate tax from price and rate
     */

    function calculateTaxFromPrice($price, $rate) {
        $tax = '0';
        $tax = $price * $rate / 100;
        return number_format($tax, '2');
    }

}

/*
 * tax rule group Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class tax_rule_group extends tax {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($table = 'supplier_tax_rule_group', $order = 'desc', $orderby = 'name') {
        parent::__construct($table);
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'name', 'is_active', 'date_add');
    }

    /*
     * Create new tax or update existing tax
     */

    function saveTaxGroup($POST) {
        global $supplier_id;
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($POST['is_active']) ? "1" : "0";
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            $this->Data['supplier_id'] = $supplier_id;
            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * Get tax by id
     */

    function getTaxGroup($id) {
        global $supplier_id;
        $this->Where = " where id='$id' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }

    /*
     * Get tax rule group name
     */

    public static function getTaxGroupName($id) {
        $query = new tax_rule_group();
        $query->Where = " where `id` = $id";
        $object = $query->DisplayOne();
        if (is_object($object)):
            return $object->name;
        endif;
    }

    /*
     * Get List of all taxes
     */

    function listTaxGroups($is_active = TRUE, $rtype = '') {
        global $supplier_id;
        $this->Where = " where supplier_id='$supplier_id'";
        if ($is_active):
            $this->Where .=" and is_active='1'";
        endif;
        $this->Where .=" order by $this->orderby $this->order";
        if ($rtype == 'array'):
            return $this->ListOfAllRecords();
        else:
            return $this->DisplayAll();
        endif;
    }

    /* check Tax Group by name */

    public static function checkTaxGroupByName($name) {
        $query = new query('supplier_tax_rule_group');
        $query->Field = " id,name";
        $query->Where = " where  name='$name'";

        $object = $query->Displayone();

        if ($object):

            return $object->id;

        else:

            /* make super attribute set */
            $Query2 = new tax_rule_group();
            $Query2->Data['name'] = $name;
            $Query2->Data['is_active'] = "1";
            $Query2->insert();

            return $Query2->GetMaxId();

        endif;



        return false;
    }

}

/*
 * tax rule Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class tax_rule extends tax {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($table = 'supplier_tax_rule', $order = 'asc', $orderby = 'priority') {
        parent::__construct($table);
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'tax_id', 'tax_rule_group_id', 'zone_id', 'description', 'apply_on_shipping', 'apply_on_gifts', 'priority', 'behaviour', 'position', 'is_active', 'date_add', 'date_upd');
    }

    /*
     * Create new tax or update existing tax
     */

    function saveTaxRule($POST) {
        global $supplier_id;
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($POST['is_active']) ? "1" : "0";
        $this->Data['apply_on_shipping'] = isset($POST['apply_on_shipping']) ? "1" : "0";
        $this->Data['apply_on_gifts'] = isset($POST['apply_on_gifts']) ? "1" : "0";

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            $this->Data['supplier_id'] = $supplier_id;
            $this->Data['date_add'] = date("Y-m-d H:i:s");
            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * Get tax by id
     */

    function getTaxRule($id) {
        global $supplier_id;
        $this->Where = " where id='$id' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }

    /*
     * Get List of all taxes
     */

    function ListRulesofGroup($group_id) {
        $Query = new query('supplier_tax_rule as ctr');
        $Query->Field = " ctr.*, czn.name as zone, ctx.name as tax";
        $Query->Where = " left join " . TABLE_PREFIX . "supplier_zone as czn on ctr.zone_id=czn.id";
        $Query->Where .=" left join " . TABLE_PREFIX . "supplier_tax as ctx on ctr.tax_id=ctx.id";
        $Query->Where .=" where ctr.tax_rule_group_id='$group_id' order by ctr.priority asc";
        return $Query->ListOfAllRecords();
    }

    /*
     * Update priority
     */

    function updatePriority($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['priority'] = ($position != '') ? $position : 0;
        return $this->Update();
    }

}

?>