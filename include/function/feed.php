<?php

function file_exists_remote($url) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_NOBODY, true);
    //Check connection only
    $result = curl_exec($curl);
    //Actual request
    $ret = false;
    if ($result !== false) {
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        //Check HTTP status code
        if ($statusCode == 200) {
            $ret = true;
        }
    }
    curl_close($curl);
    return $ret;
}

function isXML($xml) {
    libxml_use_internal_errors(true);

    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML($xml);

    $errors = libxml_get_errors();

    if (empty($errors)) {
        return true;
    }

    $error = $errors[0];
    if ($error->level < 3) {
        return true;
    }

    $explodedxml = explode("r", $xml);
    $badxml = @$explodedxml[($error->line) - 1];

    $message = $error->message . ' at line ' . $error->line . '. Bad XML: ' . htmlentities($badxml);
    return $message;
}

?>
