<?php

/*
 * Shipping Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class shipping_price extends shipping {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($table = 'supplier_shipping_price', $order = 'desc', $orderby = 'id') {
        parent::__construct($table);
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'shipping_id', 'zone_id', 'is_free', 'price', 'tax_rule_id', 'is_active', 'is_deleted', 'shipping_group', 'min', 'max');
    }

    /*
     * list price shippings
     */

    function listPriceShipping($active = false) {
        global $supplier_id;
        $query = new query('supplier_shipping_price as sp');
        $query->Field = " zn.name as zone,sg.name as group_name,sp.* ";
        $query->Where = " left join " . TABLE_PREFIX . "supplier_zone as zn on sp.zone_id=zn.id";
        $query->Where .= " left join " . TABLE_PREFIX . "supplier_shipping_group as sg on sp.shipping_group=sg.id";
        $query->Where .= " where sp.supplier_id='$supplier_id' ";
        if ($active):
            $query->Where.= " and sp.is_active='1'";
        endif;
        $query->Where.= " and sp.is_deleted='0' order by sp.id desc";
//        $query->print=1;
        return $query->ListOfAllRecords('object');
    }

    /*
     * insert or update shipping rule
     */

    function saveShipping($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        global $supplier_id;
        $this->Data['supplier_id'] = $supplier_id;
        $this->Data['shipping_id'] = SHIPPING_PRICE_ID;
        $this->Data['is_active'] = isset($POST['is_active']) ? "1" : "0";
        $this->Data['max'] = ($this->Data['max'] == '') ? "-1" : $this->Data['max'];

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {

            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * get object of shiping price entry
     */

    function getShippingPrice($id) {
        global $supplier_id;
        $this->Where = " where id='$id' and supplier_id='$supplier_id;'";
        return $this->DisplayOne();
    }

    /*
     * calculate shipping by zone id

      function calculateShippingByZone($zone_id){
      $Query = new shipping_price();
      $Query->Where=" where zone_id='$zone_id' and is_active='1' and is_deleted='0'";
      $object = $Query->DisplayOne();
      $return = array();
      if($object):
      $return['shipping_method_name'] = 'Price Based Shipping';
      $return['shipping_rule_id'] = $object->id;
      $return['is_shipping_free']='0';
      $return['shipping_method_id']=SHIPPING_PRICE_ID;

      $price = cart_detail::getCartTotal();

      $queryObj = new shipping_price_range();
      $price_range = $queryObj->getRangeByPrice($object->id,$price);
      if($price_range):
      $return['total_shipping_amount'] = number_format($price_range->price,'2');
      else:
      $return['total_shipping_amount']='0';
      endif;
      return $return;
      endif;
      return false;
      }
     */
    /*
     * calculate shipping by zone id
     */

    function calculateShippingByZone($zone_id) {

        $price = cart_detail::getCartTotal();

        $Query = new query('supplier_shipping_price as sp, supplier_shipping_group as sg');
        $Query->Field = "sg.name,sg.min_delivery_days,sg.max_delivery_days,sp.*";
        $Query->Where = " where (sp.zone_id='$zone_id' or sp.zone_id='0') and sp.min <= $price and (sp.max >= $price or sp.max =  '-1') and sp.is_deleted='0'";
        $Query->Where .=" and sp.shipping_group = sg.id";
        $Query->Where .= createUserGroupCondition('logged_in_user', 'sg');
        $Query->Where .=" group by sp.shipping_group order by sg.position asc";
        $applied_price_rules = $Query->ListOfAllRecords('object');

        $return = array();

        if (count($applied_price_rules)):
            foreach ($applied_price_rules as $kk => $vv):

                $expected_delivery_date_min = false;
                if (is_numeric($vv->min_delivery_days) && $vv->min_delivery_days > 0):
                    $expected_delivery_date_min = date::add_weekdays_to_date($vv->min_delivery_days, date("Y-m-d"), true);
                endif;

                $expected_delivery_date_max = false;
                if (is_numeric($vv->max_delivery_days) && $vv->max_delivery_days > 0):
                    $expected_delivery_date_max = date::add_weekdays_to_date($vv->max_delivery_days, date("Y-m-d"), true);
                endif;

                $rule_array = array(
                    "shipping_method_name" => $vv->name,
                    "shipping_rule_id" => $vv->id,
                    "is_shipping_free" => '0',
                    "shipping_method_id" => $vv->shipping_id,
                    "total_shipping_amount" => number_format($vv->price, '2'),
                    "shipping_group_id" => $vv->shipping_group,
                    "estimate_delivery_date_min" => $expected_delivery_date_min,
                    "estimate_delivery_date_max" => $expected_delivery_date_max
                );

                $return[] = $rule_array;
            endforeach;
        endif;
        return $return;
    }

}

class shipping_price_range extends shipping_price {

    protected $orderby;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($table = 'supplier_shipping_price_range', $order = 'desc', $orderby = 'id') {
        parent::__construct($table);
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'supplier_id', 'price_rule_id', 'min', 'max', 'price', 'is_deleted');
    }

    /*
     * insert or update shipping rule
     */

    function saveShippingPriceRange($POST) {
        global $supplier_id;
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['supplier_id'] = $supplier_id;
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * list all shipping price ranges of rule
     */

    function listShippingPriceRange($price_rule_id) {
        global $supplier_id;
        $this->Where = " where price_rule_id='$price_rule_id' and supplier_id='$supplier_id' and is_deleted='0' order by min asc";
        return $this->ListOfAllRecords();
    }

    /*
     * get last max value 
     */

    function getLastMaxPriceValue($price_rule_id) {
        global $supplier_id;
        $this->Where = " where price_rule_id='$price_rule_id' and supplier_id='$supplier_id' and is_deleted='0' order by min desc limit 0,1";
        $object = $this->DisplayOne();
        if (is_object($object)):
            return $object->max;
        endif;
        return false;
    }

    /*
     * get price range from price
     */

    function getRangeByPrice($rule_id, $price) {
        global $supplier_id;
        $this->Where = " where price_rule_id=$rule_id and `min` < $price and (`max` > $price or `max` =  '-1') and is_deleted='0' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }

}
