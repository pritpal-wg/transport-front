<?php

/*
 * Tables Class - 
 * You are not adviced to make edits into this class.
 */

class table {

    protected $host;
    protected $username;
    protected $password;
    protected $default_charset;
    public $prefix;

    function __construct($host = IMPORT_HOST, $username = IMPORT_USENAME, $password = IMPORT_PASSWORD, $prefix = TABLE_PREFIX, $default_charset = 'latin1') {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->prefix = $prefix;
        $this->default_charset = $default_charset;
        $this->dbconn = mysql_connect($this->host, $this->username, $this->password);
    }

    function createDatabase($name) {
        $sql = "CREATE DATABASE `" . $name . "`";
        if (mysql_query($sql, $this->dbconn)):
            return true;
        endif;
        return false;
    }

    /* create database tables */

    function createDatabaseTables($name) {

        mysql_select_db($name, $this->dbconn);

        // access log table
        $access_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "access_log` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `user` varchar(20) DEFAULT NULL,
			  `action` text,
			  `ip` varchar(20) DEFAULT NULL,
			  `dt` timestamp NULL DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=$this->default_charset;";
        mysql_query($access_sql, $this->dbconn);

        // admin modules table
        $admin_modules_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "admin_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=53 ;";
        mysql_query($admin_modules_sql, $this->dbconn);

        // admin modules actions table
        $modules_actions = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "admin_modules_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `label` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=322 ;";
        mysql_query($modules_actions, $this->dbconn);

        // admin_permission_level_grouping table
        $admin_permission_level_grouping_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "admin_permission_level_grouping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `permission_level_id` int(11) DEFAULT NULL,
  `actions` text NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `permission_level_id` (`permission_level_id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=209 ;";
        mysql_query($admin_permission_level_grouping_sql, $this->dbconn);

        // admin user table
        $admin_sql = "CREATE TABLE IF NOT EXISTS  `" . $this->prefix . "admin_user` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `admin_role_id` int(11) DEFAULT NULL,
            `username` varchar(255) DEFAULT NULL,
            `password` varchar(255) DEFAULT NULL,
            `phone` varchar(255) DEFAULT NULL,
            `email` varchar(255) DEFAULT NULL,
            `last_sign_in` date DEFAULT NULL,
            `is_active` enum('1','0') NOT NULL DEFAULT '1',
            `is_deleted` enum('1','0') NOT NULL DEFAULT '0',
            `last_update_date` datetime DEFAULT NULL,
            `ip_address` varchar(255) DEFAULT NULL,
            `last_access` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `allow_pages` varchar(255) NOT NULL DEFAULT '*',
            `is_loggedin` enum('1','0') NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`),
            KEY `admin_role_id` (`admin_role_id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1;";

        mysql_query($admin_sql, $this->dbconn);

        // admin_user_activity_log table
        $admin_user_activity_log_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "admin_user_activity_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_user_id` int(11) NOT NULL,
  `module` varchar(50) NOT NULL,
  `module_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `action_comment` longtext NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `admin_user_id` (`admin_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($admin_user_activity_log_sql, $this->dbconn);

        // admin user permission level table
        $admin_user_permission_level = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "admin_user_permission_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=5 ;";
        mysql_query($admin_user_permission_level, $this->dbconn);

        // admin user role table
        $admin_user_role = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "admin_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=8 ;";
        mysql_query($admin_user_role, $this->dbconn);

        // admin user role permissions table
        $admin_user_role_permissions = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "admin_user_role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `permission_level_group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `permission_level_group_id` (`permission_level_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($admin_user_role_permissions, $this->dbconn);

        // attribute table
        $attribute_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "attribute` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `attr_set_id` int(11) NOT NULL DEFAULT '0',
        `name` varchar(255) NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `color_code` varchar(255) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `attr_set_id` (`attr_set_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($attribute_sql, $this->dbconn);

        // attribute set table
        $attribute_set_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "attribute_set` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `attr_super_set_id` int(11) NOT NULL DEFAULT '0',
        `type` enum('radio', 'select', 'color', 'checkbox') NOT NULL DEFAULT 'select',
        `name` varchar(255) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `attr_super_set_id` (`attr_super_set_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($attribute_set_sql, $this->dbconn);

        // attribute_super_set table
        $attribute_super_set_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "attribute_super_set` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($attribute_super_set_sql, $this->dbconn);

        // banner table
        $banner = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "banner` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `code` varchar(100) NOT NULL,
        `description` longtext NOT NULL,
        `url` longtext NOT NULL,
        `image` varchar(255) NOT NULL,
        `mobile_image` varchar(255) NOT NULL,
        `text` longtext NOT NULL,
        `color` varchar(100) NOT NULL,
        `hits` int(11) NOT NULL,
        `from_date` datetime NOT NULL,
        `to_date` datetime NOT NULL,
        `is_active` tinyint(4) NOT NULL DEFAULT '1',
        `is_deleted` enum('0', '1') NOT NULL,
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `open_in_new_tab` enum('0', '1') NOT NULL DEFAULT '0',
        `image_alt_text` varchar(255) NOT NULL,
        `views` int(11) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `code` (`code`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($banner, $this->dbconn);

        // access log table
        $banner_hits = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "banner_hits` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `banner_id` int(11) NOT NULL,
        `ip_address` varchar(20) NOT NULL,
        `page` varchar(100) NOT NULL,
        `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `session_id` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($banner_hits, $this->dbconn);

        // banner_location table
        $banner_location = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "banner_location` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `banner_id` int(11) NOT NULL,
        `type` varchar(255) DEFAULT NULL,
        `type_id` int(11) NOT NULL,
        `position` varchar(255) NOT NULL DEFAULT 'Top',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `from_date` datetime NOT NULL,
        `to_date` datetime NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `banner_id_2` (`banner_id`, `type`, `type_id`, `position`),
        KEY `banner_id` (`banner_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($banner_location, $this->dbconn);


        // blog table
        $blog = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "blog` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `category_id` int(11) DEFAULT NULL,
        `name` varchar(255) DEFAULT NULL,
        `meta_name` varchar(255) DEFAULT NULL,
        `urlname` varchar(255) DEFAULT NULL,
        `description` text,
        `tags` text,
        `image` varchar(255) DEFAULT NULL,
        `position` int(3) NOT NULL DEFAULT '0',
        `meta_keyword` text,
        `meta_description` text,
        `is_active` tinyint(4) NOT NULL DEFAULT '1',
        `is_pin` enum('1', '0') NOT NULL DEFAULT '0',
        `visit` int(20) NOT NULL DEFAULT '0',
        `date` varchar(255) DEFAULT NULL,
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        `image_alt_text` VARCHAR( 255 ) NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($blog, $this->dbconn);


        // blog_category table
        $blog_category = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "blog_category` (
        `id` int(255) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `position` int(255) NOT NULL,
        `is_active` tinyint(1) NOT NULL DEFAULT '0',
        `meta_name` varchar(255) NOT NULL,
        `urlname` varchar(255) DEFAULT NULL,
        `meta_keyword` text,
        `meta_description` longtext,
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        `last_update_dt` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($blog_category, $this->dbconn);


        // blog_comment table
        $blog_comment = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "blog_comment` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL,
        `blog_id` int(11) NOT NULL,
        `name` varchar(255) DEFAULT NULL,
        `email` varchar(250) DEFAULT NULL,
        `website_link` varchar(150) DEFAULT NULL,
        `comment` text,
        `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `is_active` tinyint(4) NOT NULL DEFAULT '1',
        `position` int(11) NOT NULL DEFAULT '0',
        `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($blog_comment, $this->dbconn);


        // brand table
        $brand = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "brand` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `urlname` varchar(255) NOT NULL,
        `image` varchar(255) NOT NULL,
        `description` longtext NOT NULL,
        `website` varchar(255) NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `is_active` tinyint(4) NOT NULL DEFAULT '0',
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `meta_title` varchar(255) NOT NULL,
        `meta_desc` text NOT NULL,
        `meta_keyword` varchar(255) NOT NULL,
        `meta_robots_index` enum('global', 'index', 'noindex') NOT NULL DEFAULT 'global',
        `meta_robots_follow` enum('global', 'follow', 'nofollow') NOT NULL DEFAULT 'follow',
        `include_xml_sitemap` enum('global', 'include', 'exclude') NOT NULL DEFAULT 'global',
        `sitemap_priority` varchar(11) NOT NULL DEFAULT 'global',
        `canonical` longtext NOT NULL,
        `image_alt_text` VARCHAR( 255 ) NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($brand, $this->dbconn);


        // cart table
        $cart = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_secure_key` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL DEFAULT '0',
  `currency_conversion_rate` float NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(40) DEFAULT NULL,
  `billing_address_id` int(11) NOT NULL DEFAULT '0',
  `billing_address_title` varchar(255) NOT NULL,
  `billing_firstname` varchar(255) NOT NULL,
  `billing_lastname` varchar(255) NOT NULL,
  `billing_address1` varchar(255) NOT NULL,
  `billing_address2` varchar(255) NOT NULL,
  `billing_city` varchar(255) NOT NULL,
  `billing_state` varchar(255) NOT NULL,
  `billing_country` varchar(255) NOT NULL,
  `billing_zip` varchar(255) NOT NULL,
  `billing_fax` varchar(255) NOT NULL,
  `billing_email` varchar(255) NOT NULL,
  `billing_phone` varchar(255) NOT NULL,
  `billing_mobile` varchar(255) NOT NULL,
  `same_shipping_billing` enum('0','1') NOT NULL DEFAULT '1',
  `shipping_address_id` int(11) NOT NULL DEFAULT '0',
  `shipping_address_title` varchar(255) NOT NULL,
  `shipping_firstname` varchar(255) NOT NULL,
  `shipping_lastname` varchar(255) NOT NULL,
  `shipping_address1` varchar(255) NOT NULL,
  `shipping_address2` varchar(255) NOT NULL,
  `shipping_city` varchar(255) NOT NULL,
  `shipping_state` varchar(255) NOT NULL,
  `shipping_country` varchar(255) NOT NULL,
  `shipping_phone` varchar(255) NOT NULL,
  `shipping_mobile` varchar(255) NOT NULL,
  `shipping_zip` varchar(255) NOT NULL,
  `shipping_fax` varchar(255) NOT NULL,
  `shipping_email` varchar(255) NOT NULL,
  `shipping_comment` varchar(255) NOT NULL,
  `is_shipping_address_saved` enum('0','1') NOT NULL DEFAULT '0',
  `is_shipping_free` enum('1','0') NOT NULL DEFAULT '0',
  `shipping_rule_id` int(11) NOT NULL DEFAULT '0',
  `shipping_method_id` int(11) NOT NULL DEFAULT '0',
  `shipping_method_name` varchar(255) NOT NULL,
  `total_shipping_amount` float NOT NULL DEFAULT '0',
  `total_cart_weight` float NOT NULL DEFAULT '0',
  `coupon_id` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(255) NOT NULL,
  `coupon_free_shipping` enum('1','0') NOT NULL DEFAULT '0',
  `payment_method_id` int(11) NOT NULL DEFAULT '0',
  `payment_method_name` varchar(255) NOT NULL,
  `gift_message` longtext NOT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `shipping_group_id` int(11) NOT NULL,
  `is_billing_address_saved` enum('0','1') NOT NULL DEFAULT '0',
  `is_card_info_saved` enum('0','1') NOT NULL DEFAULT '0',
  `card_number` bigint(20) DEFAULT NULL,
  `card_type` varchar(30) DEFAULT NULL,
  `expiry_month` int(2) DEFAULT NULL,
  `expiry_year` int(4) DEFAULT NULL,
  `cvv` int(3) DEFAULT NULL,
  `estimate_delivery_date_min` date DEFAULT NULL,
  `estimate_delivery_date_max` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_secure_key` (`cart_secure_key`),
  KEY `billing_address_id` (`billing_address_id`),
  KEY `user_id` (`user_id`),
  KEY `shipping_address_id` (`shipping_address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($cart, $this->dbconn);


        // cart_detail table
        $cart_detail = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_detail` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_id` int(11) NOT NULL DEFAULT '0',
        `product_id` int(11) NOT NULL DEFAULT '0',
        `product_name` varchar(255) NOT NULL,
        `product_sku` varchar(255) NOT NULL,
        `product_variant_id` int(11) NOT NULL DEFAULT '0',
        `product_variant_value` varchar(255) NOT NULL,
        `product_type` enum('simple', 'downloadable', 'virtual') NOT NULL DEFAULT 'simple',
        `download_file_name` varchar(255) NOT NULL,
        `download_within_days` int(11) NOT NULL DEFAULT '-1',
        `maximum_downloads` int(11) NOT NULL DEFAULT '1',
        `quantity` int(11) NOT NULL DEFAULT '0',
        `discount_type` enum('percentage', 'flat') NOT NULL DEFAULT 'percentage',
        `discount` float NOT NULL DEFAULT '0',
        `sale_price` float NOT NULL DEFAULT '0',
        `unit_price` float NOT NULL DEFAULT '0',
        `tax_rule_group_id` int(11) NOT NULL DEFAULT '0',
        `tax_rule_group_name` varchar(255) NOT NULL,
        `tax` float NOT NULL DEFAULT '0',
        `additional_shipping_price` float NOT NULL DEFAULT '0',
        `coupon_amount` float NOT NULL DEFAULT '0',
        `is_free_gift` enum('0', '1') NOT NULL DEFAULT '0',
        `gift_wrap_charges` float NOT NULL DEFAULT '0',
        `product_weight` float NOT NULL DEFAULT '0',
        `total_item_weight` float NOT NULL DEFAULT '0',
        `total_price_tax_incl` float NOT NULL DEFAULT '0',
        `total_price_tax_excl` float NOT NULL DEFAULT '0',
        `total_shipping_amount` float NOT NULL DEFAULT '0',
        `total_gift_wrapping_amount` float NOT NULL DEFAULT '0',
        `total_discount` float NOT NULL DEFAULT '0',
        `total_coupon_amount` float NOT NULL DEFAULT '0',
        `total_tax` float NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `CART` (`cart_id`, `product_id`, `product_variant_id`),
        KEY `product_variant_id` (`product_variant_id`),
        KEY `product_id` (`product_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_detail, $this->dbconn);


        // cart_rule table
        $cart_rule = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_type` int(11) NOT NULL,
        `name` varchar(255) NOT NULL,
        `coupon_code` varchar(255) NOT NULL,
        `from_date` datetime NOT NULL,
        `to_date` datetime NOT NULL,
        `is_auto` enum('0', '1') NOT NULL DEFAULT '0',
        `quantity` int(11) NOT NULL DEFAULT '1',
        `quantity_per_user` int(11) NOT NULL DEFAULT '1',
        `priority` int(11) NOT NULL DEFAULT '1',
        `is_active` tinyint(4) NOT NULL DEFAULT '0',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `min_cart_amount` float NOT NULL,
        `min_cart_quantity` int(11) NOT NULL DEFAULT '1',
        `reduction_type` enum('flat', 'percentage') NOT NULL DEFAULT 'flat',
        `reduction` float NOT NULL,
        `is_apply_on_nondiscounted_products_only` enum('0', '1') NOT NULL DEFAULT '0',
        `is_shipping_free` enum('0', '1') NOT NULL DEFAULT '0',
        `is_zone_restricted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_country_restricted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_user_restricted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_product_restricted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_category_restricted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_brand_restricted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_supplier_restricted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_attribute_restricted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_send_gift` enum('0', '1') NOT NULL DEFAULT '0',
        `gift_product` int(11) NOT NULL DEFAULT '0',
        `gift_product_variant` int(11) NOT NULL,
        `apply_discount_on` enum('order', 'single_product', 'none') NOT NULL DEFAULT 'none',
        `apply_discount_type` enum('cheapest', 'expensive') NOT NULL DEFAULT 'cheapest',
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `description` LONGTEXT NOT NULL,
        PRIMARY KEY (`id`),
        KEY `cart_rule_type` (`cart_rule_type`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule, $this->dbconn);


        // cart_rule_attribute table
        $cart_rule_attribute = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_attribute` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_id` int(11) NOT NULL,
        `attribute_id` int(11) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `attribute_id` (`attribute_id`, `cart_rule_id`),
        KEY `cart_rule_id` (`cart_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_attribute, $this->dbconn);


        // cart_rule_brand table
        $cart_rule_brand = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_brand` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_id` int(11) NOT NULL,
        `brand_id` int(11) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `brand_id` (`brand_id`, `cart_rule_id`),
        KEY `cart_rule_id` (`cart_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_brand, $this->dbconn);


        // cart_rule_category table
        $cart_rule_category = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_category` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_id` int(11) NOT NULL,
        `category_id` int(11) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `category_id` (`category_id`, `cart_rule_id`),
        KEY `cart_rule_id` (`cart_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_category, $this->dbconn);


        // cart_rule_country table
        $cart_rule_country = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_country` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_id` int(11) NOT NULL,
        `country_id` int(11) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `country_id` (`cart_rule_id`, `country_id`),
        KEY `cart_rule_id` (`cart_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_country, $this->dbconn);

        // cart_rule_product table
        $cart_rule_product = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_product` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_id` int(11) NOT NULL,
        `product_id` int(11) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `product_id` (`product_id`, `cart_rule_id`),
        KEY `cart_rule_id` (`cart_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_product, $this->dbconn);

        // cart_rule_supplier table
        $cart_rule_supplier = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_supplier` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_id` int(11) NOT NULL,
        `supplier_id` int(11) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `supplier_id` (`supplier_id`, `cart_rule_id`),
        KEY `cart_rule_id` (`cart_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_supplier, $this->dbconn);

        // cart_rule_type table
        $cart_rule_type = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_type` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `desc` longtext NOT NULL,
        `free_shipping` enum('0', '1') NOT NULL DEFAULT '0',
        `gift` enum('0', '1') NOT NULL DEFAULT '0',
        `discount` enum('0', '1') NOT NULL DEFAULT '0',
        `is_active` enum('0', '1') NOT NULL DEFAULT '0',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `position` int(11) NOT NULL DEFAULT '0',
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_type, $this->dbconn);

        // cart_rule_type_selection table
        $cart_rule_type_selection = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_type_selection` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_type_id` int(11) NOT NULL,
        `selection` enum('zone', 'country', 'user', 'product', 'category', 'brand', 'supplier', 'attribute') NOT NULL,
        `is_main` enum('0', '1') NOT NULL DEFAULT '0',
        `position` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `cart_rule_type_id_2` (`cart_rule_type_id`, `selection`),
        KEY `cart_rule_type_id` (`cart_rule_type_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_type_selection, $this->dbconn);

        // cart_rule_user table
        $cart_rule_user = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_user` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_id` int(11) NOT NULL,
        `user_id` int(11) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `user_id` (`user_id`, `cart_rule_id`),
        KEY `cart_rule_id` (`cart_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_user, $this->dbconn);

        // cart_rule_zone table
        $cart_rule_zone = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "cart_rule_zone` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `cart_rule_id` int(11) NOT NULL,
        `zone_id` int(11) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `zone_id` (`zone_id`, `cart_rule_id`),
        KEY `cart_rule_id` (`cart_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($cart_rule_zone, $this->dbconn);

        // category table
        $category = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "category` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `urlname` varchar(255) NOT NULL,
        `image` varchar(255) NOT NULL,
        `description` longtext NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `parent_id` int(11) NOT NULL DEFAULT '0',
        `is_active` tinyint(1) NOT NULL DEFAULT '0',
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        `is_featured` enum('1', '0') NOT NULL DEFAULT '0',
        `attribute_super_set_id` int(11) NOT NULL DEFAULT '0',
        `meta_title` varchar(255) NOT NULL,
        `meta_desc` text NOT NULL,
        `meta_keyword` varchar(255) NOT NULL,
        `meta_robots_index` enum('global', 'index', 'noindex') NOT NULL DEFAULT 'global',
        `meta_robots_follow` enum('global', 'follow', 'nofollow') NOT NULL DEFAULT 'follow',
        `include_xml_sitemap` enum('global', 'include', 'exclude') NOT NULL DEFAULT 'global',
        `sitemap_priority` varchar(11) NOT NULL DEFAULT 'global',
        `canonical` longtext NOT NULL,
        `from_date` datetime NOT NULL,
        `to_date` datetime NOT NULL,
        `image_alt_text` varchar(255) NOT NULL,
        `display_catname` enum('0', '1') NOT NULL DEFAULT '1',
        `user_group_id` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '0',
        `display_in_category_listing` int(11) NOT NULL DEFAULT '1',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `parent_id` (`parent_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($category, $this->dbconn);

        // category_product table
        $category_product = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "category_product` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `category_id` int(11) NOT NULL DEFAULT '0',
        `product_id` int(11) NOT NULL DEFAULT '0',
        `position` int(11) NOT NULL DEFAULT '0',
        `is_feed` enum('0', '1') NOT NULL DEFAULT '0',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `category_product` (`product_id`, `category_id`),
        KEY `category_id` (`category_id`),
        KEY `product_id` (`product_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($category_product, $this->dbconn);



        // category_structure table
        $category_structure = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "category_structure` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `from_feed` enum('0', '1') NOT NULL DEFAULT '0',
        `feed_name` varchar(255) NOT NULL,
        `f_cat_name` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `image` varchar(255) NOT NULL,
        `description` longtext NOT NULL,
        `parent_id` int(11) NOT NULL,
        `is_active` tinyint(4) NOT NULL,
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `store_cat_id` int(11) NOT NULL,
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($category_structure, $this->dbconn);

        // city table
        $city = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "city` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `country_id` int(11) NOT NULL DEFAULT '0',
        `state_id` int(11) NOT NULL DEFAULT '0',
        `name` varchar(255) NOT NULL,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `country_id` (`country_id`),
        KEY `state_id` (`state_id`),
        KEY `city_name` (`name`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($city, $this->dbconn);

        // content table
        $content = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "content` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL DEFAULT '',
        `urlname` varchar(255) DEFAULT NULL,
        `page` longtext,
        `short_description` text,
        `meta_keyword` text,
        `meta_description` text,
        `publish_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `page_name` varchar(255) DEFAULT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `is_deleted` tinyint(1) DEFAULT '0',
        `image` varchar(255) DEFAULT NULL,
        `meta_robots_index` enum('global', 'index', 'noindex') NOT NULL DEFAULT 'global',
        `meta_robots_follow` enum('global', 'follow', 'nofollow') NOT NULL DEFAULT 'follow',
        `include_xml_sitemap` enum('global', 'include', 'exclude') NOT NULL DEFAULT 'global',
        `sitemap_priority` varchar(11) NOT NULL DEFAULT 'global',
        `canonical` longtext NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($content, $this->dbconn);

        // country table
        $country = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "country` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `iso2` char(2) NOT NULL,
        `short_name` varchar(80) NOT NULL,
        `long_name` varchar(80) NOT NULL,
        `iso3` char(3) NOT NULL,
        `numcode` varchar(6) DEFAULT NULL,
        `calling_code` varchar(8) DEFAULT NULL,
        `flag` varchar(5) DEFAULT NULL,
        `contains_states` enum('0', '1') NOT NULL DEFAULT '1',
        `need_zip_code` enum('0', '1') NOT NULL DEFAULT '1',
        `is_active` enum('0', '1') NOT NULL DEFAULT '1',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($country, $this->dbconn);

        // country table
        $db_backup = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "db_backup` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `last_updated_dt` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `document` varchar(255) DEFAULT NULL,
        `ip_address` varchar(50) DEFAULT NULL,
        `is_active` tinyint(1) DEFAULT '1',
        `is_deleted` tinyint(1) DEFAULT '0',
        `position` int(11) DEFAULT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($db_backup, $this->dbconn);

        // disclaimer table
        $disclaimer_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "disclaimer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_page_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `position` int(11) NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `auto_close` varchar(255) NOT NULL,
  `show_close_button` enum('0','1') NOT NULL DEFAULT '0',
  `open_once_in_session` enum('0','1') NOT NULL DEFAULT '0',
  `before_time` int(11) NOT NULL,
  `after_time` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `content_page_id` (`content_page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($disclaimer_sql, $this->dbconn);

        // email table
        $email = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "email` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `email_name` varchar(255) DEFAULT NULL,
        `email_fields` longtext,
        `email_subject` varchar(255) DEFAULT NULL,
        `email_text` text,
        `email_type` varchar(255) DEFAULT NULL,
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `from_name` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = MyISAM DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($email, $this->dbconn);

        $gift_certificates = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "gift_certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_price` double NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `sender_email` varchar(255) NOT NULL,
  `receiver_email` varchar(255) NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `receiver_name` varchar(255) NOT NULL,
  `sender_contact` varchar(255) NOT NULL,
  `receiver_contact` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `gift_code` varchar(255) NOT NULL,
  `is_used` enum('0','1') NOT NULL DEFAULT '1',
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($gift_certificates, $this->dbconn);

        // hypernym 
        $hypernym = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "hypernym` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(100) NOT NULL,
        `keywords` longtext NOT NULL,
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($hypernym, $this->dbconn);

        // Idea Table Structure
        $idea_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "idea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `story` text NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `approval_date` date NOT NULL,
  `date_add` date NOT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `voting_end_date` date NOT NULL,
  `approved_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($idea_sql, $this->dbconn);

        // Idea Category Table Structure
        $idea_category = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "idea_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` date NOT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($idea_category, $this->dbconn);

        // Idea Photo Table Structure
        $idea_photo = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "idea_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idea_id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `approval_date` date NOT NULL,
  `date_add` date NOT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idea_id` (`idea_id`)
) ENGINE=InnoDB DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($idea_photo, $this->dbconn);

        // manufacturer table
        $manufacturer = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "manufacturer` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `urlname` varchar(255) NOT NULL,
        `image` varchar(255) NOT NULL,
        `description` longtext NOT NULL,
        `website` varchar(255) NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `is_active` tinyint(4) NOT NULL DEFAULT '1',
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `meta_title` varchar(255) NOT NULL,
        `meta_desc` text NOT NULL,
        `meta_keyword` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `image_alt_text` VARCHAR( 255 ) NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($manufacturer, $this->dbconn);

        // navigation table
        $navigation = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "navigation` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `parent_id` int(11) DEFAULT '0',
        `nav_id` int(255) NOT NULL,
        `nav_class` varchar(255) NOT NULL,
        `is_mega_menu` tinyint(1) NOT NULL DEFAULT '0',
        `navigation_title` varchar(255) DEFAULT NULL,
        `navigation_link` varchar(255) DEFAULT NULL,
        `position` int(11) DEFAULT NULL,
        `is_active` int(11) DEFAULT NULL,
        `navigation_query` varchar(255) DEFAULT NULL,
        `is_external` tinyint(1) NOT NULL DEFAULT '0',
        `open_in_new` tinyint(1) NOT NULL DEFAULT '0',
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `nav_id` (`nav_id`)) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset";
        mysql_query($navigation, $this->dbconn);


        // navigations table

        $navigations = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "navigations` (
        `id` int(255) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) DEFAULT NULL,
        `position` varchar(10) NOT NULL DEFAULT 'top',
        `description` text,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset";
        mysql_query($navigations, $this->dbconn);


        // newsletter table
        $newsletter = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "newsletter` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `email` varchar(255) NOT NULL,
        `name` varchar(100) NOT NULL,
        `user_id` int(11) NOT NULL,
        `is_active` enum('0', '1') NOT NULL DEFAULT '0',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `email` (`email`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($newsletter, $this->dbconn);

        // option
        $options = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "option` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `show_in_filters` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($options, $this->dbconn);

        // order table
        $order = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL DEFAULT '0',
  `is_credit_used` int(11) DEFAULT '0',
  `credit_deduction_id` int(11) DEFAULT NULL,
  `credits_deducted` double DEFAULT NULL,
  `currency_conversion_rate` float NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(40) DEFAULT NULL,
  `billing_address_id` int(11) NOT NULL DEFAULT '0',
  `billing_address_title` varchar(255) NOT NULL,
  `billing_firstname` varchar(255) NOT NULL,
  `billing_lastname` varchar(255) NOT NULL,
  `billing_address1` varchar(255) NOT NULL,
  `billing_address2` varchar(255) NOT NULL,
  `billing_city` varchar(255) NOT NULL,
  `billing_state` varchar(255) NOT NULL,
  `billing_country` varchar(255) NOT NULL,
  `billing_zip` varchar(255) NOT NULL,
  `billing_fax` varchar(255) NOT NULL,
  `billing_email` varchar(255) NOT NULL,
  `billing_phone` varchar(255) NOT NULL,
  `billing_mobile` varchar(255) NOT NULL,
  `same_shipping_billing` enum('0','1') NOT NULL DEFAULT '0',
  `shipping_address_id` int(11) NOT NULL DEFAULT '0',
  `shipping_address_title` varchar(255) NOT NULL,
  `shipping_firstname` varchar(255) NOT NULL,
  `shipping_lastname` varchar(255) NOT NULL,
  `shipping_address1` varchar(255) NOT NULL,
  `shipping_address2` varchar(255) NOT NULL,
  `shipping_city` varchar(255) NOT NULL,
  `shipping_state` varchar(255) NOT NULL,
  `shipping_country` varchar(255) NOT NULL,
  `shipping_phone` varchar(255) NOT NULL,
  `shipping_mobile` varchar(255) NOT NULL,
  `shipping_zip` varchar(255) NOT NULL,
  `shipping_fax` varchar(255) NOT NULL,
  `shipping_email` varchar(255) NOT NULL,
  `shipping_comment` varchar(255) NOT NULL,
  `is_shipping_free` enum('1','0') NOT NULL DEFAULT '0',
  `shipping_rule_id` int(11) NOT NULL DEFAULT '0',
  `shipping_method_id` int(11) NOT NULL,
  `shipping_method_name` varchar(255) NOT NULL,
  `total_shipping_amount` float NOT NULL,
  `total_order_weight` float DEFAULT NULL,
  `coupon_id` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(255) NOT NULL,
  `coupon_free_shipping` enum('1','0') NOT NULL DEFAULT '0',
  `payment_method_id` int(11) NOT NULL DEFAULT '0',
  `payment_method_name` varchar(255) NOT NULL,
  `gift_message` longtext NOT NULL,
  `total_gift_wrapping_amount` float NOT NULL DEFAULT '0',
  `total_discount` float NOT NULL DEFAULT '0',
  `total_price_tax_incl` float NOT NULL,
  `total_price_tax_excl` float NOT NULL,
  `grand_total` float NOT NULL,
  `total_tax` float NOT NULL,
  `total_paid` float NOT NULL,
  `total_coupon_amount` float NOT NULL,
  `is_site_cash_used` enum('1','0') NOT NULL DEFAULT '0',
  `total_site_cash_used_amount` float NOT NULL DEFAULT '0',
  `order_comment` varchar(255) NOT NULL,
  `estimate_delivery_date` date NOT NULL,
  `transaction_id` varchar(50) NOT NULL DEFAULT '0',
  `is_payment_made` enum('1','0') NOT NULL DEFAULT '0',
  `payment_comment` varchar(255) NOT NULL,
  `payment_time` datetime NOT NULL,
  `current_state` varchar(255) NOT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `is_archive` enum('0','1') NOT NULL DEFAULT '0',
  `shipping_date` datetime NOT NULL,
  `shipping_carrier` varchar(255) NOT NULL,
  `shipping_tracking_no` varchar(50) NOT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `is_ftp` tinyint(1) NOT NULL DEFAULT '0',
  `avs` varchar(20) DEFAULT NULL,
  `card_number` bigint(20) DEFAULT NULL,
  `card_type` varchar(30) DEFAULT NULL,
  `expiry_month` int(2) DEFAULT NULL,
  `expiry_year` int(4) DEFAULT NULL,
  `cvv` int(3) DEFAULT NULL,
  `estimate_delivery_date_min` date DEFAULT NULL,
  `estimate_delivery_date_max` date DEFAULT NULL,
  `auth_code` varchar(50) NOT NULL,
  `paypal_business_email` varchar(100) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `billing_address_id` (`billing_address_id`),
  KEY `shipping_address_id` (`shipping_address_id`),
  KEY `date_add` (`date_add`),
  KEY `cart_id` (`cart_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($order, $this->dbconn);

        //order detail table
        $order_detail = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `product_name` varchar(255) NOT NULL,
  `product_sku` varchar(255) NOT NULL,
  `product_variant_id` int(11) NOT NULL DEFAULT '0',
  `product_variant_value` varchar(255) NOT NULL,
  `product_type` enum('simple','downloadable','virtual') NOT NULL DEFAULT 'simple',
  `download_file_name` varchar(255) NOT NULL,
  `download_deadline` date NOT NULL,
  `maximum_downloads` int(11) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `discount_type` enum('percentage','flat') NOT NULL DEFAULT 'percentage',
  `discount` int(11) NOT NULL DEFAULT '0',
  `sale_price` float NOT NULL DEFAULT '0',
  `unit_price` float NOT NULL DEFAULT '0',
  `tax_rule_group_id` int(11) NOT NULL DEFAULT '0',
  `tax_rule_group_name` varchar(255) NOT NULL,
  `tax` float NOT NULL DEFAULT '0',
  `additional_shipping_price` float NOT NULL DEFAULT '0',
  `coupon_amount` float NOT NULL DEFAULT '0',
  `gift_wrap_charges` float NOT NULL DEFAULT '0',
  `product_weight` float NOT NULL DEFAULT '0',
  `total_item_weight` float NOT NULL DEFAULT '0',
  `total_price_tax_incl` float NOT NULL DEFAULT '0',
  `total_price_tax_excl` float NOT NULL DEFAULT '0',
  `total_shipping_amount` float NOT NULL DEFAULT '0',
  `total_gift_wrapping_amount` float NOT NULL DEFAULT '0',
  `total_discount` float NOT NULL DEFAULT '0',
  `total_coupon_amount` float NOT NULL DEFAULT '0',
  `total_tax` float NOT NULL DEFAULT '0',
  `date_add` datetime DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_product` (`order_id`,`product_id`,`product_variant_id`),
  KEY `product_id` (`product_id`),
  KEY `product_variant_id` (`product_variant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($order_detail, $this->dbconn);

        // order packet table
        $order_packet = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order_packet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `estimate_delivery_date` date NOT NULL,
  `actual_delivery_date` date NOT NULL,
  `current_status` int(11) NOT NULL DEFAULT '0',
  `comment` longtext NOT NULL,
  `total_gift_wrapping_amount` float NOT NULL DEFAULT '0',
  `total_discount` float NOT NULL DEFAULT '0',
  `total_price_tax_incl` float NOT NULL DEFAULT '0',
  `total_price_tax_excl` float NOT NULL DEFAULT '0',
  `grand_total` float NOT NULL DEFAULT '0',
  `total_tax` float NOT NULL DEFAULT '0',
  `total_coupon_amount` float NOT NULL DEFAULT '0',
  `total_products` int(11) NOT NULL DEFAULT '0',
  `total_shipping_amount` float NOT NULL DEFAULT '0',
  `date_add` datetime DEFAULT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($order_packet, $this->dbconn);

        // order_packet_detail table
        $order_packet_detail = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order_packet_detail` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `order_packet_id` int(11) NOT NULL DEFAULT '0',
        `order_detal_id` int(11) NOT NULL DEFAULT '0',
        `quantity` int(11) NOT NULL DEFAULT '0',
        `total_price_tax_incl` float NOT NULL DEFAULT '0',
        `total_price_tax_excl` float NOT NULL DEFAULT '0',
        `total_shipping_amount` float NOT NULL DEFAULT '0',
        `total_gift_wrapping_amount` float NOT NULL DEFAULT '0',
        `total_discount` float NOT NULL DEFAULT '0',
        `total_coupon_amount` float NOT NULL DEFAULT '0',
        `total_tax` float NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `order_packet_id` (`order_packet_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($order_packet_detail, $this->dbconn);

        // order_packet_status_history table
        $order_packet_status_history = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order_packet_status_history` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `order_packet_id` int(11) NOT NULL DEFAULT '0',
        `status` int(11) NOT NULL,
        `location` varchar(255) NOT NULL,
        `comment` longtext NOT NULL,
        `is_sms_sent` enum('1', '0') NOT NULL DEFAULT '0',
        `is_email_sent` enum('1', '0') NOT NULL DEFAULT '0',
        `sms` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `order_packet_id` (`order_packet_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($order_packet_status_history, $this->dbconn);

        // order_return table
        $order_return = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order_return` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `order_id` int(11) NOT NULL DEFAULT '0',
        `user_id` int(11) NOT NULL DEFAULT '0',
        `return_date` date NOT NULL,
        `current_status` int(11) NOT NULL DEFAULT '0',
        `comment` longtext NOT NULL,
        `grand_total` float NOT NULL DEFAULT '0',
        `refund_method` varchar(255) NOT NULL,
        `refund_details` varchar(255) NOT NULL,
        `refund_start_date` date NOT NULL,
        `actual _refund_date` date NOT NULL,
        `refund_status` int(11) NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `order_id` (`order_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($order_return, $this->dbconn);

        // order_return_detail table
        $order_return_detail = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order_return_detail` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `order_return_id` int(11) NOT NULL DEFAULT '0',
        `order_id` int(11) NOT NULL DEFAULT '0',
        `order_detal_id` int(11) NOT NULL DEFAULT '0',
        `quantity` int(11) NOT NULL DEFAULT '0',
        `total_price_tax_incl` float NOT NULL DEFAULT '0',
        `total_price_tax_excl` float NOT NULL DEFAULT '0',
        `total_shipping_amount` float NOT NULL DEFAULT '0',
        `total_gift_wrapping_amount` float NOT NULL DEFAULT '0',
        `total_discount` float NOT NULL DEFAULT '0',
        `total_coupon_amount` float NOT NULL DEFAULT '0',
        `total_tax` float NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `order_return_id` (`order_return_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($order_return_detail, $this->dbconn);

        // order_status table
        $order_status = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order_status` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `email_template` varchar(255) NOT NULL,
        `sms_template` varchar(255) NOT NULL,
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($order_status, $this->dbconn);

        // order_status_history table
        $order_status_history = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "order_status_history` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `order_id` int(11) NOT NULL DEFAULT '0',
        `status` varchar(255) NOT NULL,
        `location` varchar(255) NOT NULL,
        `comment` longtext NOT NULL,
        `is_sms_sent` enum('1', '0') NOT NULL DEFAULT '0',
        `is_email_sent` enum('1', '0') NOT NULL DEFAULT '0',
        `sms` varchar(255) NOT NULL,
        `email` longtext NOT NULL,
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($order_status_history, $this->dbconn);

        // payment_method table
        $payment_method = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "payment_method` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `payment_method` varchar(255) DEFAULT NULL,
        `code` varchar(25) DEFAULT NULL,
        `folder_name` varchar(20) DEFAULT NULL,
        `is_active` tinyint(1) NOT NULL DEFAULT '1',
        `is_deleted` enum('0', '1') NOT NULL,
        `is_sandbox` enum('0', '1') NOT NULL DEFAULT '0',
        `paypal_business` varchar(255) NOT NULL,
        `cheque_in_order_of` varchar(255) NOT NULL,
        `cheque_address` longtext NOT NULL,
        `description` varchar(255) DEFAULT NULL,
        `mode` binary(1) NOT NULL DEFAULT '0',
        `position` int(11) NOT NULL,
        `email_template_id` int(11) NOT NULL,
        `api_username` varchar(255) NOT NULL,
        `api_password` varchar(255) NOT NULL,
        `api_signature` varchar(255) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `code` (`code`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($payment_method, $this->dbconn);

        // product table
        $product = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `upc` varchar(255) NOT NULL,
        `sku` varchar(255) NOT NULL,
        `brand_id` int(11) DEFAULT NULL,
        `supplier_id` int(11) DEFAULT NULL,
        `manufacturer_id` int(11) DEFAULT NULL,
        `name` varchar(255) NOT NULL,
        `urlname` varchar(255) NOT NULL,
        `short_description` text NOT NULL,
        `long_description` longtext NOT NULL,
        `quantity` int(11) NOT NULL DEFAULT '0',
        `minimal_quantity` int(11) DEFAULT NULL,
        `maximum_quantity` int(11) DEFAULT NULL,
        `wholesale_price` float NOT NULL DEFAULT '0',
        `unit_price` float NOT NULL DEFAULT '0',
        `sale_price` float NOT NULL DEFAULT '0',
        `discount_type` enum('percentage', 'flat') NOT NULL DEFAULT 'percentage',
        `discount` float NOT NULL DEFAULT '0',
        `additional_shipping_price` float NOT NULL DEFAULT '0',
        `width` float DEFAULT NULL,
        `height` float DEFAULT NULL,
        `weight` float DEFAULT NULL,
        `depth` float DEFAULT NULL,
        `product_condition` varchar(255) NOT NULL DEFAULT 'new',
        `product_type` enum('simple', 'downloadable', 'virtual') NOT NULL DEFAULT 'simple',
        `download_file` varchar(255) DEFAULT NULL,
        `download_file_name` varchar(255) NOT NULL,
        `download_within_days` int(11) NOT NULL DEFAULT '-1',
        `maximum_downloads` int(11) NOT NULL DEFAULT '-1',
        `available_from` date NOT NULL DEFAULT '0000-00-00',
        `available_to` date NOT NULL DEFAULT '0000-00-00',
        `tax_rule_group_id` int(11) NOT NULL DEFAULT '0',
        `attribute_super_set_id` int(11) NOT NULL DEFAULT '0',
        `default_category` int(11) DEFAULT NULL,
        `is_active` tinyint(4) NOT NULL DEFAULT '0',
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        `is_accessory` enum('1', '0') NOT NULL DEFAULT '0',
        `is_featured` enum('1', '0') NOT NULL DEFAULT '0',
        `is_wrappable` enum('1', '0') NOT NULL DEFAULT '0',
        `gift_wrap_charges` float NOT NULL DEFAULT '1',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `meta_title` varchar(255) NOT NULL,
        `meta_desc` text NOT NULL,
        `meta_keyword` varchar(255) NOT NULL,
        `source` longtext NOT NULL,
        `special_handling` text NOT NULL,
        `packaging` text NOT NULL,
        `map` varchar(255) DEFAULT NULL,
        `uom` varchar(255) DEFAULT NULL,
        `product_option` varchar(255) DEFAULT NULL,
        `flavor` varchar(255) DEFAULT NULL,
        `scent` varchar(255) DEFAULT NULL,
        `discontinued` enum('1', '0') NOT NULL DEFAULT '0',
        `feed_last_update` datetime NOT NULL,
        `master_product_id` varchar(255) DEFAULT NULL,
        `image_last_update` datetime NOT NULL,
        `meta_robots_index` enum('global', 'index', 'noindex') NOT NULL DEFAULT 'global',
        `meta_robots_follow` enum('global', 'follow', 'nofollow') NOT NULL DEFAULT 'follow',
        `include_xml_sitemap` enum('global', 'include', 'exclude') NOT NULL DEFAULT 'global',
        `sitemap_priority` varchar(11) NOT NULL DEFAULT 'global',
        `canonical` longtext NOT NULL,
        `show_on_home` enum('0', '1') NOT NULL DEFAULT '1',
        `show_on_category` enum('0', '1') NOT NULL DEFAULT '1',
        `show_on_product_page` enum('0', '1') NOT NULL DEFAULT '1',
        `show_on_search` enum('0', '1') NOT NULL DEFAULT '1',
        `show_on_cart` enum('0', '1') NOT NULL DEFAULT '1',
        `latest_from` datetime NOT NULL,
        `latest_to` datetime NOT NULL,
        `sale_from` datetime NOT NULL,
        `sale_to` datetime NOT NULL,
        `updated_from_feed` enum('1', '0') NOT NULL DEFAULT '0',
        `promo_text` longtext NOT NULL,
        `out_of_stock_option` varchar(50) NOT NULL DEFAULT 'default',
        `scheduled_for` datetime NOT NULL,
        `is_bestseller` int(11) DEFAULT NULL,
        `user_group_id` varchar(20) NOT NULL,
        `disable_feed_price_update` int(11) NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        UNIQUE KEY `sku` (`sku`),
        KEY `date_add` (`date_add`),
        KEY `brand_id` (`brand_id`),
        KEY `supplier_id` (`supplier_id`),
        KEY `default_category` (`default_category`),
        KEY `manufacturer_id` (`manufacturer_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product, $this->dbconn);

        // product group 
        $groupss = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_group` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `description` longtext NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($groupss, $this->dbconn);


        // group rel
        $group_rel = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_group_rel` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `group_set_id` int(11) NOT NULL,
        `group_id` int(11) NOT NULL,
        `product_id` int(11) NOT NULL,
        `use_product_image` enum('0', '1') NOT NULL DEFAULT '1',
        `image` varchar(255) NOT NULL,
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `attribute_set_id` (`group_set_id`, `product_id`),
        UNIQUE KEY `group_id` (`group_id`, `product_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($group_rel, $this->dbconn);

        //group set
        $group_set = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_group_set` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `name` (`name`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($group_set, $this->dbconn);

        // product_image table
        $product_image = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_image` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_id` int(11) NOT NULL DEFAULT '0',
        `image` varchar(255) NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `is_main` enum('1', '0') NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `from_feed` enum('1', '0') NOT NULL DEFAULT '0',
        `feed_url` text NOT NULL,
        `feed_name` varchar(255) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `product_image` (`product_id`, `image`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_image, $this->dbconn);

        //product option
        $product_option = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_option` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_id` int(11) NOT NULL DEFAULT '0',
        `option_id` int(11) NOT NULL DEFAULT '0',
        `option_value` varchar(255) NOT NULL,
        `position` int(11) DEFAULT NULL,
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `product_option` (`product_id`, `option_id`),
        KEY `option_id` (`option_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_option, $this->dbconn);

        // product_quantity_price_rule table
        $product_quantity_price_rule = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_quantity_price_rule` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_id` int(11) NOT NULL DEFAULT '0',
        `qty_from` int(11) NOT NULL DEFAULT '0',
        `qty_to` int(11) NOT NULL DEFAULT '0',
        `unit_price` float NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `product_quantity` (`qty_from`, `qty_to`, `product_id`),
        KEY `product_id` (`product_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_quantity_price_rule, $this->dbconn);

        // product_relation table
        $product_relation = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_relation` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_id` int(11) NOT NULL DEFAULT '0',
        `relation_product_id` int(11) NOT NULL DEFAULT '0',
        `relation_type` enum('related', 'cross-sell', 'up-sell') NOT NULL DEFAULT 'related',
        `date_add` datetime DEFAULT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        UNIQUE KEY `product_relation` (`product_id`, `relation_product_id`, `relation_type`),
        KEY `relation_product_id` (`relation_product_id`),
        KEY `product_id` (`product_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_relation, $this->dbconn);

        // product_review table
        $product_review = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_review` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_id` int(11) NOT NULL DEFAULT '0',
        `user_id` int(11) NOT NULL DEFAULT '0',
        `name` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `review` longtext NOT NULL,
        `rate` int(11) NOT NULL DEFAULT '0',
        `ip_address` varchar(40) DEFAULT NULL,
        `is_approved` enum('1', '0') NOT NULL DEFAULT '0',
        `approved_date` datetime NOT NULL,
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `product_id` (`product_id`),
        KEY `user_id` (`user_id`),
        KEY `is_approved` (`is_approved`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_review, $this->dbconn);

        // product stock requests
        $product_stock_request = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_stock_requests` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_id` int(11) NOT NULL,
        `variant_id` int(11) NOT NULL,
        `user_id` int(11) NOT NULL,
        `email` varchar(255) NOT NULL,
        `is_email_sent` tinyint(4) NOT NULL DEFAULT '0',
        `ip_address` varchar(20) NOT NULL,
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `product_id` (`product_id`, `email`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_stock_request, $this->dbconn);

        // product_variant table
        $product_variant = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_variant` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_id` int(11) NOT NULL DEFAULT '0',
        `attr_super_set_id` int(11) NOT NULL DEFAULT '0',
        `impact_on_price` enum('0', '1') NOT NULL DEFAULT '0',
        `impact_type` enum('increase', 'reduce') NOT NULL DEFAULT 'increase',
        `impact_price` int(11) NOT NULL DEFAULT '0',
        `wholesale_price` float NOT NULL DEFAULT '0',
        `unit_price` float NOT NULL DEFAULT '0',
        `sale_price` float NOT NULL DEFAULT '0',
        `discount_type` enum('percentage', 'flat') NOT NULL DEFAULT 'percentage',
        `discount` float NOT NULL DEFAULT '0',
        `quantity` int(11) NOT NULL DEFAULT '0',
        `minimal_quantity` int(11) NOT NULL DEFAULT '1',
        `weight` float DEFAULT NULL,
        `available_from` date NOT NULL DEFAULT '0000-00-00',
        `available_to` date NOT NULL DEFAULT '0000-00-00',
        `is_default` enum('1', '0') NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `product_variant_default` (`product_id`, `is_default`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_variant, $this->dbconn);

        // product_variant_detail table
        $product_variant_detail = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_variant_detail` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_variant_id` int(11) NOT NULL DEFAULT '0',
        `attribute_id` int(11) NOT NULL DEFAULT '0',
        `attribute_set_id` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `product_variant` (`product_variant_id`, `attribute_id`),
        KEY `attribute_id` (`attribute_id`),
        KEY `product_variant_id` (`product_variant_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_variant_detail, $this->dbconn);

        // product_variant_image table
        $product_variant_image = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_variant_image` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `product_variant_id` int(11) NOT NULL DEFAULT '0',
        `image_id` int(11) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        UNIQUE KEY `product_variant_image` (`product_variant_id`, `image_id`),
        KEY `image_id` (`image_id`),
        KEY `product_variant_id` (`product_variant_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_variant_image, $this->dbconn);

        // product_views table
        $product_views = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "product_views` (
        `id` int(255) NOT NULL AUTO_INCREMENT,
        `product_id` int(255) NOT NULL,
        `session_id` varchar(255) NOT NULL,
        `ip_address` varchar(255) NOT NULL,
        `user_id` int(255) NOT NULL DEFAULT '0',
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `unique_product` (`product_id`, `session_id`),
        KEY `user_id` (`user_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($product_views, $this->dbconn);


        // search table
        $search = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "search` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `keyword` varchar(255) NOT NULL,
        `session_id` varchar(255) NOT NULL,
        `ip_address` varchar(20) NOT NULL,
        `results` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `keyword` (`keyword`, `session_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($search, $this->dbconn);

        //search index
        $search_index = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "search_index` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `table_name` varchar(200) NOT NULL,
        `page_name` text NOT NULL,
        `keyword` text NOT NULL,
        `description` longblob,
        `item_id` int(11) NOT NULL,
        `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($search_index, $this->dbconn);

        // setting table
        $setting = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "setting` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `key` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
        `value` longtext CHARACTER SET latin1 NOT NULL,
        `name` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
        `title` varchar(255) DEFAULT NULL,
        `type` varchar(20) DEFAULT 'select',
        `options` longtext NOT NULL,
        `hint` longtext NOT NULL,
        `position` int(11) NOT NULL,
        `status` tinyint(1) NOT NULL DEFAULT '1',
        PRIMARY KEY (`id`),
        UNIQUE KEY `key` (`key`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($setting, $this->dbconn);


        // shipping table
        $shipping = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "shipping` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `code` varchar(50) NOT NULL,
        `carrier` varchar(255) NOT NULL,
        `desc` longtext NOT NULL,
        `logo` varchar(255) NOT NULL,
        `tracking_url` varchar(255) NOT NULL,
        `is_active` enum('0', '1') NOT NULL DEFAULT '0',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_default` enum('0', '1') NOT NULL DEFAULT '0',
        `sequence` int(11) NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `handling_charges` float NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($shipping, $this->dbconn);


        // shipping_flat table
        $shipping_flat = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "shipping_flat` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `shipping_id` int(11) NOT NULL,
        `zone_id` int(11) NOT NULL,
        `is_free` enum('0', '1') NOT NULL DEFAULT '0',
        `flat_type` enum('order', 'product') NOT NULL DEFAULT 'order',
        `price` float NOT NULL,
        `tax_rule_id` int(11) NOT NULL,
        `is_active` enum('0', '1') NOT NULL DEFAULT '0',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `name` varchar(255) NOT NULL,
        `delivery_time` varchar(255) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `zone_id` (`zone_id`),
        KEY `shipping_id` (`shipping_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($shipping_flat, $this->dbconn);



        $shipping_group = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "shipping_group` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `shipping_method` int(11) NOT NULL,
        `name` varchar(255) NOT NULL,
        `delivery_time` varchar(255) NOT NULL,
        `description` text NOT NULL,
        `position` int(11) NOT NULL,
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `min_delivery_days` int(3) NOT NULL DEFAULT '1',
        `max_delivery_days` int(3) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        KEY `shipping_method` (`shipping_method`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";

        mysql_query($shipping_group, $this->dbconn);

        // shipping_price table
        $shipping_price = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "shipping_price` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `shipping_id` int(11) NOT NULL,
        `zone_id` int(11) NOT NULL,
        `is_free` enum('0', '1') NOT NULL DEFAULT '0',
        `price` float NOT NULL,
        `tax_rule_id` int(11) NOT NULL,
        `is_active` enum('0', '1') NOT NULL DEFAULT '0',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `min` float NOT NULL,
        `max` float NOT NULL,
        `shipping_group` int(11) NOT NULL,
        PRIMARY KEY (`id`),
        KEY `shipping_id` (`shipping_id`),
        KEY `shipping_group` (`shipping_group`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($shipping_price, $this->dbconn);


        // shipping_price_range table
        $shipping_price_range = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "shipping_price_range` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `price_rule_id` int(11) NOT NULL,
        `min` float NOT NULL,
        `max` float NOT NULL,
        `price` float NOT NULL,
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        KEY `price_rule_id` (`price_rule_id`),
        KEY `price_rule_id_2` (`price_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($shipping_price_range, $this->dbconn);


        // shipping_weight table
        $shipping_weight = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "shipping_weight` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `shipping_id` int(11) NOT NULL,
        `zone_id` int(11) NOT NULL,
        `is_free` enum('0', '1') NOT NULL DEFAULT '0',
        `price` float NOT NULL,
        `tax_rule_id` int(11) NOT NULL,
        `is_active` enum('0', '1') NOT NULL DEFAULT '0',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `min` float NOT NULL,
        `max` float NOT NULL,
        `shipping_group` int(11) NOT NULL,
        PRIMARY KEY (`id`),
        KEY `shipping_id` (`shipping_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($shipping_weight, $this->dbconn);


        // shipping_weight_range table
        $shipping_weight_range = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "shipping_weight_range` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `weight_rule_id` int(11) NOT NULL,
        `min` float NOT NULL,
        `max` float NOT NULL,
        `price` float NOT NULL,
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        KEY `weight_rule_id` (`weight_rule_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($shipping_weight_range, $this->dbconn);


        // slide table
        $slide = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "slide` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `slideshow_id` bigint(20) NOT NULL,
        `title` varchar(255) NOT NULL,
        `short_description` text NOT NULL,
        `image` varchar(2055) NOT NULL,
        `position` int(255) DEFAULT '0',
        `is_active` tinyint(1) NOT NULL DEFAULT '1',
        `link` varchar(255) NOT NULL,
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        `subheading` varchar(255) DEFAULT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($slide, $this->dbconn);

        // Slider Table
        $slider = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "slider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `auto_scroll` int(11) NOT NULL DEFAULT '0',
  `slide_delay` int(11) NOT NULL,
  `slide_height` int(11) NOT NULL DEFAULT '0',
  `height_suffix` varchar(255) NOT NULL,
  `slide_width` int(11) NOT NULL DEFAULT '0',
  `width_suffix` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($slider, $this->dbconn);

        // Slides Table
        $slides = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "slides` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `slider_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `position` int(255) DEFAULT '0',
  `mobile_image` varchar(255) NOT NULL,
  `open_in_new_window` int(11) NOT NULL DEFAULT '0',
  `slide_text` text NOT NULL,
  `slide_alt_text` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($slides, $this->dbconn);

        mysql_query($slider, $this->dbconn);


        // state table
        $state = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "state` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `country_id` int(11) NOT NULL DEFAULT '0',
        `name` varchar(255) NOT NULL,
        `iso_code` varchar(255) NOT NULL,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `country_id` (`country_id`),
        KEY `state_name` (`name`),
        KEY `iso_code` (`iso_code`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($state, $this->dbconn);


        // supplier table
        $supplier = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "supplier` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `urlname` varchar(255) NOT NULL,
        `image` varchar(255) NOT NULL,
        `description` longtext NOT NULL,
        `phone` varchar(255) NOT NULL,
        `address` varchar(255) NOT NULL,
        `website` varchar(255) NOT NULL,
        `position` int(11) NOT NULL DEFAULT '0',
        `is_active` tinyint(4) NOT NULL DEFAULT '1',
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `meta_title` varchar(255) NOT NULL,
        `meta_desc` text NOT NULL,
        `meta_keyword` varchar(255) NOT NULL,
        `image_alt_text` VARCHAR( 255 ) NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($supplier, $this->dbconn);

        //synonums
        $synonyms = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "synonym` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `keywords` longtext NOT NULL,
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($synonyms, $this->dbconn);

        // tax table
        $tax = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "tax` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `rate` float NOT NULL,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($tax, $this->dbconn);


        // tax_rule table
        $tax_rule = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "tax_rule` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `tax_id` int(11) NOT NULL DEFAULT '0',
        `tax_rule_group_id` int(11) NOT NULL DEFAULT '0',
        `zone_id` int(11) DEFAULT '0',
        `description` longtext NOT NULL,
        `apply_on_shipping` enum('1', '0') NOT NULL DEFAULT '0',
        `apply_on_gifts` enum('1', '0') NOT NULL DEFAULT '0',
        `priority` int(11) NOT NULL,
        `behaviour` enum('tax_only', 'one_another', 'combine') NOT NULL DEFAULT 'tax_only',
        `position` int(11) NOT NULL DEFAULT '0',
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `zone_tax_rule_group` (`tax_rule_group_id`, `zone_id`, `tax_id`),
        KEY `tax_id` (`tax_id`),
        KEY `zone_id` (`zone_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($tax_rule, $this->dbconn);

        // tax_rule_group table
        $tax_rule_group = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "tax_rule_group` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `name` (`name`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($tax_rule_group, $this->dbconn);

        //url rewrite 
        $url_rewrite = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "url_rewrite` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `page` varchar(255) NOT NULL,
        `module` varchar(255) NOT NULL,
        `module_id` int(11) NOT NULL,
        `urlname` varchar(255) NOT NULL,
        `date_add` datetime NOT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `module` (`module`, `module_id`),
        UNIQUE KEY `urlname` (`urlname`)
        ) ENGINE = InnoD DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($url_rewrite, $this->dbconn);

        // user table
        $user = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "user` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `username` varchar(255) NOT NULL,
        `password` varchar(255) NOT NULL,
        `ip_address` varchar(40) DEFAULT NULL,
        `last_visit` datetime DEFAULT NULL,
        `total_visit` int(11) NOT NULL DEFAULT '0',
        `last_visit_ip_address` varchar(40) DEFAULT NULL,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `is_email_verified` enum('1', '0') NOT NULL DEFAULT '0',
        `companyname` varchar(255) NOT NULL,
        `title` varchar(255) NOT NULL,
        `firstname` varchar(255) NOT NULL,
        `lastname` varchar(255) NOT NULL,
        `age` int(11) DEFAULT NULL,
        `gender` enum('male', 'female') DEFAULT NULL,
        `address1` varchar(255) NOT NULL,
        `address2` varchar(255) NOT NULL,
        `phone` varchar(255) NOT NULL,
        `fax` varchar(255) NOT NULL,
        `city_id` int(11) DEFAULT NULL,
        `zip_code` varchar(255) NOT NULL,
        `state_id` int(11) DEFAULT NULL,
        `country_id` int(11) DEFAULT NULL,
        `is_newsletter` enum('1', '0') NOT NULL DEFAULT '0',
        `newsletter_add_date` datetime DEFAULT NULL,
        `forgot_password_token` varchar(255) NOT NULL,
        `email_confirmation_token` varchar(255) DEFAULT NULL,
        `birthdate` date DEFAULT NULL,
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `username` (`username`),
        UNIQUE KEY `user_login` (`username`, `password`),
        KEY `city_id` (`city_id`),
        KEY `state_id` (`state_id`),
        KEY `country_id` (`country_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($user, $this->dbconn);

        // user_address table
        $user_address = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "user_address` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL DEFAULT '0',
        `cart_id` int(11) NOT NULL DEFAULT '0',
        `address_name` varchar(255) NOT NULL,
        `address_type` enum('invoice', 'shiping') NOT NULL DEFAULT 'invoice',
        `is_default` enum('0', '1') NOT NULL DEFAULT '0',
        `firstname` varchar(255) NOT NULL,
        `lastname` varchar(255) NOT NULL,
        `address1` varchar(255) NOT NULL,
        `address2` varchar(255) NOT NULL,
        `city_id` int(11) DEFAULT NULL,
        `city` varchar(255) NOT NULL,
        `state_id` int(11) DEFAULT NULL,
        `country_id` int(11) DEFAULT NULL,
        `phone` varchar(255) NOT NULL,
        `mobile` varchar(255) NOT NULL,
        `zip_code` varchar(255) NOT NULL,
        `fax` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `is_deleted` enum('1', '0') NOT NULL DEFAULT '0',
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `city_id` (`city_id`),
        KEY `state_id` (`state_id`),
        KEY `country_id` (`country_id`),
        KEY `user_id` (`user_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($user_address, $this->dbconn);

        // user credit balance table
        $user_cerdit_balance_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "user_credit_balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credits` double NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($user_cerdit_balance_sql, $this->dbconn);

        // user credit deduction table
        $user_credit_deduction_sql = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "user_credit_deduction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `credits` double NOT NULL,
  `date_of_order` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($user_credit_deduction_sql, $this->dbconn);

        // user_credit_history table
        $user_credit_history = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "user_credit_history` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL DEFAULT '0',
        `order_id` int(11) NOT NULL DEFAULT '0',
        `order_return_id` int(11) NOT NULL DEFAULT '0',
        `amount` float NOT NULL,
        `amount_type` varchar(255) NOT NULL,
        `comment` longtext NOT NULL,
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        KEY `user_id` (`user_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($user_credit_history, $this->dbconn);

        // user credit transaction table
        $user_credit_transaction = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "user_credit_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `date_of_transaction` datetime NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($user_credit_transaction, $this->dbconn);

        // User Group Table
        $user_group = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `modules` longtext NOT NULL,
  `is_hidden` enum('0','1') NOT NULL DEFAULT '0',
  `is_default` enum('0','1') NOT NULL DEFAULT '0',
  `is_permanent` enum('0','1') NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=$this->default_charset AUTO_INCREMENT=1 ;";
        mysql_query($user_group, $this->dbconn);

        // user_wishlist table
        $user_wishlist = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "user_wishlist` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) DEFAULT NULL,
        `session_id` varchar(255) NOT NULL,
        `product_id` int(11) DEFAULT NULL,
        `product_variant_id` int(11) DEFAULT NULL,
        `quantity` int(11) NOT NULL,
        `ip_address` varchar(40) DEFAULT NULL,
        `date_add` datetime DEFAULT NULL,
        `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `session_id` (`session_id`, `product_id`),
        UNIQUE KEY `user_wishlist` (`user_id`, `product_id`),
        KEY `product_id` (`product_id`),
        KEY `product_variant_id` (`product_variant_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($user_wishlist, $this->dbconn);

        // web_stat table
        $web_stat = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "web_stat` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `ip_address` varchar(40) DEFAULT NULL,
        `on_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($web_stat, $this->dbconn);

        // zone table
        $zone = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "zone` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `is_active` enum('1', '0') NOT NULL DEFAULT '1',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `name` (`name`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($zone, $this->dbconn);

        // zone_city table
        $zone_city = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "zone_city` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `zone_state_id` int(11) NOT NULL DEFAULT '0',
        `city_id` int(11) NOT NULL DEFAULT '0',
        `zone_id` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `zone_state_city` (`zone_id`, `zone_state_id`, `city_id`),
        KEY `city_id` (`city_id`),
        KEY `zone_state_id` (`zone_state_id`),
        KEY `zone_id` (`zone_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($zone_city, $this->dbconn);

        // zone_country table
        $zone_country = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "zone_country` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `country_id` int(11) NOT NULL DEFAULT '0',
        `zone_id` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `zone_country` (`country_id`, `zone_id`),
        KEY `zone_id` (`zone_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($zone_country, $this->dbconn);

        // zone_detail table
        $zone_detail = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "zone_detail` (
        `id` int(255) NOT NULL AUTO_INCREMENT,
        `zone_id` int(255) NOT NULL DEFAULT '0',
        `country_id` int(255) NOT NULL DEFAULT '-1',
        `state_id` float DEFAULT '-1',
        `city_id` float DEFAULT '-1',
        `zipcode` longtext NOT NULL,
        `is_active` enum('0', '1') NOT NULL DEFAULT '0',
        `is_deleted` enum('0', '1') NOT NULL DEFAULT '0',
        `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `zone_detail` (`zone_id`, `country_id`, `state_id`, `city_id`),
        KEY `city_id` (`city_id`),
        KEY `state_id` (`state_id`),
        KEY `country_id` (`country_id`),
        KEY `zone_id` (`zone_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($zone_detail, $this->dbconn);

        // zone_state table
        $zone_state = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "zone_state` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `zone_country_id` int(11) NOT NULL DEFAULT '0',
        `zone_id` int(11) NOT NULL DEFAULT '0',
        `state_id` int(11) NOT NULL DEFAULT '0',
        `date_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `zone_country_state` (`zone_id`, `zone_country_id`, `state_id`),
        KEY `zone_country_id` (`zone_country_id`),
        KEY `state_id` (`state_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($zone_state, $this->dbconn);

        // zone_zip table
        $zone_zip = "CREATE TABLE IF NOT EXISTS `" . $this->prefix . "zone_zip` (
        `id` int(11) NOT NULL,
        `zone_id` int(11) NOT NULL DEFAULT '0',
        `zone_city_id` int(11) NOT NULL DEFAULT '0',
        `zipcode_from` varchar(50) DEFAULT NULL,
        `zipcode_to` varchar(50) DEFAULT NULL,
        KEY `zone_city_id` (`zone_city_id`),
        KEY `zone_id` (`zone_id`)
        ) ENGINE = InnoDB DEFAULT CHARSET = $this->default_charset;
        ";
        mysql_query($zone_zip, $this->dbconn);
    }

    function createTableConstrain($name) {
        mysql_select_db($name, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "navigation`
        ADD CONSTRAINT `" . $this->prefix . "navigations_fk_1` FOREIGN KEY (`nav_id`) REFERENCES `" . $this->prefix . "navigations` (`id`) ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "attribute_set`
        ADD CONSTRAINT `" . $this->prefix . "attribute_set_ibfk_1` FOREIGN KEY (`attr_super_set_id`) REFERENCES `" . $this->prefix . "attribute_super_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "banner_location`
        ADD CONSTRAINT `" . $this->prefix . "banner_location_ibfk_1` FOREIGN KEY (`banner_id`) REFERENCES `" . $this->prefix . "banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_detail`
        ADD CONSTRAINT `" . $this->prefix . "cart_detail_ibfk_1` FOREIGN KEY (`cart_id`) REFERENCES `" . $this->prefix . "cart` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_ibfk_1` FOREIGN KEY (`cart_rule_type`) REFERENCES `" . $this->prefix . "cart_rule_type` (`id`) ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_attribute`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_attribute_ibfk_1` FOREIGN KEY (`cart_rule_id`) REFERENCES `" . $this->prefix . "cart_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_attribute_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `" . $this->prefix . "attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_brand`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_brand_ibfk_1` FOREIGN KEY (`cart_rule_id`) REFERENCES `" . $this->prefix . "cart_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_brand_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `" . $this->prefix . "brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        /* $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_category`
          ADD CONSTRAINT `" . $this->prefix . "cart_rule_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `" . $this->prefix . "category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
          ADD CONSTRAINT `" . $this->prefix . "cart_rule_category_ibfk_3` FOREIGN KEY (`cart_rule_id`) REFERENCES `" . $this->prefix . "cart_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
          ";
          mysql_query($constrain, $this->dbconn); */

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_country`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_country_ibfk_1` FOREIGN KEY (`cart_rule_id`) REFERENCES `" . $this->prefix . "country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_product`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_product_ibfk_2` FOREIGN KEY (`cart_rule_id`) REFERENCES `" . $this->prefix . "cart_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_product_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_supplier`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_supplier_ibfk_1` FOREIGN KEY (`cart_rule_id`) REFERENCES `" . $this->prefix . "cart_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_supplier_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `" . $this->prefix . "supplier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_type_selection`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_type_selection_ibfk_1` FOREIGN KEY (`cart_rule_type_id`) REFERENCES `" . $this->prefix . "cart_rule_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_user`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_user_ibfk_1` FOREIGN KEY (`cart_rule_id`) REFERENCES `" . $this->prefix . "cart_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `" . $this->prefix . "user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "cart_rule_zone`
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_zone_ibfk_1` FOREIGN KEY (`cart_rule_id`) REFERENCES `" . $this->prefix . "cart_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "cart_rule_zone_ibfk_2` FOREIGN KEY (`zone_id`) REFERENCES `" . $this->prefix . "zone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "category_product`
        ADD CONSTRAINT `" . $this->prefix . "category_product_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "category_product_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `" . $this->prefix . "category` (`id`);
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "city`
        ADD CONSTRAINT `" . $this->prefix . "city_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `" . $this->prefix . "country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "city_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `" . $this->prefix . "state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "idea`
  ADD CONSTRAINT `" . $this->prefix . "idea_fk_1` FOREIGN KEY (`category_id`) REFERENCES `" . $this->prefix . "idea_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "idea_photo`
  ADD CONSTRAINT `" . $this->prefix . "idea_photo_fk_1` FOREIGN KEY (`idea_id`) REFERENCES `" . $this->prefix . "idea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "order_detail`
        ADD CONSTRAINT `" . $this->prefix . "order_detail_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `" . $this->prefix . "order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "order_packet`
        ADD CONSTRAINT `" . $this->prefix . "order_packet_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `" . $this->prefix . "order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "order_packet_detail`
        ADD CONSTRAINT `" . $this->prefix . "order_packet_detail_ibfk_1` FOREIGN KEY (`order_packet_id`) REFERENCES `" . $this->prefix . "order_packet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "order_packet_status_history`
        ADD CONSTRAINT `" . $this->prefix . "order_packet_status_history_ibfk_1` FOREIGN KEY (`order_packet_id`) REFERENCES `" . $this->prefix . "order_packet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "order_return`
        ADD CONSTRAINT `" . $this->prefix . "order_return_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `" . $this->prefix . "order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "order_return_detail`
        ADD CONSTRAINT `" . $this->prefix . "order_return_detail_ibfk_1` FOREIGN KEY (`order_return_id`) REFERENCES `" . $this->prefix . "order_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product`
        ADD CONSTRAINT `" . $this->prefix . "product_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `" . $this->prefix . "brand` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "product_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `" . $this->prefix . "supplier` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "product_ibfk_3` FOREIGN KEY (`manufacturer_id`) REFERENCES `" . $this->prefix . "manufacturer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_group_rel`
        ADD CONSTRAINT `" . $this->prefix . "product_group_rel_ibfk_4` FOREIGN KEY (`group_set_id`) REFERENCES `" . $this->prefix . "option` (`id`) ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "product_group_rel_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `" . $this->prefix . "product_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_image`
        ADD CONSTRAINT `" . $this->prefix . "product_image_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_option`
        ADD CONSTRAINT `" . $this->prefix . "product_option_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "product_option_ibfk_2` FOREIGN KEY (`option_id`) REFERENCES `" . $this->prefix . "option` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_quantity_price_rule`
        ADD CONSTRAINT `" . $this->prefix . "product_quantity_price_rule_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_relation`
        ADD CONSTRAINT `" . $this->prefix . "product_relation_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "product_relation_ibfk_2` FOREIGN KEY (`relation_product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_review`
        ADD CONSTRAINT `" . $this->prefix . "product_review_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_variant`
        ADD CONSTRAINT `" . $this->prefix . "product_variant_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `" . $this->prefix . "product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_variant_detail`
        ADD CONSTRAINT `" . $this->prefix . "product_variant_detail_ibfk_1` FOREIGN KEY (`product_variant_id`) REFERENCES `" . $this->prefix . "product_variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "product_variant_detail_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `" . $this->prefix . "attribute` (`id`);
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "product_variant_image`
        ADD CONSTRAINT `" . $this->prefix . "product_variant_image_ibfk_1` FOREIGN KEY (`product_variant_id`) REFERENCES `" . $this->prefix . "product_variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "product_variant_image_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `" . $this->prefix . "product_image` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "shipping_flat`
        ADD CONSTRAINT `" . $this->prefix . "shipping_flat_ibfk_1` FOREIGN KEY (`shipping_id`) REFERENCES `" . $this->prefix . "shipping` (`id`) ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "shipping_flat_ibfk_2` FOREIGN KEY (`zone_id`) REFERENCES `" . $this->prefix . "zone` (`id`) ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "shipping_price`
        ADD CONSTRAINT `" . $this->prefix . "shipping_price_ibfk_1` FOREIGN KEY (`shipping_group`) REFERENCES `" . $this->prefix . "shipping_group` (`id`) ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);


        $constrain = "ALTER TABLE `" . $this->prefix . "shipping_weight`
        ADD CONSTRAINT `" . $this->prefix . "shipping_weight_ibfk_1` FOREIGN KEY (`shipping_group`) REFERENCES `" . $this->prefix . "shipping_group` (`id`) ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "shipping_price_range`
        ADD CONSTRAINT `" . $this->prefix . "shipping_price_range_ibfk_1` FOREIGN KEY (`price_rule_id`) REFERENCES `" . $this->prefix . "shipping_price` (`id`) ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "shipping_weight_range`
        ADD CONSTRAINT `" . $this->prefix . "shipping_weight_range_ibfk_1` FOREIGN KEY (`weight_rule_id`) REFERENCES `" . $this->prefix . "shipping_weight` (`id`) ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "state`
        ADD CONSTRAINT `" . $this->prefix . "state_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `" . $this->prefix . "country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "tax_rule`
        ADD CONSTRAINT `" . $this->prefix . "tax_rule_ibfk_1` FOREIGN KEY (`tax_id`) REFERENCES `" . $this->prefix . "tax` (`id`),
        ADD CONSTRAINT `" . $this->prefix . "tax_rule_ibfk_2` FOREIGN KEY (`tax_rule_group_id`) REFERENCES `" . $this->prefix . "tax_rule_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "tax_rule_ibfk_3` FOREIGN KEY (`zone_id`) REFERENCES `" . $this->prefix . "zone` (`id`);
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "user`
        ADD CONSTRAINT `" . $this->prefix . "user_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `" . $this->prefix . "city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "user_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `" . $this->prefix . "state` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "user_ibfk_3` FOREIGN KEY (`country_id`) REFERENCES `" . $this->prefix . "country` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "user_address`
        ADD CONSTRAINT `" . $this->prefix . "user_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `" . $this->prefix . "user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "user_address_ibfk_4` FOREIGN KEY (`city_id`) REFERENCES `" . $this->prefix . "city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "user_address_ibfk_5` FOREIGN KEY (`state_id`) REFERENCES `" . $this->prefix . "state` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "user_address_ibfk_6` FOREIGN KEY (`country_id`) REFERENCES `" . $this->prefix . "country` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "user_credit_history`
        ADD CONSTRAINT `" . $this->prefix . "user_credit_history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `" . $this->prefix . "user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "zone_city`
        ADD CONSTRAINT `" . $this->prefix . "zone_city_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `" . $this->prefix . "city` (`id`),
        ADD CONSTRAINT `" . $this->prefix . "zone_city_ibfk_2` FOREIGN KEY (`zone_state_id`) REFERENCES `" . $this->prefix . "zone_state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "zone_city_ibfk_3` FOREIGN KEY (`zone_id`) REFERENCES `" . $this->prefix . "zone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "zone_country`
        ADD CONSTRAINT `" . $this->prefix . "zone_country_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `" . $this->prefix . "country` (`id`),
        ADD CONSTRAINT `" . $this->prefix . "zone_country_ibfk_2` FOREIGN KEY (`zone_id`) REFERENCES `" . $this->prefix . "zone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "zone_detail`
        ADD CONSTRAINT `" . $this->prefix . "zone_detail_ibfk_4` FOREIGN KEY (`zone_id`) REFERENCES `" . $this->prefix . "zone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "zone_detail_ibfk_5` FOREIGN KEY (`country_id`) REFERENCES `" . $this->prefix . "country` (`id`) ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "zone_state`
        ADD CONSTRAINT `" . $this->prefix . "zone_state_ibfk_1` FOREIGN KEY (`zone_country_id`) REFERENCES `" . $this->prefix . "zone_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "zone_state_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `" . $this->prefix . "state` (`id`),
        ADD CONSTRAINT `" . $this->prefix . "zone_state_ibfk_3` FOREIGN KEY (`zone_id`) REFERENCES `" . $this->prefix . "zone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "zone_zip`
        ADD CONSTRAINT `" . $this->prefix . "zone_zip_ibfk_1` FOREIGN KEY (`zone_city_id`) REFERENCES `" . $this->prefix . "zone_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD CONSTRAINT `" . $this->prefix . "zone_zip_ibfk_2` FOREIGN KEY (`zone_id`) REFERENCES `" . $this->prefix . "zone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "admin_user`
  ADD CONSTRAINT `" . $this->prefix . "admin_user_ibfk_1` FOREIGN KEY (`admin_role_id`) REFERENCES `" . $this->prefix . "admin_user_role` (`id`) ON UPDATE CASCADE;";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "admin_modules_actions`
  ADD CONSTRAINT " . $this->prefix . "admin_modules_actions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `" . $this->prefix . "admin_modules` (`id`);
";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "admin_permission_level_grouping`
  ADD CONSTRAINT `" . $this->prefix . "admin_permission_level_grouping_ibfk_1` FOREIGN KEY (`permission_level_id`) REFERENCES `" . $this->prefix . "admin_user_permission_level` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `" . $this->prefix . "admin_permission_level_grouping_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `" . $this->prefix . "admin_modules` (`id`) ON UPDATE CASCADE;
";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "admin_user_role_permissions`

 ADD CONSTRAINT `" . $this->prefix . "admin_user_role_permissions_ibfk_1` FOREIGN KEY (`role_id`) 

REFERENCES `" . $this->prefix . "admin_user_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,

 ADD CONSTRAINT `" . $this->prefix . "admin_user_role_permissions_ibfk_2` FOREIGN KEY 

(`permission_level_group_id`) REFERENCES `" . $this->prefix . "admin_permission_level_grouping` (`id`) ON 

DELETE CASCADE ON UPDATE CASCADE;";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "attribute`
        ADD CONSTRAINT `" . $this->prefix . "attribute_ibfk_1` FOREIGN KEY (`attr_set_id`) REFERENCES `" . $this->prefix . "attribute_set` (`id`);
        ";
        mysql_query($constrain, $this->dbconn);

        $constrain = "ALTER TABLE `" . $this->prefix . "admin_user_activity_log`
  ADD CONSTRAINT `" . $this->prefix . "admin_user_activity_log_ibfk_1` FOREIGN KEY (`admin_user_id`) REFERENCES `" . $this->prefix . "admin_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
";
        mysql_query($constrain, $this->dbconn);
    }

    function addSampleData($name, $store_email = false, $store_name = false, $default_pages, $admin_user = false, $admin_password = false, $store_support_email = false, $store_business_name = false, $store_domain_name = false, $store_legal_name = false, $store_address1 = false, $store_address2 = false, $store_city = false, $store_state = false, $store_country = false, $store_zip = false, $store_phone = false) {

        $dt = date("Y-m-d H:i:s");
        mysql_select_db($name, $this->dbconn);

        if (!$store_email || $store_email == ''):
            $store_email = 'rocky.developer004@gmail.com';
        endif;

        if (!$store_name || $store_name == ''):
            $store_name = 'eCommerce';
        else:
            $store_name = str_replace(array("'", '"'), '', $store_name);
        endif;


        if (!$admin_user || $admin_user == ''):
            $admin_user = 'admin';
        endif;

        if (!$admin_password || $admin_password == ''):
            $admin_password = '21232f297a57a5a743894a0e4a801fc3';
        endif;




        $sample = "INSERT INTO `" . $this->prefix . "admin_user_permission_level` (`id`, `name`) VALUES
(1, 'No Access'),
(2, 'Read Only Access'),
(3, 'Write Access'),
(4, 'Full Access');";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "admin_user_role` (`id`, `name`, `date_upd`) VALUES
(1, 'Super User', '2015-02-11 09:18:23');";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "admin_modules_actions` (`id`, `module_id`, `action`, `label`) VALUES
(1, 1, 'list', 'List'),
(2, 1, 'delete', 'Delete'),
(3, 1, 'create', 'Create'),
(4, 1, 'download', 'Download'),
(5, 1, 'permanent_delete', 'Permanent Delete'),
(6, 1, 'restore', 'Restore'),
(7, 1, 'thrash', 'Trash'),
(8, 2, 'list', 'List'),
(9, 2, 'update', 'Update'),
(10, 2, 'update2', 'Multiopt Option'),
(11, 2, 'insert', 'Insert'),
(12, 2, 'delete', 'Delete'),
(13, 2, 'delete_image', 'Delete Banner Image'),
(14, 2, 'location', 'Add Banner Location'),
(15, 2, 'delete_banner_location', 'Delete Banner Location'),
(16, 2, 'update_location', 'Update Banner Location'),
(17, 3, 'list', 'List'),
(18, 3, 'insert', 'Insert'),
(19, 3, 'update', 'Update'),
(20, 3, 'update2', 'Multiopt Option'),
(21, 3, 'delete', 'Delete'),
(22, 3, 'delete_image', 'Delete Image'),
(23, 4, 'list', 'List'),
(24, 4, 'update', 'Update'),
(25, 4, 'update2', 'Multiopt Option'),
(26, 4, 'delete', 'Delete'),
(27, 5, 'list', 'List'),
(28, 5, 'update', 'Update'),
(29, 5, 'update2', 'Multiopt Option'),
(30, 5, 'update_multiple_thrash', 'Multiopt for Trash'),
(31, 5, 'insert', 'Insert'),
(32, 5, 'delete', 'Delete'),
(33, 5, 'permanent_delete', 'Permanent Delete'),
(34, 5, 'restore', 'Restore'),
(35, 5, 'thrash', 'Trash'),
(36, 6, 'list', 'List'),
(37, 6, 'update', 'Update'),
(38, 6, 'update2', 'Multiopt Option'),
(39, 6, 'insert', 'Insert'),
(40, 6, 'delete', 'Delete'),
(41, 6, 'permanent_delete', 'Permanent Delete'),
(42, 6, 'restore', 'Restore'),
(43, 6, 'thrash', 'Trash'),
(44, 7, 'list', 'List'),
(45, 7, 'update', 'Update'),
(46, 8, 'list', 'List'),
(47, 8, 'update', 'Update'),
(48, 8, 'upload', 'Upload'),
(49, 9, 'list', 'List'),
(50, 9, 'insert', 'Insert'),
(51, 9, 'update', 'Update'),
(52, 9, 'delete', 'Delete'),
(53, 10, 'list', 'List'),
(54, 10, 'insert', 'Insert'),
(55, 10, 'update', 'Update'),
(56, 10, 'update2', 'Update Position'),
(57, 10, 'delete', 'Delete'),
(58, 10, 'thrash', 'Trash'),
(59, 10, 'permanent_delete', 'Permanent Delete'),
(60, 10, 'restore', 'Restore'),
(61, 10, 'update3', 'Multiopt Option'),
(62, 11, 'list', 'List'),
(63, 11, 'update', 'Update'),
(64, 11, 'update2', 'Update Position'),
(65, 12, 'type', 'Type'),
(66, 12, 'select', 'Select'),
(67, 12, 'list', 'List'),
(68, 12, 'insert', 'Insert'),
(69, 12, 'update', 'Update'),
(70, 12, 'update_conditions', 'Update Conditions'),
(71, 12, 'update_actions', 'Update Actions'),
(72, 12, 'update2', 'Multiopt Option'),
(73, 12, 'delete', 'Delete'),
(74, 13, 'list', 'List'),
(75, 13, 'update', 'Update'),
(76, 13, 'robots_edit', 'Robots Edit'),
(77, 13, 'change_password', 'Change Password'),
(78, 14, 'list', 'List'),
(79, 14, 'update', 'Update'),
(80, 15, 'list', 'List'),
(81, 15, 'delete', 'Delete'),
(82, 16, 'list', 'List'),
(83, 16, 'update', 'Update'),
(84, 16, 'update2', 'Multiopt Option'),
(85, 16, 'insert', 'Insert'),
(86, 16, 'delete', 'Delete'),
(87, 17, 'list', 'List'),
(88, 17, 'update2', 'Multiopt Option'),
(89, 18, 'list', 'List'),
(90, 18, 'update', 'Update'),
(91, 18, 'update2', 'Multiopt Option'),
(92, 18, 'insert', 'Insert'),
(93, 18, 'delete', 'Delete'),
(94, 19, 'list', 'List'),
(95, 19, 'update', 'Update'),
(96, 19, 'update2', 'Multiopt Option'),
(97, 19, 'insert', 'Insert'),
(98, 19, 'delete', 'Delete'),
(99, 20, 'list', 'List'),
(100, 20, 'update', 'Update'),
(101, 20, 'update2', 'Multiopt Option'),
(102, 20, 'insert', 'Insert'),
(103, 20, 'delete', 'Delete'),
(104, 21, 'list', 'List'),
(105, 21, 'update', 'Update'),
(106, 22, 'list', 'List'),
(107, 23, 'list', 'List'),
(108, 23, 'insert', 'Insert'),
(109, 23, 'update', 'Update'),
(110, 23, 'update2', 'Multiopt Option'),
(111, 23, 'delete', 'Delete'),
(112, 24, 'list', 'List'),
(113, 24, 'update', 'Update'),
(114, 24, 'update2', 'Multiopt Option'),
(115, 24, 'insert', 'Insert'),
(116, 24, 'delete', 'Delete'),
(117, 25, 'list', 'List'),
(118, 25, 'update', 'Update'),
(119, 25, 'update2', 'Multiopt Option'),
(120, 25, 'insert', 'Insert'),
(121, 25, 'delete', 'Delete'),
(122, 26, 'list', 'List'),
(123, 26, 'update', 'Update'),
(124, 26, 'update2', 'Multiopt Option'),
(125, 26, 'insert', 'Insert'),
(126, 26, 'delete', 'Delete'),
(127, 27, 'list', 'List'),
(128, 27, 'update', 'Update'),
(129, 27, 'archive', 'Archive'),
(130, 27, 'download_invoice', 'Download Invoice'),
(131, 27, 'packing_slip', 'Packing Slip'),
(132, 27, 'shipping_label', 'Shipping Label'),
(133, 28, 'list', 'List'),
(134, 29, 'list', 'List'),
(135, 30, 'list', 'List'),
(136, 30, 'update', 'Update'),
(137, 30, 'update2', 'Multiopt Option'),
(138, 30, 'insert', 'Insert'),
(139, 30, 'delete', 'Delete'),
(140, 31, 'list', 'List'),
(141, 31, 'update', 'Update'),
(142, 31, 'update2', 'Multiopt Option'),
(143, 31, 'insert', 'Insert'),
(144, 31, 'delete', 'Delete'),
(145, 32, 'list', 'List'),
(146, 33, 'top_products', 'Top Products'),
(147, 33, 'tax', 'Tax'),
(148, 33, 'reviews', 'Reviews'),
(149, 33, 'customer', 'Customer'),
(150, 33, 'address', 'Address'),
(151, 33, 'customer_order', 'Customer Order'),
(152, 33, 'most_viewed', 'Most Viewed'),
(153, 33, 'search', 'Search'),
(154, 34, 'list', 'List'),
(155, 34, 'print', 'Print'),
(156, 35, 'list', 'List'),
(157, 35, 'update', 'Update'),
(158, 35, 'update2', 'Multiopt Option'),
(159, 35, 'insert', 'Insert'),
(160, 35, 'delete', 'Delete'),
(161, 36, 'list', 'List'),
(162, 36, 'update', 'Update'),
(163, 36, 'update_group', 'Update Group'),
(164, 36, 'update2', 'Multiopt Option'),
(165, 36, 'update_list_rule', 'Update List Rule'),
(166, 36, 'insert', 'Insert'),
(167, 36, 'insert_group', 'Insert Group'),
(168, 36, 'delete', 'Delete'),
(169, 36, 'delete_group', 'Delete Group'),
(170, 37, 'list', 'List'),
(171, 37, 'address', 'Address'),
(172, 37, 'order', 'Order'),
(173, 37, 'update', 'Update'),
(174, 37, 'update2', 'Multiopt Option'),
(175, 37, 'update_multiple_thrash', 'Update Multiple Trash'),
(176, 37, 'insert', 'Insert'),
(177, 37, 'delete', 'Delete'),
(178, 37, 'delete_address', 'Delete Address'),
(179, 37, 'permanent_delete', 'Permanent Delete'),
(180, 37, 'restore', 'Restore'),
(181, 37, 'thrash', 'Trash'),
(182, 38, 'list', 'List'),
(183, 38, 'insert', 'Insert'),
(184, 38, 'update', 'Update'),
(185, 38, 'delete', 'Delete'),
(186, 39, 'list', 'List'),
(187, 39, 'update', 'Update'),
(188, 39, 'update2', 'Multiopt Option'),
(189, 39, 'update_multiple_thrash', 'Update Multiple Trash'),
(190, 39, 'insert', 'Insert'),
(191, 39, 'delete', 'Delete'),
(192, 39, 'delete_image', 'Delete Image'),
(193, 39, 'permanent_delete', 'Permanent Delete'),
(194, 39, 'restore', 'Restore'),
(195, 39, 'thrash', 'Trash'),
(196, 40, 'list', 'List'),
(197, 41, 'list', 'List'),
(198, 41, 'update', 'Update'),
(199, 41, 'update2', 'Multiopt Option'),
(200, 41, 'update_multiple_thrash', 'Update Multiple Trash'),
(201, 41, 'insert', 'Insert'),
(202, 41, 'delete', 'Delete'),
(203, 41, 'delete_image', 'Delete Image'),
(204, 41, 'permanent_delete', 'Permanent Delete'),
(205, 41, 'restore', 'Restore'),
(206, 41, 'thrash', 'Trash'),
(207, 42, 'list', 'List'),
(208, 42, 'update2', 'Multiopt Option'),
(209, 42, 'insert', 'Insert'),
(210, 42, 'add', 'Add'),
(211, 42, 'remove', 'Remove'),
(212, 43, 'list', 'List'),
(213, 43, 'update', 'Update'),
(214, 43, 'update2', 'Multiopt Option'),
(215, 43, 'update_multiple_thrash', 'Update Multiple Trash'),
(216, 43, 'insert', 'Insert'),
(217, 43, 'delete', 'Delete'),
(218, 43, 'delete_image', 'Delete Image'),
(219, 43, 'permanent_delete', 'Permanent Delete'),
(220, 43, 'restore', 'Restore'),
(221, 43, 'thrash', 'Trash'),
(222, 44, 'list', 'List'),
(223, 44, 'update', 'Update'),
(224, 44, 'update2', 'Multiopt Option'),
(225, 44, 'update_multiple_thrash', 'Update Multiple Trash'),
(226, 44, 'insert', 'Insert'),
(227, 44, 'delete', 'Delete'),
(228, 44, 'delete_image', 'Delete Image'),
(229, 44, 'permanent_delete', 'Permanent Delete'),
(230, 44, 'restore', 'Restore'),
(231, 44, 'thrash', 'Trash'),
(232, 45, 'list', 'List'),
(233, 45, 'update2', 'Multiopt Option'),
(234, 45, 'permanent_delete', 'Permanent Delete'),
(235, 46, 'list', 'List'),
(236, 46, 'approve', 'Approve'),
(237, 46, 'update2', 'Multiopt Option'),
(238, 46, 'permanent_delete', 'Permanent Delete'),
(239, 46, 'update', 'Update'),
(240, 47, 'list', 'List'),
(241, 47, 'list_super', 'List Super'),
(242, 47, 'list_set', 'List Set'),
(243, 47, 'insert', 'Insert'),
(244, 47, 'insert_super', 'Insert Super'),
(245, 47, 'insert_set', 'Insert Set'),
(246, 47, 'delete_attr', 'Delete'),
(247, 47, 'delete_super', 'Delete Super'),
(248, 47, 'delete_set', 'Delete Set'),
(249, 47, 'update', 'Update'),
(250, 47, 'update_super', 'Update Super'),
(251, 47, 'update_set', 'Update Set'),
(252, 47, 'update_attr_list', 'Update Attribute List'),
(253, 47, 'update2', 'Multiopt Option'),
(254, 48, 'list', 'List'),
(255, 48, 'select', 'Select'),
(256, 48, 'insert', 'Insert'),
(257, 48, 'update', 'Update'),
(258, 48, 'update2', 'Multiopt Option'),
(259, 48, 'update_multiple_thrash', 'Update Multiple Trash'),
(260, 48, 'update_variant', 'Update Variant'),
(261, 48, 'general', 'General'),
(262, 48, 'category', 'Category'),
(263, 48, 'seo', 'SEO'),
(264, 48, 'images', 'Images'),
(265, 48, 'shipping', 'Shipping'),
(266, 48, 'visibility', 'Visibility'),
(267, 48, 'price', 'Price'),
(268, 48, 'variant', 'Variant'),
(269, 48, 'update_quantity', 'Update Quantity'),
(270, 48, 'relation', 'Relation'),
(271, 48, 'grouped', 'Grouped'),
(272, 48, 'option', 'Option'),
(273, 48, 'delete_product_option', 'Delete Product Option'),
(274, 48, 'attachment', 'Attachment'),
(275, 48, 'delete_variant', 'Delete Variant'),
(276, 48, 'delete_relation', 'Delete Relation'),
(277, 48, 'delete_group_rel', 'Delete Group Relation'),
(278, 48, 'delete', 'Delete'),
(279, 48, 'delete_image', 'Delete Image'),
(280, 48, 'delete_document', 'Delete Document'),
(281, 48, 'update_image_position', 'Update Image Position'),
(282, 48, 'update_relation_position', 'Update Relation Position'),
(283, 48, 'permanent_delete', 'Permanent Delete'),
(284, 48, 'restore', 'Restore'),
(285, 48, 'thrash', 'Trash'),
(286, 49, 'list', 'List'),
(287, 49, 'update', 'Update'),
(288, 49, 'update2', 'Multiopt Option'),
(289, 49, 'update_multiple_thrash', 'Update Multiple Trash'),
(290, 49, 'insert', 'Insert'),
(291, 49, 'delete', 'Delete'),
(292, 49, 'delete_image', 'Delete Image'),
(293, 49, 'permanent_delete', 'Permanent Delete'),
(294, 49, 'restore', 'Restore'),
(295, 49, 'thrash', 'Trash'),
(296, 50, 'list', 'List'),
(297, 50, 'insert', 'Insert'),
(298, 50, 'delete', 'Delete'),
(299, 50, 'update', 'Update'),
(300, 50, 'update2', 'Position Update'),
(301, 48, 'update_product_fields', 'Update Bulk Edit'),
(302, 51, 'list', 'List'),
(303, 51, 'insert', 'Insert'),
(304, 51, 'update', 'Update'),
(305, 51, 'delete', 'Delete'),
(306, 51, 'update2', 'Delete multiple records'),
(307, 51, 'thrash', 'Thrash'),
(308, 51, 'update_multiple_thrash', 'Restore and update from thrash'),
(309, 51, 'permanent_delete', 'Delete slider permanently'),
(310, 51, 'restore', 'Restore slider'),
(311, 51, 'delete_image', 'Permanent image delete'),
(312, 52, 'list', 'List'),
(313, 52, 'insert', 'Insert'),
(314, 52, 'update', 'Update'),
(315, 52, 'delete', 'Delete'),
(316, 52, 'update2', 'Delete multiple records'),
(317, 52, 'thrash', 'Thrash'),
(318, 52, 'update_multiple_thrash', 'Restore and update from thrash'),
(319, 52, 'permanent_delete', 'Delete slider permanently'),
(320, 52, 'restore', 'Restore slider'),
(321, 52, 'delete_image', 'Permanent image delete'),
(322, 53, 'list', 'List Idea'),
(323, 53, 'insert', 'Insert Idea'),
(324, 53, 'add_category', 'Insert Category'),
(325, 53, 'category', 'List Category'),
(326, 53, 'update', 'Update Idea'),
(327, 53, 'delete', 'Delete Idea'),
(328, 53, 'delete_cat', 'Delete Idea Category'),
(329, 54, 'list', 'Generate New Sitemap'),
";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "admin_modules` (`id`, `group_name`, `module`, `label`, `description`, `date_add`) VALUES
(1, 'Backup', 'backup', 'Backup', '', '2015-01-22 11:04:41'),
(2, 'Banner', 'banner', 'Banner', '', '2015-01-22 11:11:44'),
(3, 'Blog', 'blog', 'Blog', '', '2015-01-22 11:13:28'),
(4, 'Blog', 'blog_comment', 'Blog Comment', '', '2015-01-22 11:13:28'),
(5, 'Content', 'content', 'Content', '', '2015-01-22 11:58:31'),
(6, 'Disclaimer', 'disclaimer', 'Disclaimer', '', '2015-01-22 11:58:31'),
(7, 'Email', 'email', 'Email Content', '', '2015-01-22 11:58:31'),
(8, 'Editor', 'file_editor', 'File Editor', '', '2015-01-22 11:58:31'),
(9, 'Navigation', 'navigation', 'Navigation', '', '2015-01-22 11:58:31'),
(10, 'Navigation', 'content_navigation', 'Content Navigation', '', '2015-01-22 11:58:31'),
(11, 'Payment Method', 'payment_method', 'Payment Method', '', '2015-01-22 11:58:31'),
(12, 'Promotion', 'promotion', 'Promotion', '', '2015-01-22 11:58:31'),
(13, 'Setting', 'setting', 'Setting', '', '2015-01-22 11:58:31'),
(14, 'URL Rewrite', 'urlrewrite', 'URL Rewrite', '', '2015-01-22 11:58:31'),
(15, 'Newsletter', 'newsletter', 'NewsLetter', '', '2015-01-22 11:58:31'),
(16, 'Location', 'city', 'City', '', '2015-01-22 11:58:31'),
(17, 'Location', 'country', 'Country', '', '2015-01-22 11:58:31'),
(18, 'Location', 'state', 'State', '', '2015-01-22 11:58:31'),
(19, 'Location', 'zone', 'Zone', '', '2015-01-22 11:58:31'),
(20, 'Location', 'zone_rule', 'Zone Rule', '', '2015-01-22 11:58:31'),
(21, 'Shipping Handling', 'handling', 'Handling', '', '2015-01-22 11:58:31'),
(22, 'Shipping Handling', 'shipping', 'Shipping', '', '2015-01-22 11:58:31'),
(23, 'Shipping Handling', 'shipping_flat', 'Shipping Flat', '', '2015-01-22 11:58:31'),
(24, 'Shipping Handling', 'shipping_group', 'Shipping Group', '', '2015-01-22 11:58:31'),
(25, 'Shipping Handling', 'shipping_price', 'Shipping Price', '', '2015-01-22 11:58:31'),
(26, 'Shipping Handling', 'shipping_weight', 'Shipping Weight', '', '2015-01-22 11:58:31'),
(27, 'Order', 'order', 'Order', '', '2015-01-22 11:58:31'),
(28, 'Order', 'invoices', 'Invoices', '', '2015-01-22 11:58:31'),
(29, 'Order', 'packing_slip', 'Packing Slip', '', '2015-01-22 11:58:31'),
(30, 'Search', 'hypernym', 'Hypernym', '', '2015-01-22 11:58:31'),
(31, 'Search', 'synonym', 'Synonym', '', '2015-01-22 11:58:31'),
(32, 'Reports', 'stats', 'Stats', '', '2015-01-22 11:58:31'),
(33, 'Reports', 'report', 'Report', '', '2015-01-22 11:58:31'),
(34, 'Reports', 'low_stock', 'Low Stock', '', '2015-01-22 11:58:31'),
(35, 'Tax', 'tax', 'Tax', '', '2015-01-22 11:58:31'),
(36, 'Tax', 'tax_rule', 'Tax Rule', '', '2015-01-22 11:58:31'),
(37, 'Customer', 'user', 'User', '', '2015-01-22 11:58:31'),
(38, 'Customer', 'user_group', 'Customer Groups', '', '2015-01-22 11:58:31'),
(39, 'Brand', 'brand', 'Brand', '', '2015-01-22 11:58:31'),
(40, 'Brand', 'brand_product', 'Brand Product', '', '2015-01-22 11:58:31'),
(41, 'Category', 'category', 'Category', '', '2015-01-22 11:58:31'),
(42, 'Category', 'category_product', 'Category Product', '', '2015-01-22 11:58:31'),
(43, 'Manufacturer', 'manufacturer', 'Manufacturer', '', '2015-01-22 11:58:31'),
(44, 'Supplier', 'supplier', 'Supplier', '', '2015-01-22 11:58:31'),
(45, 'Stock Requests', 'stock_requests', 'Stock Requests', '', '2015-01-22 11:58:31'),
(46, 'Review', 'review', 'Review', '', '2015-01-22 11:58:31'),
(47, 'Attribute', 'attribute', 'Attribute', '', '2015-01-22 11:58:31'),
(48, 'Product', 'product', 'Product', '', '2015-01-22 11:58:31'),
(49, 'Product', 'filter_options', 'Filter Options', '', '2015-01-22 11:58:31'),
(50, 'Product', 'option', 'Option', '', '2015-01-22 11:58:31'),
(51, 'Sliders', 'slider', 'Sliders', '', '2015-02-20 14:12:13'),
(52, 'Sliders', 'slides', 'Slides', '', '2015-02-23 16:30:26'),
(53, 'Idea', 'idea', 'Idea', '', '2015-04-16 11:00:48'),
(54, 'Sitemap', 'sitemap', 'Sitemap', '', '2015-04-23 14:25:28')
";
        mysql_query($sample, $this->dbconn);


        $sample = "INSERT INTO `" . $this->prefix . "admin_permission_level_grouping` (`id`, `module_id`, `permission_level_id`, `actions`, `date_add`) VALUES
(1, 47, 1, '', '2015-01-28 13:31:04'),
(2, 47, 2, 'list,list_super,list_set', '2015-01-28 13:31:04'),
(3, 47, 3, 'list,list_super,list_set,insert,insert_super,insert_set,delete_attr,update,update_super,update_set,update_attr_list,update2', '2015-01-28 13:31:04'),
(4, 47, 4, 'list,list_super,list_set,insert,insert_super,insert_set,delete_attr,delete_super,delete_set,update,update_super,update_set,update_attr_list,update2', '2015-01-28 13:31:04'),
(5, 1, 1, '', '2015-01-28 17:30:15'),
(6, 1, 2, 'list,download', '2015-01-28 17:30:15'),
(7, 1, 3, 'list,create,download,restore,thrash', '2015-01-28 17:30:15'),
(8, 1, 4, 'list,delete,create,download,permanent_delete,restore,thrash', '2015-01-28 17:30:15'),
(9, 2, 1, '', '2015-01-28 17:31:23'),
(10, 2, 2, 'list,update', '2015-01-28 17:31:23'),
(11, 2, 3, 'list,update,insert,location,update_location', '2015-01-28 17:31:23'),
(12, 2, 4, 'list,update,update2,insert,delete,delete_image,location,delete_banner_location,update_location', '2015-01-28 17:31:23'),
(13, 3, 1, '', '2015-01-28 17:31:58'),
(14, 3, 2, 'list', '2015-01-28 17:31:58'),
(15, 3, 3, 'list,insert,update', '2015-01-28 17:31:58'),
(16, 3, 4, 'list,insert,update,update2,delete,delete_image', '2015-01-28 17:31:58'),
(17, 4, 1, '', '2015-01-28 17:33:12'),
(18, 4, 2, 'list', '2015-01-28 17:33:12'),
(19, 4, 3, 'list,update,update2', '2015-01-28 17:33:13'),
(20, 4, 4, 'list,update,update2,delete', '2015-01-28 17:33:13'),
(21, 39, 1, '', '2015-01-28 17:33:38'),
(22, 39, 2, 'list', '2015-01-28 17:33:38'),
(23, 39, 3, 'list,update,update2,insert,delete,delete_image,restore,thrash', '2015-01-28 17:33:38'),
(24, 39, 4, 'list,update,update2,update_multiple_thrash,insert,delete,delete_image,permanent_delete,restore,thrash', '2015-01-28 17:33:38'),
(25, 40, 1, '', '2015-01-28 17:33:57'),
(26, 40, 2, 'list', '2015-01-28 17:33:57'),
(27, 40, 3, 'list', '2015-01-28 17:33:57'),
(28, 40, 4, 'list', '2015-01-28 17:33:57'),
(29, 41, 1, '', '2015-01-28 17:34:40'),
(30, 41, 2, 'list', '2015-01-28 17:34:40'),
(31, 41, 3, 'list,update,update2,update_multiple_thrash,insert,delete,delete_image,restore,thrash', '2015-01-28 17:34:40'),
(32, 41, 4, 'list,update,update2,update_multiple_thrash,insert,delete,delete_image,permanent_delete,restore,thrash', '2015-01-28 17:34:40'),
(33, 42, 1, '', '2015-01-28 17:35:11'),
(34, 42, 2, 'list', '2015-01-28 17:35:11'),
(35, 42, 3, 'list,update2,insert,add', '2015-01-28 17:35:11'),
(36, 42, 4, 'list,update2,insert,add,remove', '2015-01-28 17:35:11'),
(37, 5, 1, '', '2015-01-28 17:36:48'),
(38, 5, 2, 'list', '2015-01-28 17:36:48'),
(39, 5, 3, 'list,update,update2,insert,delete,restore,thrash', '2015-01-28 17:36:48'),
(40, 5, 4, 'list,update,update2,update_multiple_thrash,insert,delete,permanent_delete,restore,thrash', '2015-01-28 17:36:48'),
(41, 37, 1, '', '2015-01-28 17:37:15'),
(42, 37, 2, 'list', '2015-01-28 17:37:15'),
(43, 37, 3, 'list,address,order,update,update_multiple_thrash,insert,delete_address,restore,thrash', '2015-01-28 17:37:15'),
(44, 37, 4, 'list,address,order,update,update2,update_multiple_thrash,insert,delete,delete_address,permanent_delete,restore,thrash', '2015-01-28 17:37:15'),
(45, 38, 1, '', '2015-01-28 17:37:32'),
(46, 38, 2, 'list', '2015-01-28 17:37:32'),
(47, 38, 3, 'list,insert,update', '2015-01-28 17:37:32'),
(48, 38, 4, 'list,insert,update,delete', '2015-01-28 17:37:32'),
(49, 6, 1, '', '2015-01-28 17:38:08'),
(50, 6, 2, 'list', '2015-01-28 17:38:08'),
(51, 6, 3, 'list,update,insert,restore,thrash', '2015-01-28 17:38:08'),
(52, 6, 4, 'list,update,update2,insert,delete,permanent_delete,restore,thrash', '2015-01-28 17:38:08'),
(53, 8, 1, '', '2015-01-28 17:38:36'),
(54, 8, 2, 'list', '2015-01-28 17:38:36'),
(55, 8, 3, 'list,update,upload', '2015-01-28 17:38:36'),
(56, 8, 4, 'list,update,upload', '2015-01-28 17:38:37'),
(57, 7, 1, '', '2015-01-28 17:38:55'),
(58, 7, 2, 'list', '2015-01-28 17:38:55'),
(59, 7, 3, 'list,update', '2015-01-28 17:38:55'),
(60, 7, 4, 'list,update', '2015-01-28 17:38:55'),
(61, 16, 1, '', '2015-01-28 17:39:12'),
(62, 16, 2, 'list', '2015-01-28 17:39:12'),
(63, 16, 3, 'list,update,update2,insert', '2015-01-28 17:39:12'),
(64, 16, 4, 'list,update,update2,insert,delete', '2015-01-28 17:39:12'),
(65, 17, 1, '', '2015-01-28 17:42:14'),
(66, 17, 2, 'list', '2015-01-28 17:42:14'),
(67, 17, 3, 'list,update2', '2015-01-28 17:42:14'),
(68, 17, 4, 'list,update2', '2015-01-28 17:42:14'),
(69, 18, 1, '', '2015-01-28 17:42:26'),
(70, 18, 2, 'list', '2015-01-28 17:42:26'),
(71, 18, 3, 'list,update,update2,insert', '2015-01-28 17:42:26'),
(72, 18, 4, 'list,update,update2,insert,delete', '2015-01-28 17:42:26'),
(73, 19, 1, '', '2015-01-28 17:42:36'),
(74, 19, 2, 'list', '2015-01-28 17:42:36'),
(75, 19, 3, 'list,update,update2,insert', '2015-01-28 17:42:36'),
(76, 19, 4, 'list,update,update2,insert,delete', '2015-01-28 17:42:36'),
(77, 20, 1, '', '2015-01-28 17:43:03'),
(78, 20, 2, 'list', '2015-01-28 17:43:04'),
(79, 20, 3, 'list,update,update2,insert', '2015-01-28 17:43:04'),
(80, 20, 4, 'list,update,update2,insert,delete', '2015-01-28 17:43:04'),
(81, 43, 1, '', '2015-01-28 17:43:25'),
(82, 43, 2, 'list', '2015-01-28 17:43:25'),
(83, 43, 3, 'list,update,update_multiple_thrash,insert,delete_image,restore,thrash', '2015-01-28 17:43:25'),
(84, 43, 4, 'list,update,update2,update_multiple_thrash,insert,delete,delete_image,permanent_delete,restore,thrash', '2015-01-28 17:43:25'),
(85, 9, 1, '', '2015-01-28 17:43:38'),
(86, 9, 2, 'list', '2015-01-28 17:43:38'),
(87, 9, 3, 'list,insert,update', '2015-01-28 17:43:38'),
(88, 9, 4, 'list,insert,update,delete', '2015-01-28 17:43:38'),
(89, 10, 1, '', '2015-01-28 17:47:11'),
(90, 10, 2, 'list', '2015-01-28 17:47:12'),
(91, 10, 3, 'list,insert,update,update2,delete,thrash,restore,update3', '2015-01-28 17:47:12'),
(92, 10, 4, 'list,insert,update,update2,delete,thrash,permanent_delete,restore,update3', '2015-01-28 17:47:12'),
(93, 15, 1, '', '2015-01-28 17:48:19'),
(94, 15, 2, 'list', '2015-01-28 17:48:19'),
(95, 15, 3, 'list', '2015-01-28 17:48:19'),
(96, 15, 4, 'list,delete', '2015-01-28 17:48:19'),
(97, 27, 1, '', '2015-01-28 17:48:35'),
(98, 27, 2, 'list', '2015-01-28 17:48:35'),
(99, 27, 3, 'list,update,archive,download_invoice,packing_slip,shipping_label', '2015-01-28 17:48:35'),
(100, 27, 4, 'list,update,archive,download_invoice,packing_slip,shipping_label', '2015-01-28 17:48:35'),
(101, 28, 1, '', '2015-01-28 17:49:07'),
(102, 28, 2, 'list', '2015-01-28 17:49:07'),
(103, 28, 3, 'list', '2015-01-28 17:49:07'),
(104, 28, 4, 'list', '2015-01-28 17:49:07'),
(105, 29, 1, '', '2015-01-28 17:49:16'),
(106, 29, 2, 'list', '2015-01-28 17:49:16'),
(107, 29, 3, 'list', '2015-01-28 17:49:16'),
(108, 29, 4, 'list', '2015-01-28 17:49:16'),
(109, 11, 1, '', '2015-01-28 17:49:29'),
(110, 11, 2, 'list', '2015-01-28 17:49:29'),
(111, 11, 3, 'list,update,update2', '2015-01-28 17:49:29'),
(112, 11, 4, 'list,update,update2', '2015-01-28 17:49:29'),
(113, 48, 1, '', '2015-01-28 17:50:55'),
(114, 48, 2, 'list,select,general,category,seo,images,shipping,visibility,price,variant,relation,grouped,attachment', '2015-01-28 17:50:55'),
(115, 48, 3, 'list,select,insert,update,update2,update_multiple_thrash,update_variant,general,category,seo,images,shipping,visibility,price,variant,update_quantity,relation,grouped,option,delete_product_option,attachment,delete_variant,delete_relation,delete_group_rel,delete,delete_image,delete_document,update_image_position,update_relation_position,restore,thrash,update_product_fields', '2015-01-28 17:50:55'),
(116, 48, 4, 'list,select,insert,update,update2,update_multiple_thrash,update_variant,general,category,seo,images,shipping,visibility,price,variant,update_quantity,relation,grouped,option,delete_product_option,attachment,delete_variant,delete_relation,delete_group_rel,delete,delete_image,delete_document,update_image_position,update_relation_position,permanent_delete,restore,thrash,update_product_fields', '2015-01-28 17:50:56'),
(117, 49, 1, '', '2015-01-28 17:59:49'),
(118, 49, 2, 'list', '2015-01-28 17:59:49'),
(119, 49, 3, 'list,update,update2,update_multiple_thrash,insert,delete,delete_image,restore,thrash', '2015-01-28 17:59:49'),
(120, 49, 4, 'list,update,update2,update_multiple_thrash,insert,delete,delete_image,permanent_delete,restore,thrash', '2015-01-28 17:59:49'),
(121, 50, 1, '', '2015-01-28 18:00:03'),
(122, 50, 2, 'list', '2015-01-28 18:00:03'),
(123, 50, 3, 'list,insert,update,update2', '2015-01-28 18:00:03'),
(124, 50, 4, 'list,insert,delete,update,update2', '2015-01-28 18:00:03'),
(125, 12, 1, '', '2015-01-28 18:00:34'),
(126, 12, 2, 'type,select,list', '2015-01-28 18:00:34'),
(127, 12, 3, 'type,select,list,insert,update,update_conditions,update_actions', '2015-01-28 18:00:34'),
(128, 12, 4, 'type,select,list,insert,update,update_conditions,update_actions,update2,delete', '2015-01-28 18:00:34'),
(129, 32, 1, '', '2015-01-28 18:00:48'),
(130, 32, 2, 'list', '2015-01-28 18:00:48'),
(131, 32, 3, 'list', '2015-01-28 18:00:48'),
(132, 32, 4, 'list', '2015-01-28 18:00:48'),
(133, 33, 1, '', '2015-01-28 18:01:09'),
(134, 33, 2, 'top_products,tax,reviews,customer,address,customer_order,most_viewed,search', '2015-01-28 18:01:09'),
(135, 33, 3, 'top_products,tax,reviews,customer,address,customer_order,most_viewed,search', '2015-01-28 18:01:09'),
(136, 33, 4, 'top_products,tax,reviews,customer,address,customer_order,most_viewed,search', '2015-01-28 18:01:09'),
(137, 34, 1, '', '2015-01-28 18:01:23'),
(138, 34, 2, 'list', '2015-01-28 18:01:23'),
(139, 34, 3, 'list,print', '2015-01-28 18:01:24'),
(140, 34, 4, 'list,print', '2015-01-28 18:01:24'),
(141, 46, 1, '', '2015-01-28 18:02:21'),
(142, 46, 2, 'list', '2015-01-28 18:02:22'),
(143, 46, 3, 'list,approve,update', '2015-01-28 18:02:22'),
(144, 46, 4, 'list,approve,update2,permanent_delete,update', '2015-01-28 18:02:22'),
(145, 30, 1, '', '2015-01-28 18:02:39'),
(146, 30, 2, 'list', '2015-01-28 18:02:39'),
(147, 30, 3, 'list,update,insert', '2015-01-28 18:02:39'),
(148, 30, 4, 'list,update,update2,insert,delete', '2015-01-28 18:02:39'),
(149, 31, 1, '', '2015-01-28 18:02:48'),
(150, 31, 2, 'list', '2015-01-28 18:02:48'),
(151, 31, 3, 'list,update,insert', '2015-01-28 18:02:48'),
(152, 31, 4, 'list,update,update2,insert,delete', '2015-01-28 18:02:48'),
(153, 13, 1, '', '2015-01-28 18:03:28'),
(154, 13, 2, 'list', '2015-01-28 18:03:28'),
(155, 13, 3, 'list,update,robots_edit,change_password', '2015-01-28 18:03:28'),
(156, 13, 4, 'list,update,robots_edit,change_password', '2015-01-28 18:03:28'),
(157, 21, 1, '', '2015-01-28 18:04:06'),
(158, 21, 2, 'list', '2015-01-28 18:04:06'),
(159, 21, 3, 'list,update', '2015-01-28 18:04:06'),
(160, 21, 4, 'list,update', '2015-01-28 18:04:06'),
(161, 22, 1, '', '2015-01-28 18:04:14'),
(162, 22, 2, 'list', '2015-01-28 18:04:14'),
(163, 22, 3, 'list', '2015-01-28 18:04:14'),
(164, 22, 4, 'list', '2015-01-28 18:04:14'),
(165, 23, 1, '', '2015-01-28 18:04:22'),
(166, 23, 2, 'list', '2015-01-28 18:04:22'),
(167, 23, 3, 'list,insert,update,update2', '2015-01-28 18:04:22'),
(168, 23, 4, 'list,insert,update,update2,delete', '2015-01-28 18:04:22'),
(169, 24, 1, '', '2015-01-28 18:04:32'),
(170, 24, 2, 'list', '2015-01-28 18:04:32'),
(171, 24, 3, 'list,update,update2,insert', '2015-01-28 18:04:33'),
(172, 24, 4, 'list,update,update2,insert,delete', '2015-01-28 18:04:33'),
(173, 25, 1, '', '2015-01-28 18:04:54'),
(174, 25, 2, 'list', '2015-01-28 18:04:54'),
(175, 25, 3, 'list,update,update2,insert', '2015-01-28 18:04:54'),
(176, 25, 4, 'list,update,update2,insert,delete', '2015-01-28 18:04:55'),
(177, 26, 1, '', '2015-01-28 18:05:04'),
(178, 26, 2, 'list', '2015-01-28 18:05:04'),
(179, 26, 3, 'list,update,update2,insert', '2015-01-28 18:05:04'),
(180, 26, 4, 'list,update,update2,insert,delete', '2015-01-28 18:05:04'),
(181, 45, 1, '', '2015-01-28 18:05:17'),
(182, 45, 2, 'list', '2015-01-28 18:05:17'),
(183, 45, 3, 'list', '2015-01-28 18:05:17'),
(184, 45, 4, 'list,update2,permanent_delete', '2015-01-28 18:05:17'),
(185, 44, 1, '', '2015-01-28 18:05:36'),
(186, 44, 2, 'list', '2015-01-28 18:05:36'),
(187, 44, 3, 'list,update,update2,insert,delete,delete_image,restore,thrash', '2015-01-28 18:05:36'),
(188, 44, 4, 'list,update,update2,update_multiple_thrash,insert,delete,delete_image,permanent_delete,restore,thrash', '2015-01-28 18:05:36'),
(189, 35, 1, '', '2015-01-28 18:06:01'),
(190, 35, 2, 'list', '2015-01-28 18:06:01'),
(191, 35, 3, 'list,update,update2,insert', '2015-01-28 18:06:01'),
(192, 35, 4, 'list,update,update2,insert,delete', '2015-01-28 18:06:01'),
(193, 36, 1, '', '2015-01-28 18:06:23'),
(194, 36, 2, 'list', '2015-01-28 18:06:23'),
(195, 36, 3, 'list,update,update_group,update2,update_list_rule,insert,insert_group', '2015-01-28 18:06:23'),
(196, 36, 4, 'list,update,update_group,update2,update_list_rule,insert,insert_group,delete,delete_group', '2015-01-28 18:06:23'),
(197, 14, 1, '', '2015-01-28 18:06:36'),
(198, 14, 2, 'list', '2015-01-28 18:06:36'),
(199, 14, 3, 'list,update', '2015-01-28 18:06:36'),
(200, 14, 4, 'list,update', '2015-01-28 18:06:36'),
(201, 51, 1, '', '2015-02-20 14:17:05'),
(202, 51, 2, 'list', '2015-02-20 14:17:05'),
(203, 51, 3, 'list,insert,update,thrash', '2015-02-20 14:17:06'),
(204, 51, 4, 'list,insert,update,delete,update2,thrash,update_multiple_thrash,permanent_delete,restore,delete_image', '2015-02-20 14:17:06'),
(205, 52, 1, '', '2015-02-23 16:42:07'),
(206, 52, 2, 'list', '2015-02-23 16:42:07'),
(207, 52, 3, 'list,insert,update,thrash,update_multiple_thrash,restore', '2015-02-23 16:42:07'),
(208, 52, 4, 'list,insert,update,delete,update2,thrash,update_multiple_thrash,permanent_delete,restore,delete_image', '2015-02-23 16:42:07'),
(209, 53, 1, '', '2015-04-16 11:02:15'),
(210, 53, 2, 'list,category', '2015-04-16 11:02:15'),
(211, 53, 3, 'list,insert,add_category,category,update', '2015-04-16 11:02:15'),
(212, 53, 4, 'list,insert,add_category,category,update,delete,delete_cat', '2015-04-16 11:02:15'),
(213, 54, 1, '', '2015-04-23 12:34:31'),
(214, 54, 2, '', '2015-04-23 12:34:31'),
(215, 54, 3, '', '2015-04-23 12:34:31'),
(216, 54, 4, 'list', '2015-04-23 12:34:31')
;";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "admin_user_role_permissions` 

(`id`, `role_id`, `permission_level_group_id`) VALUES

(1, 1, 4),

(2, 1, 8),

(3, 1, 12),

(4, 1, 16),

(5, 1, 20),

(6, 1, 24),

(7, 1, 28),

(8, 1, 32),

(9, 1, 36),

(10, 1, 40),

(11, 1, 44),

(12, 1, 48),

(13, 1, 52),

(14, 1, 56),

(15, 1, 60),

(16, 1, 64),

(17, 1, 68),

(18, 1, 72),

(19, 1, 76),

(20, 1, 80),

(21, 1, 84),

(22, 1, 88),

(23, 1, 92),

(24, 1, 96),

(25, 1, 100),

(26, 1, 104),

(27, 1, 108),

(28, 1, 112),

(29, 1, 116),

(30, 1, 120),

(31, 1, 124),

(32, 1, 128),

(33, 1, 132),

(34, 1, 136),

(35, 1, 140),

(36, 1, 144),

(37, 1, 148),

(38, 1, 152),

(39, 1, 156),

(40, 1, 160),

(41, 1, 164),

(42, 1, 168),

(43, 1, 172),

(44, 1, 176),

(45, 1, 180),

(46, 1, 184),

(47, 1, 188),

(48, 1, 192),

(49, 1, 196),

(50, 1, 200),

(51, 1, 204),

(52, 1, 208),

(53, 1, 212),

(54, 1, 216)
;";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "navigations` (`id`, `name`, `position`, `description`, `is_active`, `is_deleted`, `date_add`, `date_upd`) VALUES
        (1, 'Top Navigation Menu', 'top', NULL, '1', 0, '2015-01-14 15:52:50', '2015-02-18 05:23:14'),
        (3, 'Mobile Navigation Menu', 'mobile', NULL, '1', 0, '2015-02-12 00:16:12', '2015-02-17 14:22:10');
        ";
        mysql_query($sample, $this->dbconn);


        if (isset($_SESSION['add_dummy_data']) && $_SESSION["add_dummy_data"] = 1) {
            $sample = "INSERT INTO `" . $this->prefix . "category` (`name`, `urlname`, `image`, `description`, `position`, `parent_id`, `is_active`, `is_deleted`, `is_featured`, `attribute_super_set_id`, `meta_title`, `meta_desc`, `meta_keyword`, `meta_robots_index`, `meta_robots_follow`, `include_xml_sitemap`, `sitemap_priority`, `canonical`, `from_date`, `to_date`, `image_alt_text`, `display_catname`, `user_group_id`, `display_in_category_listing`, `date_add`, `date_upd`) VALUES
        ('Sports Shoes', 'sports-shoes', '', '', 0, 0, 1, '0', '0', 0, '', 'Sports Shoes', 'Sports Shoes', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Casual Shoes', 'casual-shoes', '', '', 0, 0, 1, '0', '0', 0, '', 'Casual Shoes', 'Casual Shoes', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Formal Shoes', 'formal-shoes', '', '', 0, 0, 1, '0', '0', 0, '', 'Formal Shoes', 'Formal Shoes', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Floater Sandals', 'floater-sandals', '', '', 0, 0, 1, '0', '0', 0, '', 'Floater Sandals', 'Floater Sandals', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Slippers & Flip Flops', 'slippers-flipflops', '', '', 0, 0, 1, '0', '0', 0, '', 'Slippers & Flip Flops', 'Slippers & Flip Flops', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Boots', 'boots', '', '', 0, 0, 1, '0', '0', 0, '', 'Boots', 'Boots', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Sandals', 'sandals', '', '', 0, 0, 1, '0', '0', 0, '', 'Sandals', 'Sandals', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Speciality Footwear', 'speciality-footwear', '', '', 0, 0, 1, '0', '0', 0, '', 'Speciality Footwear', 'Speciality Footwear', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Ethnic', 'ethnic', '', '', 0, 0, 1, '0', '0', 0, '', 'Ethnic', 'Ethnic', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Party Shoes', 'party-shoes', '', '', 0, 0, 1, '0', '0', 0, '', 'Party Shoes', 'Party Shoes', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Shirts', 'shirts', '', '', 0, 0, 1, '0', '0', 0, '', 'Shirts', 'Shirts', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('T Shirts', 't-shirts', '', '', 0, 0, 1, '0', '0', 0, '', 'T Shirts', 'T Shirts', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Polo T shirts', 'polo-tshirts', '', '', 0, 0, 1, '0', '0', 0, '', 'Polo T shirts', 'Polo T shirts', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Jeans', 'jeans', '', '', 0, 0, 1, '0', '0', 0, '', 'Jeans', 'Jeans', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Trousers & Chinos', 'trousers-chinos', '', '', 0, 0, 1, '0', '0', 0, '', 'Trousers & Chinos', 'Trousers & Chinos', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Suitings & Shirtings', 'suitings-shirtings', '', '', 0, 0, 1, '0', '0', 0, '', 'Suitings & Shirtings', 'Suitings & Shirtings', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Kurtas , Pyjamas & Sherwanis', 'kurtas-pyjamas', '', '', 0, 0, 1, '0', '0', 0, '', 'Kurtas , Pyjamas & Sherwanis', 'Kurtas , Pyjamas & Sherwanis', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Trackpants & Tracksuits', 'trackpants-trachsuits', '', '', 0, 0, 1, '0', '0', 0, '', 'Trackpants & Tracksuits', 'Trackpants & Tracksuits', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Sweaters', 'sweaters', '', '', 0, 0, 1, '0', '0', 0, '', 'Sweaters', 'Sweaters', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Sweatshirts', 'sweatshits', '', '', 0, 0, 1, '0', '0', 0, '', 'Sweatshirts', 'Sweatshirts', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '1', '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('Gift Certificates', 'gift-certificates', '', '', 0, 0, 1, '0', '0', 0, '', '', '', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Gift Certificates', '1', '0', 1, '2015-04-13 04:52:41', '2015-04-13 08:52:41'),
('Store Credits', 'store-credits', '', '', 0, 0, 1, '0', '0', 0, '', '', '', 'global', 'follow', 'global', 'global', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Store Credits', '1', '0', 1, '2015-04-13 04:50:37', '2015-04-13 08:50:37');
        ";

            mysql_query($sample, $this->dbconn);

            $sample = "INSERT INTO `" . $this->prefix . "category_structure` (`from_feed`, `feed_name`, `f_cat_name`, `name`, `image`, `description`, `parent_id`, `is_active`, `is_deleted`, `store_cat_id`, `date_add`, `date_upd`) VALUES
        ('1', 'dummy_data', 'Sports Shoes', 'Sports Shoes', '', '', 0, 1, '0', 1, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Casual Shoes', 'Casual Shoes', '', '', 0, 1, '0', 2, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Formal Shoes', 'Formal Shoes', '', '', 0, 1, '0', 3, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Floater Sandals', 'Floater Sandals', '', '', 0, 1, '0', 4, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Slippers & Flip Flops', 'Slippers & Flip Flops', '', '', 0, 1, '0', 5, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Boots', 'Boots', '', '', 0, 1, '0', 6, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Sandals', 'Sandals', '', '', 0, 1, '0', 7, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Speciality Footwear', 'Speciality Footwear', '', '', 0, 1, '0', 8, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Ethnic', 'Ethnic', '', '', 0, 1, '0', 9, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Party Shoes', 'Party Shoes', '', '', 0, 1, '0', 10, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Shirts', 'Shirts', '', '', 0, 1, '0', 11, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'T Shirts', 'T Shirts', '', '', 0, 1, '0', 12, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Polo T shirts Gear', 'Polo T shirts Gear', '', '', 236, 1, '0', 13, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Jeans', 'Jeans', '', '', 0, 1, '0', 14, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Trousers & Chinos', 'Trousers & Chinos', '', '', 0, 1, '0', 15, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Suitings & Shirtings', 'Suitings & Shirtings', '', '', 0, 1, '0', 16, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Kurtas , Pyjamas & Sherwanis', 'Kurtas , Pyjamas & Sherwanis', '', '', 0, 1, '0', 17, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Trackpants & Tracksuits', 'Trackpants & Tracksuits', '', '', 0, 1, '0', 18, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Sweaters', 'Sweaters', '', '', 0, 1, '0', 19, '2015-03-10 03:16:26', '2015-03-10 07:16:26'),
        ('1', 'dummy_data', 'Sweatshirts', 'Sweatshirts', '', '', 0, 1, '0', 20, '2015-03-10 03:16:26', '2015-03-10 07:16:26');
        ";
//echo $sample; die;
            mysql_query($sample, $this->dbconn);

            //Add Dummy Manufacturer and Brand
            $sample = "INSERT INTO `" . $this->prefix . "manufacturer` (`id`, `name`, `urlname`, `position`, `is_active`, `is_deleted`, `date_add`) VALUES (1, 'dummy', 'dummy', 1, 1, '0', '2015-02-08 06:31:01');";
            mysql_query($sample, $this->dbconn);

            $sample = "INSERT INTO `" . $this->prefix . "brand` (`id`, `name`, `urlname`, `position`, `is_active`, `is_deleted`, `date_add`) VALUES (1, 'dummy', 'dummy', 1, 1, '0', '2015-02-08 06:31:01');";
            mysql_query($sample, $this->dbconn);

            $sample = "INSERT INTO `" . $this->prefix . "product` (`upc`, `sku`, `brand_id`, `supplier_id`, `manufacturer_id`, `name`, `urlname`, `short_description`, `long_description`, `quantity`, `minimal_quantity`, `maximum_quantity`, `wholesale_price`, `unit_price`, `sale_price`, `discount_type`, `discount`, `additional_shipping_price`, `width`, `height`, `weight`, `depth`, `product_condition`, `product_type`, `download_file`, `download_file_name`, `download_within_days`, `maximum_downloads`, `available_from`, `available_to`, `tax_rule_group_id`, `attribute_super_set_id`, `default_category`, `is_active`, `is_deleted`, `is_accessory`, `is_featured`, `is_wrappable`, `gift_wrap_charges`, `date_add`, `date_upd`, `meta_title`, `meta_desc`, `meta_keyword`, `source`, `special_handling`, `packaging`, `map`, `uom`, `product_option`, `flavor`, `scent`, `discontinued`, `feed_last_update`, `master_product_id`, `image_last_update`, `meta_robots_index`, `meta_robots_follow`, `include_xml_sitemap`, `sitemap_priority`, `canonical`, `show_on_home`, `show_on_category`, `show_on_product_page`, `show_on_search`, `show_on_cart`, `latest_from`, `latest_to`, `sale_from`, `sale_to`, `updated_from_feed`, `promo_text`, `out_of_stock_option`, `scheduled_for`, `is_bestseller`, `disable_feed_price_update`) VALUES
        ('811847010295', 'MI825-L', NULL, NULL, NULL, 'Nike Nike Ballista Iv Msl Gray Sport Shoes', 'sports-shoes', 'The Smart and Simple Way to Get Nike Products Shop for sports clothing and footwear online at test2 without breaking your budget!', '&lt;p&gt;The Smart and Simple Way to Get Nike Products Shop for sports clothing and footwear online at test2 without breaking your budget! From sports clothing to mens sport shoes including sandals, flip-flops, casual shoes, tennis shoes, basketball shoes and training shoes, all are available here at whopping discounts. Order now and get products delivered at your doorsteps. You can pay online or in cash.&lt;/p&gt;\r\n', 1, 0, 0, 42, 56.25, 56.25, 'percentage', 10, 0, 0, 0, 0.4, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 1, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:09:59', '', 'Nike Nike Ballista Iv Msl Gray Sport Shoes', 'Nike Nike Ballista Iv Msl Gray Sport Shoes', 'http://www.sextoydistributing.com/Merchant2/product/MI825-L.html', '', 'Retail Packaging', '', '', '', '', '', '0', '2014-07-25 12:55:08', 'MI825', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 2146, 0),
        ('848518003560', 'TT641-Black', 1, NULL, 1, 'Shoe Island Coffee Brown Ankle Length Shoes', 'ankle-shoes', 'Simple and minimal design combining with comfort and falling in line with your demand for the latest in fashion.', '&lt;p&gt;Simple and minimal design combining with comfort and falling in line with your demand for the latest in fashion. test2 presents an amazing range of Shoe Island footwear for men who want to look perfect for every occasion. This range is suave, fashionable and comfortable. Stylish and comfortable are the words that define this collection perfectly. Each style reflects passion for footwear and deep understanding of materials and design.&lt;/p&gt;\r\n', 1, 0, 0, 128, 195, 195, 'percentage', 10, 0, 0, 0, 1, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 2, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:10:44', '', 'Shoe Island Coffee Brown Ankle Length Shoes', 'Shoe Island Coffee Brown Ankle Length Shoes', 'http://www.sextoydistributing.com/Merchant2/product/TT641-Black.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2014-04-17 15:57:20', 'TT641', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 89, 0),
        ('848518003584', 'TT641-RedBlack', 1, NULL, 1, 'Zapatoz Black Formal Shoes', 'formal-shoes', 'Zapatoz is a popular footwear brand; primarily involved in the manufacturing of casual and formal shoes for men.', '&lt;p&gt;Zapatoz is a popular footwear brand; primarily involved in the manufacturing of casual and formal shoes for men. This range of casual and formal shoes is highly comfortable and has been designed using high-quality material. Use of bright hues makes the shoes even more appealing and stylish.&lt;/p&gt;\r\n', 1, 0, 0, 128, 195, 195, 'percentage', 10, 0, 0, 0, 1, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 3, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:11:23', '', 'Zapatoz Black Formal Shoes', 'Zapatoz Black Formal Shoes', 'http://www.sextoydistributing.com/Merchant2/product/TT641-RedBlack.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2014-04-17 15:57:20', 'TT641', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 43, 0),
        ('848518003577', 'TT641-PurBlack', 1, NULL, 1, 'Reebok Thrust Comfortable Black And Red Floaters', 'floaters', 'Reebok proudly presents an outstanding collection of men and women floater sandals.', '&lt;p&gt;Reebok proudly presents an outstanding collection of men and women floater sandals. Designed using premium quality synthetic leather, these floaters ensure maximum comfort to the wearers.&lt;/p&gt;\r\n', 1, 0, 0, 128, 195, 195, 'percentage', 10, 0, 0, 0, 1, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 4, 0, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:12:03', '', 'Reebok Thrust Comfortable Black And Red Floaters', 'Reebok Thrust Comfortable Black And Red Floaters', 'http://www.sextoydistributing.com/Merchant2/product/TT641-PurBlack.html', '', 'Vacuum Sealed', '', '', '', '', '', '1', '2014-04-30 10:45:14', 'TT641', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 29, 0),
        ('848518003515', 'TT893-BlackBlack', 1, NULL, 1, 'Puma Miami 5 Ind. Flip Flops', 'flip-flops', 'test2 brings to you the latest Puma sports shoes, which are stylish, comfortable, and understand your foot mechanics in a perfect manner ', '&lt;p&gt;test2 brings to you the latest Puma sports shoes, which are stylish, comfortable, and understand your foot mechanics in a perfect manner. Driven by the mission to make walking and playing an enjoyable experience, the brand employs cutting-edge technologies and high-quality material while designing sports shoes. Buy your pair of Puma sports shoes from test2 and be a part of the revolution.&lt;/p&gt;\r\n', 1, 0, 0, 140, 225, 225, 'percentage', 10, 0, 0, 0, 1, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 5, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:12:41', '', 'Puma Miami 5 Ind. Flip Flops', 'Puma Miami 5 Ind. Flip Flops', 'http://www.sextoydistributing.com/Merchant2/product/TT893-BlackBlack.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2013-07-01 18:05:40', 'TT893', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 145, 0),
        ('848518003539', 'TT893-RedBlack', 1, NULL, 1, 'Bacca Bucci Distinct Brown Boots', 'boots', 'Bacca Bucci is one of the renowned brands that offers a wide variety of footwear for men at reasonable prices', '&lt;p&gt;Bacca Bucci is one of the renowned brands that offers a wide variety of footwear for men at reasonable prices. Their product range is widely known for great designs and perfect finish.&lt;/p&gt;\r\n', 1, 0, 0, 140, 225, 225, 'percentage', 10, 0, 0, 0, 1, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 6, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:13:16', '', 'Bacca Bucci Distinct Brown Boots', 'Bacca Bucci Distinct Brown Boots', 'http://www.sextoydistributing.com/Merchant2/product/TT893-RedBlack.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2013-07-01 18:05:40', 'TT893', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 103, 0),
        ('848518003522', 'TT893-PurpleBlack', 1, NULL, 1, 'Abs Eva Hyper Sandals - Blue', 'sandals', 'Enjoy breathable comfort when you buy the ABS All Season Sandals. Inspired by the great outdoors, these ABS sandals are sure to become a daily favorite. ', '&lt;p&gt;Enjoy breathable comfort when you buy the ABS All Season Sandals. Inspired by the great outdoors, these ABS sandals are sure to become a daily favorite. The design of these sandals for men is stylish and make for comfortable walking outdoors or on the beach.Available in multiple styles and colors, these ABS sandals have a unique style that you will love adding to your casual wardrobe. Since we have newly launched e-commerce business , we are offering very attractive prices&lt;/p&gt;\r\n', 1, 0, 0, 140, 225, 225, 'percentage', 10, 0, 0, 0, 1, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 7, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:13:48', '', 'Abs Eva Hyper Sandals - Blue', 'Abs Eva Hyper Sandals - Blue', 'http://www.sextoydistributing.com/Merchant2/product/TT893-PurpleBlack.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2013-07-01 18:05:40', 'TT893', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 97, 0),
        ('811847015016', 'AB467-Small', 1, NULL, 1, 'Dvano Mens Black Casual Elevator Sandals', 'elevator-sandals', 'Made for comfortable and breathable material, these sandals from Dvano offers outmost durability. ', '&lt;p&gt;Made for comfortable and breathable material, these sandals from Dvano offers outmost durability. Be it a walk, casual outing, or any other daily activity, these sandals are going to be your companion everywhere. They also serve as a comfortable solution to your footwear needs. Team them up with casuals and enjoy your day ahead.&lt;/p&gt;\r\n', 1, 0, 0, 50.5, 57, 57, 'percentage', 10, 0, 0, 0, 2, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 8, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:14:31', '', 'Dvano Mens Black Casual Elevator Sandals', 'Dvano Mens Black Casual Elevator Sandals', 'http://www.sextoydistributing.com/Merchant2/product/AB467-Small.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2013-10-18 12:29:55', 'AB467', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 242, 0),
        ('811847015023', 'AB467-Medium', 1, NULL, 1, 'Ridhi Sidhi Brown Ethnic Footwear', 'ethnic', 'Add this classy and comfy pair of brown slip on flats from the house of Ridhi Sidhi to your footwear collection.', '&lt;p&gt;Add this classy and comfy pair of brown slip on flats from the house of Ridhi Sidhi to your footwear collection. Stylish and classy, this pair of slip ons exude a timeless charm. Trendy and ethnic, this pair could well be teamed up with traditional attires as well as casual ones. The fashionably stylish straps lend a classy appeal that remains unmatched. Order this pair of ethnic flats online today.&lt;/p&gt;\r\n', 1, 0, 0, 55, 70.5, 70.5, 'percentage', 10, 0, 0, 0, 2, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 9, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:14:49', '', 'Ridhi Sidhi Brown Ethnic Footwear', 'Ridhi Sidhi Brown Ethnic Footwear', 'http://www.sextoydistributing.com/Merchant2/product/AB467-Medium.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2013-10-18 12:29:36', 'AB467', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 367, 0),
        ('811847015030', 'AB467-Large', 1, NULL, 1, 'Franco Leone Party Boots', 'party-boots', 'Forget the fashion streets of the world. Francoleone, have all that you need to glam up your lifestyle. ', '&lt;p&gt;Forget the fashion streets of the world. Francoleone, have all that you need to glam up your lifestyle. From extensive range of mens shirts to matching footwear and accessories for Mens, they purvey diversity of choices under one umbrella. test2 proudly presents a wide range of footwear for your daily needs. Team them up with your personal style statement and look dashing as ever.&lt;/p&gt;\r\n', 1, 0, 0, 66, 90, 90, 'percentage', 10, 0, 0, 0, 2, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 10, 1, '0', '0', '1', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:15:05', '', 'Franco Leone Party Boots', 'Franco Leone Party Boots', 'http://www.sextoydistributing.com/Merchant2/product/AB467-Large.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2013-10-18 12:29:15', 'AB467', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 218, 0),
        ('848518004154', 'AC851', 1, NULL, 1, 'Indian Terrain Blue Linen Stripes Formals Mens Shirts', 'shirts', 'Indian Terrain Fashions Ltd. [BSE: 533329 | NSE: INDTERRAIN] is one of the leading brands of choice in premium casual wear for men.', '&lt;p&gt;Indian Terrain Fashions Ltd. [BSE: 533329 | NSE: INDTERRAIN] is one of the leading brands of choice in premium casual wear for men. Having opened its first store in September 2000, the company was listed in 2011, following a demerger from its parent company. Our range of Mens apparel includes Shirts, Trousers, T Shirts, Shorts, Mufflers, Knitwear, Jackets, Denim, Boxers and Socks and we will soon be looking to venture into accessories as well.&lt;/p&gt;\r\n', 1, 0, 0, 7.99, 9, 9, 'percentage', 10, 0, 0, 0, 0.09, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 11, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:09:21', '', 'Indian Terrain Blue Linen Stripes Formals Mens Shirts', 'Indian Terrain Blue Linen Stripes Formals Mens Shirts', 'http://www.sextoydistributing.com/Merchant2/product/AC851.html', '', 'Retail Packaging', '', '', '', '', '', '0', '2015-01-14 18:26:00', 'AC851', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 91, 0),
        ('811847000005', 'AB300-Ruby', 1, NULL, 1, 'United Colors Of Benetton Green Cotton T-shirt', 't-shirts', 'United Colors of Benetton is primarily a clothing brand owned by the famous Benetton group. However, the brand is equally popular in numerous other areas like from accessories to eyewear, and from fragrances to footwear.', '&lt;p&gt;&lt;br /&gt;\r\nUnited Colors of Benetton is primarily a clothing brand owned by the famous Benetton group. However, the brand is equally popular in numerous other areas like from accessories to eyewear, and from fragrances to footwear. Refinement mixed with sporting touches, a formal smartness combined with comfort: the United Colors of Benetton will suit a man looking for style and elegance in his everyday wardrobe.&lt;/p&gt;\r\n', 1, 0, 0, 119.95, 135, 135, 'percentage', 10, 0, 0, 0, 0.32, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 12, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:10:17', '', 'United Colors Of Benetton Green Cotton T-shirt', 'United Colors Of Benetton Green Cotton T-shirt', 'http://www.sextoydistributing.com/Merchant2/product/AB300-Ruby.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2014-12-19 12:20:23', 'AB300', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 66, 0),
        ('AB300XL', 'AB300-XLRuby', 1, NULL, 1, 'Puma Cricket Cat Polo puma red', 'puma-red', 'Puma is a leading sports brand that has been designing the best quality products for athletes for over 65 years. ', '&lt;p&gt;&lt;br /&gt;\r\nPuma is a leading sports brand that has been designing the best quality products for athletes for over 65 years. The brand offers sports and performance inspired products in diverse categories such as golf, football, fitness, running and motorsport.&lt;/p&gt;\r\n', 1, 0, 0, 143.95, 207, 207, 'percentage', 10, 0, 0, 0, 0.8, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 13, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:11:03', '', 'Puma Cricket Cat Polo puma red', 'Puma Cricket Cat Polo puma red', 'http://www.sextoydistributing.com/Merchant2/product/AB300-XLRuby.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2014-12-19 12:20:23', 'AB300', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 34, 0),
        ('811847010301', 'MI825-S', 1, NULL, 1, 'Jack &amp; Jones Blue Regular Jeans', 'jeans', 'Presenting a classy addition to your wardrobe from Jack &amp; Jones. The brand caters to one of the most stylish, trend loving and fashionable clients and offers a wide range of apparel and accessory options for both men and women. ', '&lt;p&gt;Presenting a classy addition to your wardrobe from Jack &amp;amp; Jones. The brand caters to one of the most stylish, trend loving and fashionable clients and offers a wide range of apparel and accessory options for both men and women. Made from higher quality material, these products offer durability and long life in addition to highly fashionable and trendy looks. Team it up with your everlasting style and persona, carry it off well with your high spirits and attitude to look amazing as ever.&lt;/p&gt;\r\n', 1, 0, 0, 38, 45, 45, 'percentage', 1, 0, 0, 0, 0.4, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 14, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:11:41', '', 'Jack & Jones Blue Regular Jeans', 'Jack & Jones Blue Regular Jeans', 'http://www.sextoydistributing.com/Merchant2/product/MI825-S.html', '', 'Retail Packaging', '', '', '', '', '', '0', '2014-07-25 12:54:50', 'MI825', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 1308, 0),
        ('811847014460', 'AB470-Blue', 1, NULL, 1, 'United Colors Of Benetton Black Slim Fit Chinos', 'chinos', 'United Colors of Benetton is primarily a clothing brand owned by the famous Benetton group. ', '&lt;p&gt;United Colors of Benetton is primarily a clothing brand owned by the famous Benetton group. However, the brand is equally popular in numerous other areas like from accessories to eyewear, and from fragrances to footwear. Refinement mixed with sporting touches, a formal smartness combined with comfort: the United Colors of Benetton will suit a man looking for style and elegance in his everyday wardrobe.&lt;/p&gt;\r\n', 1, 0, 0, 10.5, 11.85, 11.85, 'percentage', 10, 0, 0, 0, 0.19, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 15, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:12:20', '', 'United Colors Of Benetton Black Slim Fit Chinos', 'United Colors Of Benetton Black Slim Fit Chinos', 'http://www.sextoydistributing.com/Merchant2/product/AB470-Blue.html', '', 'Retail Packaging', '', '', '', '', '', '0', '2013-11-26 16:09:25', 'AB470', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 176, 0),
        ('811847014033', 'AB470-pink', 1, NULL, 1, 'Siyarams Black Poly Blend Formal Unstitched Suit', 'siyarams', ' Product color may slightly vary due to photographic lighting sources or your monitor settings', '&lt;p&gt;Product color may slightly vary due to photographic lighting sources or your monitor settings&lt;/p&gt;\r\n', 1, 0, 0, 10.5, 11.85, 11.85, 'percentage', 10, 0, 0, 0, 0.19, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 16, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:12:58', '', 'Siyarams Black Poly Blend Formal Unstitched Suit', 'Siyarams Black Poly Blend Formal Unstitched Suit', 'http://www.sextoydistributing.com/Merchant2/product/AB470-pink.html', '', 'Retail Packaging', '', '', '', '', '', '0', '2013-11-26 16:09:25', 'AB470', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 192, 0),
        ('811847014453', 'AB470-purple', 1, NULL, 1, 'Abhiyuthan Maroon Long Kurta', 'kurta', 'Mens Merchandise', '&lt;p&gt;Mens Merchandise&lt;/p&gt;\r\n', 1, 0, 0, 10.5, 11.85, 11.85, 'percentage', 10, 0, 0, 0, 0.19, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 17, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:13:31', '', 'Abhiyuthan Maroon Long Kurta', 'Abhiyuthan Maroon Long Kurta', 'http://www.sextoydistributing.com/Merchant2/product/AB470-purple.html', '', 'Retail Packaging', '', '', '', '', '', '0', '2013-11-26 16:09:25', 'AB470', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 112, 0),
        ('848518004499', 'AC898', 1, NULL, 1, 'PUMA MEDIUM GRAY HEATHER ESS JERSEY LONG PANTS, OP', 'long-pants', 'test2.com presents you an exclusive selection of Men clothing from renowned brand Puma. Made of best quality fabrics with attention to details, their selection is a perfect synonymous to class and style.', '&lt;p&gt;&lt;br /&gt;\r\ntest2.com presents you an exclusive selection of Men clothing from renowned brand Puma. Made of best quality fabrics with attention to details, their selection is a perfect synonymous to class and style.&lt;/p&gt;\r\n', 1, 0, 0, 30.95, 34.5, 34.5, 'percentage', 10, 0, 0, 0, 0.4, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 18, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:41', '2015-03-11 06:14:11', '', 'PUMA MEDIUM GRAY HEATHER ESS JERSEY LONG PANTS, OP', 'PUMA MEDIUM GRAY HEATHER ESS JERSEY LONG PANTS, OP', 'http://www.sextoydistributing.com/Merchant2/product/AC898.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2014-12-19 12:22:37', 'AC898', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 138, 0),
        ('848518004000', 'AC877', 1, NULL, 1, 'Celio Green Cotton Round Neck Sweater', 'sweater', 'Pep up your look with this stylish and trendy peice from Celio. The brand Celio is the international fashion brand for men offering essential menswear and high quality fashion products.', '&lt;p&gt;&lt;br /&gt;\r\nPep up your look with this stylish and trendy peice from Celio. The brand Celio is the international fashion brand for men offering essential menswear and high quality fashion products. Browse through all the Celio menswear collections and celio club, off the peg- suits, shirts, pullovers, jeans, trousers, polo shirts, t-shirts and accessories like shoes, bags, underwear. Get the latest trends in mens fashion and find inspiration for style.&lt;/p&gt;\r\n', 1, 0, 0, 12.99, 12, 12, 'percentage', 10, 0, 0, 0, 0.16, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 19, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:42', '2015-03-11 06:04:04', '', 'Celio Green Cotton Round Neck Sweater', 'Celio Green Cotton Round Neck Sweater', 'http://www.sextoydistributing.com/Merchant2/product/AC877.html', '', 'Vacuum Sealed', '', '', '', '', '', '0', '2015-02-05 19:03:01', 'AC877', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 1768, 0),
        ('848518004765', 'AC912', 1, NULL, 1, 'Dazzgear Purple Polyester Sweatshirt', 'sweatshirts', 'Fabric : Polyester Sleeves : Full', '&lt;p&gt;Fabric : Polyester Sleeves : Full&lt;/p&gt;\r\n', 1, 0, 0, 50, 56.25, 56.25, 'percentage', 10, 0, 0, 0, 0.29, 0, 'new', 'simple', '', '', -1, -1, '0000-00-00', '0000-00-00', 1, 1, 20, 1, '0', '0', '0', '0', 1, '2014-10-22 11:28:42', '2015-03-11 06:08:54', '', 'Dazzgear Purple Polyester Sweatshirt', 'Dazzgear Purple Polyester Sweatshirt', 'http://www.sextoydistributing.com/Merchant2/product/AC912.html', '', 'Retail Packaging', '', '', '', '', '', '0', '2014-04-03 11:40:09', 'AC912', '0000-00-00 00:00:00', 'global', 'follow', 'global', 'global', '', '1', '1', '1', '1', '1', '2015-03-10 11:05:13', '0000-00-00 00:00:00', '2015-02-25 12:10:46', '2015-02-25 12:10:46', '1', '', 'default', '0000-00-00 00:00:00', 140, 0);
        ";

            mysql_query($sample, $this->dbconn);

            $sample = "INSERT INTO `" . $this->prefix . "category_product` (`category_id`, `product_id`, `position`, `is_feed`, `date_add`) VALUES
        (1, 1, 0, '0', '2015-03-10 07:16:28'),
        (2, 2, 0, '0', '2015-03-10 07:16:28'),
        (3, 3, 0, '0', '2015-03-10 07:16:28'),
        (4, 4, 0, '0', '2015-03-10 07:16:28'),
        (5, 5, 0, '0', '2015-03-10 07:16:28'),
        (6, 6, 0, '0', '2015-03-10 07:16:28'),
        (7, 7, 0, '0', '2015-03-10 07:16:28'),
        (8, 8, 0, '0', '2015-03-10 07:16:28'),
        (9, 9, 0, '0', '2015-03-10 07:16:28'),
        (10, 10, 0, '0', '2015-03-10 07:16:28'),
        (11, 11, 0, '0', '2015-03-10 07:16:28'),
        (12, 12, 0, '0', '2015-03-10 07:16:28'),
        (13, 13, 0, '0', '2015-03-10 07:16:28'),
        (14, 14, 0, '0', '2015-03-10 07:16:28'),
        (15, 15, 0, '0', '2015-03-10 07:16:28'),
        (16, 16, 0, '0', '2015-03-10 07:16:28'),
        (17, 17, 0, '0', '2015-03-10 07:16:28'),
        (18, 18, 0, '0', '2015-03-10 07:16:28'),
        (19, 19, 0, '0', '2015-03-10 07:16:28'),
        (20, 20, 0, '0', '2015-03-10 07:16:28');
        ";
            //echo $sample; die;
            mysql_query($sample, $this->dbconn);

            $sample = "INSERT INTO `" . $this->prefix . "product_image` (`id`, `product_id`, `image`, `position`, `is_main`, `date_add`, `date_upd`, `from_feed`, `feed_url`, `feed_name`) VALUES
        (1, 19, 'sweater669.jpg', 0, '1', '2015-03-10 06:31:54', '2015-03-10 13:31:54', '0', '', ''),
        (2, 20, 'sweatshirt8601.jpg', 0, '1', '2015-03-10 06:38:57', '2015-03-10 13:38:57', '0', '', ''),
        (4, 17, 'Kurta420.jpg', 0, '1', '2015-03-10 06:41:42', '2015-03-10 13:41:42', '0', '', ''),
        (5, 9, 'Ridhi-Sidhi-Brown-Ethnic-Footwear-SDL376297354-1-e173f23617527.jpg', 0, '1', '2015-03-10 06:42:43', '2015-03-10 13:42:43', '0', '', ''),
        (6, 10, 'Franco-Leone-Tan-Casual-Shoes-SDL331943040-1-8403c43213927.JPG', 0, '1', '2015-03-10 06:43:31', '2015-03-10 13:43:31', '0', '', ''),
        (7, 11, 'Indian-Terrain-Blue-Formals-Shirt-SDL230952943-1-3d8b66492332.jpg', 0, '1', '2015-03-10 06:44:11', '2015-03-10 13:44:11', '0', '', ''),
        (8, 15, 'chinos1635.jpg', 0, '1', '2015-03-10 06:45:16', '2015-03-10 13:45:16', '0', '', ''),
        (9, 6, 'Bacca-Bucci-Brown-Textured-High-SDL160740644-1-26ccf8571022.jpg', 0, '1', '2015-03-10 06:45:59', '2015-03-10 13:46:08', '0', '', ''),
        (10, 1, 'Nike-Gray-Sport-Shoes-SDL041253844-1-e26f484777707.jpg', 0, '1', '2015-03-10 06:46:55', '2015-03-10 13:46:55', '0', '', ''),
        (11, 2, 'Shoe-Island-Coffee-Brown-Ankle-SDL023619285-1-c44e887720689.jpg', 0, '1', '2015-03-10 06:47:33', '2015-03-10 13:47:33', '0', '', ''),
        (12, 5, 'Puma-Gray-Flip-Flops-SDL885911537-1-3989958709263.jpg', 0, '1', '2015-03-10 06:48:06', '2015-03-10 13:48:06', '0', '', ''),
        (13, 7, 'Abs-Eva-Hyper-Sandals-Blue-SDL034888874-1-ac40726708434.jpg', 0, '1', '2015-03-10 06:48:49', '2015-03-10 13:48:49', '0', '', ''),
        (14, 4, 'Reebok-Thrust-Comfortable-Black-And-SDL617898800-1-59d9c59410685.JPG', 0, '1', '2015-03-10 06:49:24', '2015-03-10 13:49:24', '0', '', ''),
        (15, 12, 'United-Colors-Of-Benetton-Green-SDL451812590-1-6911815972975.jpg', 0, '1', '2015-03-10 06:50:01', '2015-03-10 13:50:01', '0', '', ''),
        (16, 16, 'Siyaram-s-Black-Poly-Blend-SDL998899387-1-0680874040434.jpg', 0, '1', '2015-03-10 06:50:40', '2015-03-10 13:50:40', '0', '', ''),
        (17, 18, 'Puma-Gray-SDL421688941-1-c46f767129532.JPG', 0, '1', '2015-03-10 06:51:12', '2015-03-10 13:51:12', '0', '', ''),
        (18, 13, 'Puma-Pink-Half-Polo-T-SDL261122343-1-78c222621525.JPG', 0, '1', '2015-03-10 06:51:47', '2015-03-10 13:51:47', '0', '', ''),
        (19, 8, 'Dvano-Shoes-Black-Casual-Shoes-SDL876860988-1-702be92947584.jpg', 0, '1', '2015-03-10 06:52:25', '2015-03-10 13:52:25', '0', '', ''),
        (20, 3, 'Zapatoz-Black-Formal-Shoes-SDL686942357-1-5c0f451300368.JPG', 0, '1', '2015-03-10 06:53:04', '2015-03-10 13:53:04', '0', '', ''),
        (21, 14, 'Jack-Jones-Blue-Regular-Jeans-SDL946924190-1-4796f24451367.jpg', 0, '1', '2015-03-10 06:53:40', '2015-03-10 13:53:40', '0', '', ''),
        (22, 19, 'Celio-Yellow-Cotton-Round-Neck-SDL737275101-1-f520b52738769.jpg', 0, '0', '2015-03-12 04:17:48', '2015-03-12 11:17:48', '0', '', ''),
        (23, 19, 'Status-Quo-Green-Cotton-Round-SDL449404147-1-850a954973656.jpg', 0, '0', '2015-03-12 04:19:30', '2015-03-12 11:19:30', '0', '', ''),
        (24, 20, 'Grand-Bear-Purple-Polyester-Sweatshirt-SDL196155497-1-6bcf063542594.jpg', 0, '0', '2015-03-12 04:20:27', '2015-03-12 11:20:27', '0', '', ''),
        (25, 20, 'Grand-Bear-Purple-Polyester-Sweatshirt-SDL067559067-1-d9e2781980842.jpg', 0, '0', '2015-03-12 04:20:48', '2015-03-12 11:20:48', '0', '', ''),
        (26, 4, 'Eprilla-Red-Black-Sports-Sandals-SDL174821246-1-dfc9b35264901.jpg', 0, '0', '2015-03-12 04:21:54', '2015-03-12 11:21:54', '0', '', ''),
        (27, 4, 'Sakata-Attractive-Blue-And-Red-SDL493522994-1-ad54247219268.JPG', 0, '0', '2015-03-12 04:22:24', '2015-03-12 11:22:24', '0', '', ''),
        (28, 17, 'Abhiyuthan-Maroon-Pathani-Kurta-SDL037754490-1-81ece4213031.JPG', 0, '0', '2015-03-12 04:23:25', '2015-03-12 11:23:25', '0', '', ''),
        (29, 11, 'Indian-Terrain-Blue-Formals-Shirt-SDL514134401-1-0a51819889565.jpg', 0, '0', '2015-03-12 04:24:06', '2015-03-12 11:24:06', '0', '', ''),
        (30, 11, 'Indian-Terrain-Green-Formals-Shirt-SDL409460122-1-a169997114514.jpg', 0, '0', '2015-03-12 04:24:31', '2015-03-12 11:24:31', '0', '', ''),
        (31, 1, 'Nike-Ballista-Iv-Msl-Sport-SDL494776639-1-2903a70210928.jpg', 0, '0', '2015-03-12 04:25:41', '2015-03-12 11:25:41', '0', '', ''),
        (32, 1, 'Nike-Emerge-Black-Sports-Shoes-SDL165355644-1-36cca7514550.jpg', 0, '0', '2015-03-12 04:26:02', '2015-03-12 11:26:02', '0', '', ''),
        (33, 12, 'United-Colors-Of-Benetton-Green-SDL442908532-1-cf9b738055861.jpg', 0, '0', '2015-03-12 04:26:42', '2015-03-12 11:26:42', '0', '', ''),
        (34, 12, 'United-Colors-Of-Benetton-Blue-SDL456026476-1-a60f348604205.jpg', 0, '0', '2015-03-12 04:26:57', '2015-03-12 11:26:57', '0', '', ''),
        (35, 2, 'Shoe-Island-Brown-Ankle-Length-SDL022892256-1-19bf933135656.jpg', 0, '0', '2015-03-12 04:27:36', '2015-03-12 11:27:36', '0', '', ''),
        (36, 2, 'Shoe-Island-Tan-Casual-Shoes-SDL830574222-1-fb8be82942786.JPG', 0, '0', '2015-03-12 04:28:01', '2015-03-12 11:28:01', '0', '', ''),
        (37, 3, 'Zapatoz-Black-Formal-Shoes-SDL866246219-1-2bf5986654432.JPG', 0, '0', '2015-03-12 04:28:43', '2015-03-12 11:28:43', '0', '', ''),
        (38, 3, 'Zapatoz-Brown-Formal-Shoes-SDL687636122-1-c120330752011.JPG', 0, '0', '2015-03-12 04:29:05', '2015-03-12 11:29:05', '0', '', ''),
        (39, 13, 'Puma-Blue-Half-Polo-T-SDL317258794-1-0df978640135.JPG', 0, '0', '2015-03-12 04:30:09', '2015-03-12 11:30:09', '0', '', ''),
        (40, 13, 'Tsg-Classe-Multi-color-Solid-SDL130042066-1-8091d83691421.jpg', 0, '0', '2015-03-12 04:30:32', '2015-03-12 11:30:32', '0', '', ''),
        (41, 14, 'Jack-Jones-Blue-Regular-Jeans-SDL353120016-1-17d0621416888.jpg', 0, '0', '2015-03-12 04:31:23', '2015-03-12 11:31:23', '0', '', ''),
        (42, 14, 'Jack-Jones-Blue-Slim-Jeans-SDL275076294-1-92aa986024799.jpg', 0, '0', '2015-03-12 04:31:43', '2015-03-12 11:31:43', '0', '', ''),
        (43, 15, 'United-Colors-Of-Benetton-Brown-SDL620364262-1-03ca423780941.jpg', 0, '0', '2015-03-12 04:32:32', '2015-03-12 11:32:32', '0', '', ''),
        (44, 15, 'United-Colors-Of-Benetton-Beige-SDL824854775-1-8681012349329.jpg', 0, '0', '2015-03-12 04:33:00', '2015-03-12 11:33:00', '0', '', ''),
        (45, 5, 'Puma-Black-Slippers-SDL915477605-1-d052392819164.JPG', 0, '0', '2015-03-12 04:33:51', '2015-03-12 11:33:51', '0', '', ''),
        (46, 5, 'Puma-Flip-Flops-SDL158791920-1-d49ef10027360.jpg', 0, '0', '2015-03-12 04:34:08', '2015-03-12 11:34:08', '0', '', ''),
        (47, 16, 'Siyaram-s-Unstitched-White-Suit-SDL283329700-1-b139b88487783.jpg', 0, '0', '2015-03-12 04:34:52', '2015-03-12 11:34:52', '0', '', ''),
        (48, 16, 'Siyaram-s-Black-Poly-Blend-SDL918027852-1-4c7ab42505523.jpg', 0, '0', '2015-03-12 04:35:11', '2015-03-12 11:35:11', '0', '', ''),
        (49, 7, 'ABS-Men-s-Hyper-Brown-SDL739427686-1-cdb3a46916476.jpg', 0, '0', '2015-03-12 04:35:49', '2015-03-12 11:35:49', '0', '', ''),
        (50, 7, 'Fuzion-Orange-Mesh-Or-Textile-SDL439133062-1-96c1d14324216.jpg', 0, '0', '2015-03-12 04:36:10', '2015-03-12 11:36:10', '0', '', ''),
        (51, 9, 'Ridhi-Sidhi-Brown-Ethnic-Footwear-SDL558019392-1-054c776746726.jpg', 0, '0', '2015-03-12 04:36:57', '2015-03-12 11:36:57', '0', '', ''),
        (52, 9, 'Ridhi-Sidhi-Brown-Leather-Men-SDL270682995-1-eb91652084639.jpg', 0, '0', '2015-03-12 04:37:14', '2015-03-12 11:37:14', '0', '', ''),
        (53, 6, 'Bacca-Bucci-Robust-Brown-Boots-SDL190185545-1-88f9499021087.JPG', 0, '0', '2015-03-12 04:37:57', '2015-03-12 11:37:57', '0', '', ''),
        (54, 6, 'Bacca-Bucci-Black-Boots-SDL296717684-1-75a1f72762847.jpg', 0, '0', '2015-03-12 04:38:18', '2015-03-12 11:38:18', '0', '', ''),
        (55, 18, 'Puma-Gray-SDL420185959-1-9daaf79153029.JPG', 0, '0', '2015-03-12 04:39:06', '2015-03-12 11:39:06', '0', '', ''),
        (56, 18, 'Puma-Brown-SDL426631951-1-5d55a43608020.JPG', 0, '0', '2015-03-12 04:39:25', '2015-03-12 11:39:25', '0', '', ''),
        (57, 8, 'Dvano-Shoes-Black-Casual-Shoes-SDL737955570-1-d304948946322.jpg', 0, '0', '2015-03-12 04:40:06', '2015-03-12 11:40:06', '0', '', ''),
        (58, 8, 'Dvano-Shoes-Black-Casual-Shoes-SDL894647648-1-0c15183192071.jpg', 0, '0', '2015-03-12 04:40:22', '2015-03-12 11:40:22', '0', '', ''),
        (59, 10, 'Franco-Leone-Tan-Boots-SDL559090920-1-bde2549166437.JPG', 0, '0', '2015-03-12 04:41:11', '2015-03-12 11:41:11', '0', '', ''),
        (60, 10, 'Franco-Leone-Black-Formal-Shoes-SDL897248282-1-aef0571883438.jpg', 0, '0', '2015-03-12 04:41:27', '2015-03-12 11:41:27', '0', '', '');
        ";
//echo $sample; die;
            mysql_query($sample, $this->dbconn);

            $sample = "INSERT INTO `" . $this->prefix . "url_rewrite` (`page`, `module`, `module_id`, `urlname`, `date_add`, `date_upd`) VALUES
        ('product', 'product', 19, 'sweater', '2015-03-10 22:46:53', '2015-03-11 06:07:30'),
        ('product', 'product', 20, 'sweatshirts', '2015-03-10 23:08:54', '2015-03-11 06:08:54'),
        ('product', 'product', 11, 'shirts', '2015-03-10 23:09:21', '2015-03-11 06:09:21'),
        ('product', 'product', 1, 'sports-shoes', '2015-03-10 23:09:59', '2015-03-11 06:09:59'),
        ('product', 'product', 12, 't-shirts', '2015-03-10 23:10:17', '2015-03-11 06:10:17'),
        ('product', 'product', 2, 'ankle-shoes', '2015-03-10 23:10:44', '2015-03-11 06:10:44'),
        ('product', 'product', 13, 'puma-red', '2015-03-10 23:11:03', '2015-03-11 06:11:03'),
        ('product', 'product', 3, 'formal-shoes', '2015-03-10 23:11:23', '2015-03-11 06:11:23'),
        ('product', 'product', 14, 'jeans', '2015-03-10 23:11:41', '2015-03-11 06:11:41'),
        ('product', 'product', 4, 'floaters', '2015-03-10 23:12:03', '2015-03-11 06:12:03'),
        ('product', 'product', 15, 'chinos', '2015-03-10 23:12:20', '2015-03-11 06:12:20'),
        ('product', 'product', 5, 'flip-flops', '2015-03-10 23:12:41', '2015-03-11 06:12:41'),
        ('product', 'product', 16, 'siyarams', '2015-03-10 23:12:58', '2015-03-11 06:12:58'),
        ('product', 'product', 6, 'boots', '2015-03-10 23:13:16', '2015-03-11 06:13:16'),
        ('product', 'product', 17, 'kurta', '2015-03-10 23:13:31', '2015-03-11 06:13:31'),
        ('product', 'product', 7, 'sandals', '2015-03-10 23:13:48', '2015-03-11 06:13:48'),
        ('product', 'product', 18, 'long-pants', '2015-03-10 23:14:11', '2015-03-11 06:14:11'),
        ('product', 'product', 8, 'elevator-sandals', '2015-03-10 23:14:31', '2015-03-11 06:14:31'),
        ('product', 'product', 9, 'ethnic', '2015-03-10 23:14:49', '2015-03-11 06:14:49'),
        ('product', 'product', 10, 'party-boots', '2015-03-10 23:15:05', '2015-03-11 06:15:05'),
        ('shop', 'category', 1, 'all-sports-shoes', '2015-03-11 00:05:08', '2015-03-11 07:05:08'),
        ('shop', 'category', 20, 'sweatshirt', '2015-03-11 00:12:53', '2015-03-11 07:12:53'),
        ('shop', 'category', 19, 'swaters', '2015-03-11 00:13:14', '2015-03-11 07:13:14'),
        ('shop', 'category', 18, 'tracksuits', '2015-03-11 00:13:32', '2015-03-11 07:13:32'),
        ('shop', 'category', 17, 'kurta-pyjmas', '2015-03-11 00:14:07', '2015-03-11 07:14:07'),
        ('shop', 'category', 16, 'suiting-shirtings', '2015-03-11 00:14:26', '2015-03-11 07:14:26'),
        ('shop', 'category', 15, 'trouser-chinos', '2015-03-11 00:14:43', '2015-03-11 07:14:43'),
        ('shop', 'category', 14, 'jean', '2015-03-11 00:14:55', '2015-03-11 07:14:55'),
        ('shop', 'category', 13, 'polo-tshirt', '2015-03-11 00:15:48', '2015-03-11 07:15:48'),
        ('shop', 'category', 12, 'tshirt', '2015-03-11 00:16:02', '2015-03-11 07:16:02'),
        ('shop', 'category', 11, 'shirt', '2015-03-11 00:16:16', '2015-03-11 07:16:16'),
        ('shop', 'category', 10, 'party-shoe', '2015-03-11 00:16:30', '2015-03-11 07:16:30'),
        ('shop', 'category', 9, 'ethnics', '2015-03-11 00:16:51', '2015-03-11 07:16:51'),
        ('shop', 'category', 8, 'speciality-footwear', '2015-03-11 00:17:09', '2015-03-11 07:17:09'),
        ('shop', 'category', 7, 'sandal', '2015-03-11 00:17:30', '2015-03-11 07:17:30'),
        ('shop', 'category', 6, 'boot', '2015-03-11 00:17:50', '2015-03-11 07:17:50'),
        ('shop', 'category', 5, 'slippers', '2015-03-11 00:18:02', '2015-03-11 07:18:02'),
        ('shop', 'category', 4, 'floater', '2015-03-11 00:18:14', '2015-03-11 07:18:14'),
        ('shop', 'category', 3, 'formal-shoe', '2015-03-11 00:18:27', '2015-03-11 07:18:27'),
        ('shop', 'category', 2, 'casual-shoe', '2015-03-11 00:18:40', '2015-03-11 07:18:40');
        ";
            mysql_query($sample, $this->dbconn);

            $sample = "INSERT INTO `" . $this->prefix . "navigation` (`parent_id`, `nav_id`, `nav_class`, `is_mega_menu`, `navigation_title`, `navigation_link`, `position`, `is_active`, `navigation_query`, `is_external`, `open_in_new`, `is_deleted`, `date_add`, `date_upd`) VALUES
        (0, 1, 'red', 1, 'New', 'new_products', NULL, 1, NULL, 0, 0, 0, '2015-03-11 00:01:30', '2015-03-12 12:19:46'),
        (0, 1, '', 1, 'Shirts &amp; T-Shirts', '', NULL, 1, NULL, 1, 0, 0, '2015-03-12 05:02:00', '2015-03-12 12:19:47'),
        (2, 1, '', 0, 'Shirts', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-03-12 05:02:52', '2015-03-12 12:02:52'),
        (2, 1, '', 0, 'T Shirts', 'shop', NULL, 1, 'id=12', 0, 0, 0, '2015-03-12 05:04:24', '2015-03-12 12:04:24'),
        (2, 1, '', 0, 'Polo T-Shirts', 'shop', NULL, 1, 'id=13', 0, 0, 0, '2015-03-12 05:04:45', '2015-03-12 12:04:45'),
        (0, 1, '', 1, 'Jeans , Suitings &amp; Shirtings', '', NULL, 1, NULL, 1, 0, 0, '2015-03-12 05:06:33', '2015-03-12 12:19:48'),
        (6, 1, '', 0, 'Jeans', 'shop', NULL, 1, 'id=14', 0, 0, 0, '2015-03-12 05:07:15', '2015-03-12 12:07:15'),
        (6, 1, '', 0, 'Suitings & Shirtings', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-03-12 05:08:03', '2015-03-12 12:08:03'),
        (2, 1, '', 0, 'Sweat Shirts', 'shop', NULL, 1, 'id=20', 0, 0, 0, '2015-03-12 05:09:04', '2015-03-12 12:09:04'),
        (6, 1, '', 0, 'Trousers', 'shop', NULL, 1, 'id=15', 0, 0, 0, '2015-03-12 05:09:58', '2015-03-12 12:09:58'),
        (6, 1, '', 0, 'Tracksuits', 'shop', NULL, 1, 'id=18', 0, 0, 0, '2015-03-12 05:10:25', '2015-03-12 12:10:25'),
        (0, 1, '', 1, 'Shoes', NULL, NULL, 1, NULL, 1, 0, 0, '2015-03-12 05:11:01', '2015-03-12 12:19:49'),
        (12, 1, '', 0, 'Formal shoes', 'shop', NULL, 1, 'id=3', 0, 0, 0, '2015-03-12 05:11:29', '2015-03-12 12:11:29'),
        (12, 1, '', 0, 'Casual Shoes', 'shop', NULL, 1, 'id=2', 0, 0, 0, '2015-03-12 05:11:46', '2015-03-12 12:11:46'),
        (12, 1, '', 0, 'Party Shoes', 'shop', NULL, 1, 'id=10', 0, 0, 0, '2015-03-12 05:12:02', '2015-03-12 12:12:02'),
        (0, 1, '', 1, 'Boots & Sandals', NULL, NULL, 1, NULL, 1, 0, 0, '2015-03-12 05:13:13', '2015-03-12 12:19:52'),
        (16, 1, '', 0, 'Boots', 'shop', NULL, 1, 'id=6', 0, 0, 0, '2015-03-12 05:13:31', '2015-03-12 12:13:31'),
        (16, 1, '', 0, 'Sandals', 'shop', NULL, 1, 'id=7', 0, 0, 0, '2015-03-12 05:13:49', '2015-03-12 12:13:49'),
        (16, 1, '', 0, 'Speciality Footwear', 'shop', NULL, 1, 'id=8', 0, 0, 0, '2015-03-12 05:14:28', '2015-03-12 12:14:28'),
        (0, 1, '', 1, 'Kurtas & Ethnics', NULL, NULL, 1, NULL, 1, 0, 0, '2015-03-12 05:15:01', '2015-03-12 12:19:53'),
        (20, 1, '', 0, 'Kurtas', 'shop', NULL, 1, 'id=17', 0, 0, 0, '2015-03-12 05:15:20', '2015-03-12 12:15:20'),
        (20, 1, '', 0, 'Ethnics', 'shop', NULL, 1, 'id=9', 0, 0, 0, '2015-03-12 05:15:37', '2015-03-12 12:15:37'),
        (0, 1, '', 1, 'Floaters & Slippers', NULL, NULL, 1, NULL, 1, 0, 0, '2015-03-12 05:16:23', '2015-03-12 12:19:54'),
        (23, 1, '', 0, 'Floaters', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-03-12 05:16:50', '2015-03-12 12:16:50'),
        (23, 1, '', 0, 'Slippers & Flip flops', 'shop', NULL, 1, 'id=5', 0, 0, 0, '2015-03-12 05:17:13', '2015-03-12 12:17:13'),
        (0, 1, '', 1, 'Sweaters', NULL, NULL, 1, NULL, 1, 0, 0, '2015-03-12 05:18:30', '2015-03-12 12:19:56'),
        (26, 1, '', 0, 'Sweaters', 'shop', NULL, 1, 'id=19', 0, 0, 0, '2015-03-12 05:19:14', '2015-03-12 12:19:14'),
        (0, 1, '', 1, 'All', NULL, NULL, 1, NULL, 1, 0, 0, '2015-03-12 05:21:09', '2015-03-12 12:47:51'),
        (28, 1, '', 0, 'Shirts', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-03-12 05:21:40', '2015-03-12 12:21:40'),
        (28, 1, '', 0, 'T Shirts', 'shop', NULL, 1, 'id=12', 0, 0, 0, '2015-03-12 05:21:52', '2015-03-12 12:21:52'),
        (28, 1, '', 0, 'Jeans', 'shop', NULL, 1, 'id=14', 0, 0, 0, '2015-03-12 05:22:03', '2015-03-12 12:22:03'),
        (28, 1, '', 0, 'Polo T-Shirts', 'shop', NULL, 1, 'id=13', 0, 0, 0, '2015-03-12 05:22:16', '2015-03-12 12:22:16'),
        (28, 1, '', 0, 'Sports Shoes', 'shop', NULL, 1, 'id=1', 0, 0, 0, '2015-03-12 05:22:28', '2015-03-12 12:22:28'),
        (28, 1, '', 0, 'Formal shoes', 'shop', NULL, 1, 'id=3', 0, 0, 0, '2015-03-12 05:22:48', '2015-03-12 12:22:48'),
        (28, 1, '', 0, 'Party Shoes', 'shop', NULL, 1, 'id=10', 0, 0, 0, '2015-03-12 05:23:23', '2015-03-12 12:23:23'),
        (28, 1, '', 0, 'Kurtas', 'shop', NULL, 1, 'id=17', 0, 0, 0, '2015-03-12 05:23:40', '2015-03-12 12:23:40'),
        (28, 1, '', 0, 'Ethnics', 'shop', NULL, 1, 'id=9', 0, 0, 0, '2015-03-12 05:23:53', '2015-03-12 12:23:53');
        ";
            mysql_query($sample, $this->dbconn);
        }

        $sample = "INSERT INTO `" . $this->prefix . "admin_user` (`id`, `admin_role_id`, `username`, `password`, `phone`, `email`, `last_sign_in`, `is_active`, `is_deleted`, `last_update_date`, `ip_address`, `last_access`, `allow_pages`, `is_loggedin`) VALUES
                (1, 1, '$admin_user', '$admin_password', '', '$store_email', NULL, '1', '0', NULL, NULL, '2014-06-12 22:30:20', '*', '1');
        ";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "country` (`id`, `iso2`, `short_name`, `long_name`, `iso3`, `numcode`, `calling_code`, `flag`, `contains_states`, `need_zip_code`, `is_active`, `date_add`) VALUES
        (1, 'AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', '004', '93', 'af', '1', '1', '0', CURRENT_TIMESTAMP),
        (2, 'AX', 'Aland Islands', '&Aring;land Islands', 'ALA', '248', '358', 'ax', '1', '1', '0', CURRENT_TIMESTAMP),
        (4, 'DZ', 'Algeria', 'People''s Democratic Republic of Algeria', 'DZA', '012', '213', 'dz', '1', '1', '0', CURRENT_TIMESTAMP),
        (5, 'AS', 'American Samoa', 'American Samoa', 'ASM', '016', '1+684', 'as', '1', '1', '0', CURRENT_TIMESTAMP),
        (6, 'AD', 'Andorra', 'Principality of Andorra', 'AND', '020', '376', 'ad', '1', '1', '0', CURRENT_TIMESTAMP),
        (7, 'AO', 'Angola', 'Republic of Angola', 'AGO', '024', '244', 'ao', '1', '1', '0', CURRENT_TIMESTAMP),
        (8, 'AI', 'Anguilla', 'Anguilla', 'AIA', '660', '1+264', 'ai', '1', '1', '0', CURRENT_TIMESTAMP),
        (9, 'AQ', 'Antarctica', 'Antarctica', 'ATA', '010', '672', 'aq', '1', '1', '0', CURRENT_TIMESTAMP),
        (10, 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', '028', '1+268', 'ag', '1', '1', '0', CURRENT_TIMESTAMP),
        (11, 'AR', 'Argentina', 'Argentine Republic', 'ARG', '032', '54', 'ar', '1', '1', '0', CURRENT_TIMESTAMP),
        (13, 'AW', 'Aruba', 'Aruba', 'ABW', '533', '297', 'aw', '1', '1', '0', CURRENT_TIMESTAMP),
        (14, 'AU', 'Australia', 'Commonwealth of Australia', 'AUS', '036', '61', 'au', '1', '1', '0', CURRENT_TIMESTAMP),
        (15, 'AT', 'Austria', 'Republic of Austria', 'AUT', '040', '43', 'at', '1', '1', '0', CURRENT_TIMESTAMP),
        (17, 'BS', 'Bahamas', 'Commonwealth of The Bahamas', 'BHS', '044', '1+242', 'bs', '1', '1', '0', CURRENT_TIMESTAMP),
        (19, 'BD', 'Bangladesh', 'People''s Republic of Bangladesh', 'BGD', '050', '880', 'bd', '1', '1', '0', CURRENT_TIMESTAMP),
        (20, 'BB', 'Barbados', 'Barbados', 'BRB', '052', '1+246', 'bb', '1', '1', '0', CURRENT_TIMESTAMP),
        (22, 'BE', 'Belgium', 'Kingdom of Belgium', 'BEL', '056', '32', 'be', '1', '1', '0', CURRENT_TIMESTAMP),
        (23, 'BZ', 'Belize', 'Belize', 'BLZ', '084', '501', 'bz', '1', '1', '0', CURRENT_TIMESTAMP),
        (24, 'BJ', 'Benin', 'Republic of Benin', 'BEN', '204', '229', 'bj', '1', '1', '0', CURRENT_TIMESTAMP),
        (25, 'BM', 'Bermuda', 'Bermuda Islands', 'BMU', '060', '1+441', 'bm', '1', '1', '0', CURRENT_TIMESTAMP),
        (26, 'BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', '064', '975', 'bt', '1', '1', '0', CURRENT_TIMESTAMP),
        (27, 'BO', 'Bolivia', 'Plurinational State of Bolivia', 'BOL', '068', '591', 'bo', '1', '1', '0', CURRENT_TIMESTAMP),
        (28, 'BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BES', '535', '599', 'bq', '1', '1', '0', CURRENT_TIMESTAMP),
        (30, 'BW', 'Botswana', 'Republic of Botswana', 'BWA', '072', '267', 'bw', '1', '1', '0', CURRENT_TIMESTAMP),
        (31, 'BV', 'Bouvet Island', 'Bouvet Island', 'BVT', '074', 'NONE', 'bv', '1', '1', '0', CURRENT_TIMESTAMP),
        (32, 'BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', '076', '55', 'br', '1', '1', '0', CURRENT_TIMESTAMP),
        (33, 'IO', 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'IOT', '086', '246', 'io', '1', '1', '0', CURRENT_TIMESTAMP),
        (34, 'BN', 'Brunei', 'Brunei Darussalam', 'BRN', '096', '673', 'bn', '1', '1', '0', CURRENT_TIMESTAMP),
        (36, 'BF', 'Burkina Faso', 'Burkina Faso', 'BFA', '854', '226', 'bf', '1', '1', '0', CURRENT_TIMESTAMP),
        (37, 'BI', 'Burundi', 'Republic of Burundi', 'BDI', '108', '257', 'bi', '1', '1', '0', CURRENT_TIMESTAMP),
        (38, 'KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', '116', '855', 'kh', '1', '1', '0', CURRENT_TIMESTAMP),
        (39, 'CM', 'Cameroon', 'Republic of Cameroon', 'CMR', '120', '237', 'cm', '1', '1', '0', CURRENT_TIMESTAMP),
        (40, 'CA', 'Canada', 'Canada', 'CAN', '124', '1', 'ca', '1', '1', '1', CURRENT_TIMESTAMP),
        (41, 'CV', 'Cape Verde', 'Republic of Cape Verde', 'CPV', '132', '238', 'cv', '1', '1', '0', CURRENT_TIMESTAMP),
        (42, 'KY', 'Cayman Islands', 'The Cayman Islands', 'CYM', '136', '1+345', 'ky', '1', '1', '0', CURRENT_TIMESTAMP),
        (44, 'TD', 'Chad', 'Republic of Chad', 'TCD', '148', '235', 'td', '1', '1', '0', CURRENT_TIMESTAMP),
        (45, 'CL', 'Chile', 'Republic of Chile', 'CHL', '152', '56', 'cl', '1', '1', '0', CURRENT_TIMESTAMP),
        (47, 'CX', 'Christmas Island', 'Christmas Island', 'CXR', '162', '61', 'cx', '1', '1', '0', CURRENT_TIMESTAMP),
        (48, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', '166', '61', 'cc', '1', '1', '0', CURRENT_TIMESTAMP),
        (49, 'CO', 'Colombia', 'Republic of Colombia', 'COL', '170', '57', 'co', '1', '1', '0', CURRENT_TIMESTAMP),
        (50, 'KM', 'Comoros', 'Union of the Comoros', 'COM', '174', '269', 'km', '1', '1', '0', CURRENT_TIMESTAMP),
        (51, 'CG', 'Congo', 'Republic of the Congo', 'COG', '178', '242', 'cg', '1', '1', '0', CURRENT_TIMESTAMP),
        (52, 'CK', 'Cook Islands', 'Cook Islands', 'COK', '184', '682', 'ck', '1', '1', '0', CURRENT_TIMESTAMP),
        (53, 'CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', '188', '506', 'cr', '1', '1', '0', CURRENT_TIMESTAMP),
        (54, 'CI', 'Cote d''ivoire (Ivory Coast)', 'Republic of C&ocirc;te D''Ivoire (Ivory Coast)', 'CIV', '384', '225', 'ci', '1', '1', '0', CURRENT_TIMESTAMP),
        (56, 'CU', 'Cuba', 'Republic of Cuba', 'CUB', '192', '53', 'cu', '1', '1', '0', CURRENT_TIMESTAMP),
        (57, 'CW', 'Curacao', 'Cura&ccedil;ao', 'CUW', '531', '599', 'cw', '1', '1', '0', CURRENT_TIMESTAMP),
        (60, 'CD', 'Democratic Republic of the Congo', 'Democratic Republic of the Congo', 'COD', '180', '243', 'cd', '1', '1', '0', CURRENT_TIMESTAMP),
        (61, 'DK', 'Denmark', 'Kingdom of Denmark', 'DNK', '208', '45', 'dk', '1', '1', '0', CURRENT_TIMESTAMP),
        (62, 'DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', '262', '253', 'dj', '1', '1', '0', CURRENT_TIMESTAMP),
        (63, 'DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', '212', '1+767', 'dm', '1', '1', '0', CURRENT_TIMESTAMP),
        (64, 'DO', 'Dominican Republic', 'Dominican Republic', 'DOM', '214', '1+809, 8', 'do', '1', '1', '0', CURRENT_TIMESTAMP),
        (65, 'EC', 'Ecuador', 'Republic of Ecuador', 'ECU', '218', '593', 'ec', '1', '1', '0', CURRENT_TIMESTAMP),
        (67, 'SV', 'El Salvador', 'Republic of El Salvador', 'SLV', '222', '503', 'sv', '1', '1', '0', CURRENT_TIMESTAMP),
        (68, 'GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', '226', '240', 'gq', '1', '1', '0', CURRENT_TIMESTAMP),
        (69, 'ER', 'Eritrea', 'State of Eritrea', 'ERI', '232', '291', 'er', '1', '1', '0', CURRENT_TIMESTAMP),
        (70, 'EE', 'Estonia', 'Republic of Estonia', 'EST', '233', '372', 'ee', '1', '1', '0', CURRENT_TIMESTAMP),
        (71, 'ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', '231', '251', 'et', '1', '1', '0', CURRENT_TIMESTAMP),
        (72, 'FK', 'Falkland Islands (Malvinas)', 'The Falkland Islands (Malvinas)', 'FLK', '238', '500', 'fk', '1', '1', '0', CURRENT_TIMESTAMP),
        (73, 'FO', 'Faroe Islands', 'The Faroe Islands', 'FRO', '234', '298', 'fo', '1', '1', '0', CURRENT_TIMESTAMP),
        (74, 'FJ', 'Fiji', 'Republic of Fiji', 'FJI', '242', '679', 'fj', '1', '1', '0', CURRENT_TIMESTAMP),
        (75, 'FI', 'Finland', 'Republic of Finland', 'FIN', '246', '358', 'fi', '1', '1', '0', CURRENT_TIMESTAMP),
        (76, 'FR', 'France', 'French Republic', 'FRA', '250', '33', 'fr', '1', '1', '0', CURRENT_TIMESTAMP),
        (77, 'GF', 'French Guiana', 'French Guiana', 'GUF', '254', '594', 'gf', '1', '1', '0', CURRENT_TIMESTAMP),
        (78, 'PF', 'French Polynesia', 'French Polynesia', 'PYF', '258', '689', 'pf', '1', '1', '0', CURRENT_TIMESTAMP),
        (79, 'TF', 'French Southern Territories', 'French Southern Territories', 'ATF', '260', NULL, 'tf', '1', '1', '0', CURRENT_TIMESTAMP),
        (80, 'GA', 'Gabon', 'Gabonese Republic', 'GAB', '266', '241', 'ga', '1', '1', '0', CURRENT_TIMESTAMP),
        (81, 'GM', 'Gambia', 'Republic of The Gambia', 'GMB', '270', '220', 'gm', '1', '1', '0', CURRENT_TIMESTAMP),
        (83, 'DE', 'Germany', 'Federal Republic of Germany', 'DEU', '276', '49', 'de', '1', '1', '0', CURRENT_TIMESTAMP),
        (85, 'GI', 'Gibraltar', 'Gibraltar', 'GIB', '292', '350', 'gi', '1', '1', '0', CURRENT_TIMESTAMP),
        (86, 'GR', 'Greece', 'Hellenic Republic', 'GRC', '300', '30', 'gr', '1', '1', '0', CURRENT_TIMESTAMP),
        (87, 'GL', 'Greenland', 'Greenland', 'GRL', '304', '299', 'gl', '1', '1', '0', CURRENT_TIMESTAMP),
        (88, 'GD', 'Grenada', 'Grenada', 'GRD', '308', '1+473', 'gd', '1', '1', '0', CURRENT_TIMESTAMP),
        (89, 'GP', 'Guadaloupe', 'Guadeloupe', 'GLP', '312', '590', 'gp', '1', '1', '0', CURRENT_TIMESTAMP),
        (90, 'GU', 'Guam', 'Guam', 'GUM', '316', '1+671', 'gu', '1', '1', '0', CURRENT_TIMESTAMP),
        (91, 'GT', 'Guatemala', 'Republic of Guatemala', 'GTM', '320', '502', 'gt', '1', '1', '0', CURRENT_TIMESTAMP),
        (92, 'GG', 'Guernsey', 'Guernsey', 'GGY', '831', '44', 'gg', '1', '1', '0', CURRENT_TIMESTAMP),
        (93, 'GN', 'Guinea', 'Republic of Guinea', 'GIN', '324', '224', 'gn', '1', '1', '0', CURRENT_TIMESTAMP),
        (94, 'GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', '624', '245', 'gw', '1', '1', '0', CURRENT_TIMESTAMP),
        (95, 'GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', '328', '592', 'gy', '1', '1', '0', CURRENT_TIMESTAMP),
        (96, 'HT', 'Haiti', 'Republic of Haiti', 'HTI', '332', '509', 'ht', '1', '1', '0', CURRENT_TIMESTAMP),
        (97, 'HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', '334', 'NONE', 'hm', '1', '1', '0', CURRENT_TIMESTAMP),
        (98, 'HN', 'Honduras', 'Republic of Honduras', 'HND', '340', '504', 'hn', '1', '1', '0', CURRENT_TIMESTAMP),
        (99, 'HK', 'Hong Kong', 'Hong Kong', 'HKG', '344', '852', 'hk', '1', '1', '0', CURRENT_TIMESTAMP),
        (101, 'IS', 'Iceland', 'Republic of Iceland', 'ISL', '352', '354', 'is', '1', '1', '0', CURRENT_TIMESTAMP),
        (102, 'IN', 'India', 'Republic of India', 'IND', '356', '91', 'in', '1', '1', '0', CURRENT_TIMESTAMP),
        (106, 'IE', 'Ireland', 'Ireland', 'IRL', '372', '353', 'ie', '1', '1', '0', CURRENT_TIMESTAMP),
        (107, 'IM', 'Isle of Man', 'Isle of Man', 'IMN', '833', '44', 'im', '1', '1', '0', CURRENT_TIMESTAMP),
        (109, 'IT', 'Italy', 'Italian Republic', 'ITA', '380', '39', 'jm', '1', '1', '0', CURRENT_TIMESTAMP),
        (110, 'JM', 'Jamaica', 'Jamaica', 'JAM', '388', '1+876', 'jm', '1', '1', '0', CURRENT_TIMESTAMP),
        (111, 'JP', 'Japan', 'Japan', 'JPN', '392', '81', 'jp', '1', '1', '0', CURRENT_TIMESTAMP),
        (112, 'JE', 'Jersey', 'The Bailiwick of Jersey', 'JEY', '832', '44', 'je', '1', '1', '0', CURRENT_TIMESTAMP),
        (114, 'KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', '398', '7', 'kz', '1', '1', '0', CURRENT_TIMESTAMP),
        (115, 'KE', 'Kenya', 'Republic of Kenya', 'KEN', '404', '254', 'ke', '1', '1', '0', CURRENT_TIMESTAMP),
        (116, 'KI', 'Kiribati', 'Republic of Kiribati', 'KIR', '296', '686', 'ki', '1', '1', '0', CURRENT_TIMESTAMP),
        (117, 'XK', 'Kosovo', 'Republic of Kosovo', '---', '---', '381', '', '1', '1', '0', CURRENT_TIMESTAMP),
        (119, 'KG', 'Kyrgyzstan', 'Kyrgyz Republic', 'KGZ', '417', '996', 'kg', '1', '1', '0', CURRENT_TIMESTAMP),
        (120, 'LA', 'Laos', 'Lao People''s Democratic Republic', 'LAO', '418', '856', 'la', '1', '1', '0', CURRENT_TIMESTAMP),
        (121, 'LV', 'Latvia', 'Republic of Latvia', 'LVA', '428', '371', 'lv', '1', '1', '0', CURRENT_TIMESTAMP),
        (123, 'LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', '426', '266', 'ls', '1', '1', '0', CURRENT_TIMESTAMP),
        (124, 'LR', 'Liberia', 'Republic of Liberia', 'LBR', '430', '231', 'lr', '1', '1', '0', CURRENT_TIMESTAMP),
        (125, 'LY', 'Libya', 'Libya', 'LBY', '434', '218', 'ly', '1', '1', '0', CURRENT_TIMESTAMP),
        (126, 'LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', '438', '423', 'li', '1', '1', '0', CURRENT_TIMESTAMP),
        (128, 'LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', '442', '352', 'lu', '1', '1', '0', CURRENT_TIMESTAMP),
        (129, 'MO', 'Macao', 'The Macao Special Administrative Region', 'MAC', '446', '853', 'mo', '1', '1', '0', CURRENT_TIMESTAMP),
        (131, 'MG', 'Madagascar', 'Republic of Madagascar', 'MDG', '450', '261', 'mg', '1', '1', '0', CURRENT_TIMESTAMP),
        (132, 'MW', 'Malawi', 'Republic of Malawi', 'MWI', '454', '265', 'mw', '1', '1', '0', CURRENT_TIMESTAMP),
        (134, 'MV', 'Maldives', 'Republic of Maldives', 'MDV', '462', '960', 'mv', '1', '1', '0', CURRENT_TIMESTAMP),
        (135, 'ML', 'Mali', 'Republic of Mali', 'MLI', '466', '223', 'ml', '1', '1', '0', CURRENT_TIMESTAMP),
        (136, 'MT', 'Malta', 'Republic of Malta', 'MLT', '470', '356', 'mt', '1', '1', '0', CURRENT_TIMESTAMP),
        (137, 'MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', '584', '692', 'mh', '1', '1', '0', CURRENT_TIMESTAMP),
        (138, 'MQ', 'Martinique', 'Martinique', 'MTQ', '474', '596', 'mq', '1', '1', '0', CURRENT_TIMESTAMP),
        (139, 'MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', '478', '222', 'mr', '1', '1', '0', CURRENT_TIMESTAMP),
        (140, 'MU', 'Mauritius', 'Republic of Mauritius', 'MUS', '480', '230', 'mu', '1', '1', '0', CURRENT_TIMESTAMP),
        (141, 'YT', 'Mayotte', 'Mayotte', 'MYT', '175', '262', 'yt', '1', '1', '0', CURRENT_TIMESTAMP),
        (142, 'MX', 'Mexico', 'United Mexican States', 'MEX', '484', '52', 'mx', '1', '1', '0', CURRENT_TIMESTAMP),
        (143, 'FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', '583', '691', 'fm', '1', '1', '0', CURRENT_TIMESTAMP),
        (145, 'MC', 'Monaco', 'Principality of Monaco', 'MCO', '492', '377', 'mc', '1', '1', '0', CURRENT_TIMESTAMP),
        (146, 'MN', 'Mongolia', 'Mongolia', 'MNG', '496', '976', 'mn', '1', '1', '0', CURRENT_TIMESTAMP),
        (148, 'MS', 'Montserrat', 'Montserrat', 'MSR', '500', '1+664', 'ms', '1', '1', '0', CURRENT_TIMESTAMP),
        (149, 'MA', 'Morocco', 'Kingdom of Morocco', 'MAR', '504', '212', 'ma', '1', '1', '0', CURRENT_TIMESTAMP),
        (150, 'MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', '508', '258', 'mz', '1', '1', '0', CURRENT_TIMESTAMP),
        (151, 'MM', 'Myanmar (Burma)', 'Republic of the Union of Myanmar', 'MMR', '104', '95', 'mm', '1', '1', '0', CURRENT_TIMESTAMP),
        (152, 'NA', 'Namibia', 'Republic of Namibia', 'NAM', '516', '264', 'na', '1', '1', '0', CURRENT_TIMESTAMP),
        (153, 'NR', 'Nauru', 'Republic of Nauru', 'NRU', '520', '674', 'nr', '1', '1', '0', CURRENT_TIMESTAMP),
        (154, 'NP', 'Nepal', 'Federal Democratic Republic of Nepal', 'NPL', '524', '977', 'np', '1', '1', '0', CURRENT_TIMESTAMP),
        (155, 'NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', '528', '31', 'nl', '1', '1', '0', CURRENT_TIMESTAMP),
        (156, 'NC', 'New Caledonia', 'New Caledonia', 'NCL', '540', '687', 'nc', '1', '1', '0', CURRENT_TIMESTAMP),
        (157, 'NZ', 'New Zealand', 'New Zealand', 'NZL', '554', '64', 'nz', '1', '1', '0', CURRENT_TIMESTAMP),
        (158, 'NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', '558', '505', 'ni', '1', '1', '0', CURRENT_TIMESTAMP),
        (159, 'NE', 'Niger', 'Republic of Niger', 'NER', '562', '227', 'ne', '1', '1', '0', CURRENT_TIMESTAMP),
        (161, 'NU', 'Niue', 'Niue', 'NIU', '570', '683', 'nu', '1', '1', '0', CURRENT_TIMESTAMP),
        (162, 'NF', 'Norfolk Island', 'Norfolk Island', 'NFK', '574', '672', 'nf', '1', '1', '0', CURRENT_TIMESTAMP),
        (163, 'KP', 'North Korea', 'Democratic People''s Republic of Korea', 'PRK', '408', '850', 'kp', '1', '1', '0', CURRENT_TIMESTAMP),
        (164, 'MP', 'Northern Mariana Islands', 'Northern Mariana Islands', 'MNP', '580', '1+670', 'mp', '1', '1', '0', CURRENT_TIMESTAMP),
        (165, 'NO', 'Norway', 'Kingdom of Norway', 'NOR', '578', '47', 'no', '1', '1', '0', CURRENT_TIMESTAMP),
        (168, 'PW', 'Palau', 'Republic of Palau', 'PLW', '585', '680', 'pw', '1', '1', '0', CURRENT_TIMESTAMP),
        (170, 'PA', 'Panama', 'Republic of Panama', 'PAN', '591', '507', 'pa', '1', '1', '0', CURRENT_TIMESTAMP),
        (171, 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', '598', '675', 'pg', '1', '1', '0', CURRENT_TIMESTAMP),
        (172, 'PY', 'Paraguay', 'Republic of Paraguay', 'PRY', '600', '595', 'py', '1', '1', '0', CURRENT_TIMESTAMP),
        (173, 'PE', 'Peru', 'Republic of Peru', 'PER', '604', '51', 'pe', '1', '1', '0', CURRENT_TIMESTAMP),
        (174, 'PH', 'Phillipines', 'Republic of the Philippines', 'PHL', '608', '63', 'ph', '1', '1', '0', CURRENT_TIMESTAMP),
        (175, 'PN', 'Pitcairn', 'Pitcairn', 'PCN', '612', 'NONE', 'pn', '1', '1', '0', CURRENT_TIMESTAMP),
        (177, 'PT', 'Portugal', 'Portuguese Republic', 'PRT', '620', '351', 'pt', '1', '1', '0', CURRENT_TIMESTAMP),
        (178, 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', '630', '1+939', 'pr', '1', '1', '0', CURRENT_TIMESTAMP),
        (180, 'RE', 'Reunion', 'R&eacute;union', 'REU', '638', '262', 're', '1', '1', '0', CURRENT_TIMESTAMP),
        (183, 'RW', 'Rwanda', 'Republic of Rwanda', 'RWA', '646', '250', 'rw', '1', '1', '0', CURRENT_TIMESTAMP),
        (184, 'BL', 'Saint Barthelemy', 'Saint Barth&eacute;lemy', 'BLM', '652', '590', 'bl', '1', '1', '0', CURRENT_TIMESTAMP),
        (185, 'SH', 'Saint Helena', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', '654', '290', 'sh', '1', '1', '0', CURRENT_TIMESTAMP),
        (186, 'KN', 'Saint Kitts and Nevis', 'Federation of Saint Christopher and Nevis', 'KNA', '659', '1+869', 'kn', '1', '1', '0', CURRENT_TIMESTAMP),
        (187, 'LC', 'Saint Lucia', 'Saint Lucia', 'LCA', '662', '1+758', 'lc', '1', '1', '0', CURRENT_TIMESTAMP),
        (188, 'MF', 'Saint Martin', 'Saint Martin', 'MAF', '663', '590', 'mf', '1', '1', '0', CURRENT_TIMESTAMP),
        (189, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', '666', '508', 'pm', '1', '1', '0', CURRENT_TIMESTAMP),
        (190, 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', '670', '1+784', 'vc', '1', '1', '0', CURRENT_TIMESTAMP),
        (191, 'WS', 'Samoa', 'Independent State of Samoa', 'WSM', '882', '685', 'ws', '1', '1', '0', CURRENT_TIMESTAMP),
        (192, 'SM', 'San Marino', 'Republic of San Marino', 'SMR', '674', '378', 'sm', '1', '1', '0', CURRENT_TIMESTAMP),
        (193, 'ST', 'Sao Tome and Principe', 'Democratic Republic of S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'STP', '678', '239', 'st', '1', '1', '0', CURRENT_TIMESTAMP),
        (195, 'SN', 'Senegal', 'Republic of Senegal', 'SEN', '686', '221', 'sn', '1', '1', '0', CURRENT_TIMESTAMP),
        (197, 'SC', 'Seychelles', 'Republic of Seychelles', 'SYC', '690', '248', 'sc', '1', '1', '0', CURRENT_TIMESTAMP),
        (198, 'SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', '694', '232', 'sl', '1', '1', '0', CURRENT_TIMESTAMP),
        (199, 'SG', 'Singapore', 'Republic of Singapore', 'SGP', '702', '65', 'sg', '1', '1', '0', CURRENT_TIMESTAMP),
        (200, 'SX', 'Sint Maarten', 'Sint Maarten', 'SXM', '534', '1+721', 'sx', '1', '1', '0', CURRENT_TIMESTAMP),
        (203, 'SB', 'Solomon Islands', 'Solomon Islands', 'SLB', '090', '677', 'sb', '1', '1', '0', CURRENT_TIMESTAMP),
        (204, 'SO', 'Somalia', 'Somali Republic', 'SOM', '706', '252', 'so', '1', '1', '0', CURRENT_TIMESTAMP),
        (207, 'KR', 'South Korea', 'Republic of Korea', 'KOR', '410', '82', 'kr', '1', '1', '0', CURRENT_TIMESTAMP),
        (208, 'SS', 'South Sudan', 'Republic of South Sudan', 'SSD', '728', '211', 'ss', '1', '1', '0', CURRENT_TIMESTAMP),
        (209, 'ES', 'Spain', 'Kingdom of Spain', 'ESP', '724', '34', 'es', '1', '1', '0', CURRENT_TIMESTAMP),
        (210, 'LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', '144', '94', 'lk', '1', '1', '0', CURRENT_TIMESTAMP),
        (211, 'SD', 'Sudan', 'Republic of the Sudan', 'SDN', '729', '249', 'sd', '1', '1', '0', CURRENT_TIMESTAMP),
        (212, 'SR', 'Suriname', 'Republic of Suriname', 'SUR', '740', '597', 'sr', '1', '1', '0', CURRENT_TIMESTAMP),
        (213, 'SJ', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'SJM', '744', '47', 'sj', '1', '1', '0', CURRENT_TIMESTAMP),
        (214, 'SZ', 'Swaziland', 'Kingdom of Swaziland', 'SWZ', '748', '268', 'sz', '1', '1', '0', CURRENT_TIMESTAMP),
        (215, 'SE', 'Sweden', 'Kingdom of Sweden', 'SWE', '752', '46', 'se', '1', '1', '0', CURRENT_TIMESTAMP),
        (216, 'CH', 'Switzerland', 'Swiss Confederation', 'CHE', '756', '41', 'ch', '1', '1', '0', CURRENT_TIMESTAMP),
        (218, 'TW', 'Taiwan', 'Republic of China (Taiwan)', 'TWN', '158', '886', 'tw', '1', '1', '0', CURRENT_TIMESTAMP),
        (219, 'TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', '762', '992', 'tj', '1', '1', '0', CURRENT_TIMESTAMP),
        (220, 'TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', '834', '255', 'tz', '1', '1', '0', CURRENT_TIMESTAMP),
        (221, 'TH', 'Thailand', 'Kingdom of Thailand', 'THA', '764', '66', 'th', '1', '1', '0', CURRENT_TIMESTAMP),
        (222, 'TL', 'Timor-Leste (East Timor)', 'Democratic Republic of Timor-Leste', 'TLS', '626', '670', 'tl', '1', '1', '0', CURRENT_TIMESTAMP),
        (223, 'TG', 'Togo', 'Togolese Republic', 'TGO', '768', '228', 'tg', '1', '1', '0', CURRENT_TIMESTAMP),
        (224, 'TK', 'Tokelau', 'Tokelau', 'TKL', '772', '690', 'tk', '1', '1', '0', CURRENT_TIMESTAMP),
        (225, 'TO', 'Tonga', 'Kingdom of Tonga', 'TON', '776', '676', 'to', '1', '1', '0', CURRENT_TIMESTAMP),
        (226, 'TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', '780', '1+868', 'tt', '1', '1', '0', CURRENT_TIMESTAMP),
        (227, 'TN', 'Tunisia', 'Republic of Tunisia', 'TUN', '788', '216', 'tn', '1', '1', '0', CURRENT_TIMESTAMP),
        (229, 'TM', 'Turkmenistan', 'Turkmenistan', 'TKM', '795', '993', 'tm', '1', '1', '0', CURRENT_TIMESTAMP),
        (230, 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', '796', '1+649', 'tc', '1', '1', '0', CURRENT_TIMESTAMP),
        (231, 'TV', 'Tuvalu', 'Tuvalu', 'TUV', '798', '688', 'tv', '1', '1', '0', CURRENT_TIMESTAMP),
        (232, 'UG', 'Uganda', 'Republic of Uganda', 'UGA', '800', '256', 'ug', '1', '1', '0', CURRENT_TIMESTAMP),
        (234, 'AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', '784', '971', 'ae', '1', '1', '0', CURRENT_TIMESTAMP),
        (235, 'GB', 'United Kingdom', 'United Kingdom of Great Britain and Nothern Ireland', 'GBR', '826', '44', 'uk', '1', '1', '0', CURRENT_TIMESTAMP),
        (236, 'US', 'United States', 'United States of America', 'USA', '840', '1', 'us', '1', '1', '1', CURRENT_TIMESTAMP),
        (237, 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', '581', 'NONE', 'NONE', '1', '1', '0', CURRENT_TIMESTAMP),
        (238, 'UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', '858', '598', 'uy', '1', '1', '0', CURRENT_TIMESTAMP),
        (239, 'UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', '860', '998', 'uz', '1', '1', '0', CURRENT_TIMESTAMP),
        (240, 'VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', '548', '678', 'vu', '1', '1', '0', CURRENT_TIMESTAMP),
        (241, 'VA', 'Vatican City', 'State of the Vatican City', 'VAT', '336', '39', 'va', '1', '1', '0', CURRENT_TIMESTAMP),
        (242, 'VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', '862', '58', 've', '1', '1', '0', CURRENT_TIMESTAMP),
        (243, 'VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', '704', '84', 'vn', '1', '1', '0', CURRENT_TIMESTAMP),
        (244, 'VG', 'Virgin Islands, British', 'British Virgin Islands', 'VGB', '092', '1+284', 'vg', '1', '1', '0', CURRENT_TIMESTAMP),
        (245, 'VI', 'Virgin Islands, US', 'Virgin Islands of the United States', 'VIR', '850', '1+340', 'vi', '1', '1', '0', CURRENT_TIMESTAMP),
        (246, 'WF', 'Wallis and Futuna', 'Wallis and Futuna', 'WLF', '876', '681', 'wf', '1', '1', '0', CURRENT_TIMESTAMP),
        (247, 'EH', 'Western Sahara', 'Western Sahara', 'ESH', '732', '212', 'eh', '1', '1', '0', CURRENT_TIMESTAMP),
        (249, 'ZM', 'Zambia', 'Republic of Zambia', 'ZMB', '894', '260', 'zm', '1', '1', '0', CURRENT_TIMESTAMP),
        (250, 'ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', '716', '263', 'zw', '1', '1', '0', CURRENT_TIMESTAMP);
        ";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "state`(`id`, `country_id`, `name`, `iso_code`, `is_active`, `is_deleted`, `date_add`) VALUES
        ('', 40, 'Alberta', 'AB', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'British Columbia', 'BC', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Manitoba', 'MB', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'New Brunswick', 'NB', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Newfoundland and Labrador', 'NL', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Northwest Territories', 'NT', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Nova Scotia', 'NS', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Nunavut', 'NU', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Ontario', 'ON', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Prince Edward Island', 'PE', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Quebec', 'QC', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Saskatchewan', 'SK', '1', '0', CURRENT_TIMESTAMP),
        ('', 40, 'Yukon', 'YT', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Alabama', 'AL', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Alaska', 'AK', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Arizona', 'AZ', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Arkansas', 'AR', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'California', 'CA', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Colorado', 'CO', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Connecticut', 'CT', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Delaware', 'DE', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'District of Columbia', 'DC', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Florida', 'FL', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Georgia', 'GA', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Hawaii', 'HI', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Idaho', 'ID', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Illinois', 'IL', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Indiana', 'IN', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Iowa', 'IA', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Kansas', 'KS', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Kentucky', 'KY', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Louisiana', 'LA', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Maine', 'ME', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Maryland', 'MD', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Massachusetts', 'MA', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Michigan', 'MI', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Minnesota', 'MN', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Mississippi', 'MS', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Missouri', 'MO', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Montana', 'MT', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Nebraska', 'NE', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Nevada', 'NV', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'New Hampshire', 'NH', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'New Jersey', 'NJ', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'New Mexico', 'NM', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'New York', 'NY', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'North Carolina', 'NC', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'North Dakota', 'ND', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Ohio', 'OH', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Oklahoma', 'OK', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Oregon', 'OR', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Pennsylvania', 'PA', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Rhode Island', 'RI', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'South Carolina', 'SC', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'South Dakota', 'SD', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Tennessee', 'TN', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Texas', 'TX', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Utah', 'UT', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Vermont', 'VT', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Virginia', 'VA', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Washington', 'WA', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'West Virginia', 'WV', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Wisconsin', 'WI', '1', '0', CURRENT_TIMESTAMP),
        ('', 236, 'Wyoming', 'WY', '1', '0', CURRENT_TIMESTAMP);
        ";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "email` (`id`, `email_name`, `email_fields`, `email_subject`, `email_text`, `email_type`, `is_deleted`, `from_name`) VALUES
        (1, 'New Registration - Confirm email - user', '{FIRSTNAME}, {LASTNAME}, {EMAIL},{PASSWORD},{CONFIRMATION_URL}', 'Welcome to {SITE_NAME}', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}.&lt;br /&gt;\r\n&lt;br /&gt;\r\nA new&amp;nbsp;account has been created using your email address.&lt;br /&gt;\r\n&lt;br /&gt;\r\nConfirm this by clicking the&amp;nbsp;following link.&lt;br /&gt;\r\n&lt;a href=&quot;{CONFIRMATION_URL}&quot;&gt;Click Here&lt;/a&gt;&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (2, 'New Registration - ADMIN', '{FIRSTNAME}, {LASTNAME}, {EMAIL},{PASSWORD},{CONFIRMATION_URL}', 'New account created', '&lt;p&gt;Hi Admin.&lt;br /&gt;\r\n&lt;br /&gt;\r\nNEW account has been created on site..&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\nFirst Name: {FIRSTNAME}&lt;br /&gt;\r\nLast Name: {LASTNAME}&lt;br /&gt;\r\nEmail: {EMAIL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (3, 'Verify Account - User', '{FIRSTNAME}, {LASTNAME}', 'Account Verified', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}..&lt;br /&gt;\r\n&lt;br /&gt;\r\nYour account has been activated successfully.\r\n&lt;/p&gt;\r\n\r\n', NULL, '0', '{SITE_NAME}'),
        (4, 'Contact Us - Admin', '{NAME}, {EMAIL}, {SUBJECT}, {MESSAGE}, {DATE}, {TIME}, {IP_ADDRESS}', 'Contact Us Form Submitted', '&lt;p&gt;Hi Admin.&lt;br /&gt;\r\n&lt;br /&gt;\r\nNew contact us page has been submitted on website.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (5, 'Order Success - Paypal Payment - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD},{PAYMENT_STATUS}, {DATE}, {TIME}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Order placed successfully&lt;/strong&gt;. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: #{ORDER_ID}&lt;br /&gt;\r\nOrder Amount: #{ORDER_AMOUNT}&lt;br /&gt;\r\nPayment Method: #{PAYMENT_METHOD}&lt;br /&gt;\r\nPayment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n\r\n&lt;p&gt;{PRODUCT_TABLE}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order will be shipped as soon as possible. check your&amp;nbsp;emails regularly for further updates.&lt;/p&gt;\r\n', NULL, '1', '{SITE_NAME}'),
        (6, 'Order Success - Admin', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD},{PAYMENT_STATUS}, {DATE}, {TIME}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'New Order', '&lt;p&gt;Hi Admin.&lt;br /&gt;\r\n&lt;br /&gt;\r\nNew order has been placed on site. Find the order details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: #{ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: #{ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method: #{PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n\r\n&lt;p&gt;Please check..&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (7, 'Forgot Password - User', '{FIRSTNAME}, {LASTNAME}, {CONFIRMATION_URL}', 'Forgot Password Request', '&lt;p&gt;Hi {FIRSTNAME}&amp;nbsp;{LASTNAME}..&lt;/p&gt;\r\n\r\n&lt;p&gt;Your request has been&amp;nbsp;accepted for forgot password, Please follow the link below&lt;/p&gt;\r\n\r\n&lt;p&gt;{CONFIRMATION_URL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (8, 'New Account Created - User', '{FIRSTNAME}, {LASTNAME}, {PASSWORD}', 'New Account Created', '&lt;p&gt;Hi {FIRSTNAME}&amp;nbsp;{LASTNAME}..&lt;/p&gt;\r\n\r\n&lt;p&gt;A new account has been created using you email address.&lt;/p&gt;\r\n\r\n&lt;p&gt;Password: {PASSWORD}&lt;/p&gt;\r\n\r\n&lt;p&gt;If you have not created this account, Please contact us immediately.&amp;nbsp;&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (9, 'Low Stock Notification - Admin', '{TABLE}', 'Low Stock Notification', '&lt;p&gt;Hi Admin..&lt;/p&gt;\r\n\r\n&lt;p&gt;Stock has been gone low for the following products.&lt;/p&gt;\r\n\r\n&lt;p&gt;{TABLE}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (10, 'Password Change - Customer', '{USER}, {USERID},{NEWPASSWORD},{IP_ADDRESS},{PASSWORD_CHANGE_DATE_TIME}', 'Password Change', '&lt;p&gt;Hi {USER}..&lt;/p&gt;\r\n\r\n&lt;p&gt;Your password has been recently updated on&amp;nbsp;&amp;nbsp;{PASSWORD_CHANGE_DATE_TIME} using&amp;nbsp;{IP_ADDRESS} IP Address&lt;/p&gt;\r\n\r\n&lt;p&gt;Find&amp;nbsp;the new login details below:&lt;/p&gt;\r\n\r\n&lt;p&gt;User Name&amp;nbsp;: {USERID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Password: {NEWPASSWORD}&lt;/p&gt;\r\n\r\n&lt;p&gt;If&amp;nbsp;you had not changed the password, please login and change the password again&amp;nbsp;as soon as possible, also contact us for this&amp;nbsp;immediately.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (11, 'Newsletter Subscribed - Customer', '{USER}, {DATE_TIME}', 'Newsletter Subscribed', '&lt;p&gt;Hi {USER}..&lt;/p&gt;\r\n\r\n&lt;p&gt;You have successfully subscribed for our newsletter.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (12, 'Newsletter Unsubscribed - Customer', '{USER}, {DATE_TIME}', 'Newsletter Unsubscribed', '&lt;p&gt;Hi {USER}..&lt;/p&gt;\r\n\r\n&lt;p&gt;You have successfully unsubscribed for our newsletter.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (13, 'Order Shipping Details - Customer', '{USER},{ORDER_ID}, {SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Shipment Details ', '&lt;h3&gt;&lt;strong&gt;YOUR ORDER HAS BEEN SHIPPED.&lt;/strong&gt;&lt;/h3&gt;\r\n\r\n&lt;p&gt;Hi {USER}..&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipment details have been updated for your order ({ORDER_ID})&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping Date : {SHIPPING_DATETIME}&lt;/p&gt;\r\n\r\n&lt;p&gt;Approx Delivery Date : {APPROX_DELIVERY_DATE}&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping Carrier : {SHIPPING_CARRIER}&lt;/p&gt;\r\n\r\n&lt;p&gt;Tracking Number : {TRACKING_NUMBER}&lt;/p&gt;\r\n\r\n&lt;p&gt;{SHIPPING_ADDRESS}&lt;/p&gt;\r\n\r\n&lt;p&gt;{PRODUCT_TABLE}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (14, 'Password Update - Admin', '{USERNAME},{NEWPASSWORD}', 'Password Update', '&lt;p&gt;Hi admin..&lt;/p&gt;\r\n\r\n&lt;p&gt;Admin password has been updated successfully.&lt;/p&gt;\r\n\r\n&lt;p&gt;Find new login details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Username :&amp;nbsp;{USERNAME}&lt;/p&gt;\r\n\r\n&lt;p&gt;Password :&amp;nbsp;{NEWPASSWORD}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (15, 'Order Status Update Shipping - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update Shipped', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your order has been updated {ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Find the Order Details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Status:&amp;nbsp;{CURRENT_STATE}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping Date&amp;nbsp;:&amp;nbsp;{SHIPPING_DATETIME}&lt;/p&gt;\r\n\r\n&lt;p&gt;Approx&amp;nbsp;Date&amp;nbsp;:&amp;nbsp;{APPROX_DELIVERY_DATE}&lt;/p&gt;\r\n\r\n&lt;p&gt;Shipping Carrier : {SHIPPING_CARRIER}&lt;/p&gt;\r\n\r\n&lt;p&gt;Tracking Number : {TRACKING_NUMBER}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (16, 'Order Status Update - Confirmed- User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Confirmed', '&lt;h2&gt;&lt;strong&gt;ORDER HAS BEEN CONFIRMED&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your order {ORDER_ID}&amp;nbsp;has been confirmed.&lt;/p&gt;\r\n\r\n&lt;p&gt;{SHIPPING_ADDRESS}&lt;/p&gt;\r\n\r\n&lt;p&gt;{PRODUCT_TABLE}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order wiil be dispatched as soon as possible.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (21, 'Order Status Update - Invoiced - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Invoiced', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order has been invoiced&amp;nbsp;{ORDER_ID},&lt;/p&gt;\r\n\r\n&lt;p&gt;Invoice No : {INVOICE_NUMBER}&lt;/p&gt;\r\n\r\n&lt;p&gt;{COMMENT}&lt;/p&gt;\r\n\r\n&lt;p&gt;If you have any query, Please contact us at&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;{CONTACT_US_URL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (17, 'Order Status Update - Canceled - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Canceled', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your order ({ORDER_ID}) has been canceled due to some reasons.&lt;/p&gt;\r\n\r\n&lt;p&gt;you can ask about your queries at:&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;{CONTACT_US_URL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (23, 'Order Success - By cheque - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT},{DATE}, {TIME},{CHEQUE_IN_ORDER_OF},{CHEQUE_SEND_ADDRESS},{PAYMENT_METHOD}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order placed successfully. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: {ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: {ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method : {PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;Cheque in favor of : {CHEQUE_IN_ORDER_OF}&lt;/p&gt;\r\n\r\n&lt;p&gt;Send us cheque at this address: {CHEQUE_SEND_ADDRESS}&lt;/p&gt;\r\n\r\n&lt;p&gt;After receiving cheque, we will process your order as soon as possible.&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (18, 'Order Status Update - Payment Received - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Payment Received', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Received for your order ({ORDER_ID})&lt;/p&gt;\r\n\r\n&lt;p&gt;Find the Order Details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Status:&amp;nbsp;{CURRENT_STATE}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method : {PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span class=&quot;help-block&quot;&gt;{COMMENT}&lt;/span&gt;&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (19, 'Order Status Update - Completed - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Completed', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Your order has been completed&amp;nbsp;{ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Come back soon to check new offers..&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (22, 'Order Success - Cash On Delivery - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD}, {DATE}, {TIME}, {PRODUCT_TABLE}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order placed successfully. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: {ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: {ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method: {PAYMENT_METHOD}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (20, 'Order Status Update - Download Permissions Granted - User', '{CUSTOMER},{ORDER_ID},{CURRENT_STATE},{SHIPPING_DATETIME},{APPROX_DELIVERY_DATE},{SHIPPING_CARRIER},{TRACKING_NUMBER},{TRACKING_URL},{PAYMENT_STATUS},{TRANSECTION_ID},{PAYMENT_METHOD},{GRAND_TOTAL},{COMMENT},{CONTACT_US_URL},{INVOICE_NUMBER}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Update - Download Permissions Granted', '&lt;p&gt;Hi {CUSTOMER}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Permissions has been made for your order {ORDER_ID},&lt;/p&gt;\r\n\r\n&lt;p&gt;You can download your products in your account section.&lt;/p&gt;\r\n\r\n&lt;p&gt;or if you have query about how to download product Please contact us&lt;/p&gt;\r\n\r\n&lt;p&gt;{CONTACT_US_URL}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (24, 'Order Success - Paypal Payment - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD},{PAYMENT_STATUS}, {DATE}, {TIME}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order placed successfully. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: {ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: {ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method: {PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (25, 'Share Wishlist - User', '{USERNAME}, {SPECIAL_NOTES_BY_CUSTOMER}, {PRODUCT_DATA}', 'Wishlist Share', '&lt;p&gt;Hi &amp;nbsp;..&lt;/p&gt;\r\n\r\n&lt;p&gt;{USERNAME}&amp;nbsp;has share wishlist with you.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;{SPECIAL_NOTES_BY_CUSTOMER}&lt;/p&gt;\r\n\r\n&lt;p&gt;{PRODUCT_DATA}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (26, 'Common Header', '{LOGO_URL}, {SITE_NAME}, {STORE_NAME},{ADDRESS_LINE_1},{ADDRESS_LINE_2},{STORE_CITY},{STORE_STATE},{STORE_COUNTRY},{STORE_ZIP_CODE},{STORE_PHONE_NUMBER}', 'Header for emails', '&lt;div style=&quot;width:100%;height:auto;&quot;&gt;\r\n&lt;h2 style=&quot;margin-bottom:5px;&quot;&gt;&lt;img alt=&quot;{SITE_NAME}&quot; src=&quot;{LOGO_URL}&quot; style=&quot;width:200px&quot; /&gt;&lt;/h2&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div style=&quot;clear:both;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;hr /&gt;', NULL, '0', '{SITE_NAME}'),
        (27, 'Common Footer', '{LOGO_URL}, {SITE_NAME}, {STORE_NAME},{ADDRESS_LINE_1},{ADDRESS_LINE_2},{STORE_CITY},{STORE_STATE},{STORE_COUNTRY},{STORE_ZIP_CODE},{STORE_PHONE_NUMBER},{SITE_URL}', 'Footer for emails', '&lt;hr /&gt;\r\n&lt;p&gt;{STORE_NAME}&lt;br /&gt;\r\n{STORE_PHONE_NUMBER}&lt;br /&gt;\r\n{SITE_URL}&lt;/p&gt;\r\n\r\n&lt;div style=&quot;clear:both;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (29, 'New Registration - Success - user', '{FIRSTNAME}, {LASTNAME}, {EMAIL},{PASSWORD},{CONFIRMATION_URL}, {LOGIN_URL}', 'Registration Successful ', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}&lt;br /&gt;\r\n&lt;br /&gt;\r\nNEW account created with your email.&lt;br /&gt;\r\nUse the below credentials for logging in the account.&lt;/p&gt;\r\n\r\n&lt;p&gt;Login URL : {LOGIN_URL}&lt;/p&gt;\r\n\r\n&lt;p&gt;Username :&amp;nbsp;{EMAIL}&lt;/p&gt;\r\n\r\n&lt;p&gt;Password :&amp;nbsp;{PASSWORD}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (28, 'Order Success - Card Payment - User', '{USERNAME}, {ORDER_ID}, {ORDER_AMOUNT}, {PAYMENT_METHOD},{PAYMENT_STATUS}, {DATE}, {TIME}, {PRODUCT_TABLE},{SHIPPING_ADDRESS}', 'Order Placed', '&lt;p&gt;Hi {USERNAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order placed successfully. Please find the details below.&lt;/p&gt;\r\n\r\n&lt;p&gt;Order&amp;nbsp;ID: #{ORDER_ID}&lt;/p&gt;\r\n\r\n&lt;p&gt;Order Amount: {ORDER_AMOUNT}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Method: {PAYMENT_METHOD}&lt;/p&gt;\r\n\r\n&lt;p&gt;Payment Status : {PAYMENT_STATUS}&lt;/p&gt;\r\n', NULL, '0', '{SITE_NAME}'),
        (30, 'Resend email confirmation - user', '{FIRSTNAME}, {LASTNAME}, {EMAIL}, {CONFIRMATION_URL}, {LOGIN_URL}', 'Email confirmation', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}&lt;br /&gt;\r\n&lt;br /&gt;\r\nVerify your email by clicking on the link below.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;{CONFIRMATION_URL}&quot;&gt;{CONFIRMATION_URL}&lt;/a&gt;&lt;/p&gt;\r\n', 'html', '0', '{SITE_NAME}'),
        (31, 'Email update- user', '{FIRSTNAME}, {LASTNAME}, {EMAIL}, {CONFIRMATION_URL}, {LOGIN_URL}', 'Email Updated', '&lt;p&gt;Hi {FIRSTNAME} {LASTNAME}&lt;br /&gt;\r\n&lt;br /&gt;\r\nYou had successfully update your account email address.&lt;/p&gt;\r\n\r\n&lt;p&gt;Verify new email by clicking the link below.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;{CONFIRMATION_URL}&quot;&gt;{CONFIRMATION_URL}&lt;/a&gt;&lt;/p&gt;\r\n', 'html', '0', '{SITE_NAME}'),
        (32, 'Password reminder email', '{NAME},{LAST_LOGIN},{USERNAME},{PASSWORD_RESET_LINK}', 'Password Reminder email from {SITE_NAME}', '&lt;p&gt;Hi, {NAME}&lt;/p&gt;\n\n&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;You have not logged in from the last &lt;strong&gt;{LAST_LOGIN} days.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\n\n&lt;p&gt;Please Login to your Account with&lt;br /&gt;\n&lt;b&gt;Username&lt;/b&gt; - {USERNAME}&lt;br /&gt;\n&lt;b&gt;Password&lt;/b&gt; - &lt;small&gt;As Defined By You.&lt;/small&gt;&lt;/p&gt;\n\n&lt;p&gt;If you have any problem logging in,&amp;nbsp;Please reset your Password at&lt;br /&gt;\n{PASSWORD_RESET_LINK}&lt;/p&gt;\n', '', '0', '{SITE_NAME}'),
        (33, 'Product Availability Notification', '{SITE_NAME},{NAME},{PRODUCT_NAME},{PRODUCT_LINK}', 'Your Favourite Product is Available at {SITE_NAME}', '&lt;p&gt;Hi, {NAME}&lt;/p&gt;\r\n\r\n&lt;p&gt;It is to inform you that &lt;strong&gt;{PRODUCT_NAME}&lt;/strong&gt; is made Available at {SITE_NAME}.&lt;/p&gt;\r\n\r\n&lt;p&gt;You can buy the same at - &lt;span style=&quot;line-height: 1.6em;&quot;&gt;{PRODUCT_LINK}&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Happy Shopping.&lt;/p&gt;\r\n', '', '0', '{SITE_NAME}'),
        (34, 'Gift certificate email', '{NAME},{USERNAME},{GIFT_PRICE},{GIFT_CODE},{COMMENTS},{SENDER_NAME},{SENDER_EMAIL}', 'Gift certificate from {SITE_NAME}', '<p>Hi, {NAME}</p>\n\n<p><span style=\"line-height: 1.6em;\"><strong>{SENDER_NAME}</strong>(</span><a href=\"{SENDER_EMAIL}\" style=\"line-height: 20.7999992370605px;\">{SENDER_EMAIL}</a>)<span style=\"line-height: 1.6em;\"> have purchased a gift certificate from&nbsp;<strong>{SITE_NAME} </strong>worth <strong>{GIFT_PRICE}</strong>&nbsp;.</span></p>\n\n<p><span style=\"line-height: 1.6em;\">PLease Use this code to credit </span><strong style=\"line-height: 20.7999992370605px;\">\${GIFT_PRICE}</strong><span style=\"line-height: 1.6em;\"> in your wallet.</span></p>\n\n<p><b>Code</b> - {GIFT_CODE}</p>\n\n<p><b>If you want to use it now ,&nbsp;</b><a href=\"{SITE_NAME}\">Click Here</a></p>\n', '', '0', '{SITE_NAME}');
        ";
        mysql_query($sample, $this->dbconn);


        $sample = "INSERT INTO `" . $this->prefix . "payment_method` (`id`, `payment_method`, `code`, `folder_name`, `is_active`, `is_deleted`, `is_sandbox`, `paypal_business`, `cheque_in_order_of`, `cheque_address`, `description`, `mode`, `position`, `email_template_id`, `api_username`, `api_password`, `api_signature`) VALUES
        (1, 'PayPal', 'PP', 'paypal', 0, '0', '1', '', '', '', 'Online payment with PayPal', '0', 2, 24, '', '', ''),
        (2, 'Card Payment', 'PPP', 'paypal_payflow', 1, '0', '1', '', '', '', 'Online payment with PayPal Pro - Credit Card/ Debit Card', '0', 1, 28, '', '', ''),
        (3, 'Cash on Delivery', 'COD', 'cash_on_delivery', 0, '0', '0', '', '', '', 'Pay when order received.', '0', 3, 22, '', '', ''),
        (4, 'NoChex', NULL, 'no_chex', 0, '1', '0', '', '', '', NULL, '0', 0, 0, '', '', ''),
        (5, 'RomanPay', NULL, 'romanpay', 0, '1', '0', '', '', '', NULL, '1', 0, 0, '', '', ''),
        (6, 'Save Order', NULL, '-', 0, '1', '0', '', '', '', 'save your order for later payment', '1', 0, 0, '', '', ''),
        (7, 'Payment By Cheque', 'CH', '-', 0, '0', '0', '', '', '', 'Pay us later by Cheque', '1', 4, 23, '', '', '');
        ";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "setting` (`id`, `key`, `value`, `name`, `title`, `type`, `options`, `hint`, `position`, `status`) VALUES
        (1, 'ABOUT_US_PAGE_ID', '2', 'pages', 'About us page Id', 'text', '', '', 0, 0),
        (25, 'CONTACT_US_PAGE', '3', 'pages', 'Contact Us Page Id', 'text', '', '', 0, 0),
        (55, 'GUARANTEE_PAGE__ID', '8', 'pages', 'Guarantee Page Id', 'text', '', '', 0, 0),
        (60, 'HELP_CONTENT_PAGE', '4', 'pages', 'Help Page Content Id', 'text', '', '', 0, 0),
        (90, 'PAGE_2257_ID', '6', 'pages', '2257 Page Id', 'text', '', '', 0, 0),
        (93, 'PRIVACY_POLICY_PAGE', '5', 'pages', 'Privacy policy page Id', 'text', '', '', 0, 0),
        (129, 'TERMS_CONDITIONS_PAGE', '6', 'pages', 'Term and conditions page Id', 'text', '', '', 0, 0),
        (2, 'ACTIONS_FOR_UNLOGGED_IN_USERS', 'allow', 'product', 'Allow shopping for unlogged customers', 'select', 'allow,hide_add_to_cart,hide_price_and_add_to_cart', '', 6, 1),
        (3, 'ADDRESS1', '', 'address', 'Company Address 1', 'text', '', '', 3, 1),
        (4, 'ADDRESS2', '', 'address', 'Company Address 2', 'text', '', '', 5, 1),
        (5, 'ADMIN_CATEGORY_PAGE_SIZE', '200', 'general', 'records per page in admin', 'text', '', '', 17, 1),
        (6, 'ADMIN_EMAIL', '$store_email', 'email', 'admin email adress', 'text', '', '', 1, 1),
        (7, 'ALLOW_BACKORDERS', 'no_action', 'product', 'Out of stock action', 'select', 'no_action,allow_backorders,sign_up_for_notification', '', 4, 1),
        (8, 'APPLY_TAX', '0', 'checkout', 'enable taxes', 'select', '', '', 8, 1),
        (9, 'BACKEND_STICKY_NAV_BAR', '0', 'design', 'Sticky navigation bar in admin', 'select', '', 'sticky navigation bar in admin section', 2, 1),
        (10, 'BING_VC', '', 'seo', 'Bing Verify Code', 'text', '', '', 7, 1),
        (11, 'BOTTOM_LEFT_TEXT', 'WE''RE DEDICATED TO OUR CUSTOMERS &lt;strong&gt;24/7&lt;/strong&gt;', 'design', 'Main head text on footer', 'textarea', '', '', 0, 1),
        (12, 'BOTTOM_NAVIGATION_ID', '2', 'navigation', 'select navigation for footer', 'text', '', '', 0, 1),
        (13, 'BOTTOM_RIGHT_TEXT', 'All models are 18yrs or older. 18 U.S.C. 2257 Record keeping Requirements Compliance  Statement', 'design', 'text on footer bottom right', 'textarea', '', '', 0, 1),
        (14, 'BOTTOM_TEXT_INVOICE', 'Thanks for shopping with $store_name', 'invoice', 'text on invoice footer', 'textarea', '', '', 2, 1),
        (15, 'BRAND_PAGES_FREQUENCY', 'yearly', 'xml_sitemap', 'Brand Links Frequency', 'select', 'always,hourly,daily,weekly,monthly,yearly,never', 'set brand page changes frequency', 11, 1),
        (16, 'BRAND_PAGES_PRIORITY', '0.7', 'xml_sitemap', 'Brand Page Priority', 'select', '1,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0', 'set brand pages priority in sitemap', 10, 1),
        (17, 'BRAND_PAGE_META_ROBOTS', 'index', 'meta', 'Brand Page Meta Robots', 'radio', 'index,noindex', '', 20, 1),
        (18, 'BRAND_PAGE_META_TITLE', '%%BRAND_NAME%% | %%SITENAME%%', 'meta', 'Brand Page Meta Title', 'text', '', 'You can use %%BRAND_NAME%%, %%SITENAME%%<br/>\n%%BRAND_NAME%% - Name of the brand<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 19, 1),
        (19, 'CATEGORY_PAGES_FREQUENCY', 'monthly', 'xml_sitemap', 'Category Links Frequency', 'select', 'always,hourly,daily,weekly,monthly,yearly,never', 'set category page changes frequency', 7, 1),
        (20, 'CATEGORY_PAGES_PRIORITY', '0.8', 'xml_sitemap', 'Category Page Priority', 'select', '1,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0', 'set category pages priority in sitemap', 6, 1),
        (21, 'CATEGORY_PAGE_META_ROBOTS', 'index', 'meta', 'Category Page Meta Robots', 'radio', 'index,noindex', '', 23, 1),
        (22, 'CATEGORY_PAGE_META_TITLE', '%%CATEGORY_NAME%% | %%SITENAME%% ', 'meta', 'Category Page Meta Title', 'text', '', 'You can use %%CATEGORY_NAME%%, %%SITENAME%%<br/>\n%%CATEGORY_NAME%% - Name of the category<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 22, 1),
        (23, 'CITY', '', 'address', 'City', 'text', '', '', 7, 1),
        (24, 'CMS_PAGE_SIZE_ADMIN', '50', 'general', 'Records per page in admin', 'text', '', 'records per page in admin, not applicable for order and product page', 19, 1),
        (26, 'CONTENT_PAGES_FREQUENCY', 'hourly', 'xml_sitemap', 'Content Links Frequency', 'select', 'always,hourly,daily,weekly,monthly,yearly,never', 'set content page changes frequency', 14, 1),
        (27, 'CONTENT_PAGES_PRIORITY', '1', 'xml_sitemap', 'Content Page Priority', 'select', '1,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0', 'set content pages priority in sitemap', 13, 1),
        (28, 'CONTENT_PAGE_META_ROBOTS', 'index', 'meta', 'Content Page Meta Robots', 'radio', 'index,noindex', '', 17, 1),
        (29, 'CONTENT_PAGE_META_TITLE', '%%PAGE_NAME%% | %%SITENAME%% ', 'meta', 'Content Page Meta Title', 'text', '', 'You can use %%PAGE_NAME%%,%%SITENAME%%<br/>\n%%PAGE_NAME%% - Name of the content page<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 16, 1),
        (30, 'COUNTRY', '', 'address', 'Country', 'text', '', '', 11, 1),
        (31, 'CREATE_ACCOUNT_PROMPT_AFTER_GUEST_CHECKOUT', '1', 'checkout', 'create account after guest checkout', 'select', '', 'ask customer for one click account creation after guest checkout', 3, 1),
        (32, 'CROSS_SELLS_CART_PAGE', '0', 'product', 'Show cross sells on Cart', 'select', '', 'show cross sells on cart page', 14, 1),
        (33, 'CROSS_SELLS_CATEGORIES', '', 'product', 'Category Ids for cross sells', 'text', '', 'comma seperated category ids for cross sell products', 15, 1),
        (34, 'CURRENCY_SYMBOL', '$', 'general', 'currency symbol', 'text', '', '', 9, 1),
        (35, 'CURRENCY_SYMBOL_LOCATION', 'before', 'general', 'currency symbol location (before,after)', 'text', '', '', 10, 1),
        (36, 'DEFAULT_ORDER_BY_ON_SHOP', 'rating', 'product', 'default sorting on product listing pages', 'select', 'latest,name,bestseller,price,rating', 'default sorting for categories and brand pages', 7, 1),
        (37, 'DEFAULT_ORDER_BY_METHOD', 'asc', 'product', 'sort order', 'select', 'asc,desc', 'not applicable for bestsellers and latest', 8, 1),
        (38, 'DEFAULT_PAGE_META_TITLE', '%%PAGE_NAME%% | %%SITENAME%%', 'meta', 'Pages Default Meta Title', 'text', '', 'You can use %%PAGE_NAME%%, %%SITENAME%% for pages like sale, bestsellers<br/>\n%%PAGE_NAME%% - Name of the page.<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 26, 1),
        (39, 'DIMENTIONS_UNIT', 'mm', 'general', 'dimentions unit', 'text', '', '', 12, 1),
        (40, 'EMAIL_CONFIRMATION_REQUIRED_USER', '0', 'general', 'Email verification required for new user', 'select', '', 'email should be verified before accessing new account for user', 5, 1),
        (41, 'ENABLE_GUEST_CHECKOUT', '1', 'checkout', 'enable guest checkout', 'select', '', '', 1, 1),
        (42, 'ENABLE_SSL_ADMIN', '0', 'security', 'Enable SSL for admin section', 'select', '', 'all the admin section will work on ssl<br/>please add certificate files before enabling', 0, 1),
        (43, 'ENABLE_SSL_FRONT', '0', 'security', 'Enable SSL for front end', 'select', '', 'checkout, login and account pages will work on ssl<br/>please add certificate files before enabling', 0, 1),
        (44, 'ESTIMATE_SHIPPING_ON_CART_PAGE', '0', 'checkout', 'show estimate shipping on cart page', 'select', '', 'applicable when free shipping not enabled', 6, 1),
        (45, 'FACEBOOK_APP_ID', '', 'social', 'Facebook App Id', 'text', '', '', 9, 1),
        (46, 'FACEBOOK_APP_SECRET', '', 'social', 'Facebook App Secret', 'text', '', '', 10, 1),
        (47, 'FACEBOOK_PAGE', '', 'social', 'Facebook Page ', 'text', '', '', 8, 1),
        (48, 'FEATURED_PRODUCTS_HOMEPAGE', '12', 'general', 'count no. of featured products on homepage', 'text', '', '', 14, 1),
        (49, 'FILL_META_DESC_AUTOMATICALLY', '1', 'meta', 'Auto generate meta description', 'select', '', 'Dynamically generate meta description from the page content.<br/>i.e. using short description from product', 4, 1),
        (50, 'FRONTEND_STICKY_NAV_BAR', '1', 'design', 'Sticky navigation Bar on front end', 'select', '', 'sticky navigation bar on front end', 1, 1),
        (51, 'FRONT_PRODUCT_PER_PAGE', '60', 'general', 'products per page on front end', 'text', '', '', 20, 1),
        (52, 'GOOGLE_AC', 'UA-53433480-1', 'seo', 'Google Analytic Code', 'text', '', '', 2, 1),
        (53, 'GOOGLE_PLUS_PAGE', '', 'social', 'Google Plus Page ', 'text', '', '', 2, 1),
        (54, 'GOOGLE_VC', '', 'seo', 'Google Verify Code', 'text', '', '', 4, 1),
        (56, 'HOMEPAGE_CANONICAL', '', 'meta', 'Home Page Canonical', 'text', '', 'Canonical url on homepage', 9, 1), ";

        if (isset($_SESSION["add_dummy_data"])) {
            $sample .= " (57, 'HOMEPAGE_META_DESC', '', 'meta', 'Homepage Meta Description', 'textarea', '', 'This will be the Meta Description for your homepage. The default is no Meta Description at all if this is not set.', 8, 1),
        (58, 'HOMEPAGE_META_KEYWORDS', '', 'meta', 'Homepage Meta Keywords', 'textarea', '', 'Enter a comma separated list of your most important keywords for your site that will be written as Meta Keywords on your homepage', 7, 1),
        (59, 'HOMEPAGE_META_TITLE', 'Welcome to $store_name', 'meta', 'Homepage Meta Title', 'text', '', 'Meta title for homepage, you can use %%SITENAME%%<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 6, 1), ";
        } else {
            $sample .= " (57, 'HOMEPAGE_META_DESC', 'Get FREE shipping on all Sex Toys over $69 and a FREE gift if you order any product over $20. Shop our huge selection of top adult toys. Click here to learn more.', 'meta', 'Homepage Meta Description', 'textarea', '', 'This will be the Meta Description for your homepage. The default is no Meta Description at all if this is not set.', 8, 1),
        (58, 'HOMEPAGE_META_KEYWORDS', 'Sex Toys, Vibrators, Dildos, Anal Toys, Strokers, Sex Dolls, Cock Rings, Penis Pumps, Personal Lubricant, BDSM, Sex Swings', 'meta', 'Homepage Meta Keywords', 'textarea', '', 'Enter a comma separated list of your most important keywords for your site that will be written as Meta Keywords on your homepage', 7, 1),
        (59, 'HOMEPAGE_META_TITLE', 'Sex Toys, Vibrators, Dildos, Anal Toys, Strokers, Sex Dolls, Cock Rings, Penis Pumps, Personal Lubricant, BDSM, Sex Swings', 'meta', 'Homepage Meta Title', 'text', '', 'Meta title for homepage, you can use %%SITENAME%%<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 6, 1), ";
        }

        $sample .= "(61, 'INCLUDE_BRAND_PAGES_SITEMAP', 'yes', 'xml_sitemap', 'Include Brand Pages', 'radio', 'yes,no', 'include brand pages url''s in sitemap', 8, 1),
        (62, 'INCLUDE_CATEGORY_PAGES_SITEMAP', 'yes', 'xml_sitemap', 'Include Category Pages', 'radio', 'yes,no', 'include category pages url''s in sitemap', 5, 1),
        (63, 'INCLUDE_CONTENT_PAGES_SITEMAP', 'yes', 'xml_sitemap', 'Include Content Pages', 'radio', 'yes,no', 'include content pages url''s in sitemap', 12, 1),
        (64, 'INCLUDE_PRODUCT_XML_SITEMAP', 'yes', 'xml_sitemap', 'Include Products Pages', 'radio', 'yes,no', 'include product pages url''s in sitemap', 2, 1),
        (65, 'INCLUDE_ROOT_CATEGORY_PAGES_ONLY_SITEMAP', 'no', 'xml_sitemap', 'Include only root category pages', 'radio', 'yes,no', 'include only root category pages or all categories in sitemap', 5, 1),
        (66, 'INVOICE_NAME_PREFIX', 'IN', 'invoice', 'invoice name prefix', 'text', '', '', 1, 1),
        (67, 'IS_SHIPPING_FREE', '1', 'checkout', 'enable free shipping', 'select', '', '', 5, 1),
        (68, 'IS_SMTP', 'PHP_mail', 'SMTP', 'Mailer', 'select', 'PHP_mail,SMTP', 'use smpt or php mail function', 1, 1),
        (69, 'LAST_SITEMAP_DATE', '', 'xml_sitemap', 'Previous Sitemap Generated on', 'text', '', '', 1, 1),
        (70, 'LATEST_PRODUCTS_DAY_LIMIT', '80', 'product', 'Number of days for being a latest product', 'text', '', '', 11, 1),
        (71, 'LATEST_PRODUCTS_HOMEPAGE', '12', 'general', 'count no. of latest products on homepage', 'text', '', '', 15, 1),
        (72, 'LINKS_SITEMAP_PER_PAGE', '50000', 'xml_sitemap', 'Maximum Links per sitemap', 'text', '', 'maximum number of links per sitemap', 1, 1),
        (73, 'LOW_STOCK_WARNING', '3', 'product', 'generate low stock warning if quantity less than', 'text', '', '', 12, 1),
        (74, 'MAINTANCE_MODE', '0', 'general', 'site in maintance mode', 'select', '', 'admin can visit site after logging in admin even in maintenance mode', 3, 1),
        (75, 'MAIN_NAVIGATION_ID', '1', 'navigation', 'select navigation for main menu', 'text', '', '', 0, 1),
        (76, 'MAXIMUM_META_DESC_LENGHT', '160', 'meta', 'Maximum Meta Description Length', 'text', '', 'Maximum meta description length for all modules', 3, 1),
        (77, 'MAXIMUM_META_TITLE_LENGHT', '60', 'meta', 'Maximum Meta Title Length', 'text', '', 'Maximum meta title length for all modules', 1, 1),
        (78, 'MAXIMUM_ORDER_AMOUNT', '50000', 'checkout', 'Maximum Total Order Amount', 'text', '', '', 10, 1),
        (79, 'MAXIMUM_ORDER_PER_PRODUCT', '10', 'checkout', 'Maximum Order Quantity Per Product', 'text', '', '', 11, 1),
        (80, 'MINIMUM_ORDER_AMOUNT', '10', 'checkout', 'Minimum Total Order Amount', 'text', '', '', 13, 1),
        (81, 'MINIMUM_ORDER_PER_PRODUCT', '1', 'checkout', 'Minimum Order Quantity Per Product', 'text', '', '', 14, 1),
        (82, 'MINIMUM_QUANTITY_REQUIRED_FOR_IN_STOCK', '0', 'product', 'minimum threshold quantity in stock', 'text', '', 'minimum quantity required in store for each product. <br/> i.e. if product has 2 quantity, and threshold is 2, product will show out of stock', 10, 1),
        (83, 'ONE_PAGE_CHECKOUT', '1', 'checkout', 'enable one page checkout', 'select', '', '', 15, 0),
        (84, 'ORDER_DEPARTMENT_EMAIL', '$store_email', 'email', 'order department email address', 'text', '', '', 9, 1),
        (85, 'ORDER_ID_PREFIX', 'OI', 'checkout', 'Order ID Prefix', 'text', '', 'prefix for the order id', 17, 1),
        (86, 'ORDER_ID_START_FROM', '100', 'checkout', 'Order ID Start From', 'text', '', 'put the number from which order id should start. i.e. 1000<br/>use digits only or leave empty for auto generate order id', 18, 1),
        (87, 'ORDER_PAGE_SIZE_ADMIN', '30', 'general', 'Orders per page in admin', 'text', '', '', 18, 1),
        (88, 'ORDER_PAGE_SIZE_FRONT', '50', 'general', 'Orders per page on front', 'text', '', 'front end orders per page in customer account', 23, 1),
        (89, 'OVERALL_DISCOUNT_ON_SHOP', '', 'product', 'Overall discount on all products', 'text', '', 'overall discount in percentage on all the products on shop<br/>do not use ''%'' for discount i.e. 25 for 25% discount', 9, 1),
        (91, 'PAGINATION_REL_META_TAGS', '0', 'meta', 'Pagination Rel Meta Tags', 'select', '', 'use pagination rel=next/prev meta tags', 4, 1),
        (92, 'PHONE_NUMBER', '$store_phone', 'address', 'Sales Phone Number', 'text', '', '', 12, 1),
        (94, 'PRODUCT_PAGES_FREQUENCY', 'weekly', 'xml_sitemap', 'Products Links Frequency', 'select', 'always,hourly,daily,weekly,monthly,yearly,never', 'set product page changes frequency', 4, 1),
        (95, 'PRODUCT_PAGES_PRIORITY', '0.9', 'xml_sitemap', 'Product Page Priority', 'select', '1,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0', 'set product page priority in sitemap', 3, 1),
        (96, 'PRODUCT_PAGE_META_ROBOTS', 'index', 'meta', 'Product Page Meta Robots', 'radio', 'index,noindex', '', 12, 1),
        (97, 'PRODUCT_PAGE_META_TITLE', '%%PRODUCT_NAME%% | %%SITENAME%%', 'meta', 'Product Page Meta Title', 'text', '', 'You can use %%PRODUCT_NAME%%, %%SITENAME%%<br/>\n%%PRODUCT_NAME%% - Name of the product<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 11, 1),
        (98, 'REMOVE_WORDS_FROM_SEARCH', 'i,am,you,are,where,want', 'general', 'Remove words from search', 'textarea', '', 'add comma separated words to remove from search keywrods', 7, 1),
        (99, 'SALES_EMAIL', '$store_email', 'email', 'sales email address', 'text', '', '', 3, 1),
        (100, 'SEARCH_PAGE_META_TITLE', ' %%SEARCH_KEYWORD%% | %%SITENAME%%  ', 'meta', 'Search Page Meta Title', 'text', '', 'You can use %%SEARCH_KEYWORD%%, %%SITENAME%%<br/>\n%%SEARCH_KEYWORD%% - Search keyword used for searching<br/>\n%%SITENAME%% - Website name (defined in General Preferences)<br/>', 25, 1),
        (101, 'SEND_LOW_STOCK_EMAIL_ADMIN', '1', 'email', 'notify admin for low stock after order placed', 'select', '', '', 11, 1),
        (102, 'SEND_NEW_ORDER_EMAIL_TO_ADMIN', '1', 'email', 'send email to admin when new order placed', 'select', '', '', 13, 1),
        (103, 'SEND_NEW_RESIGTER_EMAIL_TO_ADMIN', '1', 'email', 'send email to admin when someone register new account', 'select', '', '', 15, 1),
        (104, 'SHOW_CART_TOTALS_ON_CART_PAGE', '1', 'checkout', 'show totals on cart page', 'select', '', '', 7, 0),
        (105, 'SHOW_COOKIES_USE_ALERT', '1', 'security', 'Notify visitors about cookies use', 'select', '', '', 2, 1),
        (106, 'SHOW_CURRENCY_ON_TOP', '0', 'general', 'show currency select box on top navigation', 'select', '', '', 0, 0),
        (107, 'SHOW_LANGUAGE_ON_TOP', '0', 'general', 'show language select box on top navigation', 'select', '', '', 0, 0),
        (108, 'SHOW_OUTOFSTOCK_PRODUCTS', '1', 'product', 'show out of stock products on website', 'select', '', '', 3, 1),
        (109, 'SHOW_RESULTS_AUTOSEARCH_FRONT', '30', 'general', 'Results in auto search', 'text', '', 'number of search results in autosearch', 21, 1),
        (110, 'SHOW_SLIDER_ON_MOBILE', '1', 'design', 'show slider on mobile view', 'select', '', '', 0, 1),
        (111, 'SHOW_SUBCATEGORY_PRODUCTS_IN_CATEGORY', '1', 'product', 'Show Subcategory Products in Main Category', 'select', '', 'show products of subcategories on main category page', 24, 1),
        (112, 'SITEMAP_PATH', '', 'xml_sitemap', 'Sitemap Path', 'text', '', '', 0, 1),
        (113, 'SITE_CACHE_STATUS', '0', 'cache', 'site cache', 'select', '', '', 0, 1),
        (114, 'SITE_DEBUG_STATUS', '0', 'cache', 'site debug smarty', 'select', '', '', 0, 1),
        (115, 'SITE_NAME', '$store_name', 'general', 'website name', 'text', '', '', 1, 1),
        (116, 'SITE_TAG_LINE', '', 'general', 'tag line of your site', 'textarea', '', '', 0, 0),
        (117, 'SLIDER_ACTIVE', '1', 'design', 'show slider', 'select', '', '', 0, 1),
        (118, 'SMTP_AUTH', '1', 'SMTP', 'SMTP Authentication', 'select', '', '', 10, 1),
        (119, 'SMTP_HOST', '', 'SMTP', 'SMTP Host', 'text', '', '', 2, 1),
        (120, 'SMTP_PASSWORD', '', 'SMTP', 'SMTP Password', 'text', '', '', 6, 1),
        (121, 'SMTP_PORT', '', 'SMTP', 'SMTP Port', 'text', '', '', 8, 1),
        (122, 'SMTP_SECURITY', 'SSL', 'SMTP', 'SMTP Security', 'select', 'none,SSL,TLS', '', 9, 1),
        (123, 'SMTP_USERNAME', '', 'SMTP', 'SMTP Username', 'text', '', '', 3, 1),
        (124, 'STATE', '', 'address', 'State', 'text', '', '', 8, 1),
        (125, 'STOCK_MANAGEMENT', '1', 'product', 'stock management', 'select', '', '', 1, 1),
        (126, 'STORE_NAME', '$store_name', 'address', 'Store Name', 'text', '', '', 1, 1),
        (127, 'SUPPORT_EMAIL', '$store_email', 'email', 'support email address', 'text', '', '', 5, 1),
        (128, 'TERMS_CONDITIONS_CHECKBOX', '0', 'checkout', 'Terms and Condition Checkbox', 'select', '', 'Require customers to accept terms of service before processing an order.', 9, 1),
        (130, 'TWITTER_PAGE', 'https://twitter.com/developers', 'social', 'Twitter Page', 'text', '', '', 6, 1),
        (131, 'TWITTER_WIDGET_ID', '482056896665235457', 'social', 'Twitter Widget ID', 'text', '', '', 7, 1),
        (132, 'UP_SELLS_PRODUCT_PAGE', '1', 'product', 'Show upsells on Product Page', 'select', '', 'show up sells on product page', 13, 1),
        (133, 'URL_REWRITE', '0', 'seo', 'Search Engine Friendly URL', 'select', '', 'set url rewrite', 1, 1),
        (134, 'URL_SLUG_CONTACT_US_PAGE', 'contact', 'url_slug', 'Contact Us Page URL Slug', 'text', '', 'slug for contact us page, default : contact<br/>must be unique, without spaces and special characters', 0, 1),
        (135, 'URL_SLUG_CONTENT_PAGE', 'content', 'url_slug', 'Content Module URL Slug', 'text', '', 'base slug for all the content pages, default : page<br/>must be unique, without  spaces and special characters', 0, 1),
        (136, 'URL_SLUG_NEW_PRODUCTS_PAGE', 'newest-products', 'url_slug', 'New Arrivals Page URL Slug', 'text', '', 'slug for new arrivals page, default : new_products<br/>must be unique, without spaces and special characters', 0, 1),
        (137, 'URL_SLUG_ON_SALE_PAGE', 'products-on-sale', 'url_slug', 'Sale Page URL Slug', 'text', '', 'slug for on sale products page, default : on_sale<br/>must be unique, without spaces and special characters', 0, 1),
        (138, 'URL_SLUG_TOP_PRODUCTS_PAGE', 'best-seller-products', 'url_slug', 'Best Sellers Page URL Slug', 'text', '', 'slug for best sellers page, default : top_products<br/>must be unique, without spaces and special characters', 0, 1),
        (139, 'USER_DEPARTMENT_EMAIL', '$store_email', 'email', 'user department email address', 'text', '', '', 7, 1),
        (140, 'WEIGHT_DIMENTIONS', 'kg', 'general', 'weight units', 'text', '', '', 11, 1),
        (141, 'YAHOO_VC', '', 'seo', 'Yahoo Verify Code', 'text', '', '', 6, 0),
        (142, 'YOUTUBE_PAGE', '', 'social', 'Youtube Page ', 'text', '', '', 4, 1),
        (143, 'ZIP_CODE', '', 'address', 'Zip Code', 'text', '', '', 9, 1),
        (144, 'STORE_CREATE_DATE', '" . date("Y-m-d H:i:s") . "', 'general', 'Store Creation Date', 'text', '', 'date when the store was created', 0, 0),
        (145, 'TOTAL_DEFAULT_CONTENT_PAGES', '6', 'general', 'Total default pages added', 'text', '', 'total count for new default pages added while new installation', 0, 0),
        (146, 'CHECKOUT_REVIEW_PAGE', '1', 'checkout', 'Order review page on checkout', 'select', '', 'Review page will be added on checkout after filling all the details.', 9, 1),
        (147, 'FRAUD_DETECTION_SHIPPING_BILLING_ZIP_MUST_SAME', '0', 'fraud_detection', 'same zip code for billing and shipping', 'select', '', 'billing and shipping zip code must be same for validating order', 8, 1),
        (148, 'FRAUD_DETECTION_ENABLE', '0', 'fraud_detection', 'Enable Fraud Detection', 'select', '', 'all the other fraud detection preferences will work when this is enabled', 1, 1),
        (149, 'FRAUD_DETECTION_ON_BASIS', 'both_ip_and_zip_same', 'fraud_detection', 'apply condition if', 'select', 'ip_address_same,billing_zip_same,both_ip_and_zip_same', 'condition for blocking user from placing fraud orders (with in 24 hours)', 4, 1),
        (150, 'FRAUD_DETECTION_MINIMUM_ORDER_COUNT', '3', 'fraud_detection', 'block order after', 'select', '1,2,3,4,5,6,7,8,9,10', 'if condition matches, user cannot place orders more than this count<br/>\ni.e. for ip_address, user will not be able to place more than three orders with in 24 Hours from same ip address(if count 3) ', 5, 1),
        (151, 'FRAUD_DETECTION_MAXIMUM_ORDER_FROM_ONE_ACCOUNT', '3', 'fraud_detection', 'maximum orders from same account', 'select', '-1,1,2,3,4,5,6,7,8,9,10', 'maximum count of orders with in 24 hours using same account<br/>set -1 for no limit', 6, 1),
        (152, 'DATE_FORMAT_SETTINGS', 'F j, Y', 'general', 'date format', 'select', 'Y-m-d,m/d/Y,d/m/Y,d-M-Y,F j&#44; Y', 'this format will be used for all date values on frontend<br/>\r\nY-m-d - 2014-05-01<br/>\r\nm/d/Y - 05/01/2014<br/>\r\nd/m/Y - 01/05/2014<br/>\r\nd-M-Y - 01-May-2014<br/>\r\nF j&#44; Y - May 01&#44; 2014', 25, 1),
        (153, 'TIME_FORMAT_SETTINGS', 'g:i A', 'general', 'time format', 'select', 'g:i a,g:i A,H:i', 'this time format will be used on frontend<br/>\r\ng:i a - 10:11 pm<br/>\r\ng:i A - 10:11 PM<br/>\r\nH:i - 22:11<br/>', 27, 1),
        (154, 'BLACKLIST_IP_ADDRESS', '', 'security', 'Black List IP Addresses', 'textarea', '', 'add comma separated ip addresses, user will not be able to access using these ip addresses ', 0, 1),
        (155, 'SITE_TOP_HEADER_TEXT', '', 'general', 'top promo text', 'textarea', '', 'this promo text will be shown on the top of the site on all pages', 4, 1),
        (156, 'FRAUD_DETECTION_WHITELIST_IP', '', 'fraud_detection', 'Whitelist IP Addresses', 'textarea', '', 'add comma separated whitelist ip addresses.<br/>maximum order count limit from same ip will not be applicable on these ip addresses', 8, 1),
        (157, 'SEND_NEW_ACCOUNT_EMAIL_USER', '1', 'email', 'send email to user on new account register', 'select', '', '', 17, 1),
        (158, 'SITE_STATUS', 'Sandbox', 'general', 'site status', 'select', 'Live,Sandbox', 'site will work exactly same in both the modes<br/>\r\nbut in ''sandbox'' mode, an alert will be shown on every page that site is working in test mode.', 3, 1),
        (159, 'SHOW_ON_CATEGORY_BASIS', '0', 'product', 'Show Products on Category Basis', 'select', '', 'Show products in main category on a category by category basis', 25, 1),
        (160, 'TRACKING_CODE_FOOTER', '', 'tracking_codes', 'Footer Tracking Codes', 'textarea', '', 'Space to add various tracking codes to the bottom of the website', 2, 1),
        (161, 'TRACKING_CODE_HEADER', '', 'tracking_codes', 'Header Tracking Codes', 'textarea', '', 'Space to add various tracking codes to the &lt;head&gt; of the website', 1, 1),
        (162, 'PASSWORD_REMINDER', '1', 'email', 'Password reminder', 'select', '', '', 28, 1),
        (163, 'PASSWORD_REMINDER_FREQUENCY', '7', 'email', 'Password reminder frenquency(In Days)', 'text', '', '', 29, 1),
        (164, 'SITE_LOGO', '', 'general', 'Site Logo', 'image', '', '', 0, 1),
        (165, 'STORE_SUPPORT_EMAIL', '$store_support_email', 'email', 'Store support email', 'text', '', '', 1, 1),
        (166, 'STORE_BUSINESS_NAME', '$store_business_name', 'address', 'Store business name', 'text', '', '', 1, 1),
        (167, 'STORE_LEGAL_NAME', '$store_legal_name', 'address', 'Store legal name', 'text', '', '', 1, 1),
        (168, 'STORE_ADDRESS1', '$store_address1', 'address', 'Store address1', 'text', '', '', 1, 1),
        (169, 'STORE_ADDRESS2', '$store_address2', 'address', 'Store address2', 'text', '', '', 1, 1),
        (170, 'STORE_CITY', '$store_city', 'address', 'Store city', 'text', '', '', 1, 1),
        (171, 'STORE_STATE', '$store_state', 'address', 'Store state', 'text', '', '', 1, 1),
        (172, 'STORE_COUNTRY', '$store_country', 'address', 'Store country', 'text', '', '', 1, 1),
        (173, 'STORE_ZIP', '$store_zip', 'address', 'Store zip', 'text', '', '', 1, 1),
        (174, 'STORE_NUMBER', '$store_phone', 'address', 'Store phone', 'text', '', '', 1, 1);
        ";
        //  echo $sample; die;
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "shipping` (`id`, `name`, `code`, `carrier`, `desc`, `logo`, `tracking_url`, `is_active`, `is_deleted`, `is_default`, `sequence`, `position`, `date_add`, `handling_charges`) VALUES
        (1, 'Flat Rate', 'flat', 'DPD', 'Flat price can be apply on per order or per product.', 'flat', '#', '0', '0', '1', 1, 1, '2014-05-15 13:15:35', 2),
        (2, 'Price Based', 'price', 'ONDOT', 'Shipping will be apply based on the order price.', 'price', '#', '1', '0', '0', 2, 2, '2014-05-15 13:15:35', 5),
        (3, 'Weight Based', 'weight', 'UPC', 'Order weight will decide the shipping cost.', 'weight', '#', '0', '0', '0', 3, 3, '2014-05-15 13:15:35', 0),
        (4, 'FedEx', 'fedex', 'FedEx', 'Shipping based on the fedex courier service', 'fedex', 'https://www.fedex.com/fedextrack/?action=track', '0', '1', '0', 4, 4, '2014-05-15 13:15:35', 0),
        (5, 'Local Pickup', 'pickup', '', 'Customer will collect the order from store.', 'pickup', '', '0', '1', '0', 5, 5, '2014-05-15 13:15:35', 0);
        ";
        mysql_query($sample, $this->dbconn);
        /*
          $sample = "INSERT INTO `" . $this->prefix . "content` (`id`, `name`, `urlname`, `page`, `short_description`, `meta_keyword`, `meta_description`, `publish_date`, `page_name`, `position`, `is_deleted`, `image`, `meta_robots_index`, `meta_robots_follow`, `include_xml_sitemap`, `sitemap_priority`, `canonical`) VALUES
          (1, 'Homepage', 'homepage', '', NULL, 'Homepage', 'Homepage', '2014-07-01 16:08:47', 'eCommerce  |  Page  |  Homepage', 0, 0, NULL, 'index', 'nofollow', 'global', '0.9', 'ASDASD'),
          (2, 'About Us ', 'about-us', 't;upload/cms/images/about-image.jpg&quot; style=&quot;width: 1140px; height: 284px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;h1 class=&quot;center&quot;&gt;&lt;span class=&quot;light&quot;&gt;A&lt;/span&gt; Little Something About Us&lt;/h1&gt;\r\n\r\n&lt;hr class=&quot;divider&quot; /&gt;\r\n&lt;div class=&quot;text-shrink&quot;&gt;\r\n&lt;p class=&quot;text-highlight&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate, leo vel malesuada tincidunt, purus nunc tristique erat, at elementum tellus mi nec mi. Nunc rutrum ullamcorper blandit.&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;hr class=&quot;divider  divider-about&quot; /&gt;\r\n&lt;div class=&quot;row&quot;&gt;\r\n&lt;div class=&quot;col-xs-12 col-sm-6&quot;&gt;\r\n&lt;h3&gt;&lt;span class=&quot;light&quot;&gt;The&lt;/span&gt; Beginnings&lt;/h3&gt;\r\n\r\n&lt;p class=&quot;text&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pellentesque consectetur leo, eu porta metus. Nulla justo lacus, aliquam vel libero nec, faucibus enim tristique, molestie ante. Nunc a dolor eget lectus venenatis ullamcorper. Nullam dictum elementum lacus quis facilisis. Phasellus mollis tempus tempus. Fusce ut sagittis quam, quis facilisis tellus. Nam vel aliquet leo. Integer lacus tortor, ullamcorper sit amet interdum at, pulvinar sit amet risus.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;h3&gt;&lt;span class=&quot;light&quot;&gt;What&amp;#39;s&lt;/span&gt; Happening Nowdays&lt;/h3&gt;\r\n\r\n&lt;p class=&quot;text&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pellentesque consectetur leo, eu porta metus. Nulla justo lacus, aliquam vel libero nec, viverra placerat neque. Cras nec nunc sollicitudin, faucibus enim tristique, molestie ante. Nunc a dolor eget lectus venenatis ullamcorper. Nullam dictum elementum lacus quis facilisis. Etiam sit amet euismod eros. Phasellus mollis tempus tempus. Fusce ut sagittis quam, quis facilisis tellus. Nam vel aliquet leo. Integer lacus tortor, ullamcorper sit amet interdum at, pulvinar sit amet risus.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;col-xs-12 col-sm-6&quot;&gt;\r\n&lt;h3&gt;&lt;span class=&quot;light&quot;&gt;Our&lt;/span&gt; Plans in the Future&lt;/h3&gt;\r\n\r\n&lt;p class=&quot;text&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pellentesque consectetur leo, eu porta metus. Nulla justo lacus, aliquam vel libero nec, viverra placerat neque. Cras nec nunc sollicitudin, faucibus enim tristique, molestie ante. Nunc a dolor eget lectus venenatis ullamcorper. Nullam dictum elementum lacus quis facilisis. Etiam sit amet euismod eros. Phasellus mollis tempus tempus. Fusce ut sagittis quam, quis facilisis tellus. Nam vel aliquet leo. Integer lacus tortor, ullamcorper sit amet interdum at, pulvinar sit amet risus.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;h3&gt;&lt;span class=&quot;light&quot;&gt;Bottom&lt;/span&gt; Line&lt;/h3&gt;\r\n\r\n&lt;p class=&quot;text&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pellentesque consectetur leo, eu porta metus. Nulla justo lacus, aliquam vel libero nec, viverra placerat neque. Cras nec nunc sollicitudin, lectus venenatis ullamcorper. Nullam dictum elementum lacus quis facilisis. Etiam sit amet euismod eros. Phasellus mollis tempus&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n', NULL, 'A Little Something About Us', 'A Little Something About Us', '2014-07-09 14:59:22', 'A Little Something About Us', 0, 0, NULL, 'global', 'follow', 'global', '0.9', ''),
          (3, 'Contact', 'contact', '&lt;p&gt;asfd&lt;/p&gt;\r\n', NULL, 'Contact', 'Contact', '2014-07-30 14:47:30', 'Contact', 0, 0, NULL, 'global', 'follow', 'exclude', '0.9', ''),
          (4, 'Terms of Service for customer', 'terms-and-conditions', '&lt;h1&gt;Terms of Service for customer&lt;/h1&gt;\r\n&lt;p&gt;Coming Soon&lt;/p&gt;\r\n', NULL, 'Terms and Conditions', 'Terms and Conditions', '2014-07-30 14:47:33', 'Terms and Conditions', 0, 0, NULL, 'global', 'follow', 'include', '0.9', ''),
          (5, 'Terms of Service for store owner', 'terms-of-service-for-store-owner-10', '<h1>Terms of Service for store owner</h1><p>Coming Soon</p>', NULL, 'Terms of Service for store owner', 'Terms of Service for store owner', '2014-07-30 14:47:38', 'eCommerce  |  Page  |  Terms Of Service For Store Owner', 0, 0, NULL, 'global', 'follow', 'global', 'global', ''),
          (6, 'Terms of Service for supplier', 'terms-of-service-for-supplier-9', '<h1>Terms of Service for supplier</h1>\r\n\r\n<p>Coming Soon</p>\r\n', NULL, 'Terms of Service for supplier', 'Terms of Service for supplier', '2014-07-30 14:47:35', 'eCommerce  |  Page  |  Terms Of Service For Supplier', 0, 0, NULL, 'global', 'follow', 'global', 'global', '');
          ";
          mysql_query($sample, $this->dbconn);
         */
        $sample = "INSERT INTO `" . $this->prefix . "cart_rule_type` (`id`, `name`, `desc`, `free_shipping`, `gift`, `discount`, `is_active`, `is_deleted`, `position`, `date_add`, `date_upd`) VALUES
        (1, 'Buy X Brand Get free Gift', 'free gift will be sent with specific brand product.', '0', '1', '0', '1', '0', 1, '2014-05-23 02:17:26', '2014-05-23 12:55:49'),
        (2, 'Buy X Brand Get free Shipping', 'free shipping will be applied with specific brand product.', '1', '0', '0', '1', '0', 2, '2014-05-23 02:17:26', '2014-05-23 13:08:53'),
        (3, 'Buy X Brand Get Discount', 'discount will be applied with specific brand product.', '0', '0', '1', '1', '0', 3, '2014-05-23 02:17:26', '2014-05-23 13:08:53'),
        (4, 'Buy X Category Products Get Discount', 'discount will be applied for specific category products.', '0', '0', '1', '1', '0', 3, '2014-05-23 02:17:26', '2014-05-23 13:08:53'),
        (5, 'Buy X Category Products Get Free Shipping', 'free shipping will be applied for specific category products.', '1', '0', '0', '1', '0', 3, '2014-05-23 02:17:26', '2014-05-23 13:08:53'),
        (6, 'Buy X Category Products Get Free Gift', 'free gift will be sent for specific category products.', '0', '1', '0', '1', '0', 3, '2014-05-23 02:17:26', '2014-05-23 13:08:53'),
        (7, 'Buy X Supplier Products Get Free Gift', 'free gift will be sent for specific supplier products.', '0', '1', '0', '1', '0', 3, '2014-05-23 02:17:26', '2014-05-23 13:08:53'),
        (8, 'Buy X Supplier Products Get Free Shipping', 'free shipping will be applied for specific supplier products.', '1', '0', '0', '1', '0', 3, '2014-05-23 02:17:26', '2014-05-23 13:08:53'),
        (9, 'Buy X Supplier Products Get Discount', 'discount will be applied for specific supplier products.', '0', '0', '1', '1', '0', 3, '2014-05-23 02:17:26', '2014-05-23 13:08:53');
        ";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "cart_rule_type_selection` (`id`, `cart_rule_type_id`, `selection`, `is_main`, `position`, `date_add`) VALUES
        (3, 1, 'brand', '1', 1, '2014-05-23 17:30:26'),
        (5, 1, 'country', '0', 6, '2014-05-23 17:30:28'),
        (6, 1, 'user', '0', 7, '2014-05-23 17:30:28'),
        (7, 1, 'product', '0', 3, '2014-05-23 17:30:28'),
        (8, 1, 'category', '0', 2, '2014-05-23 17:30:28'),
        (9, 1, 'supplier', '0', 5, '2014-05-23 17:30:28'),
        (10, 1, 'attribute', '0', 4, '2014-05-23 17:30:28'),
        (11, 2, 'brand', '1', 1, '2014-05-23 17:30:28'),
        (13, 2, 'country', '0', 6, '2014-05-23 17:30:28'),
        (14, 2, 'user', '0', 7, '2014-05-23 17:30:28'),
        (15, 2, 'product', '0', 3, '2014-05-23 17:30:28'),
        (16, 2, 'category', '0', 2, '2014-05-23 17:30:28'),
        (17, 2, 'supplier', '0', 5, '2014-05-23 17:30:28'),
        (18, 2, 'attribute', '0', 4, '2014-05-23 17:30:28'),
        (19, 3, 'brand', '1', 1, '2014-05-23 17:30:28'),
        (21, 3, 'country', '0', 6, '2014-05-23 17:30:28'),
        (22, 3, 'user', '0', 7, '2014-05-23 17:30:28'),
        (23, 3, 'product', '0', 3, '2014-05-23 17:30:28'),
        (24, 3, 'category', '0', 2, '2014-05-23 17:30:28'),
        (25, 3, 'supplier', '0', 5, '2014-05-23 17:30:28'),
        (26, 3, 'attribute', '0', 4, '2014-05-23 17:30:28'),
        (27, 4, 'brand', '0', 2, '2014-05-23 17:33:40'),
        (29, 4, 'country', '0', 6, '2014-05-23 17:33:40'),
        (30, 4, 'user', '0', 7, '2014-05-23 17:33:40'),
        (31, 4, 'product', '0', 4, '2014-05-23 17:33:40'),
        (32, 4, 'category', '1', 1, '2014-05-23 17:33:40'),
        (33, 4, 'supplier', '0', 3, '2014-05-23 17:33:40'),
        (34, 4, 'attribute', '0', 5, '2014-05-23 17:33:40'),
        (35, 5, 'brand', '0', 2, '2014-05-23 17:33:40'),
        (37, 5, 'country', '0', 6, '2014-05-23 17:33:40'),
        (38, 5, 'user', '0', 7, '2014-05-23 17:33:40'),
        (39, 5, 'product', '0', 4, '2014-05-23 17:33:40'),
        (40, 5, 'category', '1', 1, '2014-05-23 17:33:40'),
        (41, 5, 'supplier', '0', 3, '2014-05-23 17:33:40'),
        (42, 5, 'attribute', '0', 5, '2014-05-23 17:33:42'),
        (43, 6, 'brand', '0', 2, '2014-05-23 17:33:43'),
        (45, 6, 'country', '0', 6, '2014-05-23 17:33:43'),
        (46, 6, 'user', '0', 7, '2014-05-23 17:33:43'),
        (47, 6, 'product', '0', 4, '2014-05-23 17:33:43'),
        (48, 6, 'category', '1', 1, '2014-05-23 17:33:43'),
        (49, 6, 'supplier', '0', 3, '2014-05-23 17:33:43'),
        (50, 6, 'attribute', '0', 5, '2014-05-23 17:33:43'),
        (51, 7, 'brand', '0', 2, '2014-05-23 17:45:11'),
        (53, 7, 'country', '0', 6, '2014-05-23 17:45:11'),
        (54, 7, 'user', '0', 7, '2014-05-23 17:45:11'),
        (55, 7, 'product', '0', 4, '2014-05-23 17:45:11'),
        (56, 7, 'category', '0', 3, '2014-05-23 17:45:11'),
        (57, 7, 'supplier', '1', 1, '2014-05-23 17:45:11'),
        (58, 7, 'attribute', '0', 5, '2014-05-23 17:45:11'),
        (59, 8, 'brand', '0', 2, '2014-05-23 17:45:11'),
        (61, 8, 'country', '0', 6, '2014-05-23 17:45:11'),
        (62, 8, 'user', '0', 7, '2014-05-23 17:45:11'),
        (63, 8, 'product', '0', 4, '2014-05-23 17:45:11'),
        (64, 8, 'category', '0', 3, '2014-05-23 17:45:11'),
        (65, 8, 'supplier', '1', 1, '2014-05-23 17:45:11'),
        (66, 8, 'attribute', '0', 5, '2014-05-23 17:45:11'),
        (67, 9, 'brand', '0', 2, '2014-05-23 17:45:11'),
        (69, 9, 'country', '0', 6, '2014-05-23 17:45:12'),
        (70, 9, 'user', '0', 7, '2014-05-23 17:45:12'),
        (71, 9, 'product', '0', 4, '2014-05-23 17:45:12'),
        (72, 9, 'category', '0', 3, '2014-05-23 17:45:12'),
        (73, 9, 'supplier', '1', 1, '2014-05-23 17:45:12'),
        (74, 9, 'attribute', '0', 5, '2014-05-23 17:45:12');
        ";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "tax_rule_group` (`id`, `name`, `is_active`, `date_add`) VALUES
        (1, 'Default', '1', '2014-07-22 06:28:07')";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "attribute_super_set` (`id`, `name`, `position`, `date_add`) VALUES
                (1, 'Sex Toys', 1, '2014-04-23 19:54:27');
        ";
        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "slide` (`id`, `slideshow_id`, `title`, `short_description`, `image`, `position`, `is_active`, `link`, `is_deleted`, `subheading`) VALUES
                (1, 1, 'Locally Owned &amp; Operated!', 'Awesome oppurtunity to save a lof of money on', 'main_banner.jpg', 1, '1', '/extreme-overstock-sale/', 0, NULL);
        ";
        mysql_query($sample, $this->dbconn);


        if (!isset($_SESSION["add_dummy_data"])) {

            $sample = "INSERT INTO `" . $this->prefix . "navigation` (`id`, `parent_id`, `nav_id`, `nav_class`, `is_mega_menu`, `navigation_title`, `navigation_link`, `position`, `is_active`, `navigation_query`, `is_external`, `open_in_new`, `is_deleted`, `date_add`, `date_upd`) VALUES
        (1, 0, 1, '', 0, 'New', 'new_products', 1, 1, NULL, 0, 0, 0, '2015-01-14 02:24:10', '2015-02-17 14:22:47'),
        (2, 0, 1, 'red', 0, 'Hot Deals', NULL, 2, 1, NULL, 1, 0, 0, '2015-01-14 02:25:17', '2015-02-17 14:22:48'),
        (3, 0, 1, '', 1, 'Vibrators', NULL, 3, 1, NULL, 1, 0, 0, '2015-01-14 02:25:34', '2015-02-17 14:22:49'),
        (4, 0, 1, '', 1, 'Dildos & Anal', NULL, 4, 1, NULL, 1, 0, 0, '2015-01-14 02:26:00', '2015-02-18 05:23:33'),
        (5, 0, 1, '', 1, 'For Him', NULL, 5, 1, NULL, 1, 0, 0, '2015-01-14 02:26:59', '2015-02-17 14:22:51'),
        (6, 0, 1, '', 1, 'For Her', NULL, 6, 1, NULL, 1, 0, 0, '2015-01-14 02:27:17', '2015-01-14 15:29:42'),
        (7, 0, 1, '', 1, 'For Couples', NULL, 7, 1, NULL, 1, 0, 0, '2015-01-14 02:27:29', '2015-01-14 15:29:43'),
        (8, 0, 1, '', 1, 'Lube', NULL, 8, 1, NULL, 1, 0, 0, '2015-01-14 02:27:41', '2015-01-14 15:29:44'),
        (9, 0, 1, '', 1, 'BDSM & Knik', NULL, 9, 1, NULL, 1, 0, 0, '2015-01-14 02:28:38', '2015-01-14 15:29:47'),
        (10, 2, 1, '', 0, 'Overstock', 'shop', 1, 1, 'id=215', 0, 0, 0, '2015-01-14 02:31:23', '2015-01-14 15:33:29'),
        (11, 2, 1, '', 0, 'Clearance', 'shop', 2, 1, 'id=218', 0, 0, 0, '2015-01-14 02:32:35', '2015-01-14 17:07:59'),
        (12, 3, 1, '', 0, 'View All Vibrators', 'shop', NULL, 1, 'id=262', 0, 0, 0, '2015-01-14 02:36:03', '2015-01-14 15:36:03'),
        (13, 3, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 02:36:51', '2015-01-14 15:36:51'),
        (14, 3, 1, '', 0, 'Column3', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 02:37:00', '2015-01-14 15:37:00'),
        (15, 12, 1, '', 0, 'Rabbit Vibrators', 'shop', NULL, 1, 'id=33', 0, 0, 0, '2015-01-14 02:37:35', '2015-01-14 15:37:35'),
        (16, 12, 1, '', 0, 'G Spot Vibrators', 'shop', NULL, 1, 'id=96', 0, 0, 0, '2015-01-14 02:38:02', '2015-01-14 15:38:38'),
        (17, 12, 1, '', 0, 'Eggs & Bullets', 'shop', NULL, 1, 'id=94', 0, 0, 0, '2015-01-14 02:39:15', '2015-01-14 15:39:15'),
        (18, 12, 1, '', 0, 'Wand Massagers', 'shop', NULL, 1, 'id=99', 0, 0, 0, '2015-01-14 02:39:45', '2015-01-14 15:39:45'),
        (19, 13, 1, '', 0, 'Vibrator Kits', 'shop', 1, 1, 'id=98', 0, 0, 0, '2015-01-14 02:43:55', '2015-01-14 15:44:32'),
        (20, 13, 1, '', 0, 'Silicone Vibrators', 'shop', 2, 1, 'id=208', 0, 0, 0, '2015-01-14 02:44:27', '2015-01-14 15:54:24'),
        (21, 14, 1, '', 0, 'Anal Vibrators', 'shop', 1, 1, 'id=1', 0, 0, 0, '2015-01-14 02:45:37', '2015-01-14 15:46:12'),
        (22, 14, 1, '', 0, 'Discreet Vibrators', 'shop', 2, 1, 'id=95', 0, 0, 0, '2015-01-14 02:46:05', '2015-01-14 15:46:13'),
        (23, 14, 1, '', 0, 'Realistic Vibrators', 'shop', 3, 1, 'id=97', 0, 0, 0, '2015-01-14 02:46:45', '2015-01-14 15:46:50'),
        (24, 14, 1, '', 0, 'Prostate Vibrators', 'shop', 4, 1, 'id=106', 0, 0, 0, '2015-01-14 02:47:51', '2015-01-14 15:48:02'),
        (25, 14, 1, '', 0, 'Vibrating Cock Rings', 'shop', 5, 1, 'id=130', 0, 0, 0, '2015-01-14 02:48:35', '2015-01-14 15:48:39'),
        (26, 4, 1, '', 0, 'View All Dildos & Anal Toys', 'shop', NULL, 1, 'id=264', 0, 0, 0, '2015-01-14 02:57:14', '2015-01-14 15:57:14'),
        (27, 4, 1, '', 0, 'Glass & Steel', 'shop', NULL, 1, 'id=230', 0, 0, 0, '2015-01-14 02:57:55', '2015-01-14 15:57:55'),
        (28, 4, 1, '', 0, 'Column3', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 02:58:12', '2015-01-14 15:58:12'),
        (29, 26, 1, '', 0, 'Realistic', 'shop', NULL, 1, 'id=133', 0, 0, 0, '2015-01-14 02:59:11', '2015-01-14 15:59:11'),
        (30, 26, 1, '', 0, 'Classic Dildos', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-01-14 03:01:05', '2015-01-14 16:01:05'),
        (31, 26, 1, '', 0, 'Butt Plugs', 'shop', NULL, 1, 'id=244', 0, 0, 0, '2015-01-14 03:02:12', '2015-01-14 16:02:12'),
        (32, 26, 1, '', 0, 'Beads & Balls', 'shop', NULL, 1, 'id=105', 0, 0, 0, '2015-01-14 03:03:11', '2015-01-14 16:03:11'),
        (33, 26, 1, '', 0, 'G-Spot', 'shop', NULL, 1, 'id=245', 0, 0, 0, '2015-01-14 03:03:45', '2015-01-14 16:03:45'),
        (34, 26, 1, '', 0, 'Prostate Massagers', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-01-14 03:04:48', '2015-01-14 16:04:48'),
        (35, 26, 1, '', 0, 'Vibrating', 'shop', NULL, 1, 'id=38', 0, 0, 0, '2015-01-14 03:05:40', '2015-01-14 16:05:40'),
        (36, 26, 1, '', 0, 'Premium Silicone', 'shop', NULL, 1, 'id=207', 0, 0, 0, '2015-01-14 03:06:25', '2015-01-14 16:06:25'),
        (37, 26, 1, '', 0, 'Anal Toys', 'shop', NULL, 1, 'id=2', 0, 0, 0, '2015-01-14 03:07:17', '2015-01-14 16:07:17'),
        (38, 27, 1, '', 0, 'Glass Dildos', 'shop', NULL, 1, 'id=144', 0, 0, 0, '2015-01-14 03:07:57', '2015-01-14 16:07:57'),
        (39, 27, 1, '', 0, 'Steel Dildos', 'shop', NULL, 1, 'id=145', 0, 0, 0, '2015-01-14 03:08:40', '2015-01-14 16:08:40'),
        (40, 27, 1, '', 0, 'Huge Insertables', 'shop', NULL, 1, 'id=22', 0, 0, 0, '2015-01-14 03:09:18', '2015-01-14 16:09:18'),
        (41, 27, 1, '', 0, 'Inflatable Dildos', 'shop', NULL, 1, 'id=113', 0, 0, 0, '2015-01-14 03:09:36', '2015-01-14 16:09:36'),
        (42, 28, 1, '', 0, 'Strap-Ons and Harnesses', 'shop', NULL, 1, 'id=35', 0, 0, 0, '2015-01-14 03:11:35', '2015-01-14 16:11:35'),
        (43, 28, 1, '', 0, 'Penis Sheaths & Extenders', 'shop', NULL, 1, 'id=154', 0, 0, 0, '2015-01-14 03:12:10', '2015-01-14 16:12:10'),
        (44, 28, 1, '', 0, 'Enema Gear', 'shop', NULL, 1, 'id=159', 0, 0, 0, '2015-01-14 03:12:45', '2015-01-14 16:12:45'),
        (45, 5, 1, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=259', 0, 0, 0, '2015-01-14 03:23:22', '2015-01-14 16:23:22'),
        (46, 5, 1, '', 0, 'Cock Rings', 'shop', NULL, 1, 'id=9', 0, 0, 0, '2015-01-14 03:23:50', '2015-01-14 16:23:50'),
        (47, 5, 1, '', 0, 'Penis Sheaths & Extenders', 'shop', NULL, 0, 'id=154', 0, 0, 0, '2015-01-14 03:24:26', '2015-01-14 16:37:09'),
        (48, 45, 1, '', 0, 'Masturbation Toys', 'shop', NULL, 1, 'id=26', 0, 0, 0, '2015-01-14 03:24:53', '2015-01-14 16:24:53'),
        (49, 45, 1, '', 0, 'Penis Pumps', 'shop', NULL, 1, 'id=153', 0, 0, 0, '2015-01-14 03:25:13', '2015-01-14 16:25:13'),
        (50, 45, 1, '', 0, 'Prostate Stimulators', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-01-14 03:25:30', '2015-01-14 16:25:30'),
        (51, 45, 1, '', 0, 'Butt Plugs', 'shop', NULL, 1, 'id=244', 0, 0, 0, '2015-01-14 03:25:52', '2015-01-14 16:25:52'),
        (52, 45, 1, '', 0, 'Love Dolls', 'shop', NULL, 1, 'id=25', 0, 0, 0, '2015-01-14 03:26:12', '2015-01-14 16:26:12'),
        (53, 45, 1, '', 0, 'Chastity', 'shop', NULL, 1, 'id=101', 0, 0, 0, '2015-01-14 03:26:32', '2015-01-14 16:26:32'),
        (54, 45, 1, '', 0, 'Electrosex', 'shop', NULL, 1, 'id=13', 0, 0, 0, '2015-01-14 03:26:53', '2015-01-14 16:26:53'),
        (55, 45, 1, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-01-14 03:27:10', '2015-01-14 16:27:10'),
        (56, 45, 1, '', 0, 'Lubes For Men', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-01-14 03:27:34', '2015-01-14 16:27:34'),
        (57, 46, 1, '', 0, 'Metal Cock Rings', 'shop', NULL, 1, 'id=131', 0, 0, 0, '2015-01-14 03:29:26', '2015-01-14 16:29:26'),
        (58, 46, 1, '', 0, 'Multi-Ring Cock Rings', 'shop', NULL, 1, 'id=132', 0, 0, 0, '2015-01-14 03:29:43', '2015-01-14 16:29:43'),
        (59, 46, 1, '', 0, 'Vibrating Cock Rings', 'shop', NULL, 1, 'id=130', 0, 0, 0, '2015-01-14 03:29:59', '2015-01-14 16:29:59'),
        (60, 46, 1, 'text-strong', 0, 'Penis Sheaths &amp; Extenders', 'shop', NULL, 1, 'id=154', 0, 0, 0, '2015-01-14 03:36:49', '2015-01-14 16:48:30'),
        (61, 6, 1, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=260', 0, 0, 0, '2015-01-14 03:51:59', '2015-01-14 16:51:59'),
        (62, 6, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 03:52:25', '2015-01-14 16:52:25'),
        (63, 6, 1, '', 0, 'Column3', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 03:52:32', '2015-01-14 16:52:32'),
        (64, 61, 1, '', 0, 'Rabbit', 'shop', NULL, 1, 'id=33', 0, 0, 0, '2015-01-14 03:53:45', '2015-01-14 16:53:45'),
        (65, 61, 1, '', 0, 'G Spot', 'shop', NULL, 1, 'id=96', 0, 0, 0, '2015-01-14 03:54:14', '2015-01-14 16:54:14'),
        (66, 61, 1, '', 0, 'Eggs & Bullets', 'shop', NULL, 1, 'id=94', 0, 0, 0, '2015-01-14 03:55:05', '2015-01-14 16:55:05'),
        (67, 61, 1, '', 0, 'Wand Massagers', 'shop', NULL, 1, 'id=99', 0, 0, 0, '2015-01-14 03:55:25', '2015-01-14 16:55:25'),
        (68, 61, 1, '', 0, 'View All Vibrators', 'shop', NULL, 1, 'id=228', 0, 0, 0, '2015-01-14 03:55:59', '2015-01-14 16:55:59'),
        (69, 61, 1, '', 0, 'Vibrator Kits', 'shop', NULL, 1, 'id=98', 0, 0, 0, '2015-01-14 03:56:37', '2015-01-14 16:56:37'),
        (70, 61, 1, '', 0, 'Silicone', 'shop', NULL, 1, 'id=208', 0, 0, 0, '2015-01-14 03:56:58', '2015-01-14 16:56:58'),
        (71, 62, 1, '', 0, 'Dildos & Anal', 'shop', NULL, 1, 'id=229', 0, 0, 0, '2015-01-14 03:57:30', '2015-01-14 16:57:30'),
        (72, 62, 1, '', 0, 'Realistic Dildos', 'shop', NULL, 1, 'id=133', 0, 0, 0, '2015-01-14 03:57:48', '2015-01-14 16:57:48'),
        (73, 62, 1, '', 0, 'Huge', 'shop', NULL, 1, 'id=100', 0, 0, 0, '2015-01-14 03:58:10', '2015-01-14 16:58:10'),
        (74, 62, 1, '', 0, 'Anal Vibrators', 'shop', NULL, 1, 'id=1', 0, 0, 0, '2015-01-14 03:58:30', '2015-01-14 16:58:30'),
        (75, 62, 1, '', 0, 'Classic Dildos', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-01-14 03:58:50', '2015-01-14 16:58:50'),
        (76, 62, 1, '', 0, 'Beads & Balls', 'shop', NULL, 1, 'id=105', 0, 0, 0, '2015-01-14 03:59:18', '2015-01-14 16:59:18'),
        (77, 62, 1, '', 0, 'Premium Silicone', 'shop', NULL, 1, 'id=210', 0, 0, 0, '2015-01-14 03:59:46', '2015-01-14 16:59:46'),
        (78, 62, 1, '', 0, 'Glass Dildos', 'shop', NULL, 1, 'id=144', 0, 0, 0, '2015-01-14 04:00:04', '2015-01-14 17:00:04'),
        (79, 62, 1, '', 0, 'Steel Dildos', 'shop', NULL, 1, 'id=145', 0, 0, 0, '2015-01-14 04:00:20', '2015-01-14 17:00:20'),
        (80, 62, 1, '', 0, 'Inflatable', 'shop', NULL, 1, 'id=113', 0, 0, 0, '2015-01-14 04:00:38', '2015-01-14 17:00:39'),
        (81, 63, 1, '', 0, 'Strap-Ons and Harnesses', 'shop', NULL, 1, 'id=35', 0, 0, 0, '2015-01-14 04:01:14', '2015-01-14 17:01:14'),
        (82, 63, 1, '', 0, 'Nipple & Breast Pumps', 'shop', NULL, 1, 'id=155', 0, 0, 0, '2015-01-14 04:01:37', '2015-01-14 17:01:37'),
        (83, 63, 1, '', 0, 'Nipple Toys', 'shop', NULL, 1, 'id=29', 0, 0, 0, '2015-01-14 04:01:55', '2015-01-14 17:01:55'),
        (84, 63, 1, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-01-14 04:02:11', '2015-01-14 17:02:18'),
        (85, 63, 1, '', 0, 'Lubes For Women', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-01-14 04:02:46', '2015-01-14 17:02:46'),
        (86, 7, 1, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=261', 0, 0, 0, '2015-01-14 04:03:28', '2015-01-14 17:03:28'),
        (87, 7, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 04:03:44', '2015-01-14 17:03:44'),
        (88, 86, 1, '', 0, 'Swings & Sex Aids', 'shop', NULL, 1, 'id=36', 0, 0, 0, '2015-01-14 04:04:42', '2015-01-14 17:04:42'),
        (89, 86, 1, '', 0, 'Bedroom Bondage', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-01-14 04:05:18', '2015-01-14 17:05:18'),
        (90, 86, 1, '', 0, 'Strap Ons & Harnesses', 'shop', NULL, 1, 'id=35', 0, 0, 0, '2015-01-14 04:06:08', '2015-01-14 17:06:08'),
        (91, 86, 1, '', 0, 'Stripper Poles', 'shop', NULL, 1, 'id=188', 0, 0, 0, '2015-01-14 04:06:29', '2015-01-14 17:06:29'),
        (92, 87, 1, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-01-14 04:09:50', '2015-01-14 17:09:50'),
        (93, 87, 1, '', 0, 'Chastity', 'shop', NULL, 1, 'id=7', 0, 0, 0, '2015-01-14 04:10:16', '2015-01-14 17:10:16'),
        (94, 87, 1, '', 0, 'Lube For Couples', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-01-14 04:10:37', '2015-01-14 17:10:37'),
        (95, 8, 1, '', 0, 'View All Lubes', 'shop', NULL, 1, 'id=263', 0, 0, 0, '2015-01-14 04:13:08', '2015-01-14 17:13:08'),
        (96, 8, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 04:13:18', '2015-01-14 17:13:18'),
        (97, 95, 1, '', 0, 'Water Based Lube ', 'shop', NULL, 1, 'id=120', 0, 0, 0, '2015-01-14 04:13:42', '2015-01-14 17:13:50'),
        (98, 95, 1, '', 0, 'Silicone Based Lube', 'shop', NULL, 1, 'id=121', 0, 0, 0, '2015-01-14 04:15:21', '2015-01-14 17:16:31'),
        (99, 95, 1, '', 0, 'Lube Applicators', 'shop', NULL, 1, 'id=122', 0, 0, 0, '2015-01-14 04:17:03', '2015-01-14 17:17:03'),
        (100, 95, 1, '', 0, 'Oil Based', 'shop', NULL, 1, 'id=123', 0, 0, 0, '2015-01-14 04:17:25', '2015-01-14 17:17:25'),
        (101, 96, 1, '', 0, 'Anal Lube', 'shop', NULL, 1, 'id=118', 0, 0, 0, '2015-01-14 04:18:19', '2015-01-14 17:18:19'),
        (102, 96, 1, '', 0, 'Creams and Lotions', 'shop', NULL, 1, 'id=124', 0, 0, 0, '2015-01-14 04:18:41', '2015-01-14 17:18:41'),
        (103, 96, 1, '', 0, 'Flavored Lube', 'shop', NULL, 1, 'id=119', 0, 0, 0, '2015-01-14 04:19:42', '2015-01-14 17:19:42'),
        (104, 96, 1, '', 0, 'Electrosex Lubes and Cleaners', 'shop', NULL, 1, 'id=151', 0, 0, 0, '2015-01-14 04:20:22', '2015-01-14 17:20:22'),
        (105, 9, 1, '', 0, 'View All BDSM & Kink', 'shop', NULL, 1, 'id=265', 0, 0, 0, '2015-01-14 04:21:52', '2015-01-14 17:21:52'),
        (106, 9, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 04:22:04', '2015-01-14 17:22:04'),
        (107, 9, 1, '', 0, 'Column3', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 04:22:13', '2015-01-14 17:22:13'),
        (108, 105, 1, '', 0, 'Beginner Bondage', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-01-14 04:22:30', '2015-01-14 17:22:30'),
        (109, 105, 1, '', 0, 'Leather Bondage Goods', 'shop', NULL, 1, 'id=24', 0, 0, 0, '2015-01-14 04:22:47', '2015-01-14 17:22:47'),
        (110, 105, 1, '', 0, 'Wrist & Ankle Restraints', 'shop', NULL, 1, 'id=138', 0, 0, 0, '2015-01-14 04:23:11', '2015-01-14 17:23:11'),
        (111, 105, 1, '', 0, 'Collars', 'shop', NULL, 1, 'id=137', 0, 0, 0, '2015-01-14 04:23:29', '2015-01-14 17:23:29'),
        (112, 105, 1, '', 0, 'Blindfolds', 'shop', NULL, 1, 'id=21', 0, 0, 0, '2015-01-14 04:23:56', '2015-01-14 17:23:56'),
        (113, 106, 1, '', 0, 'Mouth Gags', 'shop', NULL, 1, 'id=28', 0, 0, 0, '2015-01-14 04:25:03', '2015-01-14 17:25:03'),
        (114, 106, 1, '', 0, 'Hoods & Muzzles', 'shop', NULL, 1, 'id=73', 0, 0, 0, '2015-01-14 04:26:02', '2015-01-14 17:26:02'),
        (115, 106, 1, '', 0, 'Spreader Bars', 'shop', NULL, 1, 'id=161', 0, 0, 0, '2015-01-14 04:26:37', '2015-01-14 17:26:37'),
        (116, 106, 1, '', 0, 'Handcuffs and Steel', 'shop', NULL, 1, 'id=19', 0, 0, 0, '2015-01-14 04:42:58', '2015-01-14 17:42:58'),
        (117, 106, 1, '', 0, 'Hoods and Blindfolds', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-01-14 04:43:17', '2015-01-14 17:43:17'),
        (118, 107, 1, '', 0, 'Enema Gear', 'shop', 1, 1, 'id=159', 0, 0, 0, '2015-01-14 04:48:34', '2015-01-19 16:21:58'),
        (119, 107, 1, '', 0, 'Fucking Machines', 'shop', 2, 1, 'id=16', 0, 0, 0, '2015-01-14 04:48:48', '2015-01-19 16:21:59'),
        (120, 107, 1, '', 0, 'Chastity Devices', 'shop', 3, 1, 'id=7', 0, 0, 0, '2015-01-14 04:49:19', '2015-01-19 16:22:01'),
        (121, 107, 1, '', 0, 'Crops & Whips', 'shop', 4, 1, 'id=114', 0, 0, 0, '2015-01-14 04:49:50', '2015-01-19 16:22:13'),
        (122, 107, 1, '', 0, 'Penis Jewlery', 'shop', 5, 1, 'id=31', 0, 0, 0, '2015-01-14 04:50:12', '2015-01-19 16:22:14'),
        (124, 0, 1, '', 0, 'New2', 'new_products', NULL, 1, NULL, 0, 0, 1, '0000-00-00 00:00:00', '2015-01-19 16:18:08'),
        (125, 0, 1, 'hi', 0, 'hi', 'h', NULL, 1, NULL, 1, 1, 1, '2015-01-19 03:29:57', '2015-01-19 16:30:27'),
        (126, 0, 1, '', 0, 'test', 'product', NULL, 1, 'id=3258', 0, 0, 1, '2015-01-19 03:56:50', '2015-01-19 17:18:47'),
        (127, 0, 1, '', 1, 'New2', 'shop', NULL, 1, 'id=4', 0, 0, 1, '2015-01-19 04:19:03', '2015-01-19 17:22:17'),
        (128, 127, 1, 'bbb', 0, 'Eggs & Bullets', 'cart', NULL, 1, NULL, 0, 0, 0, '2015-01-19 04:19:36', '2015-01-19 17:19:36'),
        (129, 127, 1, '', 0, 'Eggs & Bullets', 'shop', NULL, 1, 'id=42', 0, 0, 0, '2015-01-19 04:20:11', '2015-01-19 17:20:11'),
        (130, 128, 1, '', 0, 'Eggs & Bullets', 'product', NULL, 1, 'id=3256', 0, 0, 0, '2015-01-19 04:20:44', '2015-01-19 17:20:44'),
        (131, 0, 3, '', 0, 'New', 'new_products', NULL, 1, NULL, 0, 0, 0, '2015-02-11 21:48:10', '2015-02-12 06:01:01'),
        (132, 0, 3, '', 0, 'Hot Deals', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-11 22:22:43', '2015-02-12 06:22:43'),
        (133, 132, 3, '', 0, 'Over Stock', 'shop', NULL, 1, 'id=215', 0, 0, 0, '2015-02-11 22:23:37', '2015-02-12 06:23:37'),
        (134, 132, 3, '', 0, 'Clearance', 'shop', NULL, 1, 'id=218', 0, 0, 0, '2015-02-11 22:24:18', '2015-02-12 06:24:18'),
        (135, 0, 3, '', 0, 'Vibrators', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-11 22:25:18', '2015-02-12 06:25:18'),
        (136, 135, 3, '', 0, 'View All Vibrators', 'shop', NULL, 1, 'id=228', 0, 0, 0, '2015-02-11 22:28:32', '2015-02-12 06:28:32'),
        (137, 135, 3, '', 0, 'Vibrator Kits', 'shop', NULL, 1, 'id=98', 0, 0, 0, '2015-02-11 22:29:08', '2015-02-12 06:29:08'),
        (138, 135, 3, '', 0, 'Silicone Vibrators', 'shop', NULL, 1, 'id=208', 0, 0, 0, '2015-02-11 22:29:33', '2015-02-12 06:29:33'),
        (139, 135, 3, '', 0, 'Anal Vibrators', 'shop', NULL, 1, 'id=1', 0, 0, 0, '2015-02-11 22:30:12', '2015-02-12 06:30:12'),
        (140, 135, 3, '', 0, 'Discreet Vibrators', 'shop', NULL, 1, 'id=95', 0, 0, 0, '2015-02-11 22:30:49', '2015-02-12 06:30:49'),
        (141, 135, 3, '', 0, 'Realistic Vibrators', 'shop', NULL, 1, 'id=97', 0, 0, 0, '2015-02-11 22:31:27', '2015-02-12 06:31:27'),
        (142, 135, 3, '', 0, 'Prostate Vibrators', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-02-11 22:32:03', '2015-02-12 06:32:03'),
        (143, 135, 3, '', 0, 'Vibrating Cock Rings', 'shop', NULL, 1, 'id=130', 0, 0, 0, '2015-02-11 22:32:42', '2015-02-12 06:32:42'),
        (144, 135, 3, '', 0, 'Rabbit Vibrators', 'shop', NULL, 1, 'id=33', 0, 0, 0, '2015-02-11 22:33:09', '2015-02-12 06:33:09'),
        (145, 135, 3, '', 0, 'G Spot Vibrators', 'shop', NULL, 1, 'id=241', 0, 0, 0, '2015-02-11 22:34:12', '2015-02-12 06:34:12'),
        (146, 0, 3, '', 1, 'Dildos & Anal', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 01:16:51', '2015-02-13 12:54:49'),
        (147, 146, 3, '', 0, 'View All Dildos & Anal Toys', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-02-13 04:52:59', '2015-02-13 12:52:59'),
        (148, 146, 3, '', 0, ' Dildos & Anal by Category', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:54:41', '2015-02-13 12:54:41'),
        (149, 148, 3, '', 0, 'Realistic', 'shop', NULL, 1, 'id=133', 0, 0, 0, '2015-02-13 04:55:43', '2015-02-13 12:55:43'),
        (150, 148, 3, '', 0, 'Classic Dildos', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-02-13 04:56:08', '2015-02-13 12:56:08'),
        (151, 148, 3, '', 0, 'Butt Plugs', 'shop', NULL, 1, 'id=244', 0, 0, 0, '2015-02-13 04:56:27', '2015-02-13 12:56:27'),
        (152, 148, 3, '', 0, 'Beads & Balls', 'shop', NULL, 1, 'id=105', 0, 0, 0, '2015-02-13 04:57:00', '2015-02-13 12:57:00'),
        (153, 148, 3, '', 0, 'G-Spot', 'shop', NULL, 1, 'id=245', 0, 0, 0, '2015-02-13 04:57:24', '2015-02-13 12:57:24'),
        (154, 0, 3, '', 1, 'For Him', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:57:49', '2015-02-13 12:57:54'),
        (155, 148, 3, '', 0, 'Prostate Massagers', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-02-13 04:57:55', '2015-02-13 12:57:55'),
        (156, 148, 3, '', 0, 'Vibrating', 'shop', NULL, 1, 'id=38', 0, 0, 0, '2015-02-13 04:58:14', '2015-02-13 12:58:14'),
        (157, 0, 3, '', 0, 'For Her', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:58:15', '2015-02-13 12:58:15'),
        (158, 148, 3, '', 0, 'Premium Silicone', 'shop', NULL, 1, 'id=207', 0, 0, 0, '2015-02-13 04:58:42', '2015-02-13 12:58:42'),
        (159, 0, 3, '', 0, 'For Couples', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:58:53', '2015-02-13 12:58:53'),
        (160, 148, 3, '', 0, 'Anal Toys', 'shop', NULL, 1, 'id=2', 0, 0, 0, '2015-02-13 04:59:08', '2015-02-13 12:59:08'),
        (161, 0, 3, '', 0, 'Lube', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:59:14', '2015-02-13 12:59:14'),
        (162, 0, 3, '', 0, 'BDSM & Knik', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:59:39', '2015-02-13 12:59:39'),
        (163, 146, 3, '', 0, 'Glass & Steel Dildos', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:00:02', '2015-02-13 13:00:02'),
        (164, 163, 3, '', 0, 'Glass Dildos', 'shop', NULL, 1, 'id=144', 0, 0, 0, '2015-02-13 05:00:47', '2015-02-13 13:00:47'),
        (165, 162, 3, '', 0, 'View All BDSM & Kink', 'shop', NULL, 1, 'id=234', 0, 0, 0, '2015-02-13 05:00:59', '2015-02-13 13:00:59'),
        (166, 163, 3, '', 0, 'Steel Dildos', 'shop', NULL, 1, 'id=145', 0, 0, 0, '2015-02-13 05:01:15', '2015-02-13 13:01:15'),
        (167, 162, 3, '', 0, 'Mouth Gags', 'shop', NULL, 1, 'id=28', 0, 0, 0, '2015-02-13 05:01:30', '2015-02-13 13:01:30'),
        (168, 162, 3, '', 0, 'Hoods & Muzzles', 'shop', NULL, 1, 'id=73', 0, 0, 0, '2015-02-13 05:02:09', '2015-02-13 13:02:09'),
        (169, 163, 3, '', 0, 'Huge Insertables', 'shop', NULL, 1, 'id=22', 0, 0, 0, '2015-02-13 05:02:36', '2015-02-13 13:02:36'),
        (170, 162, 3, '', 0, 'Spreader Bars', 'shop', NULL, 1, 'id=161', 0, 0, 0, '2015-02-13 05:02:48', '2015-02-13 13:02:48'),
        (171, 163, 3, '', 0, 'Inflatable Dildos', 'shop', NULL, 1, 'id=113', 0, 0, 0, '2015-02-13 05:02:57', '2015-02-13 13:02:57'),
        (172, 162, 3, '', 0, 'Handcuffs and Steel', 'shop', NULL, 1, 'id=19', 0, 0, 0, '2015-02-13 05:03:13', '2015-02-13 13:03:13'),
        (173, 162, 3, '', 0, 'Hoods and Blindfolds', 'shop', NULL, 1, 'id=21', 0, 0, 0, '2015-02-13 05:03:32', '2015-02-13 13:03:32'),
        (174, 162, 3, '', 0, 'Enema Gear', 'shop', NULL, 1, 'id=159', 0, 0, 0, '2015-02-13 05:04:00', '2015-02-13 13:04:00'),
        (175, 146, 3, '', 0, 'Strap-Ons and Harnesses', '', NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:04:11', '2015-02-13 13:20:26'),
        (176, 162, 3, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-02-13 05:04:18', '2015-02-13 13:04:18'),
        (177, 146, 3, '', 0, 'Penis Sheaths & Extenders', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:04:36', '2015-02-13 13:04:36'),
        (178, 162, 3, '', 0, 'Chastity Devices', 'shop', NULL, 1, 'id=7', 0, 0, 0, '2015-02-13 05:04:37', '2015-02-13 13:04:37'),
        (179, 146, 3, '', 0, 'Enema Gear', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:04:54', '2015-02-13 13:04:54'),
        (180, 154, 3, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=101', 0, 0, 0, '2015-02-13 05:07:35', '2015-02-13 13:07:35'),
        (181, 154, 3, '', 0, 'Cock Rings', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:07:58', '2015-02-13 13:07:58'),
        (182, 161, 3, '', 0, 'View All Lubes', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-02-13 05:08:16', '2015-02-13 13:08:16'),
        (183, 181, 3, '', 0, 'Metal Cock Rings', 'shop', NULL, 1, 'id=131', 0, 0, 0, '2015-02-13 05:08:37', '2015-02-13 13:08:37'),
        (184, 161, 3, '', 0, 'Anal Lube', 'shop', NULL, 1, 'id=118', 0, 0, 0, '2015-02-13 05:08:47', '2015-02-13 13:08:47'),
        (185, 181, 3, '', 0, 'Multi-Ring Cock Rings', 'shop', NULL, 1, 'id=132', 0, 0, 0, '2015-02-13 05:08:54', '2015-02-13 13:08:54'),
        (186, 161, 3, '', 0, 'Creams and Lotions', 'shop', NULL, 1, 'id=124', 0, 0, 0, '2015-02-13 05:09:15', '2015-02-13 13:09:15'),
        (187, 181, 3, '', 0, 'Vibrating Cock Rings', 'shop', NULL, 1, 'id=130', 0, 0, 0, '2015-02-13 05:09:22', '2015-02-13 13:09:22'),
        (188, 161, 3, '', 0, 'Flavored Lube', 'shop', NULL, 1, 'id=119', 0, 0, 0, '2015-02-13 05:09:33', '2015-02-13 13:09:33'),
        (189, 161, 3, '', 0, 'Electrosex Lubes and Cleaners', 'shop', NULL, 1, 'id=151', 0, 0, 0, '2015-02-13 05:09:50', '2015-02-13 13:09:50'),
        (190, 161, 3, '', 0, 'Water Based Lube', 'shop', NULL, 1, 'id=120', 0, 0, 0, '2015-02-13 05:10:16', '2015-02-13 13:10:16'),
        (191, 154, 3, '', 0, 'Penis Sheaths & Extenders', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:10:49', '2015-02-13 13:10:49'),
        (192, 154, 3, '', 0, 'For Him By Category', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:11:23', '2015-02-13 13:11:23'),
        (193, 161, 3, '', 0, 'Silicone Based Lube', 'shop', NULL, 1, 'id=121', 0, 0, 0, '2015-02-13 05:11:41', '2015-02-13 13:11:41'),
        (194, 161, 3, '', 0, 'Lube Applicators', 'shop', NULL, 1, 'id=122', 0, 0, 0, '2015-02-13 05:11:57', '2015-02-13 13:11:57'),
        (195, 161, 3, '', 0, 'Oil Based', 'shop', NULL, 1, 'id=123', 0, 0, 0, '2015-02-13 05:12:12', '2015-02-13 13:12:12'),
        (196, 192, 3, '', 0, 'Masturbation Toys', 'shop', NULL, 1, 'id=26', 0, 0, 0, '2015-02-13 05:12:22', '2015-02-13 13:12:22'),
        (197, 192, 3, '', 0, 'Penis Pumps', 'shop', NULL, 1, 'id=153', 0, 0, 0, '2015-02-13 05:12:43', '2015-02-13 13:12:43'),
        (198, 192, 3, '', 0, 'Prostate Stimulators', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-02-13 05:13:05', '2015-02-13 13:13:05'),
        (199, 192, 3, '', 0, 'Butt Plugs', 'shop', NULL, 1, 'id=244', 0, 0, 0, '2015-02-13 05:13:30', '2015-02-13 13:13:30'),
        (200, 159, 3, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-02-13 05:13:33', '2015-02-13 13:13:33'),
        (201, 159, 3, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-02-13 05:13:49', '2015-02-13 13:13:49'),
        (202, 192, 3, '', 0, 'Love Dolls', 'shop', NULL, 1, 'id=25', 0, 0, 0, '2015-02-13 05:13:53', '2015-02-13 13:13:53'),
        (203, 159, 3, '', 0, 'Chastity', 'shop', NULL, 1, 'id=7', 0, 0, 0, '2015-02-13 05:14:06', '2015-02-13 13:14:06'),
        (204, 192, 3, '', 0, 'Chastity', 'shop', NULL, 1, 'id=101', 0, 0, 0, '2015-02-13 05:14:16', '2015-02-13 13:14:16'),
        (205, 192, 3, '', 0, 'Electrosex', 'shop', NULL, 1, 'id=13', 0, 0, 0, '2015-02-13 05:14:39', '2015-02-13 13:14:39'),
        (206, 159, 3, '', 0, 'Lube For Couples', 'shop', NULL, 1, 'id=118', 0, 0, 0, '2015-02-13 05:14:46', '2015-02-13 13:14:46'),
        (207, 192, 3, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-02-13 05:15:04', '2015-02-13 13:15:04'),
        (208, 159, 3, '', 0, 'Swings & Sex Aids', 'shop', NULL, 1, 'id=237', 0, 0, 0, '2015-02-13 05:15:11', '2015-02-13 13:15:11'),
        (209, 192, 3, '', 0, 'Lubes For Men', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-02-13 05:15:33', '2015-02-13 13:15:53'),
        (210, 159, 3, '', 0, 'Bedroom Bondage', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-02-13 05:15:51', '2015-02-13 13:15:51'),
        (211, 159, 3, '', 0, 'Strap Ons & Harnesses', 'shop', NULL, 1, 'id=35', 0, 0, 0, '2015-02-13 05:16:13', '2015-02-13 13:16:13'),
        (212, 159, 3, '', 0, 'Stripper Poles', 'shop', NULL, 1, 'id=188', 0, 0, 0, '2015-02-13 05:16:36', '2015-02-13 13:16:36'),
        (213, 157, 3, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=102', 0, 0, 0, '2015-02-13 05:17:03', '2015-02-13 13:17:03'),
        (214, 157, 3, '', 0, 'Dildos & Anal', 'shop', NULL, 1, 'id=229', 0, 0, 0, '2015-02-13 05:17:35', '2015-02-13 13:17:35'),
        (215, 157, 3, '', 0, 'Realistic Dildos', 'shop', NULL, 1, 'id=133', 0, 0, 0, '2015-02-13 05:17:58', '2015-02-13 13:17:58'),
        (216, 157, 3, '', 0, 'Huge', 'shop', NULL, 1, 'id=100', 0, 0, 0, '2015-02-13 05:19:15', '2015-02-13 13:19:15'),
        (217, 157, 3, '', 0, 'Anal Vibrators', 'shop', NULL, 1, 'id=1', 0, 0, 0, '2015-02-13 05:19:40', '2015-02-13 13:19:40'),
        (218, 157, 3, '', 0, 'Classic Dildos', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-02-13 05:20:00', '2015-02-13 13:20:00'),
        (219, 157, 3, '', 0, 'Beads & Balls', 'shop', NULL, 1, 'id=105', 0, 0, 0, '2015-02-13 05:20:23', '2015-02-13 13:20:23'),
        (220, 157, 3, '', 0, 'Premium Silicone', 'shop', NULL, 1, 'id=210', 0, 0, 0, '2015-02-13 05:20:44', '2015-02-13 13:20:44'),
        (221, 157, 3, '', 0, 'Glass Dildos', 'shop', NULL, 1, 'id=144', 0, 0, 0, '2015-02-13 05:21:11', '2015-02-13 13:21:11');";
            mysql_query($sample, $this->dbconn);
        }
//        $sample = "INSERT INTO `" . $this->prefix . "cart_rule` (`id`, `cart_rule_type`, `name`, `coupon_code`, `from_date`, `to_date`, `is_auto`, `quantity`, `quantity_per_user`, `priority`, `is_active`, `is_deleted`, `min_cart_amount`, `min_cart_quantity`, `reduction_type`, `reduction`, `is_apply_on_nondiscounted_products_only`, `is_shipping_free`, `is_zone_restricted`, `is_country_restricted`, `is_user_restricted`, `is_product_restricted`, `is_category_restricted`, `is_brand_restricted`, `is_supplier_restricted`, `is_attribute_restricted`, `is_send_gift`, `gift_product`, `gift_product_variant`, `apply_discount_on`, `apply_discount_type`, `date_add`, `date_upd`, `description`) VALUES
//                        (1, 6, 'Free Bunny Vibe for more than $20 purchase', '', '2014-12-01 21:25:51', '2015-06-01 21:25:51', '0', '1000', '10', '2', '1', '0', '20', '1', 'flat', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '4682','885', 'none', 'cheapest', '" . $dt . "', '2014-12-02 10:55:51','Free Bunny Vibe for more than $20 purchase'),
//			(2, 4, 'Sale 30% discount on Extreme overstock sale', '30MORE', '2014-12-01 21:45:46', '2015-06-01 21:45:46', '1', '1000', '10', '1', '1', '0', '0', '1', 'percentage', '30', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0','0', 'single_product', 'cheapest', '" . $dt . "', '2014-12-02 11:15:46','Sale 30% discount on Extreme overstock sale');";
//        mysql_query($sample, $this->dbconn);
//        $sample = "INSERT INTO `" . $this->prefix . "cart_rule_category` (`id`, `cart_rule_id`, `category_id`) VALUES
//                        (1, 2, 237);";
//        mysql_query($sample, $this->dbconn);
//        $sample = "INSERT INTO `" . $this->prefix . "cart_rule` (`id`, `cart_rule_type`, `name`, `coupon_code`, `from_date`, `to_date`, `is_auto`, `quantity`, `quantity_per_user`, `priority`, `is_active`, `is_deleted`, `min_cart_amount`, `min_cart_quantity`,
//          `reduction_type`, `reduction`, `is_apply_on_nondiscounted_products_only`, `is_shipping_free`, `is_zone_restricted`, `is_country_restricted`, `is_user_restricted`, `is_product_restricted`, `is_category_restricted`, `is_brand_restricted`, `is_supplier_restricted`,
//           `is_attribute_restricted`, `is_send_gift`, `gift_product`, `gift_product_variant`, `apply_discount_on`, `apply_discount_type`, `date_add`, `date_upd`, `description`) VALUES
//             (1, 6, 'Free Bunny Vibe for more than $20 purchase', '', '2014-12-01 21:25:51', '2015-06-01 21:25:51', '0', '1000', '10', '2', '1', '0', '20', '1', 'flat', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '4682','885', 'none', 'cheapest', '" . $dt . "', '2014-12-02 10:55:51','Free Bunny Vibe for more than $20 purchase'),
//			(2, 4, 'Sale 30% discount on Extreme overstock sale', '30MORE', '2014-12-01 21:45:46', '2015-06-01 21:45:46', '1', '1000', '10', '1', '1', '0', '0', '1', 'percentage', '30', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0','0', 'single_product', 'cheapest', '" . $dt . "', '2014-12-02 11:15:46','Sale 30% discount on Extreme overstock sale');";
//        mysql_query($sample, $this->dbconn);
        // echo "INSERT INTO `" . $this->prefix . "cart_rule_category` (`id`, `cart_rule_id`, `category_id`) VALUES (1, 2, 237);"; die;
//        $sample = "INSERT INTO `" . $this->prefix . "cart_rule_category` (`id`, `cart_rule_id`, `category_id`) VALUES (1, 2, 237);";
//        mysql_query($sample, $this->dbconn);

        $sample = "INSERT INTO `" . $this->prefix . "banner` (`id`, `name`, `code`, `description`, `url`, `image`, `text`, `color`, `hits`, `from_date`, `to_date`, `is_active`, `is_deleted`, `date_add`, `date_upd`, `open_in_new_tab`, `image_alt_text`, `views`) VALUES
                        (1, 'Slide right side', 'slide-right-side', '', '/shop', 'right_banner.jpg', '', '', 5, '2014-10-13 12:46:32', '2016-01-01 06:05:32', 1, '0', '2014-10-13 12:48:13', '2014-11-28 10:36:00', '1', 'Free Bunny vibe', 2985);";
        mysql_query($sample, $this->dbconn);

        if (isset($_SESSION["add_dummy_data"])) {
            $sample = "INSERT INTO `" . $this->prefix . "banner` (`name`, `code`, `description`, `url`, `image`,`mobile_image`, `text`, `color`, `hits`, `from_date`, `to_date`, `is_active`, `is_deleted`, `date_add`, `date_upd`, `open_in_new_tab`, `image_alt_text`, `views`) VALUES
			('Shirts &amp; T-Shirts', 'shirts---t-shirts', '', '', '1411019420_guys_polo_tshirt_inside_banner78981156.jpg', '', '', '', 0, '2015-03-12 05:24:35', '0000-00-00 00:00:00', 1, '0', '2015-03-12 05:26:07', '2015-03-14 06:21:01', '0', 'dummy-banner', 0),
			('Jeans', 'jeans', '', '', 'Jeans17746290.jpg', '', '', '', 0, '2015-03-12 05:35:22', '0000-00-00 00:00:00', 1, '0', '2015-03-12 05:35:47', '2015-03-14 05:42:26', '0', 'jeans', 0),
			('Shoes', 'shoes', '', '', 'banner3_action8889393.jpg', '', '', '', 0, '2015-03-12 05:38:22', '0000-00-00 00:00:00', 1, '0', '2015-03-12 05:38:54', '2015-03-14 05:54:37', '0', 'shoes', 0),
			('Boots &amp; Sandals', 'boots---sandals', '', '', 'banner_boottrend15-700x39586295726.jpg', '', '', '', 0, '2015-03-12 05:39:33', '0000-00-00 00:00:00', 1, '0', '2015-03-12 05:39:51', '2015-03-14 06:00:18', '0', 'boots---sandals', 0),
			('Kurtas &amp; Ethnics', 'kurtas---ethnics', '', '', 'banner45595876.jpg', '', '', '', 0, '2015-03-12 05:40:17', '0000-00-00 00:00:00', 1, '0', '2015-03-12 05:40:31', '2015-03-14 06:02:43', '0', 'kurtas---ethnics', 0),
			('Floaters &amp; Slippers', 'floaters---slippers', '', '', 'banner-198127544.jpg', '', '', '', 0, '2015-03-12 05:43:46', '0000-00-00 00:00:00', 1, '0', '2015-03-12 05:43:59', '2015-03-14 06:08:58', '0', 'floaters---slippers', 0),
			('Sweaters', 'sweaters', '', '', 'SLP_Banners_OctWeek3_MensSweaters36720315.jpg', '', '', '', 0, '2015-03-12 05:44:58', '0000-00-00 00:00:00', 1, '0', '2015-03-12 05:46:08', '2015-03-14 06:10:58', '0', 'sweaters', 0),
			('All', 'all', '', '', 'Casual_Shirts_Banner54989616.jpg', '', '', '', 0, '2015-03-12 05:46:59', '0000-00-00 00:00:00', 1, '0', '2015-03-12 05:47:17', '2015-03-14 06:24:10', '0', 'all', 0);";

            mysql_query($sample, $this->dbconn);
        }

        $sample = "INSERT INTO `" . $this->prefix . "banner_location` (`id`, `banner_id`, `type`, `type_id`, `position`, `date_add`, `from_date`, `to_date`) VALUES
                        (1, 1, 'home', 0, 'Right', '2014-10-13 07:18:19', '2014-10-13 12:46:32', '2016-01-01 06:05:32');";
        mysql_query($sample, $this->dbconn);

        if (isset($_SESSION["add_dummy_data"])) {
            $sample = "INSERT INTO `" . $this->prefix . "banner_location` (`banner_id`, `type`, `type_id`, `position`, `date_add`, `from_date`, `to_date`) VALUES
        	 	(2, 'mega_menus', 2, 'Right', '2015-03-12 12:26:41', '2015-03-12 05:24:35', '0000-00-00 00:00:00'),
				(3, 'mega_menus', 6, 'Right', '2015-03-12 12:37:58', '2015-03-12 05:35:22', '0000-00-00 00:00:00'),
				(4, 'mega_menus', 12, 'Right', '2015-03-12 12:39:10', '2015-03-12 05:38:22', '0000-00-00 00:00:00'),
				(5, 'mega_menus', 16, 'Right', '2015-03-12 12:40:08', '2015-03-12 05:39:33', '0000-00-00 00:00:00'),
				(6, 'mega_menus', 20, 'Right', '2015-03-12 12:40:47', '2015-03-12 05:40:17', '0000-00-00 00:00:00'),
				(7, 'mega_menus', 23, 'Right', '2015-03-12 12:44:15', '2015-03-12 05:43:46', '0000-00-00 00:00:00'),
				(8, 'mega_menus', 26, 'Right', '2015-03-12 12:46:24', '2015-03-12 05:44:58', '0000-00-00 00:00:00'),
				(9, 'mega_menus', 28, 'Right', '2015-03-12 12:48:21', '2015-03-12 05:46:59', '0000-00-00 00:00:00');";
            mysql_query($sample, $this->dbconn);
        }


        $sample = "INSERT INTO `" . $this->prefix . "order_status` (`id`, `name`, `email_template`, `sms_template`, `date_add`, `date_upd`) VALUES
                    (1, 'Pending Payment', '', '', '2014-06-12 20:37:18', '2014-06-12 15:02:03'),
                    (2, 'Payment Failed', '', '', '2014-06-12 20:37:18', '2014-07-29 11:11:18'),
                    (3, 'Confirmed', '16', '', '2014-06-12 20:37:18', '2014-07-29 11:12:07'),
                    (4, 'On hold', '', '', '2014-06-12 20:37:18', '2014-07-29 11:11:07'),
                    (5, 'Canceled', '17', '', '2014-06-12 20:37:18', '2014-07-29 11:11:00'),
                    (6, 'Suspected Fraud', '', '', '2014-06-12 20:37:18', '2014-07-29 11:10:58'),
                    (7, 'Payment Received', '18', '', '2014-06-12 20:37:18', '2014-07-29 11:10:55'),
                    (8, 'Payment Refund', '', '', '2014-06-12 20:37:18', '2014-07-29 11:10:51'),
                    (9, 'Order Shipped', '15', '', '2014-06-12 20:37:18', '2014-07-29 11:10:48'),
                    (10, 'Order Invoiced', '21', '', '2014-06-12 20:37:18', '2014-07-29 11:10:45'),
                    (11, 'Completed', '19', '', '2014-06-12 20:37:18', '2014-07-29 11:10:42'),
                    (12, 'Download Permissions Granted', '20', '', '2014-06-12 20:37:18', '2014-07-29 11:10:39');";
        mysql_query($sample, $this->dbconn);

        if (!empty($default_pages)):
            $sr = 1;
            foreach ($default_pages as $object):
                //print_r($object);
                $page = addslashes($object->page);

                $sample = "INSERT INTO `" . $this->prefix . "content` (`id`, `name`, `urlname`, `page`, `short_description`, `meta_keyword`, `meta_description`, `publish_date`, `page_name`, `position`, `is_deleted`, `image`, `meta_robots_index`, `meta_robots_follow`, `include_xml_sitemap`, `sitemap_priority`, `canonical`) VALUES ";
                $sample .= " ($sr, '$object->name', '" . $object->urlname . "','" . $page . "', '', '', '', '" . date("Y-m-d H:i:s") . "', '" . $object->page_name . "', 0, 0, NULL, 'index', 'follow', 'global', '', '');";

                $sample_url = "INSERT INTO `" . $this->prefix . "url_rewrite` ( `page`, `module`, `module_id`, `urlname`, `date_add`, `date_upd`) VALUES ";
                $sample_url .= " ( 'content', 'content','$sr', '" . $object->urlname . "','" . date("Y-m-d H:i:s") . "','');";

                $sample = str_replace(array('{STORE_LEGAL_NAME}', '&lt;STORE_LEGAL_NAME&gt;'), $store_legal_name, $sample);
                $sample = str_replace(array('{STORE_SUPPORT_EMAIL}', '&lt;STORE_SUPPORT_EMAIL&gt;'), $store_support_email, $sample);
                $sample = str_replace(array('{STORE_NUMBER}', '&lt;STORE_NUMBER&gt;'), $store_phone, $sample);

                mysql_query($sample, $this->dbconn);
                mysql_query($sample_url, $this->dbconn);

                $sr++;
            endforeach;


        endif;

        if (isset($_SESSION["add_dummy_data"])) {
            unset($_SESSION["add_dummy_data"]);
        }
        //add contact us page
        // $sample = "INSERT INTO `" . $this->prefix . "content` (`id`, `name`, `urlname`, `page`, `short_description`, `meta_keyword`, `meta_description`, `publish_date`, `page_name`, `position`, `is_deleted`, `image`, `meta_robots_index`, `meta_robots_follow`, `include_xml_sitemap`, `sitemap_priority`, `canonical`) VALUES ";
        // $sample .= " ('6, 'Contact Us', 'contact-us-content','', '', '', '', '".date("Y-m-d H:i:s")."', 'Contact Us', 0, 0, NULL, 'index', 'follow', 'global', '', '');";
        //  $sample_url = "INSERT INTO `" . $this->prefix . "url_rewrite` (`id`, `page`, `module`, `module_id`, `urlname`, `date_add`, `date_upd`) VALUES ";
        // $sample_url .= " ('', 'content', 'content','6', 'contact-us-content','".date("Y-m-d H:i:s")."','');";
        //  mysql_query($sample, $this->dbconn);
        // mysql_query($sample_url, $this->dbconn);

        /*
          $sample = "INSERT INTO `" . $this->prefix . "url_rewrite` (`id`, `page`, `module`, `module_id`, `urlname`, `date_add`, `date_upd`) VALUES
          ('1', 'content', 'content', 1, 'homepage', '2014-07-10 10:35:20', '2014-07-10 10:35:20'),
          ('2', 'content', 'content', 2, 'about-us', '2014-07-10 10:35:20', '2014-07-30 15:11:43'),
          ('3', 'content', 'content', 3, 'contact', '2014-07-30 20:41:02', '2014-07-30 15:11:46'),
          ('4', 'content', 'content', 4, 'terms-of-service-for-customer', '2014-07-30 20:41:05', '2014-07-30 15:11:05'),
          ('5', 'content', 'content', 5, 'terms-of-service-for-supplier', '2014-07-30 20:41:05', '2014-07-30 15:11:50'),
          ('6', 'content', 'content', 6, 'terms-of-service-for-store-owner', '2014-07-30 20:41:06', '2014-07-30 15:11:53');";
          mysql_query($sample, $this->dbconn);
         * 
         */
    }

}

?>
