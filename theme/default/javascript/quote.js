$(document).ready(function () {
    $('.total_deposit_charges_amount').change();
}).on('click', '#price_info-trigger,#price_info-close', function (e) {
    e.preventDefault();
    $('.price_info').toggle();
}).on('change', '#service_level_id,#lead_source_id', function () {
    var el = $(this);
    var field = el.attr('id');
    var value = el.val();
    if (value) {
        var quote_id = el.attr('data-quote_id');
        $.post(ajax_url, {
            action: 'set_additional_option',
            quote_id: quote_id,
            field: field,
            value: value
        }, function (data) {
            $('#pricings_total_tbody').html(data);
            $('.total_deposit_charges_amount').change();
        });
    }
}).on('click', '.edit_quote_vehicle', function (e) {
    e.preventDefault();
    var el = $(this);
    var vehicle_div = el.closest('.quote_vehicle');
    var top_div = vehicle_div.find('.quote_vehicle_top');
    var bottom_div = vehicle_div.find('.quote_vehicle_bottom');
    top_div.addClass('drag_up');
    bottom_div.addClass('drag_up');
}).on('change', '.quote_vehicle_make', function () {
    var make_select = $(this);
    var make_id = make_select.val();
    var quote_vehicle_container = make_select.closest(".quote_vehicle");
    var model_select = quote_vehicle_container.find('.quote_vehicle_model');
    var data = sync_call(ajax_url, {
        action: 'get_vehicle_models',
        make_id: make_id,
    }, false, true);
    model_select.html(data);
}).on('change', '.quote_vehicle_model', function () {
    var model_select = $(this);
    var quote_vehicle_container = model_select.closest(".quote_vehicle");
    var selected_option = $('option:selected', this);
    var vehicle_type_id = selected_option.attr('data-type_id');
    var vehicle_type = selected_option.attr('data-type_name');
    if (!vehicle_type_id) {
        vehicle_type_id = 0;
    }
    if (!vehicle_type) {
        vehicle_type = '';
    }
    quote_vehicle_container.find('.quote_vehicle_type_id').val(vehicle_type_id);
    quote_vehicle_container.find('.quote_vehicle_type').val(vehicle_type);
}).on('click', '#save_vehicle_data', function () {
    var el = $(this);
    var quote_vehicle_container = el.closest(".quote_vehicle");
    var type_id = quote_vehicle_container.find('.quote_vehicle_type_id').val();
    if (type_id !== undefined && type_id !== '' && type_id != 0) {
        var type_text = allTypes[type_id]['name'];
        var quote_id = quote_vehicle_container.find('.quote_vehicle_quote_id').val();
        var quote_request_vehicle_id = quote_vehicle_container.find('.quote_vehicle_pk').val();
        if (!quote_request_vehicle_id) {
            quote_request_vehicle_id = 0;
        }
        var year = quote_vehicle_container.find('.quote_vehicle_year').val();
        var make_id = quote_vehicle_container.find('.quote_vehicle_make').val();
        var make_text = allMakes[make_id]['name'];
        var model_id = quote_vehicle_container.find('.quote_vehicle_model').val();
        var model_text = allModels[model_id]['model_name'];
        var running_status_id = quote_vehicle_container.find('.quote_vehicle_running_status').val();
        var running_status_id_text = runningStatuses[running_status_id]['name'];
        var top_div = quote_vehicle_container.find('.quote_vehicle_top');
        var bottom_div = quote_vehicle_container.find('.quote_vehicle_bottom');

        var quote_vehicle_top = quote_vehicle_container.find('.quote_vehicle_top');
        var make_text_container = quote_vehicle_top.find('#make_text');
        var model_text_container = quote_vehicle_top.find('#model_text');
        var type_text_container = quote_vehicle_top.find('#type_text');
        var year_text_container = quote_vehicle_top.find('#year_text');
        var running_status_id_text_container = quote_vehicle_top.find('#running_status_id_text');
        $.post(ajax_url, {
            action: 'save_quote_vehicle',
            quote_id: quote_id,
            quote_request_vehicle_id: quote_request_vehicle_id,
            type_id: type_id,
            year: year,
            make_id: make_id,
            model_id: model_id,
            running_status_id: running_status_id
        }, function (data) {
            $('#pricings_total_tbody').html(data);
            $('.total_deposit_charges_amount').change();
            top_div.removeClass('drag_up');
            bottom_div.removeClass('drag_up');
            make_text_container.html(make_text);
            model_text_container.html(model_text);
            type_text_container.html(type_text);
            running_status_id_text_container.html(running_status_id_text);
            year_text_container.html(year);
            quote_vehicle_container.addClass('check_delete');
        });
    } else {
        toastr.error('Please select proper Make & Model', 'Error');
    }
}).on('click', '#quote_vehicle_add', function () {
    var quote_id = $(this).attr('data-quote_id');
    $.get(ajax_url, {
        action: 'new_quote_vehicle',
        quote_id: quote_id
    }, function (data) {
        $('#quote_vehicle_container').append(data);
    });
}).on('click', '.delete_quote_vehile', function () {
    toastr.remove();
    var el = $(this);
    var quote_id = el.attr('data-quote_id');
    var quote_vehicle = $('.check_delete');
    var quote_request_vehicle = el.attr('data-quote_request_vehicle');
    if (quote_vehicle.length > 1) {
        el.closest(".quote_vehicle").hide(400, function () {
            $(this).remove();
        });
        $.post(ajax_url, {
            action: 'delete_quote_vehicle',
            quote_id: quote_id,
            quote_request_vehicle: quote_request_vehicle
        }, function (data) {
            $('#pricings_total_tbody').html(data);
            $('.total_deposit_charges_amount').change();
        });
    } else {
        toastr.error('You are not allowed to delete all vehicles!', 'Error');
    }
}).on('change', '.total_deposit_charges_amount', function () {
    var amount = $(this).html();
    $('.total_deposit_charges').html(amount);
});

function log(e, clear) {
    if (clear === true)
        console.clear();
    console.log(e);
}