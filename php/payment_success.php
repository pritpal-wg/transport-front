<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
if (isset($_POST['txn_id']) && isset($_POST['custom']) && $_POST['txn_id'] && $_POST['custom']) {
    $obj = new query('quote_request');
    $obj->Data['id'] = $_POST['custom'];
    $obj->Data['payment_status'] = $_POST['payment_status'];
    $obj->Data['payment_method'] = 'Paypal';
    $obj->Data['payment_date'] = date('Y-m-d');
    $obj->Data['payment_transaction_id'] = $_POST['txn_id'];
    $obj->Data['quote_option_price'] = $_POST['mc_gross'];
    $obj->Data['quote_status'] = 1;
    $obj->Update();

    $object = new query('quote_request');
    $object->Field = "user_id";
    $object->Where = "WHERE id=" . $_POST['custom'];
    $quote_info = $object->DisplayOne();
    $user_id = $quote_info->user_id;

    $obj = new query('quote_request_user');
    $obj->Field = "email_address";
    $obj->Where = "WHERE id='$user_id'";
    $user_info = $obj->DisplayOne();
    $email = $user_info->email_address;
    $message = "Thanku For Payment Of $" . $_POST['mc_gross'];
    mail($email, 'Quote Deposit Payment', $message);
} else {
    Redirect(make_url('home'));
}
/* * ************************ SEO information ******************************* */
$content = add_metatags("Quote", 'Quote', 'Quote');
/* * ***** End SEO information Section ********************************** */
$smarty->renderLayout();
