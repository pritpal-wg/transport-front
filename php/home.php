<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

if (isset($_POST['save_quote'])) {
    if (isset($_POST['source_zip']))
        $s = $_POST['source_zip'] . '-' . $admin_allowed_country;
    else
        $s = $_POST['source_addr'];
    if (isset($_POST['destination_zip']))
        $d = $_POST['destination_zip'] . '-' . $admin_allowed_country;
    else
        $d = $_POST['destination_addr'];
    $distance_info = getDistance($s, $d);
    $distance = 0;
    $distance_text = 'Not Calculated';
    $distance_calculated = false;
    if (is_object($distance_info)) {
        $distance_calculated = true;
        $distance = $distance_info->value;
        $distance_text = $distance_info->text;
    }
    $source_info = getPlace($s);
    $destination_info = getPlace($d);
    //save quote request
    {
        $obj = new query('quote_request');
        $obj->Data['quote_type'] = $_POST['quote_type'];
        $obj->Data['ip_address'] = $_SERVER["REMOTE_ADDR"];
        if (!empty($source_info)) {
            $obj->Data['source_port_name'] = $source_info['name'];
            $obj->Data['source_port_city'] = $source_info['city'];
            $obj->Data['source_port_state'] = $source_info['state'];
            $obj->Data['source_port_country'] = $source_info['country'];
        }
        if (!empty($destination_info)) {
            $obj->Data['destination_port_name'] = $destination_info['name'];
            $obj->Data['destination_port_city'] = $destination_info['city'];
            $obj->Data['destination_port_state'] = $destination_info['state'];
            $obj->Data['destination_port_country'] = $destination_info['country'];
        }
        $obj->Data['distance_val'] = $distance;
        $obj->Data['distance_text'] = $distance_text;
        $obj->Insert();
        $quote_id = $obj->GetMaxId();
    }
    //save vehicle
    {
        $obj = new query('quote_request_vehicle');
        $obj->Data['quote_id'] = $quote_id;
        $obj->Data['model_id'] = $_POST['model'];
        $obj->Data['make_id'] = $_POST['make'];
        $obj->Data['type_id'] = $_POST['vehicle_type_id'];
        $obj->Data['year'] = $_POST['year'];
        $obj->Data['running_status_id'] = $_POST['running_status_id'];
        $obj->Insert();
    }
    //save source address
    {
        $obj = new query('quote_request_address');
        $obj->Data['quote_id'] = $quote_id;
        $obj->Data['address_type'] = 'source';
        if (!empty($source_info)) {
            $source_location = $source_info['location'];
            $obj->Data['zip'] = isset($_POST['source_zip']) ? $_POST['source_zip'] : $source_info['zip'];
            $obj->Data['city'] = $source_info['city'];
            $obj->Data['state'] = $source_info['state'];
            $obj->Data['country'] = $source_info['country'];
            if(is_object($source_location)){
                $obj->Data['lat'] = $source_location->lat;
                $obj->Data['lng'] = $source_location->lng;
            }
        }
        $obj->Insert();
        $source_address_id = $obj->GetMaxId();
    }
    //save destination address
    {
        $obj = new query('quote_request_address');
        $obj->Data['quote_id'] = $quote_id;
        $obj->Data['address_type'] = 'destination';
        if (!empty($destination_info)) {
            $destination_location = $destination_info['location'];
            $obj->Data['zip'] = isset($_POST['destination_zip']) ? $_POST['destination_zip'] : $destination_info['zip'];
            $obj->Data['city'] = $destination_info['city'];
            $obj->Data['state'] = $destination_info['state'];
            $obj->Data['country'] = $destination_info['country'];
            if(is_object($destination_location)){
                $obj->Data['lat'] = $destination_location->lat;
                $obj->Data['lng'] = $destination_location->lng;
            }
        }
        $obj->Insert();
        $destination_address_id = $obj->GetMaxId();
    }
    //update source and destination address ids
    {
        $obj = new query('quote_request');
        $obj->Data['id'] = $quote_id;
        $obj->Data['source_address_id'] = $source_address_id;
        $obj->Data['destination_address_id'] = $destination_address_id;
        $obj->Update();
    }
    //redirect to home with success
    Redirect(make_url('quote', "id=$quote_id"));
}
$content = add_metatags("Home", 'Home', 'Home');

$smarty->assign_notnull('start_year', date('Y'));
$smarty->assign_notnull('end_year', date('Y', strtotime(date('Y') . " - 50 Year")));

$obj = new vehicle_manufacturer;
$smarty->assign_notnull('makes', $obj->getItems());

$obj = new running_status;
$smarty->assign_notnull('running_statuses', $obj->getItems());

$smarty->renderLayout();
?>