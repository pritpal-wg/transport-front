<?php

/* SSDN */
/*
 *   WEBSITE DEVELOPMENT BY cWebConsultants India
 *   Shopping cart software - cWebCart (c) of cWebConsultants India
 *   for support and sales enquiries call - info@cWebConsultants.com
 *   This is a proprietary software - unauthorized distribution & modification is strictly prohibited
 */
/* * ********* INCLUDE FILES *************** */
include_once("include/config/config.php");
include_once(DIR_FS_SITE . 'include/functionClass/class.php');

/* WEBSITE STATISTICS */
if (!isset($_SESSION['visitor'])):
    $_SESSION['visitor'] = 1;
    $qu = new query('web_stat');
    $qu->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
    $qu->Insert();
endif;

/* * ************ INCLUDE BASIC FUNTIONS - VITAL FUNCTIONS *********** */
$include_fucntions = array('http', 'image_manipulation', 'calender', 'url_rewrite', 'email');
include_functions($include_fucntions);


/* * ********** FOR URL REWRITE *************** */
//load_url();

/* * *********** website statistic recorded. *********************** */
$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "home";

if (in_array($page, $not_to_open_page) || !file_exists(DIR_FS_SITE_PHP . "$page.php"))
    $page = '404';
$smarty = new smarty_controller($page);
$smarty->assign_notnull('PAGE', $page, true);
include_once(DIR_FS_SITE_PHP . "common.php");
require_once(DIR_FS_SITE_PHP . "$page.php");
?>