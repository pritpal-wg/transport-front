<div id="main">
    <h1>{$HOME_CONTENT->name}</h1>
    <div class="description">{$HOME_CONTENT->page|html_entity_decode}</div>
    <div id="associations"></div>
</div>