<?php
function get_cart_breif()
{
	$cart_obj= new cart();$v=0;
	echo 'Items in cart:'.$cart_obj->get_cart_total_items().'<br/>';
	echo 'Subtotal:'.number_format($t=$cart_obj->get_cart_total(), 2).'<br/>';
	echo (CART_VAT)?'VAT:'.number_format($v=$cart_obj->get_cart_vat($t), 2).'<br/>':'';
	echo 'Shipping:'.number_format($s=$cart_obj->get_cart_shipping(), 2).'<br/>';
	echo 'Grand Total:'.number_format($t+$v+$s, 2);
}

function cart_stock($id)
{
	
	$query= new query('product');
	$query->Where="where id='$id' and is_active=1";
	$query->Field="stock";
	$ob=$query->DisplayOne();
	if($ob->stock>0):
		echo '<img src="'.DIR_WS_SITE_GRAPHIC_SUBPAGE_GRAPHIC.'sign_instock.jpg" align="absmiddle" vspace="5"/> <span class="Price">In Stock</span>';
	else:
		echo '<img src="'.DIR_WS_SITE_GRAPHIC_SUBPAGE_GRAPHIC.'sign_outof_stock.jpg" align="absmiddle"vspace="15" /> <span class="Price">Out of Stock</span>';
	endif;

}

function get_billing_var_if_set($var)
{
	global $billing_address;
	return isset($billing_address[$var])?$billing_address[$var]:'';
}

function get_zipcode_name($selected=0)
{
	$query= new query('delivery');
    $query->Where="where id!=''";
    $list=$query->ListOfAllRecords();
	$dropdown=array();
           foreach($list as $k=>$v):
                $values=explode(',', $v['zipcode']);                
                foreach($values as $kk=>$vv):
                       $vv=trim($vv);
						 $dropdown[]=$vv; 
                endforeach;
           endforeach;	
	return $dropdown;
	
   /* if($cart_check):
	 $dropdown='<select name="shipping_code" id="shipping_code" class="pro_list shipping_code" >';
	 $dropdown.='<option value="" class="shipping_code">'.'Select Postal Code'.'</option>';
           foreach($list as $k=>$v):
                $values=explode(',', $v['zipcode']);                
                foreach($values as $kk=>$vv):
                       $vv=trim($vv);
				if($cart_check->delivery_code==$vv):
						 $dropdown.='<option  class="shipping_code" value="'.$vv.'" selected>'.$vv.'</option>'; 
					else:						
						 $dropdown.='<option  class="shipping_code" value="'.$vv.'">'.$vv.'</option>';
				endif;	   
                      
                endforeach;
           endforeach;	
		     $dropdown.='</select>';
			 echo $dropdown.'</p><br class="clear">';
	else:
	 $dropdown='<select name="shipping_code" id="shipping_code" class="pro_list shipping_code" >';
	 $dropdown.='<option value="" class="shipping_code">'.'Select Postal Code'.'</option>';
           foreach($list as $k=>$v):
                $values=explode(',', $v['zipcode']);                
                foreach($values as $kk=>$vv):
                       $vv=trim($vv);	
						 $dropdown.='<option  class="shipping_code" value="'.$vv.'">'.$vv.'</option>';
				endforeach;
           endforeach;	
		     $dropdown.='</select>';
			 echo $dropdown.'</p><br class="clear">';

	endif;
	*/
}

/* copied functions */
function get_product_parent_id($id)
{
	$object= get_object('product', $id);
	return $object->parent_id;
}
function get_total_sub_categories($id, $tablename='category')
{
	#get total sub-categories.
		$QueryObj =new query($tablename);
		$QueryObj->Where="where parent_id='".$id."'";
		return $QueryObj->count();
}
function get_total_products($id)
{
	#get total products
		$QueryObj =new query('category_product');
		$QueryObj->Where="where category_id='".$id."'";
		return $QueryObj->count();
}



function product_chain($id, $pid, $is_product=0)
{
	
	$chain=array();
	while($id!=0):
		$QueryObj2 =new query('category');
		$QueryObj2->Where="where id='".$id."'";
		$cat=$QueryObj2->DisplayOne();
		$chain[]=display_url($cat->name,'product', 'id='.$id.'&category=1', '');
		$id=$cat->parent_id;
	endwhile;
	//print_r($chain);exit;
	$cat_chain='';
	for($i=count($chain)-1; $i>=0;$i--):
		$cat_chain.=$chain[$i].' &nbsp;&rsaquo;&nbsp; ';
	endfor;
	return $cat_chain;
}
function category_chain_front($id, $is_product=0)
{
	$chain=array();	$idd=$id;
	while($id!=0):
		$QueryObj =new query('category');
		$QueryObj->Where="where id='".$id."'";
		$cat=$QueryObj->DisplayOne();		if($id==$idd)			if($is_product)
				$chain[]=display_url($cat->name,'product', 'id='.$cat->id.'&category=1', '');			else				$chain[]=display_url($cat->name,'product', 'id='.$cat->id.'&category=1', 'red');					else			$chain[]=display_url($cat->name,'product', 'id='.$cat->id.'&category=1', 'no');					$id=$cat->parent_id;
	endwhile;
	//print_r($chain);exit;
	$cat_chain='';	$t=count($chain);
	for($i=count($chain)-1; $i>=0;$i--):			if($i==0){			if($is_product)
				$cat_chain.=$chain[$i].'  &nbsp;&rsaquo;&nbsp; ';			else				$cat_chain.=$chain[$i];		}		else{			$cat_chain.=$chain[$i].' &nbsp;&rsaquo;&nbsp; ';		}	endfor;
	return $cat_chain;
}
function get_plural($noun)
{
	switch ($noun)
	{
		case 'category': return 'Categories';
		case 'gallery': return 'Galleries';
		case 'product': return 'Products';
		default:'Categories';
	}
}

function category_chain($id, $tablename='category')
{
	$chain=array();
	while($id!=0):
		$QueryObj =new query($tablename);
		$QueryObj->Where="where id='".$id."'";
		$cat=$QueryObj->DisplayOne();
		$chain[]=make_admin_link(make_admin_url($tablename, 'list', 'list', 'id='.$cat->parent_id), $cat->name, 'click here to reach this '.$tablename);
		$id=$cat->parent_id;
	endwhile;
	$cat_chain='';
	for($i=count($chain)-1; $i>=0;$i--):
		$cat_chain.=$chain[$i].' :: ';
	endfor;
	return $cat_chain.ucfirst(get_plural($tablename));
}

function validate_user($table, $details=array())
{
	$query= new query($table);
	$query->Where="where username='$details[username]' and password='$details[password]' and is_active=1";
	if($user=$query->DisplayOne()):
		return $user;
	else:
		return false;
	endif;	
}
function download_users()
{
	$users= new query('user');
	$users->DisplayAll();
	$users_arr= array();
	if($users->GetNumRows()):
		while($user= $users->GetArrayFromRecord()):
			$user['total orders']=get_total_orders_by_user($user['id']);
			array_push($users_arr, $user);
		endwhile;
	endif;
	$file=make_csv_from_array($users_arr);
	$filename="users".'.csv';
	$fh=@fopen('download/'.$filename,"w");
	fwrite($fh, $file);
	fclose($fh);
	download_file('download/'.$filename);
}
/*
function get_total_orders_by_user($id)
{
	$q= new query('orders');
	$q->Field="count(*) as total";
	$q->Where="where user_id='".$id."'";
	$o=$q->DisplayOne();
	return $o->total;
}
*/
function make_name($n, $name)
		{
			$spaces='';
			for($i=$n;$i>0;$i--):
				$spaces.='&nbsp;&nbsp;';
			endfor;
			return $spaces.$name;
		}	
					
function find_category($id, $cat_arr)
		{
			//echo $id; exit;
			if(is_array($cat_arr)):
				foreach ($cat_arr as $k=>$v):
					if($v['id']==$id):
						return $k;
					endif;
				endforeach;
			endif;
			return false;
		}		
function insert_category($temparr, $k, $cat_arr)
		{
			if(!isset($cat_arr[$k+1])):
				$cat_arr[$k+1]=$temparr;
				return;
			endif;
			$count=count($cat_arr);
			for($i=$count;$i>($k+1);$i--):
				$cat_arr[$i]=$cat_arr[$i-1];
			endfor;
			$cat_arr[$k+1]=$temparr;
			return $cat_arr;
		}		
function get_category_list()
{
	$QueryObj= new query('category');
	$QueryObj->DisplayAll();
	$cat_arr=array();
	while($cat= $QueryObj->GetObjectFromRecord()):
			if($cat->parent_id==0):
				$entry_arr=array('id'=>$cat->id, 'name'=>$cat->name, 'position'=>$cat->position);
				@array_push($cat_arr, $entry_arr);
			elseif($cat->parent_id!=0):
				$k=find_category($cat->parent_id, $cat_arr);
				//echo $k; exit;
				//echo $cat->parent_id; exit;
				//print_r($k); exit;
				if($k)
					$position=$cat_arr[$k]['position']+1;
					
				$position=1;
				$name=make_name($position, $cat->name);
				$entry_arr=array('id'=>$cat->id, 'name'=>$name, 'position'=>$position);
				$cat_arr=insert_category($entry_arr, $k, $cat_arr);
			endif;
	endwhile;
	return $cat_arr;
}

function get_main_category_list($id)
{
	$QueryObj= new query('category');
	$QueryObj->Where="where parent_id='".$id."' and is_active='1' order by position";
	$QueryObj->DisplayAll();
	$cat_arr=array();
	while($cat= $QueryObj->GetObjectFromRecord()):
			if($cat->parent_id==0):
				$entry_arr=array('id'=>$cat->id, 'name'=>$cat->name);
				@array_push($cat_arr, $entry_arr);
			elseif(!$cat->parent_id==0):
				$entry_arr=array('id'=>$cat->id, 'name'=>$cat->name);
				@array_push($cat_arr, $entry_arr);
			endif;
			
	endwhile;
	return $cat_arr;
}

function get_category_name_by_id($id)
{
	if($id==0):
		return 'Root';
	else:
		$query= new query('category');
		$query->Where="where id='$id'";
		$obj=$query->DisplayOne();
		if(is_object($obj)):
			return $obj->name;
		else:
			return '';
		endif;
	endif;
}
/*
function get_country_name_by_id($id)
{
	if($id==0):
		return 'Root';
	else:
		$query= new query('country');
		$query->Where="where id='$id'";
		$obj=$query->DisplayOne();
		if(is_object($obj)):
			return $obj->name;
		else:
			return '';
		endif;
	endif;
}
*/

function get_country_ISO2_by_name($id)
{
	if($id!=''):
		$query= new query('country');
		$query->Where="where name='$id'";
		$obj=$query->DisplayOne();
		if(is_object($obj)):
			return $obj->numcode;
		else:
			return '';
		endif;
	endif;
}
/* Price Detail*/
function get_attribute_price($id)
{
       $val=array();
             $q= new query('attribute_value');
             $q->Where="where id='$id'";
             $object=$q->DisplayOne();
             $values=explode(',', $object->name);
             foreach($values as $kk=>$vv):
             $val[]=trim(strtolower($vv));
             endforeach;
             return $val;
}

function get_parent_cat_id($id)
{
	$query= new query('category');
	$query->Where="where id='$id'";
	$obj=$query->DisplayOne();
	return is_object($obj)?$obj->parent_id:'';
}
function get_product_list()
{
	$query= new query('product');
	$query->DisplayAll();
	$product_list=array();
	$product_name='';	
	while($object= $query->GetObjectFromRecord()):
		array_push($product_list, array('id'=>$object->id, 'name'=>$object->name));
	endwhile;
	return $product_list;
}
/*
function get_country_list()
{
	$query= new query('country');
	$query->Where="where is_active=1";
	$query->DisplayAll();
	$country_list=array();
	$country_name='';	
	while($object= $query->GetObjectFromRecord()):
		$country_name=$object->name;
		$idd=$object->id;
		$country_list[$object->id]=$country_name;
	endwhile;
	return $country_list;
}
*/
function get_zone_list($zone_id)
{	
	$query=new query('zone_country');
	$query->Where="where zone_id=$zone_id";
	$query->DisplayAll();//print_r($query);exit;
	$country_list=array();
	$country_name='';	
	while($object=$query->GetObjectFromRecord()):
		$country_name=get_category_name_by_id($object->country_id);
		$idd=$object->country_id;
		array_push($country_list, array('id'=>$object->id, 'name'=>$country_name));
	endwhile;
	return $country_list;
}

function copy_images($from, $to)
{
	$query= new query('pimage');
	$query->Where="where product_id='$from'";
	$query->DisplayAll();
	while($img= $query->GetObjectFromRecord()):
		$q= new query('pimage');
		$q->Data['image']=$img->image;
		$q->Data['position']=$img->position;
		$q->Data['product_id']=$to;
		$q->Insert();
	endwhile;
}
function copy_related_products($from, $to)
{
	$query= new query('related_product');
	$query->Where="where product_id='$from'";
	$query->DisplayAll();
	while($pro= $query->GetObjectFromRecord()):
		$q= new query('related_product');
		$q->Data['related_id']=$pro->related_id;
		$q->Data['product_id']=$to;
		$q->Insert();
	endwhile;
}
function copy_attributes($from, $to)
{
	$query= new query('attribute');
	$query->Where="where product_id='$from'";
	$query->DisplayAll();
	while($att= $query->GetObjectFromRecord()):
		$q= new query('attribute');
		$q->Data['name']=$att->name;
		$q->Data['is_paid']=$att->is_paid;
		$q->Data['product_id']=$to;
		$q->Insert();
		$new_at_id= $q->GetMaxId();
		$qu= new query('attribute_value');
		$qu->Where="where attribute_id='$att->id'";
		$qu->DisplayAll();
		while($at_val= $qu->GetObjectFromRecord()):
			$que= new query('attribute_value');
			$que->Data['stock']=$at_val->stock;
			$que->Data['price']=$at_val->price;
			$que->Data['name']=$at_val->name;
			$que->Data['attribute_id']=$new_at_id;
			$que->Insert();
		endwhile;
	endwhile;
}
function get_pro_name_by_id($id)
{
	$q= new query('product');
	$q->Where="where id='$id'";
	if($o=$q->DisplayOne()):
		return $o->name;
	else:
		return false;
	endif;
}

function get_latest_products($count=6)
{
$query= new query();
$query->InitilizeSQL();
$query->Field="product.*, pimage.image as image";
$query->TableName="product, product_check_box_value, pimage";
$query->Where="where product.id=product_check_box_value.product_id and product_check_box_value.option_id='5' and pimage.product_id=product.id and pimage.position=1 order by product_check_box_value.position limit 0,$count";
//$query->print=1;
$query->DisplayAll();
$latest_products=array();
if($query->GetNumRows()):
	while ($pro=$query->GetArrayFromRecord()) {
		$latest_products[]=$pro;
	}
endif;
return $latest_products;
}
function get_bestseller_products($count)
{
	$query= new query();
	$query->InitilizeSQL();
	$query->Field="product.*, pimage.image as image";
	$query->TableName="product, product_check_box_value, pimage";
	$query->Where="where product.id=product_check_box_value.product_id and product_check_box_value.option_id='4' and pimage.product_id=product.id and pimage.position=1 order by product_check_box_value.position limit 0, $count";
	$query->DisplayAll();
	$best_sellers=array();
	if($query->GetNumRows()):
	while ($pro=$query->GetArrayFromRecord()) {
		$best_sellers[]=$pro;
	}
	endif;
	return $best_sellers;
}
function get_featured_products($count=0)
{
	$query= new query();
	$query->InitilizeSQL();
	$query->Field="product.*, pimage.image as image";
	$query->TableName="product, product_check_box_value, pimage";
	$query->Where="where product.id=product_check_box_value.product_id and product_check_box_value.option_id='2' and pimage.product_id=product.id and pimage.position=1 order by product_check_box_value.position ASC limit 0, $count";
	$query->DisplayAll();
	$featured=array();
	if($query->GetNumRows()):
		while ($pro=$query->GetArrayFromRecord()) {
			$featured[]=$pro;
		}
	endif;
	return $featured;
}

function get_related_products($id, $count=1)
{
	$related_product=array();
	$query= new query('related_product, product, pimage');
	$query->Field="product.*, pimage.image as image";
	$query->Where="where related_product.product_id='$id' and product.id=related_product.related_id and pimage.product_id=related_product.related_id and pimage.main_image=1 limit 0, $count";
	//$query->print=1;
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object = $query->GetArrayFromRecord()):
			$related_product[]=$object;
		endwhile;
	endif;
	return $related_product;
}



function get_pimages($id)
{
	$prope= new query('pimage');
	$prope->Where="where product_id='$id' and main_image='1'";
	$prope1=$prope->DisplayOne();
	//print_r($prope1);exit;
	return $prope1->image;
	
}

/**
 * To get all images related a one product
 *
 * @param integer $id
 * @param integer $count
 * @param bool $repeat_main
 * @return array
 */
function get_product_images($id, $count, $repeat_main=1)
{
	$query= new query();
	$query->InitilizeSQL();
	$query->TableName="pimage";
	$query->Where="where product_id='$id' and main_image=0 order by position limit 0, $count";
	$query->DisplayAll();
	$product_image=array();
	if($query->GetNumRows()):
		while ($pro=$query->GetArrayFromRecord()) {
			$product_image[]=$pro;
		}
	endif;
	return $product_image;
}

function get_product_images_for_change($id)
{
	$query= new query();
	$query->InitilizeSQL();
	$query->TableName="pimage";
	$query->Where="where id='$id' and main_image=0";
	if($img=$query->DisplayOne()):
		return $img->image;
	else:
		return false;
	endif;
	
	
}





function get_attribute_values($id)
{
	$val=array();
	$q= new query('attribute');
	$q->Where="where id='$id'";
	$object=$q->DisplayOne();
        $values=explode(',', $object->value);
        foreach($values as $kk=>$vv):
             $val[]=trim(strtolower($vv));
        endforeach;
        return $val;
}

function get_attribute_select_box($name, $values)
{
	echo '<select name="attribute['.$name.']" class="select_input">';
	foreach ($values as $k=>$v):
		echo '<option value="'.$v.'">'.ucfirst($v).'</option>';
	endforeach;
	echo '</select>';
}

function display_attributes($attributes)
{
	if(USE_PRODUCT_ATTRIBUTE):
		$sr=1;
		foreach ($attributes as $k=>$v):
		if($sr%2==0):
			echo '<div style="float:left;" >';
			
		else:
			echo '<div style="float:right;" >';
		endif;	
			echo '<div class="select_box_left"></div>';	
			echo '<div class="select_box_center" style="width:198px;">';
			echo get_attribute_select_box($v['id'], get_attribute_values($v['id']));
			echo '</div>';
			echo '</div>';	
		$sr++;		
		endforeach;
		
	endif;
}



function cart_attribute_stock($id)
{	
	$q= new query('attribute_value,attribute');
	$q->Where="where attribute_value.attribute_id=attribute.id and attribute.product_id='$id'";
	$q->Field="attribute_value.stock";
	$q->DisplayAll();
	$values=array();
	while($ob=$q->GetArrayFromRecord()):
		if($ob['stock']):
			return true;
		else:
			return false; 
		endif;
	endwhile;
}

function get_n_words($string, $number=1)
{
	if($string==''):
		return false;
	endif;
	$num = is_numeric($number)?$number:1;
	$temp= explode(' ', $string);
	$final='';
	$num= (count($temp)<$num)?count($temp):$num;
	for($i=0;$i<$num;$i++):
		$final.=$temp[$i].' ';
	endfor;
	return substr($final, 0, strlen($final)-1);
}


function create_user_account_from_order_id($order_id){
	
	if(!$order_id):
		exit;
	endif;
	
	#fetch order object
	$order = get_object('orders', $order_id);
	
	$user_object1= new query('user');
	$user_object1->Where="where username='$order->billing_email'";
	$object=$user_object1->DisplayOne();
	
	if(!is_object($object)):
		
		#create user data
		$user=array();
		$user['username']=$order->billing_email;
		$user['password']=date("ymdhis");
		$user['firstname']=$order->billing_firstname;
		$user['lastname']=$order->billing_lastname;
		$user['address1']=$order->billing_address1;
		$user['address2']=$order->billing_address2;
		$user['city']=$order->billing_city;
		$user['state']=$order->billing_state;
		$user['country']=$order->billing_country;
		$user['zip']=$order->billing_zip;
		$user['fax']=$order->billing_fax;
		$user['phone']=$order->billing_phone;
		$user['is_active']=1;
		$user['is_email_verified']=1;
		$user['on_date']=date("Y/m/d");
		$user['ip_address']=$_SERVER['REMOTE_HOST'];
		
		#add to database/create user
		$user_object= new query('user');
		$user_object->Data=$user;
		$user_object->Insert();
	endif;
	return true;
}


function get_shape_option($selected='')
{
	$query= new query('diamond_attribute');
	$query->Where="where parent_id='1' order by position";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_color_option($selected='')
{
	$query= new query('diamond_attribute');
	$query->Where="where parent_id='2' order by position";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}
function get_clarity_option($selected='')
{
	$query= new query('diamond_attribute');
	$query->Where="where parent_id='3' order by position";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}
function get_certificate_option($selected='')
{
	$query= new query('diamond_attribute');
	$query->Where="where parent_id='71' order by position";
	//$query->print=1;
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_cut_option($selected='')
{
	$query= new query('diamond_attribute');
	$query->Where="where parent_id='4' order by position";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}


function get_polish_option($selected='')
{
	$query= new query('diamond_attribute');
	$query->Where="where parent_id='5' order by position";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}
function get_symmetery_option($selected='')
{
	$query= new query('diamond_attribute');
	$query->Where="where parent_id='6' order by position";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_flourescence_option($selected='')
{
	$query= new query('diamond_attribute');
	$query->Where="where parent_id='7' order by position";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_mount_metal_option($selected='')
{
	$query= new query('mount_metal');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_mount_size_option($selected='')
{
	$query= new query('mount_size');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}


function get_mount_type_option($selected='')
{
	$query= array("Ring","Pendants","Earring");
	foreach($query as $k=>$v):
		if($selected==$v):
			echo '<option value="'.$v.'" selected=selected>'.$v.'</option>';
		else:
			echo '<option value="'.$v.'">'.$v.'</option>';
		endif;
	endforeach;
}


function get_wedding_size_option($selected='')
{
	$query= new query('wedding_size');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}


function get_wedding_metal_option($selected='')
{
	$query= new query('wedding_metal');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_wedding_weight_option($selected='')
{
	$query= new query('wedding_weight');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_wedding_width_option($selected='')
{
	$query= new query('wedding_width');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_wedding_finish_option($selected='')
{
	$query= new query('wedding_finish');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.html_entity_decode($object->name).'</option>';
			else:
				echo '<option value="'.$object->name.'">'.html_entity_decode($object->name).'</option>';
			endif;
		endwhile;
	endif;
}

function get_wedding_profile_option($selected='')
{
	$query= new query('wedding_profile');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.$object->name.'</option>';
			else:
				echo '<option value="'.$object->name.'">'.$object->name.'</option>';
			endif;
		endwhile;
	endif;
}

function get_wedding_engraving_option($selected='')
{
	$query= new query('wedding_engraving');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while($object=$query->GetObjectFromRecord()):
			if($selected==$object->name):
				echo '<option value="'.$object->name.'" selected=selected>'.html_entity_decode($object->name).'</option>';
			else:
				echo '<option value="'.$object->name.'">'.html_entity_decode($object->name).'</option>';
			endif;
		endwhile;
	endif;
}

function get_random_row($table){

$query= new query($table);
$query->Where="where is_active='1' order by rand() limit 1";
if($object=$query->DisplayOne()):
	return $object;		
endif;
return 0;
}

function product_drop_down($data, $name, $selected=array())
{
	echo '<select name="'.$name.'" size="10" style="width:400px;" multiple>';
	foreach ($data as $value):
		if(in_array($value['id'],$selected)):
			echo '<option value="'.$value['id'].'" selected="selected">'.ucfirst($value['name']).'</option>';
		else:
			echo '<option value="'.$value['id'].'">'.ucfirst($value['name']).'</option>';
		endif;
	endforeach;
	echo'</select>';
}

function get_attribute_dd($id, $selected=0, $name='attribute_id', $size='100'){
	$query = new query('attribute');
	$query->Where="where product_id='$id'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		echo '<select name="'.$name.'" style="width:'.$size.'">';
			while($object=$query->GetObjectFromRecord()):
				if($selected && $selected==$object->id):
					echo '<option value="'.$object->id.'" selected>'.ucfirst($object->name).'</option>';
				else:
					echo '<option value="'.$object->id.'">'.ucfirst($object->name).'</option>';
				endif;
			endwhile;
		echo '</select>';
	endif;
}

function get_attname_by_id($id){
	$object =get_object('attribute', $id);
	return $object->name;
}

function get_category_list_control($id)
{
	if(!get_total_sub_categories($id) && !get_total_products($id)):
	?>
        <a href="<?php echo make_admin_url('category', 'list', 'list', 'id='.$id);?>">Category</a>&nbsp;(<?php echo get_total_sub_categories($id);?>)&nbsp;
        <a href="<?php echo make_admin_url('cat_product', 'list', 'list', 'id='.$id);?>">Products</a>&nbsp;(<?php echo get_total_products($id)?>)
	<?php
		elseif(get_total_sub_categories($id) && !get_total_products($id)):?>
		<a href="<?php echo make_admin_url('category', 'list', 'list', 'id='.$id);?>"><?php echo get_control_icon('folder_explore')?>Manage Subcategory</a>&nbsp;(<?php echo get_total_sub_categories($id);?>)<br/>
	<?php
		elseif(!get_total_sub_categories($id) && get_total_products($id)):?>
		<a href="<?php echo make_admin_url('cat_product', 'list', 'list', 'id='.$id);?>"><img src="<?php echo DIR_WS_SITE_CONTROL?>images/file.gif" border="0" align="absmiddle"/>Manage Products</a>&nbsp;(<?php echo get_total_products($id)?>)
	<?php
	endif;

}



function get_category_position_control($catid, $id, $position=1, $page=1)
{
	echo '<a href="'.make_admin_url('category', 'update2', 'list', 'page='.$page.'&id='.$id.'&up='.$position.'&cat_id='.$catid).'"><img src="'.DIR_WS_SITE_CONTROL.'images/up.gif"></a>';
	echo '&nbsp;';
	echo '<a href="'.make_admin_url('category', 'update2', 'list', 'page='.$page.'&id='.$id.'&down='.$position.'&cat_id='.$catid).'"><img src="'.DIR_WS_SITE_CONTROL.'images/down.gif"></a>';
}
function get_total_products_by_user($id)
{
	$q= new query('product');
	$q->Field="count(*) as total";
	$q->Where="where user_id='".$id."'";
	$o=$q->DisplayOne();
	return $o->total;
}

function get_category_status_link($id, $status)
{
	echo '<select name="is_active['.$id.']" size="1">';
	if($status):
		echo '<option value="1" selected>Active</option>';
		echo '<option value="0">Not-Active</option>';
	else:
		echo '<option value="1" selected>Active</option>';
		echo '<option value="0" selected>Not-Active</option>';
	endif;
	echo '</select>';
}

/*
function get_country_drop_down($name, $selected='')
{
	
	$query= new query('country');
	$query->Where="where is_active='1'";
	$query->DisplayAll();

	while ($value=$query->GetArrayFromRecord()):
		if($value['name']==$selected):
			echo '<option value="'.$value['name'].'" selected="selected">'.ucfirst($value['name']).'</option>';
		else:
			echo '<option value="'.$value['name'].'">'.ucfirst($value['name']).'</option>';
		endif;
	endwhile;
	
}




function country_drop_down($data, $name, $selected=array())
{	
	 $data1=array();
	 $data1=array_diff($data, $selected);
	 echo '<select name="'.$name.'" size="10" style="width:200px;" multiple>';
	 foreach ($data1 as $k=>$value):
		echo '<option value="'.$k.'">'.ucfirst($value).'</option>';
	 endforeach;
	 echo'</select>';
	
}
*/
function order_status_drop_down($name='order_status', $selected=0, $id=0)
{
	global $conf_order_status;
	echo '<select name="'.$name.'" size="1" onchange="getvalue(this.form);">';
	foreach ($conf_order_status as $value):
	if(strtolower($value)==strtolower($selected)):
		echo '<option selected="selected" value="'.strtolower($value).'">'.ucfirst($value).'</option>';
	else:
		echo '<option value="'.strtolower($value).'">'.ucfirst($value).'</option>';
	endif;
	endforeach;
	echo '</select>';
}


function order_status_dd($selected=0)
{
	global $conf_order_status;
	foreach ($conf_order_status as $value):
	if(strtolower($value)==strtolower($selected)):
		echo '<option selected="selected" value="'.strtolower($value).'">'.ucfirst($value).'</option>';
	else:
		echo '<option value="'.strtolower($value).'">'.ucfirst($value).'</option>';
	endif;
	endforeach;

}

function update_last_access($id, $status)
{
	$q= new query('admin_user');
	$q->Data['last_access']=date("Y-m-d h:i:s");
	$q->Data['is_loggedin']=$status;
	$q->Data['id']=$id;
	$q->Update();
}

/* New functions  - Category & Products*/
function list_all_categories($parent_id){
    $query= new query('category');
    $query->Where="where parent_id='$parent_id' and is_active='1'  order by position";
    return $query->ListOfAllRecords();
}

function get_all_attribute_dd(){
    $query= new query('all_attribute');
    $query->Where="where is_active='1'";
    $list=$query->ListOfAllRecords();
    if(count($list)):
        foreach($list as $k=>$v):
             echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
        endforeach;
    endif;
}

function display_product_attribute_dd($id)
{
    $query= new query('attribute');
    $query->Where="where product_id='$id'";
    $list=$query->ListOfAllRecords();
    if(count($list)):
           foreach($list as $k=>$v):
                echo '<p><label>'.$v['name'].'</label><br/>';
                $values=explode(',', $v['value']);
                $dropdown='<select name="attr['.$v['id'].']'.'" size="1">';
                foreach($values as $kk=>$vv):
                       $vv=trim($vv);
                       $dropdown.='<option value="'.$vv.'">'.$vv.'</option>';
                endforeach;
                $dropdown.='</select>';
                echo $dropdown.'</p><br class="clear">';
           endforeach;
    endif;
}


/*  shipping page functions */
function isset_cart_shipping_address($cart_id){
    $query=new query('cart_more_detail');
    $query->Where="where cart_id='$cart_id'";
    $object=$query->DisplayOne();
    if(is_object($object)):
            if($object->shipping_firstname!='' && $object->shipping_lastname!=''):
                     return true;
            else:
                    return false;
            endif;
    endif;
    return false;
}



function get_cart_shipping_address($cart_id){
    $query=new query('cart_more_detail');
    $query->Where="where cart_id='$cart_id'";
    $object=$query->DisplayOne();
    $shipping_address=array();
    if(is_object($object)):
            $shipping_address['firstname']=$object->shipping_firstname;
            $shipping_address['lastname']=$object->shipping_lastname;
            $shipping_address['address1']=$object->shipping_address1;
            $shipping_address['address2']=$object->shipping_address2;
            $shipping_address['city']=$object->shipping_city;
            $shipping_address['state']=$object->shipping_state;
            $shipping_address['country']=$object->shipping_country;
            $shipping_address['fax']=$object->shipping_fax;
            $shipping_address['phone']=$object->shipping_phone;
            $shipping_address['email']=$object->shipping_email;
            $shipping_address['zip']=$object->shipping_zip;
    endif;
    return $shipping_address;
}





function get_ajax_user_address($id,$cart_id){
            $q= new query('cart_more_detail');
            $q->Where="where id='".$id."' and cart_id='".$cart_id."'";
            //$q->print=1;
            $q->DisplayAll();
            $shipping_address=array();
            if($q->GetNumRows()):
            while($object_add = $q->GetObjectFromRecord()):
            $shipping_address['id']=$object_add->id;
            $shipping_address['title']=$object_add->title;
            $shipping_address['firstname']=$object_add->shipping_firstname;
            $shipping_address['lastname']=$object_add->shipping_lastname;
            $shipping_address['email']=$object_add->shipping_email;
            $shipping_address['address1']=$object_add->shipping_address1;
            $shipping_address['address2']=$object_add->shipping_address2;
            $shipping_address['zip']=$object_add->shipping_zip;
            $shipping_address['phone']=$object_add->shipping_phone;
            endwhile;
            endif;
            return($shipping_address);
}












function isset_account_shipping_address($user_id){
    $query=new query('address');
    $query->Where="where user_id='$user_id' and address_type='ship'";
    $object=$query->DisplayOne();
    if(is_object($object)):
        return true;
    endif;
    return false;
}

function get_account_shipping_address($user_id){
    $query=new query('address');
    $query->Where="where user_id='$user_id' and address_type='ship'";
    $object=$query->DisplayOne();
    $shipping_address=array();
    if(is_object($object)):
        $shipping_address['firstname']=$object->first_name;
        $shipping_address['lastname']=$object->last_name;
        $shipping_address['address1']=$object->address1;
        $shipping_address['address2']=$object->address2;
        $shipping_address['city']=$object->city;
        $shipping_address['state']=$object->state;
        $shipping_address['country']=$object->country;
        $shipping_address['fax']=$object->fax;
        $shipping_address['phone']=$object->phone;
        $shipping_address['email']=$object->email;
        $shipping_address['zip']=$object->zip;
    endif;
    return $shipping_address;
}


/*  billing page functions */
function isset_cart_billing_address($cart_id){
    $query=new query('cart_more_detail');
    $query->Where="where cart_id='$cart_id'";
    $object=$query->DisplayOne();
    if(is_object($object)):
            if($object->billing_firstname!='' && $object->billing_lastname!=''):
                     return true;
            else:
                    return false;
            endif;
    endif;
    return false;
}



function get_cart_billing_address($cart_id){
    $query=new query('cart_more_detail');
    $query->Where="where cart_id='$cart_id'";
    $object=$query->DisplayOne();
    $billing_address=array();
    if(is_object($object)):
            $billing_address['firstname']=$object->billing_firstname;
            $billing_address['lastname']=$object->billing_lastname;
            $billing_address['address1']=$object->billing_address1;
            $billing_address['address2']=$object->billing_address2;
            $billing_address['city']=$object->billing_city;
            $billing_address['state']=$object->billing_state;
            $billing_address['country']=$object->billing_country;
            $billing_address['fax']=$object->billing_fax;
            $billing_address['phone']=$object->billing_phone;
            $billing_address['email']=$object->billing_email;
            $billing_address['zip']=$object->billing_zip;
    endif;
    return $billing_address;
}












function isset_account_billing_address($user_id){
    $query=new query('address');
    $query->Where="where user_id='$user_id' and address_type='bill'";
    $object=$query->DisplayOne();
    if(is_object($object)):
        return true;
    endif;
    return false;
}

function get_account_billing_address($user_id){
    $query=new query('address');
    $query->Where="where user_id='$user_id' and address_type='bill'";
    $object=$query->DisplayOne();
    $billing_address=array();
    if(is_object($object)):
        $billing_address['firstname']=$object->first_name;
        $billing_address['lastname']=$object->last_name;
        $billing_address['address1']=$object->address1;
        $billing_address['address2']=$object->address2;
        $billing_address['city']=$object->city;
        $billing_address['state']=$object->state;
        $billing_address['country']=$object->country;
        $billing_address['fax']=$object->fax;
        $billing_address['phone']=$object->phone;
        $billing_address['email']=$object->email;
        $billing_address['zip']=$object->zip;
    endif;
    return $billing_address;
}

function get_profile_address($user_id){
    $object=get_object('user', $user_id);
    $address=array();
     if(is_object($object)):
        $address['firstname']=$object->firstname;
        $address['lastname']=$object->lastname;
        $address['address1']=$object->address1;
        $address['address2']=$object->address2;
        $address['city']=$object->city;
        $address['state']=$object->state;
        $address['country']=$object->country;
        $address['fax']=$object->fax;
        $address['phone']=$object->phone;
        $address['email']=$object->username;
        $address['zip']=$object->zip;
    endif;
    return $address;
}

function if_sub_cat_or_product_exist($cat_id)
{
	#check for sub categories.
	$query= new query('category');
	$query->Where="where parent_id='$cat_id'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		return true;
	else:
		return false;
	endif;

	#check for products.
	$query= new query('product');
	$query->Where="where parent_id='$cat_id'";
	$query->DisplayAll();
	return ($query->GetNumRows())?true:false;
}

function get_content_navigation_type($selected=array()){
	$query= new query('content_navigation');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while ($object=$query->GetObjectFromRecord()) {
			if(in_array($object->id, $selected)):
				echo '<option value="'.$object->id.'" selected>'.ucfirst($object->name).'</option>';
			else:
				echo '<option value="'.$object->id.'" >'.ucfirst($object->name).'</option>';
			endif;
		}
	endif;
}

function get_content_collection($selected=array()){
	$query= new query('content_collection');
	$query->Where="where is_active='yes'";
	$query->DisplayAll();
	if($query->GetNumRows()):
		while ($object=$query->GetObjectFromRecord()) {
			if(in_array($object->id, $selected)):
				echo '<option value="'.$object->id.'" selected>'.ucfirst($object->name).'</option>';
			else:
				echo '<option value="'.$object->id.'" >'.ucfirst($object->name).'</option>';
			endif;
		}
	endif;
}

function get_content_position_control($catid, $id, $parent_id, $position=1, $page=1)
{
	echo '<a href="'.make_admin_url('content', 'update2', 'list', 'page='.$page.'&id='.$id.'&parent_id='.$parent_id.'&up='.$position.'&cat_id='.$catid).'"><img src="'.DIR_WS_SITE_CONTROL_IMAGE.'up.gif"></a>';
	echo '&nbsp;';
	echo '<a href="'.make_admin_url('content', 'update2', 'list', 'page='.$page.'&id='.$id.'&parent_id='.$parent_id.'&down='.$position.'&cat_id='.$catid).'"><img src="'.DIR_WS_SITE_CONTROL_IMAGE.'down.gif"></a>';
}

function get_all_sub_cats($tablename, $id)
{
	$sub_cat='';
	$q= new query($tablename);
	$q->Where="where parent_id='".$id."'";
	$q->DisplayAll();
	if($q->GetNumRows()):
		while ($item= $q->GetObjectFromRecord()) {
			$sub_cat.="'".$item->id."'".', ';
		}
		return substr($sub_cat, 0, strlen($sub_cat)-2);
	else:
		return false;
	endif;
}
function get_attribute_for_cart($item_id)

{

	#$object= get_object_by_col('cart_attribute', 'cart_instance_id', $item_id);

	$object= new query('cart_attribute');

	$object->Where="where cart_instance_id='".$item_id."'";

	$object->DisplayAll();

	$string='';

	if($object->GetNumRows()):

		while($v=$object->GetArrayFromRecord()):

			if($v['is_paid_attribute']):

				$string.='<strong>'.$v['attribute_name'].'</strong>:-'.$v['attribute_value_name'].'(&pound;'.number_format($v['price'], 2).')'.'<br/>';

			else:

				$string.='<strong>'.$v['attribute_name'].'</strong>:-'.$v['attribute_value_name'].'<br/>';

			endif;

		endwhile;

	endif;

	return $string;

}


?>