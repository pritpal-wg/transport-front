<?php

/*
 * Ajax FIle- this file manages routing of all ajax scripts
 * You are not adviced to make edit  this File.
 *   
 */
/* Handle actions here. */
$root = rtrim(dirname(__DIR__), '/\\') . '/';
require_once $root . 'include/config/config.php';
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
/* call smarty controller */
$smarty = new smarty_controller('ajax');
$smarty->assign_notnull('PAGE', 'ajax', true);
$action = 'list';
if (isset($_GET['action'])) {
    if ($_GET['action'] && !isset($_GET['smarty'])) {
        $action = trim($_GET['action']);
        if (file_exists(DIR_FS_SITE_PHP . "ajax/_GET/$action.php")) {
            extract($_GET);
            require_once DIR_FS_SITE_PHP . "ajax/_GET/$action.php";
            die;
        }
    }
}
if (isset($_POST['action'])) {
    if ($_POST['action'] && !isset($_POST['smarty'])) {
        $action = trim($_POST['action']);
        if (file_exists(DIR_FS_SITE_PHP . "ajax/_POST/$action.php")) {
            extract($_POST);
            require_once DIR_FS_SITE_PHP . "ajax/_POST/$action.php";
            die;
        }
    }
}
$error = array(
    'status' => 'Error',
    'error' => 'Request Not Supported',
    'description' => 'The Current Request is not supported by the ajax Controller',
);
@header("Content-Type: text/json", TRUE);
echo json_encode($error, JSON_PRETTY_PRINT);
