<?php

class websiteDomain extends cwebc {

    function __construct() {
        parent::__construct('website_domain');
        $this->requiredVars = array('id', 'user_id', 'website_id', 'domain', 'server_name', 'tld', 'is_primary',
            'main_sub_domain', 'date_add', 'date_upt', 'protocol');
    }

    function addDomain($POST) {

        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            $this->Data['date_add'] = date('Y-m-d H:i:s');
            if ($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }

    /*
     * get all domain of user
     */

    function getUserDomains($user_id) {
        $query = new query('website_domain as wd');
        $query->Field = " wd.*,uw.website_name ";
        $query->Where = " LEFT JOIN " . TABLE_PREFIX . "user_website as uw ON wd.website_id=uw.id";
        $query->Where.=" where wd.user_id = '$user_id'";
        return $query->ListOfAllRecords('object');
    }

    /*
     * check if the domain primary or not
     */

    public static function checkDomainPrimary($id) {
        $Query = new websiteDomain();
        $Query->Where = " where id='$id'";
        $object = $Query->DisplayOne();
        if (is_object($object)):
            return $object->is_primary;
        endif;
        return false;
    }

    /*
     * get website primary domain
     */

    function getWebsitePrimaryDomain($website_id, $user_id) {
        $this->Where = " where website_id='$website_id' and is_primary = '1' and user_id='$user_id'";
        return $this->DisplayOne();
    }

    /*
     * set primary status
     */

    public static function setDomainPrimaryStatus($domain_id, $status) {
        $Query = new websiteDomain();
        $Query->Data['is_primary'] = $status;
        $Query->Data['id'] = $domain_id;
        return $Query->Update();
    }

    /*
     * get domain
     */

    function getDomain($id) {
        $this->Where = " where id='$id'";
        return $this->DisplayOne();
    }

    /*
     * edit connection file of user
     */

    public static function editConnectionFileUser($folder_name, $domain_name, $domain_name_ssl) {

        $str = implode("", file(DIR_FS_SITE . 'user/' . $folder_name . '/include/connection.php'));
        $fp = fopen(DIR_FS_SITE . 'user/' . $folder_name . '/include/connection.php', 'w');

        $domain_name = rtrim(trim($domain_name), '/');
        if ($domain_name == ''):
            $domain_name = 'http://www';
        endif;

        $domain_name_ssl = rtrim(trim($domain_name_ssl), '/');
        if ($domain_name_ssl == ''):
            $domain_name_ssl = 'https://www';
        endif;

        $str = preg_replace('/\SERVER", "[^"]+"/', 'SERVER", "' . $domain_name . '"', $str);
        $str = preg_replace('/\SERVER_SSL", "[^"]+"/', 'SERVER_SSL", "' . $domain_name_ssl . '"', $str);

        fwrite($fp, $str, strlen($str));
        fclose($fp);

        return true;
    }

    /*
     * edit htaccess 
     */

    public static function editHtaccessFileForDomain($user_folder, $domain, $tld, $domain_id, $action = 'add') {

        $backup_folder_name = DIR_FS_SITE . 'account/backup/htaccess';

        if (!is_dir($backup_folder_name)) {
            @mkdir($backup_folder_name, 0755, true);
            chmod($backup_folder_name, 0755);  // octal; correct value of mode
        }

        $file_path = DIR_FS_SITE . '.htaccess';

        $backup_file_path = $backup_folder_name . '/htaccess_' . date("Y-m-d-H-i-s") . '.txt';

        $str = implode("", file($file_path));
        $fp = fopen($file_path, 'w');

        $domain = rtrim(trim($domain), '/');

        $replacement = "#domain_" . $domain_id . " starts";
        $replacement .= "\nRewriteCond %{HTTP_HOST} ^(www\.)?" . $domain . '\.' . $tld;
        $replacement .= "\nRewriteRule (.*) /user/" . $user_folder . '/$1 [L]';
        $replacement .= "\n#domain_" . $domain_id . " ends";

        if ($action == 'update'):

            $pattern = '/\#domain_' . $domain_id . ' starts';
            $pattern .= '[^"]+_' . $domain_id . ' ends/';

            $replacement = preg_replace('/(\$|\\\\)(?=\d)/', '\\\\\1', $replacement);

            $str = preg_replace($pattern, $replacement, $str);

        elseif ($action == 'delete'):

            $pattern = '/\#domain_' . $domain_id . ' starts';
            $pattern .= '[^"]+domain_' . $domain_id . ' ends/';

            $str = preg_replace($pattern, '', $str);

        else:

            $str .= "\n\n" . $replacement . "\n";

        endif;

        $str = trim($str);

        fwrite($fp, $str, strlen($str));
        fclose($fp);

        //else:
        //return false;
        //endif;

        return true;
    }

}

?>