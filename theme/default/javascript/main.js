$(document).ready(function () {

    /* ======= Twitter Bootstrap hover dropdown ======= */
    /* Ref: https://github.com/CWSpear/bootstrap-hover-dropdown */
    /* apply dropdownHover to all elements with the data-hover="dropdown" attribute */

    $('[data-hover="dropdown"]').dropdownHover();

    /* ======= jQuery Responsive equal heights plugin ======= */
    /* Ref: https://github.com/liabru/jquery-match-height */

    $('#who .item-inner').matchHeight();
    $('#testimonials .item-inner .quote').matchHeight();
    $('#latest-blog .item-inner').matchHeight();
    $('#services .item-inner').matchHeight();
    $('#team .item-inner').matchHeight();

    /* ======= jQuery Placeholder ======= */
    /* Ref: https://github.com/mathiasbynens/jquery-placeholder */

    $('input, textarea').placeholder();

    /* ======= jQuery FitVids - Responsive Video ======= */
    /* Ref: https://github.com/davatron5000/FitVids.js/blob/master/README.md */
    $(".video-container").fitVids();


    /* ======= Fixed Header animation ======= */

    $(window).on('scroll', function () {

        if ($(window).scrollTop() > 80) {
            $('#header').addClass('header-shrink');
        }
        else {
            $('#header').removeClass('header-shrink');
        }
    });

    if ($('#make').length)
        $('#make').change();

}).on('change', '#make', function () {
    var el = $(this);
    var make_id = el.val();
    var target = $("#model");
    var data = sync_call(ajax_url, {
        action: 'get_vehicle_models',
        make_id: make_id,
    }, false, true);
    target.html(data).change();
}).on('change', '#model', function () {
    var selected_option = $('option:selected', this);
    var vehicle_type_id = selected_option.attr('data-type_id');
    var vehicle_type = selected_option.attr('data-type_name');
    if (!vehicle_type_id)
        vehicle_type_id = 0;
    if (!vehicle_type)
        vehicle_type = '';
    $('#vehicle_type_id').val(vehicle_type_id);
    $('#vehicle_type_name').val(vehicle_type);
}).on('keyup', '#source_zip', function () {
    if ($(this).val() === ' ') {
        $(this).val('');
    } else {
        if ($(this).val()) {
            $('#source_addr').attr('disabled', 'true');
        } else {
            $('#source_addr').removeAttr('disabled');
        }
    }
}).on('keyup', '#source_addr', function () {
    if ($(this).val() === ' ') {
        $(this).val('');
    } else {
        if ($(this).val()) {
            $('#source_zip').attr('disabled', 'true');
        } else {
            $('#source_zip').removeAttr('disabled');
        }
    }
}).on('keyup', '#destination_zip', function () {
    if ($(this).val() === ' ') {
        $(this).val('');
    } else {
        if ($(this).val()) {
            $('#destination_addr').attr('disabled', 'true');
        } else {
            $('#destination_addr').removeAttr('disabled');
        }
    }
}).on('keyup', '#destination_addr', function () {
    if ($(this).val() === ' ') {
        $(this).val('');
    } else {
        if ($(this).val()) {
            $('#destination_zip').attr('disabled', 'true');
        } else {
            $('#destination_zip').removeAttr('disabled');
        }
    }
}).on('blur', '#source_zip,#destination_zip', function () {
    var e = $(this);
    var zip = e.val();
    if (zip) {
        $.post("https://maps.googleapis.com/maps/api/geocode/json?address=" + zip + "&key=" + gapi_key, function (data) {
            var got_country = false;
            var country_code = '';
            $.each(data.results, function (i, result) {
                if (!got_country) {
                    country_code = getCountryCode(result);
                    if (country_code === admin_allowed_country) {
                        got_country = true;
                    }
                }
            });
            if (country_code !== admin_allowed_country) {
                alert('Please Enter Correct Zip Code!');
                e.val('').focus().keyup();
            }
        });
    }
}).on('focus', '#source_addr,#destination_addr', function () {
    var el = $(this);
    el.geocomplete({
        componentRestrictions: {
            country: admin_allowed_country,
        },
    });
});

function getCountryCode(result) {
    var got_cc = false;
    var cc = '';
    $.each(result.address_components, function (l1, v1) {
        if (!got_cc) {
            $.each(v1.types, function (l2, v2) {
                if (v2 === 'country') {
                    cc = v1.short_name;
                    got_cc = true;
                }
            });
        }
    });
    return cc;
}

function sync_call(url, data, post_call, original_response) {
    post_call = typeof post_call === 'boolean' ? post_call : false;
    original_response = typeof original_response !== 'undefined' ? original_response : false;
    $.ajaxSetup({
        async: false
    });
    if (post_call === true) {
        if (original_response === true)
            return $.post(url, data).responseText;
        return $.parseJSON($.post(url, data).responseText);
    }
    if (original_response === true)
        return $.get(url, data).responseText;
    return $.parseJSON($.get(url, data).responseText);
}