<!DOCTYPE html>
<html lang="en"> 
    <head>
        {head}
        {add_js name='plugins/jquery-1.11.3.min'}
        <script type="text/javascript">
            $(document).ready(function () {
                $("#submit_payment").trigger("click");
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="innerTB">
                <div class="jumbotron" style="text-align: center">
                    <h1 class="separator bottom">Pay Now</h1>
                    <p class="lead">Please wait while you are being re-directed to our payment gateway. Please note this connection is now secure.</p>
                </div>
                <div class="widget widget-heading-simple widget-success">
                    <form id="payment_form" name='vote' action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="POST">            
                        <input type="hidden" name="business" value="rina-facilitator@cwebconsultants.com"/>
                        <input type="hidden" name="cmd" value="_xclick"/>  
                        <input type="hidden" name="item_name" value="Deposit Amount"/>  
                        <input type="hidden" name="quantity" value="1"/> 
                        <input type="hidden" name="item_number" value="Deposit Amount"/>  
                        <input type="hidden" name="amount" value="{$total_amount}"/>  
                        <input type="hidden" name="currency_code" value="USD"/>  
                        <input type="hidden" name="return" value="{make_url('payment_success')}"/>
                        <input type="hidden" name="notify_url" value="{make_url('ipn')}"/> 
                        <input type="hidden" name="cancel_return" value="{make_url('cancel')}"/>  
                        <input type="hidden" name="custom" value="{$quote_id}"/>  
                        <input type="hidden" name="rm" value="2"/>  
                        <input type="submit" style='display:none;' name="submit" value="Submit" id='submit_payment'/>  	
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>