<?php
/*
 * Shipping Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class shipping_flat extends shipping {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($order='desc', $orderby='id'){
        parent::__construct('supplier_shipping_flat');
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id', 'supplier_id', 'shipping_id', 'zone_id', 'is_free', 'flat_type','price', 'tax_rule_id','is_active','is_deleted', 'delivery_time','name','user_group_id','min_delivery_days','max_delivery_days');
    }
    /*
     * list flat shippings
     */
    function listFlatShipping($active=false){
        global $supplier_id;
        $query = new query('supplier_shipping_flat as sf');
        $query -> Field=" zn.name as zone,sf.* ";
        $query->Where = " left join ".TABLE_PREFIX."supplier_zone as zn on sf.zone_id=zn.id";
        $query->Where .= " where sf.supplier_id = '$supplier_id' ";
        if($active):
            $query->Where.= " and sf.is_active='1'";
        endif;
        $query->Where .= createUserGroupCondition('logged_in_user','sf');
        $query->Where.= " and sf.is_deleted='0' order by sf.id desc";
        return $query->ListOfAllRecords();

    }
    /*
     * insert or update shipping rule
     */
    function saveShipping($POST){
        global $supplier_id;
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['supplier_id']=$supplier_id;
        $this->Data['shipping_id']=SHIPPING_FLAT_ID;
        $this->Data['is_active']=isset($POST['is_active'])?"1":"0";
        $this->Data['is_free']=isset($POST['is_free'])?"1":"0";
        
        if(isset($POST['user_group_ids']) && count($POST['user_group_ids'])):
            $this->Data['user_group_id'] = implode(',', $POST['user_group_ids']);
        else:
            $this->Data['user_group_id'] = '0';
        endif;
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $id = $this->Data['id'];
            if($this->Update())
              return $id;
        }
        else{
          
            if($this->Insert())
                return $this->GetMaxId();
        }
        return false;
    }
    
    /*
     * get object pf shiping flat entry
     */
    function getShippingFlat($id){
        global $supplier_id;
        $this->Where=" where id='$id' and supplier_id='$supplier_id'";
        return $this->DisplayOne();
    }
    
    function calculateShippingByZone($zone_id){
        global $supplier_id;
        $Query = new shipping_flat();
        $Query->Where=" where supplier_id='$supplier_id' and zone_id='$zone_id' and is_active='1' and is_deleted='0'";
        $Query->Where.= createUserGroupCondition('logged_in_user');
        $object = $Query->DisplayOne();
        $return = array();
        if($object):
            
            $expected_delivery_date_min  = false;
            if(is_numeric($object->min_delivery_days) && $object->min_delivery_days > 0): 
                $expected_delivery_date_min = date::add_weekdays_to_date($object->min_delivery_days, date("Y-m-d"),true);
            endif;

            $expected_delivery_date_max  = false;
            if(is_numeric($object->max_delivery_days) && $object->max_delivery_days > 0):
                $expected_delivery_date_max = date::add_weekdays_to_date($object->max_delivery_days, date("Y-m-d"),true);
            endif;
                
            if($object->is_free):
                $total_shipping_amount='0';
                $is_shipping_free='1';
            else:
                if($object->flat_type=='product'):
                    $QueryObj1 = new cart_detail();
                    $quantity = $QueryObj1 -> getCartTotalItems();
                    $total_shipping_amount = number_format($quantity*$object->price,'2');
                else:
                    $total_shipping_amount = number_format($object->price,'2');
                endif;
                $is_shipping_free='0';
            endif;    
            
            $rule_array = array(
                    "shipping_method_name" => $object->name,
                    "shipping_rule_id" => $object->id,
                    "is_shipping_free" => $is_shipping_free,
                    "shipping_method_id" => $object->shipping_id,
                    "total_shipping_amount" => $total_shipping_amount,
                    "shipping_group_id" => '',
                    "estimate_delivery_date_min" => $expected_delivery_date_min,
                    "estimate_delivery_date_max" => $expected_delivery_date_max
            );
            
            $return[] = $rule_array;
        endif;
        return $return;
    }
    
    public static function getApproxTimeForRule($id){
        global $supplier_id;
        $query = new query('supplier_shipping_flat');
        $query->Field="delivery_time";
        $query->Where=" where id='$id' and supplier_id='$supplier_id'";
        $object = $query->DisplayOne();
        if(is_object($object)):
            return $object->delivery_time;
        else:
            return false;
        endif;
    }
}