<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
$quote_id = isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0 ? $_GET['id'] : 0;
if (!$quote_id)
    Redirect(make_url('home'));
//get quote
{
    $obj = new query('quote_request as qr');
    $obj->Where = "WHERE qr.id=$quote_id";
    $quote_info = $obj->DisplayOne();
    if (!$quote_info || $quote_info->quote_status)
        Redirect(make_url('home'));
}

//check if data submitted
if (isset($_POST['save'])) {
    //Save User
    {
        $user_data = $_POST['user'];
        $obj = new query('quote_request_user');
        $obj->Data = $user_data;
        $obj->Insert();
        $user_id = $obj->GetMaxId();

        //update quote request by user id
        {
            $obj = new query('quote_request');
            $obj->Data['id'] = $quote_id;
            $obj->Data['user_id'] = $user_id;
            $obj->Update();
        }
    }

    //update addresses
    {
        //update source
        {
            $data = $_POST['address']['source'];
            $obj = new query('quote_request_address');
            $obj->Data = $data;
            $obj->Update();
        }

        //update source
        {
            $data = $_POST['address']['destination'];
            $obj = new query('quote_request_address');
            $obj->Data = $data;
            $obj->Update();
        }
    }

    //redirect to home
    {
        Redirect(make_url('payment', 'quote_id=' . $quote_id));
    }
}

//get quote addresses
{
    $obj = new query('quote_request_address');
    $obj->Where = "WHERE quote_id=$quote_id";
    $quote_addresses = $obj->ListOfAllRecords();
    //formatting of quote addresses
    {
        if (!empty($quote_addresses)) {
            $temp = array();
            foreach ($quote_addresses as $quote_address)
                $temp[$quote_address['address_type']] = $quote_address;
            $quote_addresses = $temp;
        }
    }
}

//get quote vehicles
{
    $obj = new query('quote_request_vehicle');
    $obj->Where = "WHERE quote_id=$quote_id";
    $quote_vehicles = $obj->ListOfAllRecords();
    //formatting of quote vehicles
    {
        if (!empty($quote_vehicles)) {
            $temp = array();
            foreach ($quote_vehicles as $quote_vehicle)
                $temp[$quote_vehicle['id']] = $quote_vehicle;
            $quote_vehicles = $temp;
        }
    }

    //get all vehicle models
    {
        $obj = new vehicle_model;
        $allModels = $obj->getItems();
        if (!empty($allModels)) {
            $temp = array();
            foreach ($allModels as $allModel)
                $temp[$allModel['id']] = $allModel;
            $allModels = $temp;
        }
    }

    //get all vehicle makes
    {
        $obj = new vehicle_manufacturer;
        $allMakes = $obj->getItems();
        if (!empty($allMakes)) {
            $temp = array();
            foreach ($allMakes as $allMake)
                $temp[$allMake['id']] = $allMake;
            $allMakes = $temp;
        }
    }

    //get all vehicle types
    {
        $obj = new vehicle_type;
        $allTypes = $obj->getItems();
        if (!empty($allTypes)) {
            $temp = array();
            foreach ($allTypes as $allType)
                $temp[$allType['id']] = $allType;
            $allTypes = $temp;
        }
    }

    //get all $runningStatuses
    {
        $obj = new running_status;
        $runningStatuses = $obj->getItems();
        if (!empty($runningStatuses)) {
            $temp = array();
            foreach ($runningStatuses as $runningStatus)
                $temp[$runningStatus['id']] = $runningStatus;
            $runningStatuses = $temp;
        }
    }
}

//get service levels
{
    $obj = new service_level;
    $service_levels = $obj->getItems();
}

//get lead sources
{
    $obj = new lead_source;
    $lead_sources = $obj->getItems();
}

//get quote distance in miles
{
    $quote_info->distance_miles = round(($quote_info->distance_val / 1609), 2);
}

//get quote pricing
$quote_pricings = getPricings($quote_id);

//assign values to smarty
{
    $smarty->assign_notnull("quote_info", $quote_info, true);
    $smarty->assign_notnull("quote_addresses", $quote_addresses, true);
    $smarty->assign_notnull("quote_pricings", $quote_pricings, true);
    $smarty->assign_notnull("quote_vehicles", $quote_vehicles, true);
    $smarty->assign_notnull("service_levels", $service_levels, true);
    $smarty->assign_notnull("lead_sources", $lead_sources, true);
    $smarty->assign_notnull("allModels", $allModels, true);
    $smarty->assign_notnull("allMakes", $allMakes, true);
    $smarty->assign_notnull("allTypes", $allTypes, true);
    $smarty->assign_notnull("runningStatuses", $runningStatuses, true);

    //make start and end years
    {
        $smarty->assign_notnull('start_year', date('Y'));
        $smarty->assign_notnull('end_year', date('Y', strtotime(date('Y') . " - 50 Year")));
    }
}
/* * ************************ SEO information ******************************* */
$content = add_metatags("Quote", 'Quote', 'Quote');
/* * ***** End SEO information Section ********************************** */
$smarty->renderLayout();
?>