<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.is_var_set_post.php
 * Type:        function
 * Name:        is_var_set_post
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - name         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * name = css name
 */

function smarty_function_is_var_set_post($params, &$smarty)
{  
       $value=isset($params['value'])?$params['value']:"";
       if(isset($params['value'])):
           if(isset ($_POST[$value]) && $_POST[$value]!=''):
               echo $_POST[$value];
           else:
               echo '';
           endif;  
       endif;
            
}
?>
