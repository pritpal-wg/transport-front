<?php
//get all vehicle makes
{
    $obj = new vehicle_manufacturer;
    $allMakes = $obj->getItems();
    if (!empty($allMakes)) {
        $temp = array();
        foreach ($allMakes as $allMake)
            $temp[$allMake['id']] = $allMake;
        $allMakes = $temp;
    }
}
//get all $runningStatuses
{
    $obj = new running_status;
    $runningStatuses = $obj->getItems();
    if (!empty($runningStatuses)) {
        $temp = array();
        foreach ($runningStatuses as $runningStatus)
            $temp[$runningStatus['id']] = $runningStatus;
        $runningStatuses = $temp;
    }
}
//get all vehicle models
{
    $obj = new vehicle_model;
    $allModels = $obj->getItems();
    if (!empty($allModels)) {
        $temp = array();
        foreach ($allModels as $allModel)
            $temp[$allModel['id']] = $allModel;
        $allModels = $temp;
    }
}
$start_year = date('Y');
$end_year = date('Y', strtotime(date('Y') . " - 50 Year"));
?>
<div class="col-md-3" data-for="quote_vehicle">
    <div class="quote_vehicle">
        <input type="hidden" class="quote_vehicle_type_id" value=""/>
        <input type="hidden" class="quote_vehicle_quote_id" value="<?php echo $quote_id; ?>"/>
        <div class="pull-right">
            <button type="button" class="btn btn-xs btn-danger delete_quote_vehile"><i class="fa fa-times"></i></button>
        </div>
        <div class="quote_vehicle_top drag_up transition_400">
            <h4 id="make_text"></h4>
            <h4 id="model_text"></h4>
            <h4 id="type_text"></h4>
            <h4 id="year_text"></h4>
            <h4 id="running_status_id_text"></h4>
            <button class="btn btn-info btn-block btn-sm edit_quote_vehicle"><i class="fa fa-edit"></i>Edit</button>
        </div>
        <div class="quote_vehicle_bottom drag_up transition_400">
            <div class="form-group">
                <select class="form-control quote_vehicle_make">
                    <option value="" hidden>--Select Make--</option>
                    <?php foreach ($allMakes as $allMake_id => $allMake) { ?>
                        <option value="<?php echo $allMake_id; ?>"><?php echo $allMake['name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control quote_vehicle_model">
                    <option value="" hidden>--Select Model--</option>
                    <?php foreach ($allModels as $allModel_id => $allModel) { ?>
                        <option value="<?php echo $allModel_id; ?>"><?php echo $allModel['model_name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control quote_vehicle_type" value="" readonly />
            </div>
            <div class="form-group">
                <select class="form-control quote_vehicle_year" autocomplete="off">
                    <?php for ($i = $start_year; $i >= $end_year; $i--) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control quote_vehicle_running_status" autocomplete="off">
                    <?php foreach ($runningStatuses as $runningStatus) { ?>
                        <option value="<?php echo $runningStatus['id'] ?>"><?php echo $runningStatus['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-sm btn-block btn-success" id="save_vehicle_data">Done</button>
            </div>
        </div>
    </div>
</div>