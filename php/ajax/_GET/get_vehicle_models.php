<?php

$obj = new query('`vehicle_model` as vm');
$obj->Field = "vm.id,vm.model_name,vm.type_id, vt.name as type_name";
$obj->Where = "LEFT JOIN `vehicle_type` AS vt on vt.id=vm.type_id";
$obj->Where .= " WHERE vm.manufacturer_id=$make_id AND vm.status=1 GROUP BY vm.id";
$data = $obj->ListOfAllRecords();
echo "<option value=\"\">Model</option>";
if (empty($data))
    die;
foreach ($data as $e) {
    echo "<option value=\"$e[id]\" data-type_id=\"$e[type_id]\" data-type_name=\"$e[type_name]\">$e[model_name]</option>";
}