<?php

include_once("include/config/config.php");
include_once(DIR_FS_SITE . 'include/functionClass/productClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/optionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/categoryClass.php');


//assign products from sub cateogry to main 'see all' category
$cat_ids = array(
            262 => '33,96,94,99,98,238,95,208,1,95,97,106,130',
            264 => '35,154,159,18,145,22,113,230,243,133,11,244,105,245,106,38,207,2',
            259 => '154,130,132,131,9,26,153,106,244,25,101,13,16,233',
            260 => '233,16,29,155,35,229,133,100,1,11,105,210,144,145,113,98,238,95,208,33,96,94,99,228',
            261 => '16,7,238,233,188,35,4,36',
            263 => '120,121,122,123,118,124,119,151',
            265 => '159,16,7,114,31,28,73,161,19,4,5,24,138,137,21'
);

foreach($cat_ids as $kk=>$vv){
        $query = new category_product();
        $query -> Field = " DISTINCT(product_id) ";
        $query -> Where = "  WHERE category_id IN ($vv) ";
        $ProductIds = $query ->ListOfAllRecords('object');

        foreach($ProductIds as $kkk=>$vvv){
            $QueryInsert = new category_product();
            $QueryInsert -> Data['category_id'] = $kk;
            $QueryInsert -> Data['product_id'] = $vvv->product_id;
            $QueryInsert -> Data['is_feed'] = '0';
            $QueryInsert -> Insert();
        }        
}



/*
// hide categories if brand name and category name same
$categoriesWithSameNameBrand = category::getCategoriesSameAsBrandName();
$store_cat_ids = '';
if(isset($categoriesWithSameNameBrand) && count($categoriesWithSameNameBrand)):
    foreach($categoriesWithSameNameBrand as $kk=>$vv):
        $store_cat_ids .= $vv->store_cat_id.',';
    endforeach;
    $store_cat_ids = rtrim($store_cat_ids, ',');
    if($store_cat_ids!=''):
        $QueryObj = new category();
        $QueryObj -> Where = " where id IN ($store_cat_ids)";
        $QueryObj -> Data['is_active'] = '0';
        $QueryObj ->UpdateCustom();
    endif;
endif;



/*
  $queyObj = new query('category1');
  $all_cats = $queyObj ->ListOfAllRecords();

  $import_array = array();

  foreach($all_cats as $kk=>$vv):
  $single = array();
  $single['id'] = $vv['id'];
  $single['name'] = html_entity_decode($vv['name']);
  $single['f_cat_name'] = html_entity_decode($vv['name']);
  $single['from_feed'] = '1';
  $single['feed_name'] = 'xrproducts4';
  $single['parent_id'] = $vv['parent_id'];
  $single['image'] = $vv['image'];
  $single['description'] = html_entity_decode($vv['description']);
  $single['is_active'] = $vv['is_active'];
  $import_array[] = $single;
  endforeach;


  // save the results into a file
  $handle = @fopen('category', "w");
  fwrite($handle, serialize($import_array));
  fseek($handle, 0);

  exit;
 */


// check if feed has created or updated some products than create groups
/*
$group_products = array();

$group_products = product::getGroupsToMadeProducts();

echo "<pre>";
if ($group_products && count($group_products) > 0) {

    foreach ($group_products as $master_group) {
        

        // get products with master group id 

        $products_to_group = array();
        $products_to_group = product:: getproductsWithMasterProductId($master_group->master_product_id);
        
        //echo "<pre>";
        //print_r($products_to_group);exit;

        // group products here 
        if ($products_to_group && count($products_to_group)):

            foreach ($products_to_group as $gproduct):
                
                if (isset($gproduct->options) && count($gproduct->options)):


                    foreach ($gproduct->options as $gkey => $gval):

                    
                        foreach ($products_to_group as $ggprokey => $ggproduct):

                            if ($ggprokey != $gproduct->id):
                                
                                if (isset($ggproduct->options[$gkey]) && $ggproduct->options[$gkey] != '' && $ggproduct->options[$gkey] != $gval):

                                    // check if group already created usign both the products
                                    if (!product_group_rel::checkProductsGroupRelation($ggprokey, $gproduct->id)):

                                        // steps add group with option set
                                        // check if group exists
                                        $group_check['group_set_id'] = $gkey;
                                        $group_check['product_id'] = $gproduct->id;

                                        $groupRelObj = new product_group_rel();
                                        $group_id = $groupRelObj->checkProductGroup($group_check);

                                        $group_check1['group_set_id'] = $gkey;
                                        $group_check1['group_product_id'] = $ggproduct->id;

                                        $groupRelObj1 = new product_group_rel();
                                        $group_id2 = $groupRelObj1->checkProductGroup2($group_check1);

                                        $grouprel_id = 0;
                                        if ($group_id):
                                            $group_rel = array();
                                            $group_rel['group_id'] = $group_id;
                                            $group_rel['group_set_id'] = $gkey;
                                            $group_rel['product_id'] = $ggproduct->id;
                                            $group_rel['use_product_image'] = '1';
                                            $addGroupObj = new product_group_rel();
                                            $grouprel_id = $addGroupObj->saveGroupRelation($group_rel);

                                        elseif ($group_id2):
                                            $group_rel = array();
                                            $group_rel['group_id'] = $group_id2;
                                            $group_rel['group_set_id'] = $gkey;
                                            $group_rel['product_id'] = $gproduct->id;
                                            $group_rel['use_product_image'] = '1';
                                            $addGroupObj = new product_group_rel();
                                            $grouprel_id = $addGroupObj->saveGroupRelation($_POST);

                                        else:
                                            $group_new = array();
                                            $group_new['name'] = $gproduct->name;

                                            $addGroupObj = new product_group();
                                            $group_id = $addGroupObj->saveGroup($group_new);

                                            if ($group_id):

                                                $group_rel = array();
                                                $group_rel['group_id'] = $group_id;
                                                $group_rel['group_set_id'] = $gkey;
                                                $group_rel['product_id'] = $ggproduct->id;
                                                $group_rel['use_product_image'] = '1';
                                                $addGroupObj = new product_group_rel();
                                                $grouprel_id = $addGroupObj->saveGroupRelation($group_rel);

                                                $group_rel = array();
                                                $group_rel['group_id'] = $group_id;
                                                $group_rel['group_set_id'] = $gkey;
                                                $group_rel['product_id'] = $gproduct->id;
                                                $group_rel['use_product_image'] = '1';
                                                $addGroupObj = new product_group_rel();
                                                $grouprel_id = $addGroupObj->saveGroupRelation($group_rel);
                                            endif;

                                        endif;
                                    endif;
                                endif;

                            endif;


                        endforeach;


                    endforeach;



                endif;

            endforeach;

        endif;
        //echo "yes";
        //exit;
    } //loop on all groups
} //if group products found
 * 
 */
?>