<?php

$quote_id = $_GET['quote_id'];
if (!$quote_id) {
    Redirect(make_url('home'));
}
$quote_pricings = getPricings($quote_id);
$total_amount = $quote_pricings['deposit']['total'];
$smarty->assign_notnull("total_amount", $total_amount, true);
$smarty->assign_notnull("quote_id", $quote_id, true);
/* * ************************ SEO information ******************************* */
$content = add_metatags("Quote", 'Quote', 'Quote');
/* * ***** End SEO information Section ********************************** */
$smarty->renderLayout();
?>