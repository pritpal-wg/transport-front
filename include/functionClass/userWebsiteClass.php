<?php

/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's Management System
 */

class userWebsite extends cwebc {

    function __construct() {
        parent::__construct('user_website');
        $this->requiredVars = array('id', 'user_id', 'website_name', 'store_name', 'description', 'website_folder_name', 'is_domain_pointed', 'domain_name', 'ftp_host', 'ftp_user_name', 'ftp_password', 'database_hostname', 'database_name', 'database_user', 'database_password', 'on_date', 'update_date', 'is_active', 'is_deleted', 'is_feed', 'supplier_rate_multiplier', 'store_support_email', 'store_phone', 'store_business_name', 'store_domain_name', 'store_legal_name', 'store_address1', 'store_address2', 'store_city', 'store_state', 'store_country', 'store_zip');
    }

    public static function resetWebsite($id) {
        global $is_local;
        $Websitedetail = new userWebsite();
        $webDetail = $Websitedetail->getWebsite($id);
        $USER_WEBISTE_PATH = DIR_FS_SITE . "user/" . $webDetail->website_folder_name . '/';
        $DBDataBase = $webDetail->database_name;
        $DBUserName = $webDetail->database_user;
        $DBPassword = $is_local ? trim($webDetail->database_password) : ($_SERVER['SERVER_ADDR'] == '128.199.143.5' ? 'GtZhBjPDRYA5aDSVm' : "Z469i13C80M8ELRTSJxW");
        $DBHostName = $webDetail->database_hostname;
        $temp_link = mysqli_connect($DBHostName, $DBUserName, $DBPassword, $DBDataBase);
        // print_r($temp_link);
        // $delete_dumy_products = new product();
        $query = "DELETE FROM `" . TABLE_PREFIX . "product`";
        // echo $query; die;
        mysqli_query($temp_link, $query);

        $query = "DELETE FROM `" . TABLE_PREFIX . "product_image`";
        // echo $query; die;
        mysqli_query($temp_link, $query);

        $query = "DELETE FROM `" . TABLE_PREFIX . "url_rewrite`";
        // echo $query; die;
        mysqli_query($temp_link, $query);

        $query = "DELETE FROM `" . TABLE_PREFIX . "category`";
        // echo $query; die;
        mysqli_query($temp_link, $query);

        $query = "DELETE FROM `" . TABLE_PREFIX . "category_structure`";
        // echo $query; die;
        mysqli_query($temp_link, $query);

        $query = "DELETE FROM `" . TABLE_PREFIX . "category_product`";
        // echo $query; die;
        mysqli_query($temp_link, $query);

        $query = "DELETE FROM `" . TABLE_PREFIX . "navigation`";
        // echo $query; die;
        mysqli_query($temp_link, $query);

        $query = "DELETE FROM `" . TABLE_PREFIX . "banner` WHERE id!=1";
        // echo $query; die;
        mysqli_query($temp_link, $query);

        $query = "DELETE FROM `" . TABLE_PREFIX . "banner_location` WHERE id!=1";
        // echo $query; die;
        mysqli_query($temp_link, $query);


        $query = "ALTER TABLE `" . TABLE_PREFIX . "banner` AUTO_INCREMENT=2;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);

        $query = "ALTER TABLE `" . TABLE_PREFIX . "banner_location` AUTO_INCREMENT=2;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);

        $query = "ALTER TABLE `" . TABLE_PREFIX . "navigation` AUTO_INCREMENT=1;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);

        $query = "ALTER TABLE `" . TABLE_PREFIX . "product` AUTO_INCREMENT=1;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);

        $query = "ALTER TABLE `" . TABLE_PREFIX . "product_image` AUTO_INCREMENT=1;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);

        $query = "ALTER TABLE `" . TABLE_PREFIX . "url_rewrite` AUTO_INCREMENT=1;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);

        $query = "ALTER TABLE `" . TABLE_PREFIX . "category` AUTO_INCREMENT=1;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);

        $query = "ALTER TABLE `" . TABLE_PREFIX . "category_product` AUTO_INCREMENT=1;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);

        $query = "ALTER TABLE `" . TABLE_PREFIX . "category_structure` AUTO_INCREMENT=1;";
        //echo $sample; die;
        mysqli_query($temp_link, $query);


        $query = "INSERT INTO `" . TABLE_PREFIX . "navigation` (`id`, `parent_id`, `nav_id`, `nav_class`, `is_mega_menu`, `navigation_title`, `navigation_link`, `position`, `is_active`, `navigation_query`, `is_external`, `open_in_new`, `is_deleted`, `date_add`, `date_upd`) VALUES
(1, 0, 1, '', 0, 'New', 'new_products', 1, 1, NULL, 0, 0, 0, '2015-01-14 02:24:10', '2015-02-17 14:22:47'),
(2, 0, 1, 'red', 0, 'Hot Deals', NULL, 2, 1, NULL, 1, 0, 0, '2015-01-14 02:25:17', '2015-02-17 14:22:48'),
(3, 0, 1, '', 1, 'Vibrators', NULL, 3, 1, NULL, 1, 0, 0, '2015-01-14 02:25:34', '2015-02-17 14:22:49'),
(4, 0, 1, '', 1, 'Dildos & Anal', NULL, 4, 1, NULL, 1, 0, 0, '2015-01-14 02:26:00', '2015-02-18 05:23:33'),
(5, 0, 1, '', 1, 'For Him', NULL, 5, 1, NULL, 1, 0, 0, '2015-01-14 02:26:59', '2015-02-17 14:22:51'),
(6, 0, 1, '', 1, 'For Her', NULL, 6, 1, NULL, 1, 0, 0, '2015-01-14 02:27:17', '2015-01-14 15:29:42'),
(7, 0, 1, '', 1, 'For Couples', NULL, 7, 1, NULL, 1, 0, 0, '2015-01-14 02:27:29', '2015-01-14 15:29:43'),
(8, 0, 1, '', 1, 'Lube', NULL, 8, 1, NULL, 1, 0, 0, '2015-01-14 02:27:41', '2015-01-14 15:29:44'),
(9, 0, 1, '', 1, 'BDSM & Knik', NULL, 9, 1, NULL, 1, 0, 0, '2015-01-14 02:28:38', '2015-01-14 15:29:47'),
(10, 2, 1, '', 0, 'Overstock', 'shop', 1, 1, 'id=215', 0, 0, 0, '2015-01-14 02:31:23', '2015-01-14 15:33:29'),
(11, 2, 1, '', 0, 'Clearance', 'shop', 2, 1, 'id=218', 0, 0, 0, '2015-01-14 02:32:35', '2015-01-14 17:07:59'),
(12, 3, 1, '', 0, 'View All Vibrators', 'shop', NULL, 1, 'id=262', 0, 0, 0, '2015-01-14 02:36:03', '2015-01-14 15:36:03'),
(13, 3, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 02:36:51', '2015-01-14 15:36:51'),
(14, 3, 1, '', 0, 'Column3', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 02:37:00', '2015-01-14 15:37:00'),
(15, 12, 1, '', 0, 'Rabbit Vibrators', 'shop', NULL, 1, 'id=33', 0, 0, 0, '2015-01-14 02:37:35', '2015-01-14 15:37:35'),
(16, 12, 1, '', 0, 'G Spot Vibrators', 'shop', NULL, 1, 'id=96', 0, 0, 0, '2015-01-14 02:38:02', '2015-01-14 15:38:38'),
(17, 12, 1, '', 0, 'Eggs & Bullets', 'shop', NULL, 1, 'id=94', 0, 0, 0, '2015-01-14 02:39:15', '2015-01-14 15:39:15'),
(18, 12, 1, '', 0, 'Wand Massagers', 'shop', NULL, 1, 'id=99', 0, 0, 0, '2015-01-14 02:39:45', '2015-01-14 15:39:45'),
(19, 13, 1, '', 0, 'Vibrator Kits', 'shop', 1, 1, 'id=98', 0, 0, 0, '2015-01-14 02:43:55', '2015-01-14 15:44:32'),
(20, 13, 1, '', 0, 'Silicone Vibrators', 'shop', 2, 1, 'id=208', 0, 0, 0, '2015-01-14 02:44:27', '2015-01-14 15:54:24'),
(21, 14, 1, '', 0, 'Anal Vibrators', 'shop', 1, 1, 'id=1', 0, 0, 0, '2015-01-14 02:45:37', '2015-01-14 15:46:12'),
(22, 14, 1, '', 0, 'Discreet Vibrators', 'shop', 2, 1, 'id=95', 0, 0, 0, '2015-01-14 02:46:05', '2015-01-14 15:46:13'),
(23, 14, 1, '', 0, 'Realistic Vibrators', 'shop', 3, 1, 'id=97', 0, 0, 0, '2015-01-14 02:46:45', '2015-01-14 15:46:50'),
(24, 14, 1, '', 0, 'Prostate Vibrators', 'shop', 4, 1, 'id=106', 0, 0, 0, '2015-01-14 02:47:51', '2015-01-14 15:48:02'),
(25, 14, 1, '', 0, 'Vibrating Cock Rings', 'shop', 5, 1, 'id=130', 0, 0, 0, '2015-01-14 02:48:35', '2015-01-14 15:48:39'),
(26, 4, 1, '', 0, 'View All Dildos & Anal Toys', 'shop', NULL, 1, 'id=264', 0, 0, 0, '2015-01-14 02:57:14', '2015-01-14 15:57:14'),
(27, 4, 1, '', 0, 'Glass & Steel', 'shop', NULL, 1, 'id=230', 0, 0, 0, '2015-01-14 02:57:55', '2015-01-14 15:57:55'),
(28, 4, 1, '', 0, 'Column3', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 02:58:12', '2015-01-14 15:58:12'),
(29, 26, 1, '', 0, 'Realistic', 'shop', NULL, 1, 'id=133', 0, 0, 0, '2015-01-14 02:59:11', '2015-01-14 15:59:11'),
(30, 26, 1, '', 0, 'Classic Dildos', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-01-14 03:01:05', '2015-01-14 16:01:05'),
(31, 26, 1, '', 0, 'Butt Plugs', 'shop', NULL, 1, 'id=244', 0, 0, 0, '2015-01-14 03:02:12', '2015-01-14 16:02:12'),
(32, 26, 1, '', 0, 'Beads & Balls', 'shop', NULL, 1, 'id=105', 0, 0, 0, '2015-01-14 03:03:11', '2015-01-14 16:03:11'),
(33, 26, 1, '', 0, 'G-Spot', 'shop', NULL, 1, 'id=245', 0, 0, 0, '2015-01-14 03:03:45', '2015-01-14 16:03:45'),
(34, 26, 1, '', 0, 'Prostate Massagers', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-01-14 03:04:48', '2015-01-14 16:04:48'),
(35, 26, 1, '', 0, 'Vibrating', 'shop', NULL, 1, 'id=38', 0, 0, 0, '2015-01-14 03:05:40', '2015-01-14 16:05:40'),
(36, 26, 1, '', 0, 'Premium Silicone', 'shop', NULL, 1, 'id=207', 0, 0, 0, '2015-01-14 03:06:25', '2015-01-14 16:06:25'),
(37, 26, 1, '', 0, 'Anal Toys', 'shop', NULL, 1, 'id=2', 0, 0, 0, '2015-01-14 03:07:17', '2015-01-14 16:07:17'),
(38, 27, 1, '', 0, 'Glass Dildos', 'shop', NULL, 1, 'id=144', 0, 0, 0, '2015-01-14 03:07:57', '2015-01-14 16:07:57'),
(39, 27, 1, '', 0, 'Steel Dildos', 'shop', NULL, 1, 'id=145', 0, 0, 0, '2015-01-14 03:08:40', '2015-01-14 16:08:40'),
(40, 27, 1, '', 0, 'Huge Insertables', 'shop', NULL, 1, 'id=22', 0, 0, 0, '2015-01-14 03:09:18', '2015-01-14 16:09:18'),
(41, 27, 1, '', 0, 'Inflatable Dildos', 'shop', NULL, 1, 'id=113', 0, 0, 0, '2015-01-14 03:09:36', '2015-01-14 16:09:36'),
(42, 28, 1, '', 0, 'Strap-Ons and Harnesses', 'shop', NULL, 1, 'id=35', 0, 0, 0, '2015-01-14 03:11:35', '2015-01-14 16:11:35'),
(43, 28, 1, '', 0, 'Penis Sheaths & Extenders', 'shop', NULL, 1, 'id=154', 0, 0, 0, '2015-01-14 03:12:10', '2015-01-14 16:12:10'),
(44, 28, 1, '', 0, 'Enema Gear', 'shop', NULL, 1, 'id=159', 0, 0, 0, '2015-01-14 03:12:45', '2015-01-14 16:12:45'),
(45, 5, 1, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=259', 0, 0, 0, '2015-01-14 03:23:22', '2015-01-14 16:23:22'),
(46, 5, 1, '', 0, 'Cock Rings', 'shop', NULL, 1, 'id=9', 0, 0, 0, '2015-01-14 03:23:50', '2015-01-14 16:23:50'),
(47, 5, 1, '', 0, 'Penis Sheaths & Extenders', 'shop', NULL, 0, 'id=154', 0, 0, 0, '2015-01-14 03:24:26', '2015-01-14 16:37:09'),
(48, 45, 1, '', 0, 'Masturbation Toys', 'shop', NULL, 1, 'id=26', 0, 0, 0, '2015-01-14 03:24:53', '2015-01-14 16:24:53'),
(49, 45, 1, '', 0, 'Penis Pumps', 'shop', NULL, 1, 'id=153', 0, 0, 0, '2015-01-14 03:25:13', '2015-01-14 16:25:13'),
(50, 45, 1, '', 0, 'Prostate Stimulators', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-01-14 03:25:30', '2015-01-14 16:25:30'),
(51, 45, 1, '', 0, 'Butt Plugs', 'shop', NULL, 1, 'id=244', 0, 0, 0, '2015-01-14 03:25:52', '2015-01-14 16:25:52'),
(52, 45, 1, '', 0, 'Love Dolls', 'shop', NULL, 1, 'id=25', 0, 0, 0, '2015-01-14 03:26:12', '2015-01-14 16:26:12'),
(53, 45, 1, '', 0, 'Chastity', 'shop', NULL, 1, 'id=101', 0, 0, 0, '2015-01-14 03:26:32', '2015-01-14 16:26:32'),
(54, 45, 1, '', 0, 'Electrosex', 'shop', NULL, 1, 'id=13', 0, 0, 0, '2015-01-14 03:26:53', '2015-01-14 16:26:53'),
(55, 45, 1, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-01-14 03:27:10', '2015-01-14 16:27:10'),
(56, 45, 1, '', 0, 'Lubes For Men', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-01-14 03:27:34', '2015-01-14 16:27:34'),
(57, 46, 1, '', 0, 'Metal Cock Rings', 'shop', NULL, 1, 'id=131', 0, 0, 0, '2015-01-14 03:29:26', '2015-01-14 16:29:26'),
(58, 46, 1, '', 0, 'Multi-Ring Cock Rings', 'shop', NULL, 1, 'id=132', 0, 0, 0, '2015-01-14 03:29:43', '2015-01-14 16:29:43'),
(59, 46, 1, '', 0, 'Vibrating Cock Rings', 'shop', NULL, 1, 'id=130', 0, 0, 0, '2015-01-14 03:29:59', '2015-01-14 16:29:59'),
(60, 46, 1, 'text-strong', 0, 'Penis Sheaths &amp; Extenders', 'shop', NULL, 1, 'id=154', 0, 0, 0, '2015-01-14 03:36:49', '2015-01-14 16:48:30'),
(61, 6, 1, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=260', 0, 0, 0, '2015-01-14 03:51:59', '2015-01-14 16:51:59'),
(62, 6, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 03:52:25', '2015-01-14 16:52:25'),
(63, 6, 1, '', 0, 'Column3', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 03:52:32', '2015-01-14 16:52:32'),
(64, 61, 1, '', 0, 'Rabbit', 'shop', NULL, 1, 'id=33', 0, 0, 0, '2015-01-14 03:53:45', '2015-01-14 16:53:45'),
(65, 61, 1, '', 0, 'G Spot', 'shop', NULL, 1, 'id=96', 0, 0, 0, '2015-01-14 03:54:14', '2015-01-14 16:54:14'),
(66, 61, 1, '', 0, 'Eggs & Bullets', 'shop', NULL, 1, 'id=94', 0, 0, 0, '2015-01-14 03:55:05', '2015-01-14 16:55:05'),
(67, 61, 1, '', 0, 'Wand Massagers', 'shop', NULL, 1, 'id=99', 0, 0, 0, '2015-01-14 03:55:25', '2015-01-14 16:55:25'),
(68, 61, 1, '', 0, 'View All Vibrators', 'shop', NULL, 1, 'id=228', 0, 0, 0, '2015-01-14 03:55:59', '2015-01-14 16:55:59'),
(69, 61, 1, '', 0, 'Vibrator Kits', 'shop', NULL, 1, 'id=98', 0, 0, 0, '2015-01-14 03:56:37', '2015-01-14 16:56:37'),
(70, 61, 1, '', 0, 'Silicone', 'shop', NULL, 1, 'id=208', 0, 0, 0, '2015-01-14 03:56:58', '2015-01-14 16:56:58'),
(71, 62, 1, '', 0, 'Dildos & Anal', 'shop', NULL, 1, 'id=229', 0, 0, 0, '2015-01-14 03:57:30', '2015-01-14 16:57:30'),
(72, 62, 1, '', 0, 'Realistic Dildos', 'shop', NULL, 1, 'id=133', 0, 0, 0, '2015-01-14 03:57:48', '2015-01-14 16:57:48'),
(73, 62, 1, '', 0, 'Huge', 'shop', NULL, 1, 'id=100', 0, 0, 0, '2015-01-14 03:58:10', '2015-01-14 16:58:10'),
(74, 62, 1, '', 0, 'Anal Vibrators', 'shop', NULL, 1, 'id=1', 0, 0, 0, '2015-01-14 03:58:30', '2015-01-14 16:58:30'),
(75, 62, 1, '', 0, 'Classic Dildos', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-01-14 03:58:50', '2015-01-14 16:58:50'),
(76, 62, 1, '', 0, 'Beads & Balls', 'shop', NULL, 1, 'id=105', 0, 0, 0, '2015-01-14 03:59:18', '2015-01-14 16:59:18'),
(77, 62, 1, '', 0, 'Premium Silicone', 'shop', NULL, 1, 'id=210', 0, 0, 0, '2015-01-14 03:59:46', '2015-01-14 16:59:46'),
(78, 62, 1, '', 0, 'Glass Dildos', 'shop', NULL, 1, 'id=144', 0, 0, 0, '2015-01-14 04:00:04', '2015-01-14 17:00:04'),
(79, 62, 1, '', 0, 'Steel Dildos', 'shop', NULL, 1, 'id=145', 0, 0, 0, '2015-01-14 04:00:20', '2015-01-14 17:00:20'),
(80, 62, 1, '', 0, 'Inflatable', 'shop', NULL, 1, 'id=113', 0, 0, 0, '2015-01-14 04:00:38', '2015-01-14 17:00:39'),
(81, 63, 1, '', 0, 'Strap-Ons and Harnesses', 'shop', NULL, 1, 'id=35', 0, 0, 0, '2015-01-14 04:01:14', '2015-01-14 17:01:14'),
(82, 63, 1, '', 0, 'Nipple & Breast Pumps', 'shop', NULL, 1, 'id=155', 0, 0, 0, '2015-01-14 04:01:37', '2015-01-14 17:01:37'),
(83, 63, 1, '', 0, 'Nipple Toys', 'shop', NULL, 1, 'id=29', 0, 0, 0, '2015-01-14 04:01:55', '2015-01-14 17:01:55'),
(84, 63, 1, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-01-14 04:02:11', '2015-01-14 17:02:18'),
(85, 63, 1, '', 0, 'Lubes For Women', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-01-14 04:02:46', '2015-01-14 17:02:46'),
(86, 7, 1, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=261', 0, 0, 0, '2015-01-14 04:03:28', '2015-01-14 17:03:28'),
(87, 7, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 04:03:44', '2015-01-14 17:03:44'),
(88, 86, 1, '', 0, 'Swings & Sex Aids', 'shop', NULL, 1, 'id=36', 0, 0, 0, '2015-01-14 04:04:42', '2015-01-14 17:04:42'),
(89, 86, 1, '', 0, 'Bedroom Bondage', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-01-14 04:05:18', '2015-01-14 17:05:18'),
(90, 86, 1, '', 0, 'Strap Ons & Harnesses', 'shop', NULL, 1, 'id=35', 0, 0, 0, '2015-01-14 04:06:08', '2015-01-14 17:06:08'),
(91, 86, 1, '', 0, 'Stripper Poles', 'shop', NULL, 1, 'id=188', 0, 0, 0, '2015-01-14 04:06:29', '2015-01-14 17:06:29'),
(92, 87, 1, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-01-14 04:09:50', '2015-01-14 17:09:50'),
(93, 87, 1, '', 0, 'Chastity', 'shop', NULL, 1, 'id=7', 0, 0, 0, '2015-01-14 04:10:16', '2015-01-14 17:10:16'),
(94, 87, 1, '', 0, 'Lube For Couples', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-01-14 04:10:37', '2015-01-14 17:10:37'),
(95, 8, 1, '', 0, 'View All Lubes', 'shop', NULL, 1, 'id=263', 0, 0, 0, '2015-01-14 04:13:08', '2015-01-14 17:13:08'),
(96, 8, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 04:13:18', '2015-01-14 17:13:18'),
(97, 95, 1, '', 0, 'Water Based Lube ', 'shop', NULL, 1, 'id=120', 0, 0, 0, '2015-01-14 04:13:42', '2015-01-14 17:13:50'),
(98, 95, 1, '', 0, 'Silicone Based Lube', 'shop', NULL, 1, 'id=121', 0, 0, 0, '2015-01-14 04:15:21', '2015-01-14 17:16:31'),
(99, 95, 1, '', 0, 'Lube Applicators', 'shop', NULL, 1, 'id=122', 0, 0, 0, '2015-01-14 04:17:03', '2015-01-14 17:17:03'),
(100, 95, 1, '', 0, 'Oil Based', 'shop', NULL, 1, 'id=123', 0, 0, 0, '2015-01-14 04:17:25', '2015-01-14 17:17:25'),
(101, 96, 1, '', 0, 'Anal Lube', 'shop', NULL, 1, 'id=118', 0, 0, 0, '2015-01-14 04:18:19', '2015-01-14 17:18:19'),
(102, 96, 1, '', 0, 'Creams and Lotions', 'shop', NULL, 1, 'id=124', 0, 0, 0, '2015-01-14 04:18:41', '2015-01-14 17:18:41'),
(103, 96, 1, '', 0, 'Flavored Lube', 'shop', NULL, 1, 'id=119', 0, 0, 0, '2015-01-14 04:19:42', '2015-01-14 17:19:42'),
(104, 96, 1, '', 0, 'Electrosex Lubes and Cleaners', 'shop', NULL, 1, 'id=151', 0, 0, 0, '2015-01-14 04:20:22', '2015-01-14 17:20:22'),
(105, 9, 1, '', 0, 'View All BDSM & Kink', 'shop', NULL, 1, 'id=265', 0, 0, 0, '2015-01-14 04:21:52', '2015-01-14 17:21:52'),
(106, 9, 1, '', 0, 'Column2', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 04:22:04', '2015-01-14 17:22:04'),
(107, 9, 1, '', 0, 'Column3', NULL, NULL, 1, NULL, 1, 0, 0, '2015-01-14 04:22:13', '2015-01-14 17:22:13'),
(108, 105, 1, '', 0, 'Beginner Bondage', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-01-14 04:22:30', '2015-01-14 17:22:30'),
(109, 105, 1, '', 0, 'Leather Bondage Goods', 'shop', NULL, 1, 'id=24', 0, 0, 0, '2015-01-14 04:22:47', '2015-01-14 17:22:47'),
(110, 105, 1, '', 0, 'Wrist & Ankle Restraints', 'shop', NULL, 1, 'id=138', 0, 0, 0, '2015-01-14 04:23:11', '2015-01-14 17:23:11'),
(111, 105, 1, '', 0, 'Collars', 'shop', NULL, 1, 'id=137', 0, 0, 0, '2015-01-14 04:23:29', '2015-01-14 17:23:29'),
(112, 105, 1, '', 0, 'Blindfolds', 'shop', NULL, 1, 'id=21', 0, 0, 0, '2015-01-14 04:23:56', '2015-01-14 17:23:56'),
(113, 106, 1, '', 0, 'Mouth Gags', 'shop', NULL, 1, 'id=28', 0, 0, 0, '2015-01-14 04:25:03', '2015-01-14 17:25:03'),
(114, 106, 1, '', 0, 'Hoods & Muzzles', 'shop', NULL, 1, 'id=73', 0, 0, 0, '2015-01-14 04:26:02', '2015-01-14 17:26:02'),
(115, 106, 1, '', 0, 'Spreader Bars', 'shop', NULL, 1, 'id=161', 0, 0, 0, '2015-01-14 04:26:37', '2015-01-14 17:26:37'),
(116, 106, 1, '', 0, 'Handcuffs and Steel', 'shop', NULL, 1, 'id=19', 0, 0, 0, '2015-01-14 04:42:58', '2015-01-14 17:42:58'),
(117, 106, 1, '', 0, 'Hoods and Blindfolds', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-01-14 04:43:17', '2015-01-14 17:43:17'),
(118, 107, 1, '', 0, 'Enema Gear', 'shop', 1, 1, 'id=159', 0, 0, 0, '2015-01-14 04:48:34', '2015-01-19 16:21:58'),
(119, 107, 1, '', 0, 'Fucking Machines', 'shop', 2, 1, 'id=16', 0, 0, 0, '2015-01-14 04:48:48', '2015-01-19 16:21:59'),
(120, 107, 1, '', 0, 'Chastity Devices', 'shop', 3, 1, 'id=7', 0, 0, 0, '2015-01-14 04:49:19', '2015-01-19 16:22:01'),
(121, 107, 1, '', 0, 'Crops & Whips', 'shop', 4, 1, 'id=114', 0, 0, 0, '2015-01-14 04:49:50', '2015-01-19 16:22:13'),
(122, 107, 1, '', 0, 'Penis Jewlery', 'shop', 5, 1, 'id=31', 0, 0, 0, '2015-01-14 04:50:12', '2015-01-19 16:22:14'),
(124, 0, 1, '', 0, 'New2', 'new_products', NULL, 1, NULL, 0, 0, 1, '0000-00-00 00:00:00', '2015-01-19 16:18:08'),
(125, 0, 1, 'hi', 0, 'hi', 'h', NULL, 1, NULL, 1, 1, 1, '2015-01-19 03:29:57', '2015-01-19 16:30:27'),
(126, 0, 1, '', 0, 'test', 'product', NULL, 1, 'id=3258', 0, 0, 1, '2015-01-19 03:56:50', '2015-01-19 17:18:47'),
(127, 0, 1, '', 1, 'New2', 'shop', NULL, 1, 'id=4', 0, 0, 1, '2015-01-19 04:19:03', '2015-01-19 17:22:17'),
(128, 127, 1, 'bbb', 0, 'Eggs & Bullets', 'cart', NULL, 1, NULL, 0, 0, 0, '2015-01-19 04:19:36', '2015-01-19 17:19:36'),
(129, 127, 1, '', 0, 'Eggs & Bullets', 'shop', NULL, 1, 'id=42', 0, 0, 0, '2015-01-19 04:20:11', '2015-01-19 17:20:11'),
(130, 128, 1, '', 0, 'Eggs & Bullets', 'product', NULL, 1, 'id=3256', 0, 0, 0, '2015-01-19 04:20:44', '2015-01-19 17:20:44'),
(131, 0, 3, '', 0, 'New', 'new_products', NULL, 1, NULL, 0, 0, 0, '2015-02-11 21:48:10', '2015-02-12 06:01:01'),
(132, 0, 3, '', 0, 'Hot Deals', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-11 22:22:43', '2015-02-12 06:22:43'),
(133, 132, 3, '', 0, 'Over Stock', 'shop', NULL, 1, 'id=215', 0, 0, 0, '2015-02-11 22:23:37', '2015-02-12 06:23:37'),
(134, 132, 3, '', 0, 'Clearance', 'shop', NULL, 1, 'id=218', 0, 0, 0, '2015-02-11 22:24:18', '2015-02-12 06:24:18'),
(135, 0, 3, '', 0, 'Vibrators', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-11 22:25:18', '2015-02-12 06:25:18'),
(136, 135, 3, '', 0, 'View All Vibrators', 'shop', NULL, 1, 'id=228', 0, 0, 0, '2015-02-11 22:28:32', '2015-02-12 06:28:32'),
(137, 135, 3, '', 0, 'Vibrator Kits', 'shop', NULL, 1, 'id=98', 0, 0, 0, '2015-02-11 22:29:08', '2015-02-12 06:29:08'),
(138, 135, 3, '', 0, 'Silicone Vibrators', 'shop', NULL, 1, 'id=208', 0, 0, 0, '2015-02-11 22:29:33', '2015-02-12 06:29:33'),
(139, 135, 3, '', 0, 'Anal Vibrators', 'shop', NULL, 1, 'id=1', 0, 0, 0, '2015-02-11 22:30:12', '2015-02-12 06:30:12'),
(140, 135, 3, '', 0, 'Discreet Vibrators', 'shop', NULL, 1, 'id=95', 0, 0, 0, '2015-02-11 22:30:49', '2015-02-12 06:30:49'),
(141, 135, 3, '', 0, 'Realistic Vibrators', 'shop', NULL, 1, 'id=97', 0, 0, 0, '2015-02-11 22:31:27', '2015-02-12 06:31:27'),
(142, 135, 3, '', 0, 'Prostate Vibrators', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-02-11 22:32:03', '2015-02-12 06:32:03'),
(143, 135, 3, '', 0, 'Vibrating Cock Rings', 'shop', NULL, 1, 'id=130', 0, 0, 0, '2015-02-11 22:32:42', '2015-02-12 06:32:42'),
(144, 135, 3, '', 0, 'Rabbit Vibrators', 'shop', NULL, 1, 'id=33', 0, 0, 0, '2015-02-11 22:33:09', '2015-02-12 06:33:09'),
(145, 135, 3, '', 0, 'G Spot Vibrators', 'shop', NULL, 1, 'id=241', 0, 0, 0, '2015-02-11 22:34:12', '2015-02-12 06:34:12'),
(146, 0, 3, '', 1, 'Dildos & Anal', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 01:16:51', '2015-02-13 12:54:49'),
(147, 146, 3, '', 0, 'View All Dildos & Anal Toys', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-02-13 04:52:59', '2015-02-13 12:52:59'),
(148, 146, 3, '', 0, ' Dildos & Anal by Category', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:54:41', '2015-02-13 12:54:41'),
(149, 148, 3, '', 0, 'Realistic', 'shop', NULL, 1, 'id=133', 0, 0, 0, '2015-02-13 04:55:43', '2015-02-13 12:55:43'),
(150, 148, 3, '', 0, 'Classic Dildos', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-02-13 04:56:08', '2015-02-13 12:56:08'),
(151, 148, 3, '', 0, 'Butt Plugs', 'shop', NULL, 1, 'id=244', 0, 0, 0, '2015-02-13 04:56:27', '2015-02-13 12:56:27'),
(152, 148, 3, '', 0, 'Beads & Balls', 'shop', NULL, 1, 'id=105', 0, 0, 0, '2015-02-13 04:57:00', '2015-02-13 12:57:00'),
(153, 148, 3, '', 0, 'G-Spot', 'shop', NULL, 1, 'id=245', 0, 0, 0, '2015-02-13 04:57:24', '2015-02-13 12:57:24'),
(154, 0, 3, '', 1, 'For Him', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:57:49', '2015-02-13 12:57:54'),
(155, 148, 3, '', 0, 'Prostate Massagers', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-02-13 04:57:55', '2015-02-13 12:57:55'),
(156, 148, 3, '', 0, 'Vibrating', 'shop', NULL, 1, 'id=38', 0, 0, 0, '2015-02-13 04:58:14', '2015-02-13 12:58:14'),
(157, 0, 3, '', 0, 'For Her', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:58:15', '2015-02-13 12:58:15'),
(158, 148, 3, '', 0, 'Premium Silicone', 'shop', NULL, 1, 'id=207', 0, 0, 0, '2015-02-13 04:58:42', '2015-02-13 12:58:42'),
(159, 0, 3, '', 0, 'For Couples', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:58:53', '2015-02-13 12:58:53'),
(160, 148, 3, '', 0, 'Anal Toys', 'shop', NULL, 1, 'id=2', 0, 0, 0, '2015-02-13 04:59:08', '2015-02-13 12:59:08'),
(161, 0, 3, '', 0, 'Lube', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:59:14', '2015-02-13 12:59:14'),
(162, 0, 3, '', 0, 'BDSM & Knik', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 04:59:39', '2015-02-13 12:59:39'),
(163, 146, 3, '', 0, 'Glass & Steel Dildos', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:00:02', '2015-02-13 13:00:02'),
(164, 163, 3, '', 0, 'Glass Dildos', 'shop', NULL, 1, 'id=144', 0, 0, 0, '2015-02-13 05:00:47', '2015-02-13 13:00:47'),
(165, 162, 3, '', 0, 'View All BDSM & Kink', 'shop', NULL, 1, 'id=234', 0, 0, 0, '2015-02-13 05:00:59', '2015-02-13 13:00:59'),
(166, 163, 3, '', 0, 'Steel Dildos', 'shop', NULL, 1, 'id=145', 0, 0, 0, '2015-02-13 05:01:15', '2015-02-13 13:01:15'),
(167, 162, 3, '', 0, 'Mouth Gags', 'shop', NULL, 1, 'id=28', 0, 0, 0, '2015-02-13 05:01:30', '2015-02-13 13:01:30'),
(168, 162, 3, '', 0, 'Hoods & Muzzles', 'shop', NULL, 1, 'id=73', 0, 0, 0, '2015-02-13 05:02:09', '2015-02-13 13:02:09'),
(169, 163, 3, '', 0, 'Huge Insertables', 'shop', NULL, 1, 'id=22', 0, 0, 0, '2015-02-13 05:02:36', '2015-02-13 13:02:36'),
(170, 162, 3, '', 0, 'Spreader Bars', 'shop', NULL, 1, 'id=161', 0, 0, 0, '2015-02-13 05:02:48', '2015-02-13 13:02:48'),
(171, 163, 3, '', 0, 'Inflatable Dildos', 'shop', NULL, 1, 'id=113', 0, 0, 0, '2015-02-13 05:02:57', '2015-02-13 13:02:57'),
(172, 162, 3, '', 0, 'Handcuffs and Steel', 'shop', NULL, 1, 'id=19', 0, 0, 0, '2015-02-13 05:03:13', '2015-02-13 13:03:13'),
(173, 162, 3, '', 0, 'Hoods and Blindfolds', 'shop', NULL, 1, 'id=21', 0, 0, 0, '2015-02-13 05:03:32', '2015-02-13 13:03:32'),
(174, 162, 3, '', 0, 'Enema Gear', 'shop', NULL, 1, 'id=159', 0, 0, 0, '2015-02-13 05:04:00', '2015-02-13 13:04:00'),
(175, 146, 3, '', 0, 'Strap-Ons and Harnesses', '', NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:04:11', '2015-02-13 13:20:26'),
(176, 162, 3, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-02-13 05:04:18', '2015-02-13 13:04:18'),
(177, 146, 3, '', 0, 'Penis Sheaths & Extenders', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:04:36', '2015-02-13 13:04:36'),
(178, 162, 3, '', 0, 'Chastity Devices', 'shop', NULL, 1, 'id=7', 0, 0, 0, '2015-02-13 05:04:37', '2015-02-13 13:04:37'),
(179, 146, 3, '', 0, 'Enema Gear', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:04:54', '2015-02-13 13:04:54'),
(180, 154, 3, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=101', 0, 0, 0, '2015-02-13 05:07:35', '2015-02-13 13:07:35'),
(181, 154, 3, '', 0, 'Cock Rings', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:07:58', '2015-02-13 13:07:58'),
(182, 161, 3, '', 0, 'View All Lubes', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-02-13 05:08:16', '2015-02-13 13:08:16'),
(183, 181, 3, '', 0, 'Metal Cock Rings', 'shop', NULL, 1, 'id=131', 0, 0, 0, '2015-02-13 05:08:37', '2015-02-13 13:08:37'),
(184, 161, 3, '', 0, 'Anal Lube', 'shop', NULL, 1, 'id=118', 0, 0, 0, '2015-02-13 05:08:47', '2015-02-13 13:08:47'),
(185, 181, 3, '', 0, 'Multi-Ring Cock Rings', 'shop', NULL, 1, 'id=132', 0, 0, 0, '2015-02-13 05:08:54', '2015-02-13 13:08:54'),
(186, 161, 3, '', 0, 'Creams and Lotions', 'shop', NULL, 1, 'id=124', 0, 0, 0, '2015-02-13 05:09:15', '2015-02-13 13:09:15'),
(187, 181, 3, '', 0, 'Vibrating Cock Rings', 'shop', NULL, 1, 'id=130', 0, 0, 0, '2015-02-13 05:09:22', '2015-02-13 13:09:22'),
(188, 161, 3, '', 0, 'Flavored Lube', 'shop', NULL, 1, 'id=119', 0, 0, 0, '2015-02-13 05:09:33', '2015-02-13 13:09:33'),
(189, 161, 3, '', 0, 'Electrosex Lubes and Cleaners', 'shop', NULL, 1, 'id=151', 0, 0, 0, '2015-02-13 05:09:50', '2015-02-13 13:09:50'),
(190, 161, 3, '', 0, 'Water Based Lube', 'shop', NULL, 1, 'id=120', 0, 0, 0, '2015-02-13 05:10:16', '2015-02-13 13:10:16'),
(191, 154, 3, '', 0, 'Penis Sheaths & Extenders', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:10:49', '2015-02-13 13:10:49'),
(192, 154, 3, '', 0, 'For Him By Category', NULL, NULL, 1, NULL, 1, 0, 0, '2015-02-13 05:11:23', '2015-02-13 13:11:23'),
(193, 161, 3, '', 0, 'Silicone Based Lube', 'shop', NULL, 1, 'id=121', 0, 0, 0, '2015-02-13 05:11:41', '2015-02-13 13:11:41'),
(194, 161, 3, '', 0, 'Lube Applicators', 'shop', NULL, 1, 'id=122', 0, 0, 0, '2015-02-13 05:11:57', '2015-02-13 13:11:57'),
(195, 161, 3, '', 0, 'Oil Based', 'shop', NULL, 1, 'id=123', 0, 0, 0, '2015-02-13 05:12:12', '2015-02-13 13:12:12'),
(196, 192, 3, '', 0, 'Masturbation Toys', 'shop', NULL, 1, 'id=26', 0, 0, 0, '2015-02-13 05:12:22', '2015-02-13 13:12:22'),
(197, 192, 3, '', 0, 'Penis Pumps', 'shop', NULL, 1, 'id=153', 0, 0, 0, '2015-02-13 05:12:43', '2015-02-13 13:12:43'),
(198, 192, 3, '', 0, 'Prostate Stimulators', 'shop', NULL, 1, 'id=106', 0, 0, 0, '2015-02-13 05:13:05', '2015-02-13 13:13:05'),
(199, 192, 3, '', 0, 'Butt Plugs', 'shop', NULL, 1, 'id=244', 0, 0, 0, '2015-02-13 05:13:30', '2015-02-13 13:13:30'),
(200, 159, 3, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-02-13 05:13:33', '2015-02-13 13:13:33'),
(201, 159, 3, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-02-13 05:13:49', '2015-02-13 13:13:49'),
(202, 192, 3, '', 0, 'Love Dolls', 'shop', NULL, 1, 'id=25', 0, 0, 0, '2015-02-13 05:13:53', '2015-02-13 13:13:53'),
(203, 159, 3, '', 0, 'Chastity', 'shop', NULL, 1, 'id=7', 0, 0, 0, '2015-02-13 05:14:06', '2015-02-13 13:14:06'),
(204, 192, 3, '', 0, 'Chastity', 'shop', NULL, 1, 'id=101', 0, 0, 0, '2015-02-13 05:14:16', '2015-02-13 13:14:16'),
(205, 192, 3, '', 0, 'Electrosex', 'shop', NULL, 1, 'id=13', 0, 0, 0, '2015-02-13 05:14:39', '2015-02-13 13:14:39'),
(206, 159, 3, '', 0, 'Lube For Couples', 'shop', NULL, 1, 'id=118', 0, 0, 0, '2015-02-13 05:14:46', '2015-02-13 13:14:46'),
(207, 192, 3, '', 0, 'Fucking Machines', 'shop', NULL, 1, 'id=16', 0, 0, 0, '2015-02-13 05:15:04', '2015-02-13 13:15:04'),
(208, 159, 3, '', 0, 'Swings & Sex Aids', 'shop', NULL, 1, 'id=237', 0, 0, 0, '2015-02-13 05:15:11', '2015-02-13 13:15:11'),
(209, 192, 3, '', 0, 'Lubes For Men', 'shop', NULL, 1, 'id=233', 0, 0, 0, '2015-02-13 05:15:33', '2015-02-13 13:15:53'),
(210, 159, 3, '', 0, 'Bedroom Bondage', 'shop', NULL, 1, 'id=4', 0, 0, 0, '2015-02-13 05:15:51', '2015-02-13 13:15:51'),
(211, 159, 3, '', 0, 'Strap Ons & Harnesses', 'shop', NULL, 1, 'id=35', 0, 0, 0, '2015-02-13 05:16:13', '2015-02-13 13:16:13'),
(212, 159, 3, '', 0, 'Stripper Poles', 'shop', NULL, 1, 'id=188', 0, 0, 0, '2015-02-13 05:16:36', '2015-02-13 13:16:36'),
(213, 157, 3, '', 0, 'View All Bestsellers', 'shop', NULL, 1, 'id=102', 0, 0, 0, '2015-02-13 05:17:03', '2015-02-13 13:17:03'),
(214, 157, 3, '', 0, 'Dildos & Anal', 'shop', NULL, 1, 'id=229', 0, 0, 0, '2015-02-13 05:17:35', '2015-02-13 13:17:35'),
(215, 157, 3, '', 0, 'Realistic Dildos', 'shop', NULL, 1, 'id=133', 0, 0, 0, '2015-02-13 05:17:58', '2015-02-13 13:17:58'),
(216, 157, 3, '', 0, 'Huge', 'shop', NULL, 1, 'id=100', 0, 0, 0, '2015-02-13 05:19:15', '2015-02-13 13:19:15'),
(217, 157, 3, '', 0, 'Anal Vibrators', 'shop', NULL, 1, 'id=1', 0, 0, 0, '2015-02-13 05:19:40', '2015-02-13 13:19:40'),
(218, 157, 3, '', 0, 'Classic Dildos', 'shop', NULL, 1, 'id=11', 0, 0, 0, '2015-02-13 05:20:00', '2015-02-13 13:20:00'),
(219, 157, 3, '', 0, 'Beads & Balls', 'shop', NULL, 1, 'id=105', 0, 0, 0, '2015-02-13 05:20:23', '2015-02-13 13:20:23'),
(220, 157, 3, '', 0, 'Premium Silicone', 'shop', NULL, 1, 'id=210', 0, 0, 0, '2015-02-13 05:20:44', '2015-02-13 13:20:44'),
(221, 157, 3, '', 0, 'Glass Dildos', 'shop', NULL, 1, 'id=144', 0, 0, 0, '2015-02-13 05:21:11', '2015-02-13 13:21:11'),
(222, 1, 1, 'demo', 0, 'Navigation Sub Menu 1', 'product', 1, 1, 'id=2502', 0, 0, 0, '2015-02-16 03:07:22', '2015-02-16 11:07:52');";
        mysqli_query($temp_link, $query);
        mysqli_close($temp_link);
    }

    #Get List of all Active Users

    function getActiveUserWebsite($user_id, $result_type = 'object') {

        $this->Where = "where user_id='" . $user_id . "' AND is_deleted='0' AND is_active='1'";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    #Get List of all Deleted Users Website

    function listDeletedUserWebsite($user_id, $result_type = 'object') {
        $this->Where = "where is_deleted='1' And user_id='" . $user_id . "'";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    function getActiveWebsiteById($id) {
        $this->Where = "where id='$id' and is_active='1'";
//        $this->print=1;
        return $this->DisplayOne();
    }

    function saveUserWebsite($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update())
                return $id;
        }
        else {
            $this->Data['on_date'] = date('Y:m:d H:m:s');
            //$this->print =1;
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    #get User

    function getWebsite($id) {
        return $this->_getObject('user_website', $id);
    }

    #get Website Name

    function getWebsiteName($id) {
        $this->Field = 'id,website_name';
        $this->Where = " where id='" . $id . "'";
        return $this->DisplayOne();
    }

    #get User Website With Subscription	

    function getWebsiteAndSubscription($website_id) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription');
        $web->Field = 'user_website.*,website_subscription.plan_id,website_subscription.period_in_months,website_subscription.amount,website_subscription.from_date,website_subscription.to_date,website_subscription.is_trial';
        $web->Where = "where user_website.id='" . $website_id . "' AND website_subscription.is_current='1' and user_website.id=website_subscription.website_id";

        return $web->DisplayOne();
    }

    #get User Website With Subscription	 by Subscription id

    function getWebsiteAndSubscriptionBySubcriptionID($subscription_id) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription');
        $web->Field = 'user_website.*,website_subscription.plan_id,website_subscription.period_in_months,website_subscription.amount,website_subscription.from_date,website_subscription.to_date,website_subscription.is_trial';
        $web->Where = "where website_subscription.id='" . $subscription_id . "' AND website_subscription.is_current='1' and user_website.id=website_subscription.website_id";

        return $web->DisplayOne();
    }

    #get User Website Subscription	Detail

    function getWebsiteSubscriptionDetail($sub_id) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,subscription_plan as subscription');
        $web->Field = 'user_website.id as website_id,user_website.website_name,user_website.domain_name,user_website.is_active,subscription.title as subscription_name,website_subscription.id as id,website_subscription.plan_id,website_subscription.period_in_months,website_subscription.amount,website_subscription.from_date,website_subscription.to_date,website_subscription.next_payment_date,website_subscription.is_trial';
        $web->Where = "where website_subscription.id='" . $sub_id . "' and user_website.id=website_subscription.website_id AND subscription.id=website_subscription.plan_id";
        return $web->DisplayOne();
    }

    #get User All Website With Subscription	

    function getAllWebsitesWithUserInfo($result_type = 'object') {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,user as user');
        $web->Field = 'user_website.*,user.id as user_id,user.first_name,user.last_name,user.email as user_email,website_subscription.id as subscription_id,website_subscription.plan_id,website_subscription.period_in_months,website_subscription.amount,website_subscription.from_date,website_subscription.to_date,website_subscription.is_trial';
        $web->Where = "where user_website.user_id=user.id AND website_subscription.is_current='1' and user_website.id=website_subscription.website_id";

        return $web->ListOfAllRecords('object');
    }

    #get User All Website With Subscription In perticular Perioud	

    function getAllWebsitesWithUserInfoReport($from, $to, $type) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,user as user');
        $web->Field = 'user_website.*,user.id as user_id,user.first_name,user.last_name,user.email as user_email,website_subscription.id as subscription_id,website_subscription.plan_id,website_subscription.period_in_months,website_subscription.amount,website_subscription.from_date,website_subscription.to_date,website_subscription.is_trial';
        if ($type != 'All'):
            $web->Where = "where user_website.user_id=user.id AND website_subscription.is_current='1' AND website_subscription.is_trial='" . $type . "' AND DATE(user_website.on_date) >= CAST('" . $from . "' AS DATE ) AND DATE(user_website.on_date) <= CAST('" . $to . "' AS DATE ) AND user_website.id=website_subscription.website_id";
        else:
            $web->Where = "where user_website.user_id=user.id AND DATE(user_website.on_date) >= CAST('" . $from . "' AS DATE ) AND DATE(user_website.on_date) <= CAST('" . $to . "' AS DATE ) AND website_subscription.is_current='1' and user_website.id=website_subscription.website_id";
        endif;

        return $web->ListOfAllRecords('object');
    }

    #get User All Website With Subscription In perticular Perioud for reminder	

    function getAllWebsitesWithUserInfoToReminder($from, $to) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,user as user');
        $web->Field = 'user_website.website_name,user.id as user_id,user.first_name,user.last_name,user.email as user_email,website_subscription.id as subscription_id,website_subscription.plan_id,website_subscription.period_in_months,website_subscription.amount,website_subscription.from_date,website_subscription.to_date,website_subscription.is_trial';
        $web->Where = "where user_website.user_id=user.id AND DATE(user_website.on_date) >= CAST('" . $from . "' AS DATE ) AND DATE(user_website.on_date) <= CAST('" . $to . "' AS DATE ) AND website_subscription.is_current='1' and user_website.id=website_subscription.website_id";
        return $web->ListOfAllRecords('object');
    }

    #get Total Trial Users	

    function getCountTrialUser() {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,user as user');
        $web->Field = 'count(*) as total';
        $web->Where = "where user_website.user_id=user.id AND website_subscription.is_current='1' AND website_subscription.is_trial='1' and user_website.id=website_subscription.website_id";

        $rec = $web->DisplayOne();
        if ($rec):
            return $rec->total;
        else:
            return '0';
        endif;
    }

    #get Total Regular Users	

    function getCountRegularUser() {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,user as user');
        $web->Field = 'count(*) as total';
        $web->Where = "where user_website.user_id=user.id AND website_subscription.is_current='1' AND website_subscription.is_trial!='1' and user_website.id=website_subscription.website_id";

        $rec = $web->DisplayOne();
        if ($rec):
            return $rec->total;
        else:
            return '0';
        endif;
    }

    #get Average Regular Users	

    function getAverageRegularUser($year) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,user as user');
        $web->Field = 'count(*) as total';
        $web->Where = "where user_website.user_id=user.id AND website_subscription.is_current='1' AND YEAR(user_website.on_date)='" . $year . "' AND website_subscription.is_trial!='1' and user_website.id=website_subscription.website_id";

        $rec = $web->DisplayOne();
        if ($rec):
            $total = $rec->total / 12;
        else:
            $total = '0';
        endif;
        return ceil($total);
    }

    #get Average Trial Users	

    function getAverageTrialUser($year) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription,user as user');
        $web->Field = 'count(*) as total';
        $web->Where = "where user_website.user_id=user.id AND website_subscription.is_current='1' AND YEAR(user_website.on_date)='" . $year . "' AND website_subscription.is_trial='1' and user_website.id=website_subscription.website_id";

        $rec = $web->DisplayOne();
        if ($rec):
            $total = $rec->total / 12;
        else:
            $total = '0';
        endif;
        return ceil($total);
    }

    /* set reset key Value */

    function saveResetValue($id, $key, $value) {
        $this->Data['id'] = $id;
        $this->Data[$key] = $value;
        $this->Update();
        return true;
    }

    #get User All Website With Subscription	

    function getUserWebsites($user_id) {
        $web = new query('user_website as user_website ,website_subscription as website_subscription');
        $web->Field = 'user_website.*,website_subscription.id as subscription_id,website_subscription.plan_id as plan_id,website_subscription.period_in_months,website_subscription.amount,website_subscription.from_date,website_subscription.to_date,website_subscription.is_trial';
        $web->Where = "where user_website.user_id='" . $user_id . "' AND website_subscription.is_current='1' and user_website.id=website_subscription.website_id ";
        return $web->ListOfAllRecords('object');
    }

    #delete a page by id     

    function deleteWebsite($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /*
     * add feeds input method
     */

    function saveFeedsInput($id, $is_feed) {
        $this->Data['id'] = $id;
        $this->Data['is_feed'] = $is_feed;
        return $this->Update();
    }

    /*
     * add feeds input method
     */

    function saveMultiplierRate($id, $rate) {
        $this->Data['id'] = $id;
        $this->Data['supplier_rate_multiplier'] = $rate;
        return $this->Update();
    }

    /*
     * get user single website
     */

    public static function getUserWebsite($id) {
        $query = new userWebsite();
        $query->Field = "id";
        $query->Where = " where user_id='$id'";
        $object = $query->DisplayOne();
        if (is_object($object)):
            return $object->id;
        else:
            return '0';
        endif;
    }

    #Check Single Field exist Or Not

    public static function check_field_value_exist($field, $value, $id = 0) {

        $query = new userWebsite();

        if ($id && $id != 0 && $id != ""):
            $query->Where = "where `" . $field . "`='" . mysql_real_escape_string($value) . "' AND id!='" . $id . "'";
        else:
            $query->Where = "where `" . $field . "`='" . mysql_real_escape_string($value) . "'";
        endif;

        $user = $query->DisplayOne();
        if ($user && is_object($user)):
            return $user;
        else:
            return false;
        endif;
    }

    /*
     * get folder name from id
     */

    public static function getWebsiteFolerName($id) {
        $query = new userWebsite();
        $query->Field = " website_folder_name ";
        $query->Where = " where id='$id'";
        $object = $query->DisplayOne();
        if (is_object($object)):
            return $object->website_folder_name;
        endif;
        return false;
    }

    function deactivateWebsite($id) {
        $this->Data["is_active"] = 0;
        $this->Data["id"] = $id;
        $this->Update();
    }

    function activateWebsite($id) {
        $this->Data["is_active"] = 1;
        $this->Data["id"] = $id;
        // $this->print=1;
        $this->Update();
    }

    function getwebsitebyname($name) {
        $this->Where = " where website_name = '" . $name . "'";
        // $this->print=1;
        return $this->DisplayOne();
    }

    function deactivate_realted_store($id) {
        $this->Where = " Where user_id =" . $id;
        $this->Data["is_active"] = 0;
        $this->UpdateCustom();
    }

    public static function deleteWebsitePermanently($website_id) {
        $obj = new userWebsite;
        $obj->id = $website_id;
        return $obj->Delete();
    }

}

?>