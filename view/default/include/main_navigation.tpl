<header class="header  js--navbar">
  <div class="container">
    <div class="row">
      <div class="col-xs-10  col-md-3">
        <div class="header-logo">
          {if $smarty.const.LOGO_PATH <> ''}
            <a href="index.html"><img alt="Logo" src="{$smarty.const.LOGO_PATH}" width="200" height="90"></a>
          {/if}
        </div>
      </div>
      <div class="col-xs-2  visible-sm  visible-xs">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle  collapsed" data-toggle="collapse" data-target="#collapsible-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
      </div>
      <div class="col-xs-12  col-md-7">
        <nav class="navbar  navbar-default" role="navigation">
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse  navbar-collapse" id="collapsible-navbar">
    <ul class="nav  navbar-nav">
        {nocache}
        {if count($MAIN_NAVIGATION)}
                {foreach from=$MAIN_NAVIGATION key=kk item=vv}  
                        {if isset($vv->sub_nav) && count($vv->sub_nav)}
                             <li class="dropdown">
                               {if $vv->is_external eq '1'}
                                   <a target="_blank" href="http://{$vv->navigation_link}" class="dropdown-toggle">{$vv->navigation_title}<b class="caret"></b></a> 
                               {else}
                                   <a class="dropdown-toggle" href="{make_url page=$vv->navigation_link query=$vv->navigation_query}{if $vv->navigation_location <> ''}{$vv->navigation_location}{/if}">{$vv->navigation_title}<b class="caret"></b></a>  
                               {/if}
                                  <ul class="dropdown-menu">
                                     {foreach from=$vv->sub_nav key=kkk item=vvv}
                                          <li class="dropdown">
                                             {if isset($vvv->sub_nav) && count($vvv->sub_nav)}
                                                  {if $vvv->is_external eq '1'}
                                                        <a class="dropdown-toggle" target="_blank" href="http://{$vvv->navigation_link}">{$vvv->navigation_title}</a> 
                                                  {else}
                                                        <a class="dropdown-toggle" href="{make_url page=$vvv->navigation_link query=$vvv->navigation_query}{if $vvv->navigation_location <> ''}{$vvv->navigation_location}{/if}">{$vvv->navigation_title}</a>
                                                  {/if} 
                                                  <ul class="dropdown-menu">
                                                   {foreach from=$vvv->sub_nav key=kkkk item=vvvv}
                                                        <li>
                                                          {if $vvvv->is_external eq '1'}
                                                                <a target="_blank" href="http://{$vvvv->navigation_link}">{$vvvv->navigation_title}</a> 
                                                          {else}
                                                                <a href="{make_url page=$vvvv->navigation_link query=$vvvv->navigation_query}{if $vvvv->navigation_location <> ''}{$vvvv->navigation_location}{/if}">{$vvvv->navigation_title}</a>
                                                          {/if} 
                                                        </li>
                                                   {/foreach}
                                                   </ul>
                                             {else} 
                                                  {if $vvv->is_external eq '1'}
                                                        <a target="_blank" href="http://{$vvv->navigation_link}">{$vvv->navigation_title}</a> 
                                                  {else}
                                                        <a href="{make_url page=$vvv->navigation_link query=$vvv->navigation_query}{if $vvv->navigation_location <> ''}{$vvv->navigation_location}{/if}">{$vvv->navigation_title}</a>
                                                  {/if} 
                                             {/if} 
                                         </li>
                                     {/foreach}
                                 </ul>
                             </li>
                         {else}
                             <li>
                              {if $vv->is_external eq '1'}
                                    <a target="_blank" href="http://{$vv->navigation_link}">{$vv->navigation_title}</a>
                              {else}
                                    <a href="{make_url page=$vv->navigation_link query=$vv->navigation_query}{if $vv->navigation_location <> ''}{$vv->navigation_location}{/if}">{$vv->navigation_title}</a
                              {/if}   
                             </li>                                
                        {/if}
              {/foreach}
         {/if}
    {/nocache}  
      <li class="hidden-xs  hidden-sm">
        <a href="#" class="js--toggle-search-mode"><span class="glyphicon  glyphicon-search  glyphicon-search--nav"></span></a>
      </li>
    </ul>
    <!-- search for mobile devices -->
    <form action="#" method="post" class="visible-xs  visible-sm  mobile-navbar-form" role="form">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search">
        <span class="input-group-addon">
          <button type="submit" class="mobile-navbar-form__appended-btn"><span class="glyphicon  glyphicon-search  glyphicon-search--nav"></span></button>
        </span>
      </div>
    </form>
    <div class="mobile-cart  visible-xs  visible-sm  push-down-15">
        <span class="header-cart__text--price"><span class="header-cart__text">CART</span> $49.35</span>
      <a href="cart.html" class="header-cart__items">
        <span class="header-cart__items-num">3</span>
      </a>
    </div>
  </div><!-- /.navbar-collapse -->
</nav>
      </div>
      <div class="col-xs-12  col-md-2  hidden-sm  hidden-xs">
        <!-- Cart in header -->
<div class="header-cart">
  <span class="header-cart__text--price"><span class="header-cart__text">CART</span> $49.35</span>
  <a href="#" class="header-cart__items">
    <span class="header-cart__items-num">3</span>
  </a>
  <!-- Open cart panel -->
  <div class="header-cart__open-cart">
  
    <div class="header-cart__product  clearfix  js--cart-remove-target">
      <div class="header-cart__product-image">
        <img alt="Product in the cart" src="images/dummy/product-cart.jpg" width="40" height="50">
      </div>
      <div class="header-cart__product-image--hover">
        <a href="#" class="js--remove-item" data-target=".js--cart-remove-target"><span class="glyphicon  glyphicon-circle  glyphicon-remove"></span></a>
      </div>
      <div class="header-cart__product-title">
        <a class="header-cart__link" href="single-product.html">Eatable Hemp</a>
        <span class="header-cart__qty">Qty: 1</span>
      </div>
      <div class="header-cart__price">
        $16.45
      </div>
    </div>
  
    <div class="header-cart__product  clearfix  js--cart-remove-target">
      <div class="header-cart__product-image">
        <img alt="Product in the cart" src="images/dummy/product-cart.jpg" width="40" height="50">
      </div>
      <div class="header-cart__product-image--hover">
        <a href="#" class="js--remove-item" data-target=".js--cart-remove-target"><span class="glyphicon  glyphicon-circle  glyphicon-remove"></span></a>
      </div>
      <div class="header-cart__product-title">
        <a class="header-cart__link" href="single-product.html">Eatable Hemp</a>
        <span class="header-cart__qty">Qty: 1</span>
      </div>
      <div class="header-cart__price">
        $16.45
      </div>
    </div>
  
    <div class="header-cart__product  clearfix  js--cart-remove-target">
      <div class="header-cart__product-image">
        <img alt="Product in the cart" src="images/dummy/product-cart.jpg" width="40" height="50">
      </div>
      <div class="header-cart__product-image--hover">
        <a href="#" class="js--remove-item" data-target=".js--cart-remove-target"><span class="glyphicon  glyphicon-circle  glyphicon-remove"></span></a>
      </div>
      <div class="header-cart__product-title">
        <a class="header-cart__link" href="single-product.html">Eatable Hemp</a>
        <span class="header-cart__qty">Qty: 1</span>
      </div>
      <div class="header-cart__price">
        $16.45
      </div>
    </div>
  
    <hr class="header-cart__divider">
    <div class="header-cart__subtotal-box">
      <span class="header-cart__subtotal">CART SUBTOTAL:</span>
      <span class="header-cart__subtotal-price">$49.35</span>
    </div>
    <a class="btn btn-darker" href="cart.html">Procced to checkout</a>
  </div>
</div>
      </div>
    </div>
  </div>

  <!--Search open pannel-->
  <div class="search-panel">
    <div class="container">
      <div class="row">
        <div class="col-sm-11">
          <form class="search-panel__form" action="search-results.html">
            <button type="submit"><span class="glyphicon  glyphicon-search"></span></button>
            <input type="text" name="s" class="form-control" placeholder="Enter your search keyword">
          </form>
        </div>
        <div class="col-sm-1">
          <div class="search-panel__close  pull-right">
            <a href="#" class="js--toggle-search-mode"><span class="glyphicon  glyphicon-circle  glyphicon-remove"></span></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>