<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.display_form_error.php
 * Type:        function
 * Name:        show
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - name         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * name = css name
 */

function smarty_function_display_form_error()
{	
       global $error_obj;
       $error_obj->errorShow();
}
?>
