<?php

//pr($_POST);
$obj = new query('quote_request');
$obj->Data['id'] = $quote_id;
$obj->Data[$field] = $value;
$obj->Update();
$quote_pricings = getPricings($quote_id);
$pricing_total = 0;
echo '<thead><tr class="warning"><th style="width: 50%">Quote for</th><th>Price</th></tr></thead>';
echo "<tbody>";
foreach ($quote_pricings as $quote_option => $qp) {
    echo "<tr><td>" . ucfirst($quote_option) . "</td><td>$" . number_format($qp['total'], 2) . "</td></tr>";
    $pricing_total+=$qp['total'];
}
echo "</tbody>";
echo '<tfoot><tr><th>Total</th><th>$' . number_format($pricing_total, 2) . '</th></tr></tfoot>';
?>