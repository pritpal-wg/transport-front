<div class="container">
    <div class="row">
        <div class="col-md-12">
            {if is_object($list)}
                <h1>{$list->name}</h1>
                {html_entity_decode($list->short_description)}
            {/if}
        </div>
    </div>
</div>