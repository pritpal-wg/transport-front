<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        {head}
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Merriweather+Sans:700,300italic,400italic,700italic,300,400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Russo+One' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        {add_css name='plugins/bootstrap/css/bootstrap.min,plugins/font-awesome/css/font-awesome,plugins/elegant_font/css/style,plugins/flexslider/flexslider,plugins/owl-carousel/owl.carousel,plugins/owl-carousel/owl.theme'}
        <link href="{DIR_WS_SITE_CSS}style.css" rel="stylesheet" />
        <link href="{DIR_WS_SITE_CSS}color-styles/styles.css" id="theme-style" rel="stylesheet" />
        {add_css name='panel-tabs'}
        {add_js name='plugins/jquery-1.11.3.min,plugins/bootstrap/js/bootstrap.min,plugins/bootstrap-hover-dropdown.min,plugins/back-to-top,plugins/jquery-placeholder/jquery.placeholder,plugins/jquery-match-height/jquery.matchHeight-min,plugins/FitVids/jquery.fitvids,plugins/flexslider/jquery.flexslider-min,flexslider-custom,plugins/jquery.validate.min,form-validation-custom,plugins/isMobile/isMobile.min,form-mobile-fix,plugins/owl-carousel/owl.carousel,owl-custom,style-switcher'}
        <script>
            var admin_allowed_country = "{$admin_allowed_country}";
            var gapi_key = "{$gapi_key}";
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key={$gapi_key}&signed_in=true&libraries=places" async></script>
        {add_js name='plugins/geocomplete,main'}
        {literal}
            <style>
                body {
                    background: #3b4452;
                }
            </style>
        {/literal}
    </head>
    <body class="home-page">
        {include '../include/top.tpl'}
        <div class="container" style="margin-top: 40px">
            <div class="well well-lg" style="text-align: center">
                <h1>Payment Successfull. <a href="{make_url('home')}">Go Back To Home</a></h1>
            </div>
        </div>
        {include '../include/footer.tpl'}
        <!-- Main Javascript -->
    </body>
</html>