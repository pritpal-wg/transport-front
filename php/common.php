<?php

include_once(DIR_FS_SITE . 'admin/include/functionClass/contentClass.php');
require_once DIR_FS_SITE . 'admin/include/functionClass/quoteClass.php';
require_once DIR_FS_SITE . 'admin/include/functionClass/vehicleClass.php';

//get these from Admin
$admin_allowed_country = "IN";
$gapi_key = "AIzaSyB0UTAkiGpQGDwBErGwGHrWkTi0zjyMUlQ";

//assign to Smarty
$smarty->assign_notnull('admin_allowed_country', $admin_allowed_country);
$smarty->assign_notnull('gapi_key', $gapi_key);

#get the user ID for login	
$user_id = $login_session->get_user_id();
$smarty->assign_notnull("USER_ID", $user_id, true);

$query = new content;
$lists = $query->getItems(false);
$smarty->assign_notnull("lists", $lists, true);
?>